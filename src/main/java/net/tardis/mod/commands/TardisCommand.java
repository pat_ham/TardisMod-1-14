package net.tardis.mod.commands;

import com.mojang.brigadier.CommandDispatcher;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.tardis.mod.Tardis;
import net.tardis.mod.commands.subcommands.CreateCommand;
import net.tardis.mod.commands.subcommands.InteriorCommand;
import net.tardis.mod.commands.subcommands.RefuelCommand;
import net.tardis.mod.commands.subcommands.SetupCommand;
import net.tardis.mod.commands.subcommands.UnlockCommand;

public class TardisCommand {
	
    public static void register(CommandDispatcher<CommandSource> dispatcher){
        dispatcher.register(
                Commands.literal(Tardis.MODID)
                        .then(CreateCommand.register(dispatcher))
                        .then(InteriorCommand.register(dispatcher))
                        .then(RefuelCommand.register(dispatcher))
                        .then(UnlockCommand.register(dispatcher))
                        .then(SetupCommand.register(dispatcher))

        );
    }
}
