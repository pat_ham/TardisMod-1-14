package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class RefuelCommand extends TCommand {
    private static final RefuelCommand CMD = new RefuelCommand();

    public RefuelCommand() {
        super(PermissionEnum.REFUEL);
    }

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("refuel")
                .then(Commands.argument("username", StringArgumentType.string())
                        .suggests((context, builder) -> ISuggestionProvider.suggest(ServerLifecycleHooks.getCurrentServer().getOnlinePlayerNames(), builder))
                        .then(Commands.argument("amount", IntegerArgumentType.integer())
                                .executes(CMD)));


    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        CommandSource source = context.getSource();
        String username = context.getArgument("username", String.class);
        Integer amount = context.getArgument("amount", Integer.class);

        if (canExecute(source)) {
        	PlayerEntity player = source.getServer().getPlayerList().getPlayerByUsername(username);
            if(player != null) {
            	ConsoleTile console = TardisHelper.getConsole(context.getSource().getServer(), player.getUniqueID()).orElse(null);
                console.setArtron(console.getArtron() + amount.floatValue());
                source.sendFeedback(new TranslationTextComponent("message.tardis.refuel_command", username, console.getArtron()), true);
            }
            else context.getSource().sendErrorMessage(new StringTextComponent(""));
        }
        else source.sendErrorMessage(new StringTextComponent(Constants.Message.NO_PERMISSION));
        return Command.SINGLE_SUCCESS;
    }
}
