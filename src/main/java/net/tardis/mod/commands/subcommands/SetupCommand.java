package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class SetupCommand extends TCommand{

	public static final SetupCommand CMD = new SetupCommand(PermissionEnum.CREATE);
	
	public SetupCommand(PermissionEnum permission) {
		super(permission);
	}
	
	public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
		return Commands.literal("setup").executes(CMD);
	}

	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		CommandSource source = context.getSource();
		if(this.canExecute(context.getSource())) {
			ServerPlayerEntity ent = context.getSource().asPlayer();
			DimensionType type = TardisHelper.getTardisByUUID(ent.getUniqueID());
			
			if(type == null) {
				context.getSource().sendFeedback(new StringTextComponent(Constants.Message.NO_TARDIS), true);
				return Command.SINGLE_SUCCESS;
			}
			
			ServerWorld world = context.getSource().getServer().getWorld(TardisHelper.getTardisByUUID(ent.getUniqueID()));
			world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
				PanelInventory comp = cap.getEngineInventoryForSide(Direction.NORTH);
				PanelInventory artron = cap.getEngineInventoryForSide(Direction.WEST);
				
				//Add capacitors
				for(int i = 0; i < artron.getSizeInventory(); ++i) {
					artron.setInventorySlotContents(i, new ItemStack(TItems.ARTRON_CAPACITOR));
				}
				
				TardisHelper.getConsoleInWorld(world).ifPresent(tile -> {
					
					int index = 0;
					for(Subsystem s : tile.getSubSystems()) {
						
						if(index >= comp.getSizeInventory())
							break;
						
						comp.setInventorySlotContents(index, new ItemStack(s.getItemKey()));
						++index;
					}
					
					tile.updateArtronValues();
					tile.setArtron(1000000F);
				});
				ent.addItemStackToInventory(new ItemStack(TItems.KEY));
				try {
					source.sendFeedback(new TranslationTextComponent("command.tardis.setup.success", source.asPlayer().getDisplayName()), true);
				} catch (CommandSyntaxException e) {
					source.sendErrorMessage(new StringTextComponent(e.getMessage()));
					e.printStackTrace();
				}
				
			});
			
		}
		
		return Command.SINGLE_SUCCESS;
	}

}
