package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tags.TardisBlockTags;
import net.tardis.mod.tileentities.ConsoleTile;

public class ARSSpawnMessage {

	public Item key;
	
	public ARSSpawnMessage(Item item) {
		this.key = item;
	}
	
	public static void encode(ARSSpawnMessage mes, PacketBuffer buf) {
		buf.writeResourceLocation(mes.key.getRegistryName());
	}
	
	public static ARSSpawnMessage decode(PacketBuffer buf) {
		return new ARSSpawnMessage(ForgeRegistries.ITEMS.getValue(buf.readResourceLocation()));
	}
	
	public static void handle(ARSSpawnMessage mes, Supplier<NetworkEvent.Context> con) {
		con.get().enqueueWork(() -> {
			ServerWorld world = con.get().getSender().getServerWorld();
			if(world.getTileEntity(TardisHelper.TARDIS_POS) instanceof ConsoleTile) {
				ConsoleTile console = (ConsoleTile)world.getTileEntity(TardisHelper.TARDIS_POS);
				//TODO: Set an actual value
				if(mes.key instanceof BlockItem && ((BlockItem)mes.key).getBlock().isIn(TardisBlockTags.ARS)) {
					if(console.getArtron() > mes.key.getMaxStackSize()) {
						console.setArtron(console.getArtron() - mes.key.getMaxStackSize());
						InventoryHelper.spawnItemStack(world,
								con.get().getSender().posX,
								con.get().getSender().posY,
								con.get().getSender().posZ,
								new ItemStack(mes.key, mes.key.getMaxStackSize()));
					}
					else con.get().getSender().sendStatusMessage(new TranslationTextComponent(Constants.Translations.NOT_ENOUGH_ARTRON, mes.key.getMaxStackSize()), true);
				}
			}
		});
		con.get().setPacketHandled(true);
	}
}
