package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class ChangeInteriorMessage {
	
	public ResourceLocation room;
	
	public ChangeInteriorMessage(ResourceLocation room) {
		this.room = room;
	}
	
	public static void encode(ChangeInteriorMessage mes, PacketBuffer buffer) {
		buffer.writeResourceLocation(mes.room);
	}
	
	public static ChangeInteriorMessage decode(PacketBuffer buffer) {
		return new ChangeInteriorMessage(buffer.readResourceLocation());
	}
	
	public static void handle(ChangeInteriorMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			if(context.get().getSender().world.dimension.getType().getModType() == TDimensions.TARDIS) {
				TileEntity te = context.get().getSender().world.getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile) {
					ConsoleTile console = (ConsoleTile)te;
					if(console.getInteriorManager().canChangeInterior() || context.get().getSender().isCreative()) {
						ConsoleRoom room = ConsoleRoom.REGISTRY.get(mes.room);
						if(room != null) {
							int fuelUsage = TConfig.CONFIG.interiorChangeArtronUse.get();
							if(console.getArtron() > fuelUsage) {
								if(console.canDoAdminFunction(context.get().getSender())) {
									console.setArtron(console.getArtron() - fuelUsage);
									//Set timeout to 1 minute
									int ticks = TConfig.CONFIG.interiorChangeCooldownTime.get() * 20;
									console.getInteriorManager().setInteriorChangeTime(ticks);
									console.setConsoleRoom(room);
									room.spawnConsoleRoom(context.get().getSender().getServerWorld(), false);
								}
								else context.get().getSender().sendStatusMessage(Constants.Translations.NOT_ADMIN, true);
							}
							else context.get().getSender().sendStatusMessage(new TranslationTextComponent(Constants.Translations.NOT_ENOUGH_ARTRON, fuelUsage), true);
						}
					}
					else context.get().getSender().sendStatusMessage(new TranslationTextComponent("error.tardis.interior", console.getInteriorManager().getInteriorChangeTime() / 20), true);
				}
			}
		});
		context.get().setPacketHandled(true);
	}

}
