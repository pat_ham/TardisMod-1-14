package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.ConsoleTile;

public class WaypointDeleteMessage {

	int index = 0;
	
	public WaypointDeleteMessage(int index) {
		this.index = index;
	}
	
	public static void encode(WaypointDeleteMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.index);
	}
	
	public static WaypointDeleteMessage decode(PacketBuffer buf) {
		return new WaypointDeleteMessage(buf.readInt());
	}
	
	public static void handle(WaypointDeleteMessage mes, Supplier<NetworkEvent.Context> con) {
		con.get().enqueueWork(() -> {
			TileEntity te = con.get().getSender().world.getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				ConsoleTile console = (ConsoleTile)te;
				//Sanity check
				if(mes.index < console.getWaypoints().size() && mes.index >= 0) {
					String name = console.getWaypoints().get(mes.index).getName();
					console.getWaypoints().set(mes.index, SpaceTimeCoord.UNIVERAL_CENTER);
					con.get().getSender().sendStatusMessage(new TranslationTextComponent("status.tardis.waypoints.delete", name), true);
					console.updateClient();
				}
			}
		});
		con.get().setPacketHandled(true);
	}
}
