package net.tardis.mod.network.packets;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.recipe.Recipes;
import net.tardis.mod.recipe.WeldRecipe;

public class RecipeSyncMessage {

	private List<WeldRecipe> recipes;
	
	public RecipeSyncMessage(List<WeldRecipe> recipes) {
		this.recipes = recipes;
	}
	
	public static void encode(RecipeSyncMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.recipes.size());
		for(WeldRecipe rec : mes.recipes) {
			buf.writeCompoundTag(rec.serialize());
		}
	}
	
	public static RecipeSyncMessage decode(PacketBuffer buf) {
		int size = buf.readInt();
		
		List<WeldRecipe> rec = new ArrayList<WeldRecipe>();
		
		for(int i = 0; i < size; ++i) {
			rec.add(WeldRecipe.deserialize(buf.readCompoundTag()));
		}
		
		return new RecipeSyncMessage(rec);
	}
	
	public static void handle(RecipeSyncMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			Recipes.WELD_RECIPE.clear();
			Recipes.WELD_RECIPE.addAll(mes.recipes);
		});
	}
}
