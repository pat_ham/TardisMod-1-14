package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.LandingSystem;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.sounds.TSounds;


public class VMTeleportMessage {

	public BlockPos pos;
	public int value;
	public boolean teleportPrecise;
	
	public VMTeleportMessage(BlockPos pos, int value, boolean teleportPrecise) {
		this.pos = pos;
		this.value = value;
		this.teleportPrecise = teleportPrecise;
	}


	public static void encode(VMTeleportMessage mes,PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
		buf.writeInt(mes.value);
		buf.writeBoolean(mes.teleportPrecise);
	}
	
	public static VMTeleportMessage decode(PacketBuffer buf) {
		return new VMTeleportMessage(buf.readBlockPos(),buf.readInt(),buf.readBoolean());
	}
	
        public static void handle(VMTeleportMessage mes,  Supplier<NetworkEvent.Context> ctx)
        {
        	ctx.get().enqueueWork(()->{
        		 	ServerPlayerEntity sender = ctx.get().getSender();
        		 	if (!LandingSystem.isPosBelowOrAboveWorld(sender.world.getDimension().getType(),mes.pos.getY()) 
        		 			&& LandingSystem.isBlockWithinWorldBorder(sender.getServerWorld(), mes.pos.getX(),mes.pos.getY(),mes.pos.getZ()) 
        		 			&& Helper.canVMTravelToDimension(sender.dimension)) {
        		 		double diff = Math.sqrt(sender.getPosition().distanceSq(mes.pos)); //Calculates difference between dest and current pos
                     	float fuelDischarge = (float) (TConfig.CONFIG.vmBaseFuelUsage.get() + (TConfig.CONFIG.vmFuelUsageMultiplier.get() * diff)); //Scales discharge amount
                     	//This checks for which itemstack we want to open the container for
            			//If we are holding a VM, use it as the itemstack. Otherwise, get the stack closest to slot 0
                     	ItemStack vm = new ItemStack(TItems.VORTEX_MANIP);
                     	ItemStack stack = PlayerHelper.getHeldOrNearestStack(sender, vm);
                     	stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> {
                         		if (cap.getTotalCurrentCharge() >= fuelDischarge) { //Don't let them teleport if they don't have enough fuel to start the journey
                         			cap.setDischargeAmount(fuelDischarge);
                         			mes.pos = mes.teleportPrecise ? mes.pos : LandingSystem.getTopBlock(sender.world, mes.pos); //Ensures the vm will tp you to a safe spot
                    		 		SpaceTimeCoord coord = new SpaceTimeCoord(sender.dimension, mes.pos);
                                	sender.getCapability(Capabilities.PLAYER_DATA).ifPresent(playerCap -> {
                                		playerCap.setDestination(coord);
                                		playerCap.calcDisplacement(sender.getPosition(), mes.pos);
                                	});
                                	sender.getServerWorld().playSound(null, sender.getPosition(), TSounds.VM_TELEPORT, SoundCategory.BLOCKS, 0.25F, 1F); //Play sound at start position
                                	cap.setVmUsed(true); //Tell server that we used the VM to teleport into Vortex Dim
                                	sender.teleport(sender.world.getServer().getWorld(TDimensions.VORTEX_TYPE), 0, 500 + (0.5 * diff), 0, 0, 90); //Tps you higher into the vortex depending on how far you travel
                         		}
                         		else {
                         			sender.sendStatusMessage(new TranslationTextComponent("message.vm.fuel_empty",fuelDischarge), false);
                         		}
                         	}); 
                     	}
        		 	else {
        		 		sender.sendStatusMessage(new TranslationTextComponent("message.vm.invalidPos"), false);
        		 	} 			
        	});
        	ctx.get().setPacketHandled(true);
        }
    
}
