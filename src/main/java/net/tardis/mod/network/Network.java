package net.tardis.mod.network;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.network.packets.ARSSpawnMessage;
import net.tardis.mod.network.packets.BOTIMessage;
import net.tardis.mod.network.packets.BessieHornMessage;
import net.tardis.mod.network.packets.BrokenTardisSpawn;
import net.tardis.mod.network.packets.ChangeExtAnimationMessage;
import net.tardis.mod.network.packets.ChangeExtVarMessage;
import net.tardis.mod.network.packets.ChangeHumMessage;
import net.tardis.mod.network.packets.ChangeInteriorMessage;
import net.tardis.mod.network.packets.CommunicatorMessage;
import net.tardis.mod.network.packets.ConsoleChangeMessage;
import net.tardis.mod.network.packets.ConsoleVariantMessage;
import net.tardis.mod.network.packets.ExteriorChangeMessage;
import net.tardis.mod.network.packets.LightUpdateMessage;
import net.tardis.mod.network.packets.MissControlMessage;
import net.tardis.mod.network.packets.ProtocolMessage;
import net.tardis.mod.network.packets.QuantiscopeTabMessage;
import net.tardis.mod.network.packets.RecipeSyncMessage;
import net.tardis.mod.network.packets.SonicPartChangeMessage;
import net.tardis.mod.network.packets.StopHumMessage;
import net.tardis.mod.network.packets.SyncPlayerMessage;
import net.tardis.mod.network.packets.SyncSonicMessage;
import net.tardis.mod.network.packets.TelepathicMessage;
import net.tardis.mod.network.packets.UpdateARSTablet;
import net.tardis.mod.network.packets.UpdateControlMessage;
import net.tardis.mod.network.packets.UpdateManualPageMessage;
import net.tardis.mod.network.packets.VMFunctionMessage;
import net.tardis.mod.network.packets.VMTeleportMessage;
import net.tardis.mod.network.packets.WaypointDeleteMessage;
import net.tardis.mod.network.packets.WaypointLoadMessage;
import net.tardis.mod.network.packets.WaypointSaveMessage;

public class Network {

	private static int id = 0;
    private static final String PROTOCOL_VERSION = Integer.toString(1);
    public static final SimpleChannel INSTANCE = NetworkRegistry.ChannelBuilder
    		.named(new ResourceLocation(Tardis.MODID, "main_channel"))
    		.clientAcceptedVersions(PROTOCOL_VERSION::equals)
    		.serverAcceptedVersions(PROTOCOL_VERSION::equals)
    		.networkProtocolVersion(() -> PROTOCOL_VERSION).simpleChannel();


    public static void init() {
        INSTANCE.registerMessage(++id, UpdateControlMessage.class, UpdateControlMessage::encode, UpdateControlMessage::decode, UpdateControlMessage::handle);
        INSTANCE.registerMessage(++id, ProtocolMessage.class, ProtocolMessage::encode, ProtocolMessage::decode, ProtocolMessage::handle);
        INSTANCE.registerMessage(++id, LightUpdateMessage.class, LightUpdateMessage::encode, LightUpdateMessage::decode, LightUpdateMessage::handle);
        INSTANCE.registerMessage(++id, VMTeleportMessage.class, VMTeleportMessage::encode, VMTeleportMessage::decode, VMTeleportMessage::handle);
        INSTANCE.registerMessage(++id, ChangeInteriorMessage.class, ChangeInteriorMessage::encode, ChangeInteriorMessage::decode, ChangeInteriorMessage::handle);
        INSTANCE.registerMessage(++id, ConsoleChangeMessage.class, ConsoleChangeMessage::encode, ConsoleChangeMessage::decode, ConsoleChangeMessage::handle);
        INSTANCE.registerMessage(++id, SyncSonicMessage.class, SyncSonicMessage::encode, SyncSonicMessage::decode, SyncSonicMessage::handle);
        INSTANCE.registerMessage(++id, ARSSpawnMessage.class, ARSSpawnMessage::encode, ARSSpawnMessage::decode, ARSSpawnMessage::handle);
        INSTANCE.registerMessage(++id, SonicPartChangeMessage.class, SonicPartChangeMessage::encode, SonicPartChangeMessage::decode, SonicPartChangeMessage::handle);
        INSTANCE.registerMessage(++id, UpdateManualPageMessage.class, UpdateManualPageMessage::encode, UpdateManualPageMessage::decode, UpdateManualPageMessage::handle);
        INSTANCE.registerMessage(++id, QuantiscopeTabMessage.class, QuantiscopeTabMessage::encode, QuantiscopeTabMessage::decode, QuantiscopeTabMessage::handle);
        INSTANCE.registerMessage(++id, StopHumMessage.class, StopHumMessage::encode, StopHumMessage::decode, StopHumMessage::handle);
        INSTANCE.registerMessage(++id, ChangeHumMessage.class, ChangeHumMessage::encode, ChangeHumMessage::decode, ChangeHumMessage::handle);
        INSTANCE.registerMessage(++id, BessieHornMessage.class, BessieHornMessage::encode, BessieHornMessage::decode, BessieHornMessage::handle);
        INSTANCE.registerMessage(++id, ExteriorChangeMessage.class, ExteriorChangeMessage::encode, ExteriorChangeMessage::decode, ExteriorChangeMessage::handle);
        INSTANCE.registerMessage(++id, WaypointSaveMessage.class, WaypointSaveMessage::encode, WaypointSaveMessage::decode, WaypointSaveMessage::handle);
        INSTANCE.registerMessage(++id, WaypointLoadMessage.class, WaypointLoadMessage::encode, WaypointLoadMessage::decode, WaypointLoadMessage::handle);
        INSTANCE.registerMessage(++id, WaypointDeleteMessage.class, WaypointDeleteMessage::encode, WaypointDeleteMessage::decode, WaypointDeleteMessage::handle);
        INSTANCE.registerMessage(++id, ChangeExtAnimationMessage.class, ChangeExtAnimationMessage::encode, ChangeExtAnimationMessage::decode, ChangeExtAnimationMessage::handle);
        INSTANCE.registerMessage(++id, TelepathicMessage.class, TelepathicMessage::encode, TelepathicMessage::decode, TelepathicMessage::handle);
        INSTANCE.registerMessage(++id, UpdateARSTablet.class, UpdateARSTablet::encode, UpdateARSTablet::decode, UpdateARSTablet::handle);
        INSTANCE.registerMessage(++id, CommunicatorMessage.class, CommunicatorMessage::encode, CommunicatorMessage::decode, CommunicatorMessage::handle);
        INSTANCE.registerMessage(++id, VMFunctionMessage.class, VMFunctionMessage::encode, VMFunctionMessage::decode, VMFunctionMessage::handle);
        INSTANCE.registerMessage(++id, ChangeExtVarMessage.class, ChangeExtVarMessage::encode, ChangeExtVarMessage::decode, ChangeExtVarMessage::handle);
        INSTANCE.registerMessage(++id, ConsoleVariantMessage.class, ConsoleVariantMessage::encode, ConsoleVariantMessage::decode, ConsoleVariantMessage::handle);
        
        //To Client
        INSTANCE.registerMessage(++id, MissControlMessage.class, MissControlMessage::encode, MissControlMessage::decode, MissControlMessage::handle);
        INSTANCE.registerMessage(++id, BrokenTardisSpawn.class, BrokenTardisSpawn::encode, BrokenTardisSpawn::decode, BrokenTardisSpawn::handle);
        INSTANCE.registerMessage(++id, BOTIMessage.class, BOTIMessage::encode, BOTIMessage::decode, BOTIMessage::handle);
        INSTANCE.registerMessage(++id, RecipeSyncMessage.class, RecipeSyncMessage::encode, RecipeSyncMessage::decode, RecipeSyncMessage::handle);
        
        INSTANCE.registerMessage(++id, SyncPlayerMessage.class, SyncPlayerMessage::encode, SyncPlayerMessage::decode, SyncPlayerMessage::handle);
    }

    /**
     * Sends a packet to the server.<br>
     * Must be called Client side.
     */
    public static void sendToServer(Object msg) {
        INSTANCE.sendToServer(msg);
    }

    /**
     * Send a packet to a specific player.<br>
     * Must be called Server side.
     */
    public static void sendTo(Object msg, ServerPlayerEntity player) {
        if (!(player instanceof FakePlayer)) {
            INSTANCE.sendTo(msg, player.connection.netManager, NetworkDirection.PLAY_TO_CLIENT);
        }
    }

    public static void sendPacketToAll(Object packet) {
    	
    	//Fixing a client crash
    	if(ServerLifecycleHooks.getCurrentServer() == null || ServerLifecycleHooks.getCurrentServer().getPlayerList() == null)
    		return;
    	
        for (ServerPlayerEntity player : ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayers()) {
            sendTo(packet, player);
        }
        
    }

}
