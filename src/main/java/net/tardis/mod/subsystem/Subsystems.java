package net.tardis.mod.subsystem;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;
import net.tardis.mod.registries.TardisRegistries;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class Subsystems {

	public static SubsystemEntry<FlightSubsystem> FLIGHT;
	public static SubsystemEntry<FluidLinksSubsystem> FLUID_LINKS;
	public static SubsystemEntry<ChameleonSubsystem> CHAMELEON;
	public static SubsystemEntry<AntennaSubsystem> ANTENNA;
	public static SubsystemEntry<TemporalGraceSubsystem> TEMPORAL_GRACE;
	
	@SubscribeEvent
	public static void registerAll(FMLCommonSetupEvent event) {
		TardisRegistries.registerRegisters(() -> {
			FLIGHT = register("flight", new SubsystemEntry<FlightSubsystem>(FlightSubsystem::new, TItems.DEMAT_CIRCUIT));
			FLUID_LINKS = register("fluid_link", new SubsystemEntry<FluidLinksSubsystem>(FluidLinksSubsystem::new, TItems.FLUID_LINK));
			CHAMELEON = register("chameleon", new SubsystemEntry<ChameleonSubsystem>(ChameleonSubsystem::new, TItems.CHAMELEON_CIRCUIT));
			ANTENNA = register("antenna", new SubsystemEntry<AntennaSubsystem>(AntennaSubsystem::new, TItems.INTERSTITAL_ANTENNA));
			TEMPORAL_GRACE = register("temporal_grace", new SubsystemEntry<TemporalGraceSubsystem>(TemporalGraceSubsystem::new, TItems.TEMPORAL_GRACE));
		});
	}
	
	public static <T extends Subsystem> SubsystemEntry<T> register(ResourceLocation key, SubsystemEntry<T> system) {
		TardisRegistries.SUBSYSTEM_REGISTRY.register(key, system);
		return system;
	}
	
	public static <T extends Subsystem> SubsystemEntry<T> register(String key, SubsystemEntry<T> system) {
		return register(new ResourceLocation(Tardis.MODID, key), system);
	}
}
