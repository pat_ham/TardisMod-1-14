package net.tardis.mod.subsystem;

import javax.annotation.Nullable;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public abstract class Subsystem implements IRegisterable<Subsystem>, INBTSerializable<CompoundNBT>{

	private Item itemKey;
	private ResourceLocation registryName;
	private ItemStack item = ItemStack.EMPTY;
	protected ConsoleTile console;
	
	/**
	 * 
	 * @param Console - The console tile that this subsystem is attached to
	 * @param Item - The Item Component for this subsystem, the game looks for this item in the component
	 * 			panel of the engine, make sure the item is damagable
	 */
	
	public Subsystem(ConsoleTile console, Item item) {
		this.console = console;
		this.itemKey = item;
	}

	@Override
	public Subsystem setRegistryName(ResourceLocation regName) {
		this.registryName = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.registryName;
	}
	
	public Item getItemKey() {
		return this.itemKey;
	}
	
	/**
	 * 
	 * @return ItemStack in the engine matching this Subsystem's component item
	 */
	public ItemStack getItem() {
		if(item == null || item.isEmpty())
			this.getItemFromWorld(console.getWorld());
		return this.item == null ? ItemStack.EMPTY : item;
	}
	
	/**
	 * Damages the component's item in the engine
	 * @param player
	 * @param amt
	 */
	public void damage(@Nullable ServerPlayerEntity player, int amt) {
		this.getItem().attemptDamageItem(amt, console.getWorld().rand, player);
	}
	
	/**
	 * 
	 * @return If the compoenent can do it's thinkg
	 */
	public boolean canBeUsed() {
		return this.getItem().getDamage() < this.getItem().getMaxDamage();
	}

	/**
	 * This is for internal use, please use {@link #getItem()} instead
	 * @param world
	 */
	public void getItemFromWorld(World world) {
		world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
			PanelInventory inv = cap.getEngineInventoryForSide(Direction.NORTH);
			for(int i = 0; i < inv.getSizeInventory(); ++i) {
				if(inv.getStackInSlot(i).getItem() == itemKey) {
					this.item = inv.getStackInSlot(i);
					return;
				}
			}
		});
	}
	
	/**
	 * 
	 * @return If this system will stop the TARDIS Taking off
	 * 			If you're adding a system not essential to flight, return false
	 */
	public boolean stopsFlight() {
		return !this.canBeUsed();
	}

    public abstract void onTakeoff();

    public abstract void onLand();

    public abstract void onFlightSecond();
    
    /**
     * 
     * @return If this subsystem should make the console spark, non-essential probably should just return false
     */
    public boolean shouldSpark() {
    	ItemStack stack = this.getItem();
    	
    	//Non-damagable items and stop / by 0
    	if(stack.getMaxDamage() <= 0)
    		return false;
    	
    	if((1.0F - (stack.getDamage() / stack.getMaxDamage())) < 0.3)
    		return true;
    	return false;
    }

    /**
     * 
     * @param softCrash - if this is true, was just a missed control, otherwise was a subsystem failure or something to knock it out of flight
     */
	public void explode(boolean softCrash) {
		this.damage(null, 10);
	}
	
}
