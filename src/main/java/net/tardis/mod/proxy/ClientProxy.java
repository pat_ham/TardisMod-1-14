package net.tardis.mod.proxy;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.client.guis.ArsScreen;
import net.tardis.mod.client.guis.ArsTabletScreen;
import net.tardis.mod.client.guis.CommunicatorScreen;
import net.tardis.mod.client.guis.ManualScreen;
import net.tardis.mod.client.guis.TelepathicScreen;
import net.tardis.mod.client.guis.monitors.EyeMonitorScreen;
import net.tardis.mod.client.guis.monitors.GalvanicMonitorScreen;
import net.tardis.mod.client.guis.monitors.RCAMonitorScreen;
import net.tardis.mod.client.guis.monitors.SteamMonitorScreen;
import net.tardis.mod.client.guis.vm.VortexMGui;
import net.tardis.mod.client.guis.vm.VortexMTeleportGui;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.experimental.sound.EntityMovingSound;
import net.tardis.mod.misc.GuiContext;

@OnlyIn(Dist.CLIENT)
public class ClientProxy implements IProxy {

	@Override
	public void init() {}
	
	@Override
	public void openGUI(int guiId, GuiContext context) {
		switch (guiId) {
			case Constants.Gui.MONITOR_MAIN_STEAM:
				Minecraft.getInstance().displayGuiScreen(new SteamMonitorScreen());
				break;
			case Constants.Gui.VORTEX_MAIN:
				Minecraft.getInstance().displayGuiScreen(new VortexMGui());
				break;
			case Constants.Gui.VORTEX_TELE:
				Minecraft.getInstance().displayGuiScreen(new VortexMTeleportGui());
				break;
			case Constants.Gui.MONITOR_MAIN_EYE:
				Minecraft.getInstance().displayGuiScreen(new EyeMonitorScreen());
				break;
			case Constants.Gui.MANUAL:
				Minecraft.getInstance().displayGuiScreen(new ManualScreen(context));
				break;
			case Constants.Gui.ARS_EGG:
				Minecraft.getInstance().displayGuiScreen(new ArsScreen());
				break;
			case Constants.Gui.TELEPATHIC:
				Minecraft.getInstance().displayGuiScreen(new TelepathicScreen());
				break;
			case Constants.Gui.MONITOR_MAIN_GALVANIC:
				Minecraft.getInstance().displayGuiScreen(new GalvanicMonitorScreen());
				break;
			case Constants.Gui.MONITOR_MAIN_RCA:
				Minecraft.getInstance().displayGuiScreen(new RCAMonitorScreen());
				break;
			case Constants.Gui.ARS_TABLET:
				Minecraft.getInstance().displayGuiScreen(new ArsTabletScreen(context));
				break;
			case Constants.Gui.COMMUNICATOR:
				Minecraft.getInstance().displayGuiScreen(new CommunicatorScreen());
				break;
			case Constants.Gui.NONE:
				Minecraft.getInstance().displayGuiScreen(null);
			default:
				break;
		}

	}

    @Override
	public void playMovingSound(Entity entity, SoundEvent soundEvent, SoundCategory soundCategory, float vol, boolean repeat) {
		Minecraft.getInstance().getSoundHandler().play(new EntityMovingSound(entity, soundEvent, soundCategory, vol, repeat));
    }

	@Override
	public World getClientWorld() {
		return Minecraft.getInstance().world;
	}

	@Override
	public PlayerEntity getClientPlayer() {
		return Minecraft.getInstance().player;
	}
	
	@Override
	public void shutTheFuckUp(SoundEvent event, SoundCategory type) {
		Minecraft.getInstance().getSoundHandler().stop(event == null ? null : event.getRegistryName(), type);
	}
}

