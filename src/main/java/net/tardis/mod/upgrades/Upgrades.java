package net.tardis.mod.upgrades;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.subsystem.FlightSubsystem;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.TemporalGraceSubsystem;
import net.tardis.mod.upgrades.UpgradeEntry.IConsoleSpawner;

public class Upgrades {
	
	public static UpgradeEntry<AtriumUpgrade> ATRIUM;
	public static UpgradeEntry<ElectrolyticConvertersUpgrade> ELECTRO_CONVERT;
	
	public static <T extends Upgrade> UpgradeEntry<T> register(ResourceLocation name, IConsoleSpawner<T> spawn, Item item, Class<? extends Subsystem> sys){
		UpgradeEntry<T> entry = new UpgradeEntry<T>(spawn, item, sys);
		entry.setRegistryName(name);
		TardisRegistries.UPGRADES.register(name, entry);
		return entry;
	}
	
	public static <T extends Upgrade> UpgradeEntry<T> register(String name, IConsoleSpawner<T> spawn, Item item, Class<? extends Subsystem> sys){
		return register(new ResourceLocation(Tardis.MODID, name), spawn, item, sys);
	}
	
	public static void registerAll() {
		TardisRegistries.registerRegisters(() -> {
			ATRIUM = register("atrium", AtriumUpgrade::new, TItems.ATRIUM_UPGRADE, FlightSubsystem.class);
			ELECTRO_CONVERT = register("electro_converters", ElectrolyticConvertersUpgrade::new, TItems.ELECTRO_CONVERT_UPGRADE, TemporalGraceSubsystem.class);
		});
	}

}
