package net.tardis.mod.ars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ArmorStandEntity;
import net.minecraft.entity.item.ItemFrameEntity;
import net.minecraft.entity.item.LeashKnotEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.items.CapabilityItemHandler;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.ReclamationTile;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class ConsoleRoom {

	public static HashMap<ResourceLocation, ConsoleRoom> REGISTRY = new HashMap<ResourceLocation, ConsoleRoom>();
	
	public static ConsoleRoom STEAM;
	public static ConsoleRoom JADE;
	public static ConsoleRoom NAUTILUS;
	public static ConsoleRoom OMEGA;
	public static ConsoleRoom ALABASTER;
	public static ConsoleRoom CORAL;
	public static ConsoleRoom METALLIC;
	public static ConsoleRoom ARCHITECT;

	//Broken exteriors
	public static ConsoleRoom BROKEN_STEAM;

	private ResourceLocation registryName;
	private ResourceLocation texture;
	private BlockPos offset;
	private ResourceLocation file;
	private TranslationTextComponent displayName;
	private boolean unlocked = true;
	
	public ConsoleRoom(BlockPos offset, boolean isDefault, ResourceLocation file, ResourceLocation texture, String name) {
		this.offset = offset;
		this.file = file;
		this.texture = texture;
		this.displayName = new TranslationTextComponent(name);
		this.unlocked = isDefault;
	}
	
	public ConsoleRoom(BlockPos offset, boolean unlocked, String file, String texture) {
		this(offset, unlocked,
				new ResourceLocation(Tardis.MODID, "tardis/structures/" + file),
				new ResourceLocation(Tardis.MODID, "textures/gui/interiors/" + texture + ".png"), 
				"interiors.tardis." + file);
	}
	
	
	public ConsoleRoom setRegistryName(ResourceLocation name) {
		this.registryName = name;
		return this;
	}
	
	public ResourceLocation getRegistryName() {
		return registryName;
	}
	
	public ResourceLocation getTexture() {
		return this.texture;
	}
	
	public TranslationTextComponent getDisplayName() {
		return this.displayName;
	}
	
	public boolean isDefault() {
		return this.unlocked;
	}

	@SubscribeEvent
	public static void registerConsoleRooms(FMLCommonSetupEvent event) {
		ConsoleRoom.STEAM = register(new ConsoleRoom(new BlockPos(13, 8, 30), true, "interior_steam", "steam"), "interior_steam");
		ConsoleRoom.JADE = register(new ConsoleRoom(new BlockPos(9, 12, 30), true, "interior_jade", "jade"), "interior_jade");
		ConsoleRoom.NAUTILUS = register(new ConsoleRoom(new BlockPos(14, 13, 30), false, "interior_nautilus", "nautilus"), "nautilus");
		ConsoleRoom.OMEGA = register(new ConsoleRoom(new BlockPos(14, 15, 30), true, "interior_omega", "omega"), "omega"); //Enable this as a test
		ConsoleRoom.ALABASTER = register(new ConsoleRoom(new BlockPos(15, 8, 30), true, "interior_alabaster", "alabaster"), "alabaster");
		ConsoleRoom.ARCHITECT = register(new ConsoleRoom(new BlockPos(14, 12, 30), true, "interior_architect", "architect"), "architect");
		//ConsoleRoom.CORAL = register(new ConsoleRoom(new BlockPos(12, 7, 30), false, "interior_coral", "coral"), "coral");
		//ConsoleRoom.METALLIC = register(new ConsoleRoom(new BlockPos(12, 9, 30), true, "interior_metallic", "metallic"), "metallic");

		ConsoleRoom.BROKEN_STEAM = register(new ConsoleRoom(new BlockPos(13, 8, 30), false, "interior_broken_steam", "steam"), "broken_steam");
	}

	public static ConsoleRoom register(ConsoleRoom room, ResourceLocation registryName) {
		REGISTRY.put(registryName, room.setRegistryName(registryName));
		return room;
	}

	public static ConsoleRoom register(ConsoleRoom room, String registryName) {
		return ConsoleRoom.register(room, new ResourceLocation(Tardis.MODID, registryName));
	}
    
	public void spawnConsoleRoom(ServerWorld world, boolean overrideStructure) {
		ConsoleTile console = null;
		CompoundNBT consoleData = null;
		BlockState consoleState = null;
		
		int radius = 30;

		//Save the console
		if(world.getTileEntity(TardisHelper.TARDIS_POS) instanceof ConsoleTile) {
			console = (ConsoleTile)world.getTileEntity(TardisHelper.TARDIS_POS);
			consoleData = console.serializeNBT();
			consoleState = world.getBlockState(TardisHelper.TARDIS_POS);
		}

		List<ItemStack> allInvs = new ArrayList<ItemStack>();
		
		boolean hasCorridors = world.getBlockState(new BlockPos(0, 129, -30)).getBlock() != TBlocks.corridor_spawn;

		//Delete all old blocks
		BlockPos clearRadius = new BlockPos(radius, radius, radius);
		BlockPos.getAllInBox(TardisHelper.TARDIS_POS.subtract(clearRadius), TardisHelper.TARDIS_POS.add(clearRadius)).forEach((pos) -> {
			TileEntity tile = world.getTileEntity(pos);
			if(tile instanceof IInventory) {
				IInventory inv = (IInventory)tile;
				for(int i = 0; i < inv.getSizeInventory(); ++ i) {
					ItemStack stack = inv.getStackInSlot(i);
					if(!stack.isEmpty())
						allInvs.add(stack.copy());
				}
			}
			else if(tile != null) {
				//Handle vanilla containers and those that use item handler capability
				tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(cap -> {
					for(int i = 0; i < cap.getSlots(); ++i) {
						ItemStack stack = cap.getStackInSlot(i);
						if(!stack.isEmpty())
							allInvs.add(stack.copy());
					}
				});
			}
			world.setBlockState(pos, Blocks.AIR.getDefaultState(), 34);
		});


		//Kill all non-living entities
		AxisAlignedBB killBox = new AxisAlignedBB(-radius, -radius, -radius, radius, radius, radius).offset(TardisHelper.TARDIS_POS);
		BlockPos tpPos = TardisHelper.TARDIS_POS.offset(Direction.NORTH);
		for(Entity entity : world.getEntitiesWithinAABB(Entity.class, killBox)){
			if(!(entity instanceof LivingEntity))
				entity.remove();
			if(entity instanceof PlayerEntity)
				entity.setPositionAndUpdate(tpPos.getX() + 0.5, tpPos.getY() + 0.5, tpPos.getZ() + 0.5);
			if(entity instanceof ArmorStandEntity) {
				for (ItemStack armor : entity.getArmorInventoryList())
					allInvs.add(armor.copy());
				entity.remove();
			}
			if (entity instanceof ItemFrameEntity) {
				allInvs.add(((ItemFrameEntity)entity).getDisplayedItem());
				entity.remove();
			}
			if (entity instanceof LeashKnotEntity) {
				allInvs.add(new ItemStack(Items.LEAD));
				entity.remove();
			}
		}

		//Spawn console room
		Template temp = world.getStructureTemplateManager().getTemplate(file);
		temp.addBlocksToWorld(world, TardisHelper.TARDIS_POS.subtract(this.offset), new PlacementSettings().setIgnoreEntities(false));

		//Remove End Cap if corridors have already been generated
		if(hasCorridors && !overrideStructure) {
			BlockPos corridor = new BlockPos(0, 129, -30);
			for(int x = -1; x < 2; ++x) {
				for(int y = -1; y < 2; ++y) {
					world.setBlockState(corridor.add(x, y, 0), Blocks.AIR.getDefaultState());
				}
			}
		}
		
		//Re-add console
		if(console != null) {
			world.setBlockState(TardisHelper.TARDIS_POS, consoleState);
			world.getTileEntity(TardisHelper.TARDIS_POS).deserializeNBT(consoleData);
		}

		//Spawn Reclamation Unit
		if(!allInvs.isEmpty()) {
			world.setBlockState(TardisHelper.TARDIS_POS.south(2), TBlocks.reclamation_unit.getDefaultState());
			TileEntity te = world.getTileEntity(TardisHelper.TARDIS_POS.south(2));
			if(te instanceof ReclamationTile) {
				ReclamationTile unit = (ReclamationTile)te;
				for(ItemStack stack : allInvs) {
					unit.addItemStack(stack);
				}
			}
		}

		//Update lighting
		ChunkPos startPos = world.getChunk(TardisHelper.TARDIS_POS.subtract(temp.getSize())).getPos();
		ChunkPos endPos = world.getChunk(TardisHelper.TARDIS_POS.add(temp.getSize())).getPos();

		for(int x = startPos.x; x < endPos.x; ++x) {
			for(int z = startPos.z; z < endPos.z; ++z) {
				world.getChunkProvider().getLightManager().lightChunk(world.getChunk(x, z), true);
			}
		}

	}
}
