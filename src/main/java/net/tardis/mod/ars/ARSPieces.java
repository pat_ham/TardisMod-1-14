package net.tardis.mod.ars;

import net.minecraft.util.math.BlockPos;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.registries.TardisRegistries;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class ARSPieces {
	
	@SubscribeEvent
	public static void register(FMLCommonSetupEvent event) {
		register("short", new ARSPiece("short", new BlockPos(3, 3, 9)));
		register("long", new ARSPiece("long", new BlockPos(3, 3, 21)));
		register("fourway", new ARSPiece("fourway", new BlockPos(4, 3, 9)));
		register("t_junction", new ARSPiece("t_junction", new BlockPos(4, 3, 8)));
		register("turn_right", new ARSPiece("turn_right", new BlockPos(3, 3, 8)));
		register("turn_left", new ARSPiece("turn_left", new BlockPos(4, 3, 8)));
		
		//Rooms
		register("large_room", new ARSPiece("large_room", new BlockPos(10, 2, 27)));
		register("library", new ARSPiece("library", new BlockPos(13, 2, 29)));
		register("lab", new ARSPiece("lab", new BlockPos(10, 2, 18)));
		register("workroom_1", new ARSPiece("workroom_1", new BlockPos(6, 3, 13)));
		register("workroom_2", new ARSPiece("workroom_2", new BlockPos(7, 3, 15)));
		register("workroom_3", new ARSPiece("workroom_3", new BlockPos(7, 3, 15)));
		register("storeroom", new ARSPiece("storeroom", new BlockPos(3, 3, 7)));
	}
	
	public static ARSPiece register(String name, ARSPiece piece) {
		TardisRegistries.ARS_PIECES.register(name, piece);
		return piece;
	}
}
