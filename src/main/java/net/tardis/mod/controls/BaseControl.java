package net.tardis.mod.controls;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class BaseControl implements IControl{

	private ResourceLocation registryName;
	private ConsoleTile console;
	private TranslationTextComponent translation; 
	private int animTicks = 0;
	private boolean isDirty = false;
	private boolean isGlowing = false;
	
	public BaseControl(ConsoleTile console) {
		this.console = console;
	}

	@Override
	public void setConsole(ConsoleTile console) {
		this.console = console;
	}

	@Override
	public ConsoleTile getConsole() {
		return this.console;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return registryName;
	}

	@Override
	public IControl setRegistryName(ResourceLocation name) {
		this.registryName = name;
		this.translation = new TranslationTextComponent("control." + name.getNamespace() + "." + name.getPath());
		return this;
	}

	@Override
	public TranslationTextComponent getDisplayName() {
		return this.translation;
	}

	@Override
	public int getAnimationTicks() {
		return this.animTicks;
	}

	@Override
	public void setAnimationTicks(int ticks) {
		this.animTicks = ticks;
	}

	@Override
	public void onPacketUpdate() {}
	
	@Override
	public void markDirty() {
		this.isDirty = true;
	}
	
	@Override
	public void clean() {
		this.isDirty = false;
	}
	
	@Override
	public boolean isDirty() {
		return this.isDirty;
	}

	@Override
	public boolean isGlowing() {
		return this.isGlowing;
	}

	@Override
	public void setGlow(boolean glow) {
		this.isGlowing = glow;
	}
	
	public void startAnimation() {
		this.setAnimationTicks(20);
	}
	
}
