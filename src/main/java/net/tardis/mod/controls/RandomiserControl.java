package net.tardis.mod.controls;

import java.util.Random;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class RandomiserControl extends BaseControl{
	
	private static Random rand = new Random();
	
	public RandomiserControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.099999994F, 0.099999994F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.1875F, 0.1875F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		return EntitySize.flexible(4 / 16.0F, 4 / 16.0F);
	}
	
	@Override
	public Vec3d getPos() {
		ConsoleTile console = this.getConsole();
		if(console instanceof NemoConsoleTile)
			return new Vec3d(-7 / 16.0, 8 / 16.0, -4 / 16.0);

		if(console instanceof GalvanicConsoleTile)
			return new Vec3d(0.556702295430046, 0.45615, 0.5865187044643609);

		if(console instanceof CoralConsoleTile){
			return new Vec3d(-0.5385992072894297, 0.75, -0.6715900297198358);
		}
		
		if(console instanceof HartnelConsoleTile)
			return new Vec3d(0.6786012934946977, 0.5, 0.8388692577190731);

		if(console instanceof ArtDecoConsoleTile)
			return new Vec3d(-0.9274182945945997, 0.34375, 1.0067927165188537);
		
		if(console instanceof ToyotaConsoleTile)
			return new Vec3d(-1.060776999086416, 0.5, 0.5484626764863711);
		
		return new Vec3d(0, 12 / 16.0, 9 / 16.0);
	}


	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		
		if(!player.world.isRemote) {
			int rad = 5 * console.coordIncr;
			BlockPos dest = console.getDestination().add(rad - rand.nextInt(rad * 2), 0, rad - rand.nextInt(rad * 2));
			console.setDestination(console.getDestinationDimension(), dest);
			this.setAnimationTicks(10);
		}
		return true;
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.RANDOMISER;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}
	
	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
}
