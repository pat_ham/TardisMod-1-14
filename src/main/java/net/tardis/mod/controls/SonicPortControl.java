package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.items.TItems;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class SonicPortControl extends BaseControl{

	public SonicPortControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		return EntitySize.flexible(0.25F, 0.25F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			if(player.getHeldItemMainhand().getItem() == TItems.SONIC && console.getSonicItem().isEmpty()) {
				console.setSonicItem(player.getHeldItemMainhand());
				player.setHeldItem(Hand.MAIN_HAND, ItemStack.EMPTY);
				console.getSonicItem().getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(cap -> {
					for(Schematic s : cap.getSchematics()) {
						s.onConsumedByTARDIS(console, player);
					}
					cap.getSchematics().clear();
						
				});
			}
			else if(player.getHeldItemMainhand().isEmpty() && !console.getSonicItem().isEmpty()) {
				InventoryHelper.spawnItemStack(console.getWorld(), player.posX, player.posY, player.posZ, console.getSonicItem());
				console.setSonicItem(ItemStack.EMPTY);
			}
		}
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(0, 6 / 16.0, 11 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(0.4405431448823669, 0.59375, 0.2563013906978433);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(-0.6710054675519425, 0.425, 0.75);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(0.673311443352075, 0.5625, 0.3773743738868549);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(-0.0037532100096278054, 0.28125, -1.3730009392275866);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.9361218815809402, 0.65625, 0.17859217599815258);

		return new Vec3d(-11 / 16.0, 6 / 16.0, 6 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return console.getSonicItem() != null ? SoundEvents.ENTITY_ITEM_FRAME_ADD_ITEM : SoundEvents.ENTITY_ITEM_FRAME_REMOVE_ITEM;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		
	}

}
