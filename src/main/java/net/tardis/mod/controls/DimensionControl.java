package net.tardis.mod.controls;

import java.util.ArrayList;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.common.DimensionManager;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class DimensionControl extends BaseControl {
	
	private static final String MESSAGE = "message.tardis.control.dimchange";
	private ArrayList<DimensionType> dimList = new ArrayList<DimensionType>();
	private int index = 0;
	
	public DimensionControl(ConsoleTile console) {
		super(console);
		createDimList();
	}
	
	@Override
	public EntitySize getSize() {

		if(getConsole() instanceof NemoConsoleTile) 
			return EntitySize.flexible(4 / 16.0F, 4 / 16.0F);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.225F, 0.225F);

        if(getConsole() instanceof CoralConsoleTile){
        	return EntitySize.flexible(0.45F, 0.35F);
		}
        if(this.getConsole() instanceof HartnelConsoleTile)
        	return EntitySize.flexible(0.1875F, 0.1875F);
        
        if(this.getConsole() instanceof ArtDecoConsoleTile)
        	return EntitySize.flexible(0.25F, 0.25F);
        
        if(this.getConsole() instanceof ToyotaConsoleTile)
        	return EntitySize.flexible(0.375F, 0.375F);

		return EntitySize.flexible(6 / 16.0F, 2 / 16.0F);
	}
	

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(0, 12 / 16.0, 8 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(0.004546756986454792, 0.5499999970197678, 0.43130290108982927);


        if(getConsole() instanceof  CoralConsoleTile){
        	return new Vec3d(-0.75, 0.25, -0.006205979489853686);
		}
        
        if(this.getConsole() instanceof HartnelConsoleTile)
        	return new Vec3d(1.0720202954823064, 0.5, 0.1943270264671455);
        
        if(this.getConsole() instanceof ArtDecoConsoleTile)
        	return new Vec3d(-0.8850957102375704, 0.34375, 0.507849993868962);
        
        if(this.getConsole() instanceof ToyotaConsoleTile)
        	return new Vec3d(-0.882092658275639, 0.59375, -0.5211303853403279);

		return new Vec3d(-0.1 / 16.0, 7 / 16.0, 12 / 16.0);
	}
	
	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		return this.doDimChangeAction(console, player);
	}
	
	@Override
	public void onHit(ConsoleTile console, PlayerEntity player) {
		this.doDimChangeAction(console, player);
	}
	
	private boolean doDimChangeAction(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			this.createDimList();
			if(!this.dimList.isEmpty()) {
				this.modIndex(player.isSneaking() ? -1 : 1);
				DimensionType type = this.dimList.get(index);
				console.setDestination(type, console.getDestination());
				player.sendStatusMessage(new TranslationTextComponent(MESSAGE, Helper.formatDimName(type)), true);
				this.setAnimationTicks(20);
			}
			else index = 0;
		}
		return true;
	}
	
	private void modIndex(int i) {
		if(this.index + i >= this.dimList.size()) {
			this.index = 0;
			return;
		}
		if(this.index + i < 0) {
			this.index = this.dimList.size() - 1;
			return;
		}
		this.index += i;
	}
	
	private void createDimList(){
		dimList = new ArrayList<DimensionType>();
		for(DimensionType type : DimensionManager.getRegistry()) {
			if(Helper.canTravelToDimension(type))
				dimList.add(type);
		}
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.DIMENSION;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

}
