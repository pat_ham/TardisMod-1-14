package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class XControl extends BaseControl{
	
	public XControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {

		if(getConsole() instanceof CoralConsoleTile || this.getConsole() instanceof ToyotaConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}

		return EntitySize.flexible(0.0625F, 0.0625F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			console.setDestination(console.getDestinationDimension(), console.getDestination()
				.add(new Vec3i(player.isSneaking() ? -console.getCoordIncr() : console.getCoordIncr(), 0, 0)));
			this.setAnimationTicks(10);
		}
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-8 / 16.0, 10 / 16.0, 2.5 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.512132253102513, 0.625, 0.17666778356266222);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(-0.2693364510041545, 0.59375, 0.7503852322234601);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.9054690056290194, 0.5625, 0.39195598787366204);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(1.01234647419064, 0.4375, 0.2777061131802042);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.6649904910322374, 0.71875, 0.20260675213669666);

		return new Vec3d(-7.25 / 16.0, 9.85 / 16.0, -6 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_ONE;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
}
