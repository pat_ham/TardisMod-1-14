package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class StabilizerControl extends BaseControl{

	private boolean isStabilized = true;
	
	public StabilizerControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize(){
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.4375F, 0.1875F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.12500000000000003F, 0.125F);

		return EntitySize.flexible(0.125F, 0.125F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			this.isStabilized = !this.isStabilized;
			player.sendStatusMessage(new TranslationTextComponent("status.tardis.control.stabilzer." + this.isStabilized), true);
			this.setAnimationTicks(20);
			this.markDirty();
		}
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile) 
			return new Vec3d(13.5 / 16.0, 7 / 16.0, 2.5 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(0.5455551641720471, 0.425, -0.5866870884910467);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(0.4348, 0.45, -0.72);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(0.769493356617752, 0.46875, -0.803508609511675);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(0.3751087059966548, 0.3125, 1.2359473783507973);

		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.30178040037581466, 0.65625, 0.9308479946860322);
		
		return new Vec3d(-7 / 16.0, 10 / 16.0, 4 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return this.isStabilized ? TSounds.STABILIZER_ON : TSounds.STABILIZER_OFF;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("stabilized", this.isStabilized);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.isStabilized = nbt.getBoolean("stabilized");
	}
	
	public boolean isStabilized() {
		return this.isStabilized;
	}
	
	public void setStabilized(boolean stabilized) {
		this.isStabilized = stabilized;
	}

}
