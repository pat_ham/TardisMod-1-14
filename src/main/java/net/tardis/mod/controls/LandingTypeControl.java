package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class LandingTypeControl extends BaseControl{

	private EnumLandType landType = EnumLandType.UP;
	
	public LandingTypeControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(2 / 16.0F, 2 / 16.0F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1625F, 0.1625F);

		if (getConsole() instanceof CoralConsoleTile) {
			return EntitySize.flexible(0.25F, 0.25F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.3125F, 0.3125F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.12500000000000003F, 0.125F);

		return EntitySize.flexible(5 / 16.0F, 4 / 16.0F);
	}
	
	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-12 / 16.0F, 7 / 16.0F, -3 / 16.0F);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.5335388604239691, 0.39374999701976776, 0.5526540125111579);

		if (getConsole() instanceof CoralConsoleTile) {
			return new Vec3d(-0.6238382686350805, 0.78125, -0.35754379507632855);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.3724457446363493, 0.59375, -0.3023108921154344);

		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(0.37350361471531834, 0.1875, -1.286907423523185);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.2962180723627459, 0.65625, -0.8781569088929955);
		
		return new Vec3d(-0.5 / 16.0, 5 / 16.0, -15 / 16.0);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!player.world.isRemote) {
			
			this.landType = this.landType == EnumLandType.UP ? EnumLandType.DOWN : EnumLandType.UP;
			
			player.sendStatusMessage(new StringTextComponent(new TranslationTextComponent("message.tardis.control.land_type").getFormattedText() + " " 
					+ new StringTextComponent(getLandType().name()).applyTextStyle(TextFormatting.LIGHT_PURPLE).getFormattedText()), true);
		}
		return true;
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return this.landType == EnumLandType.UP ? TSounds.LANDING_TYPE_UP : TSounds.LANDING_TYPE_DOWN;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.landType = EnumLandType.values()[tag.getInt("land_type")];
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("land_type", this.landType.ordinal());
		return tag;
	}
	
	public void setLandType(EnumLandType type) {
		this.landType = type;
	}
	
	public EnumLandType getLandType() {
		return this.landType;
	}
	
	public static enum EnumLandType{
		UP,
		DOWN
	}

}
