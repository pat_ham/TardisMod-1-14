package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class YControl extends BaseControl{
	
	public YControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {

		if(getConsole() instanceof CoralConsoleTile || this.getConsole() instanceof ToyotaConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}

		return EntitySize.flexible(0.0625F, 0.0625F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			console.setDestination(console.getDestinationDimension(), console.getDestination()
				.add(0, player.isSneaking() ? -console.getCoordIncr() : console.getCoordIncr(), 0));
			this.setAnimationTicks(10);
		}
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-6.5 / 16.0, 10.5 / 16.0, 4 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.4568998228936493, 0.625, 0.2641350176457876);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(-0.3041092153647018, 0.46875, 0.8127489915200166);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.8503918002329903, 0.5625, 0.49217136);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(1.0426042660083035, 0.4375, 0.2004973626698041);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.5625482953017267, 0.71875, 0.33457478063369095);

		return new Vec3d(-8.1 / 16.0, 9.85 / 16.0, -4.75 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_ONE;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

}
