package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class MonitorControl extends BaseControl{

	public MonitorControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.5625F, 0.5625F);
		}
		if (getConsole() instanceof ToyotaConsoleTile) {
			return EntitySize.flexible(0.5F, 0.5F);
		}
		return EntitySize.flexible(0.25F, 0.25F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(console.getWorld().isRemote) {
			if(console instanceof GalvanicConsoleTile) {
				Tardis.proxy.openGUI(Constants.Gui.MONITOR_MAIN_GALVANIC, null);
			}
			else {
				Tardis.proxy.openGUI(Constants.Gui.MONITOR_MAIN_EYE, null);
			}
		}
		return true;
	}

	@Override
	public Vec3d getPos() {

		if(getConsole() instanceof CoralConsoleTile) {
			return new Vec3d(-0.0022099959688814397, 0.9375, -0.6315483867637458);
		}
		if (getConsole() instanceof ToyotaConsoleTile) {
			return new Vec3d(0.8035256253649128, 0.5, 0.35);
		}

		return new Vec3d(-0.5282378865176893, 0.59375, -0.2954572166748212);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}

}
