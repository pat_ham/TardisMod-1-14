package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.items.TItems;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class TelepathicControl extends BaseControl{

	//private Thread search;
	
	private boolean canUseStructures = false;
	
	public TelepathicControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile) 
			return EntitySize.fixed(4 / 16.0F, 4 / 16.0F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.25F, 0.25F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.375F, 0.375F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.625F, 0.25F);

		return EntitySize.fixed(6 / 16.0F, 4 / 16.0F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!player.isSneaking() && console.getWorld().isRemote)
			Tardis.proxy.openGUI(Constants.Gui.TELEPATHIC, null);
		else if(player.isSneaking())
			player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
				cap.setHasUsedTelepathics(true);
				player.sendStatusMessage(new TranslationTextComponent("status.tardis.telepathic.connected"), true);
			});
		
		if(!console.getWorld().isRemote)
			console.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(world -> {
				PanelInventory inv = world.getEngineInventoryForSide(Direction.SOUTH);
				this.canUseStructures = false;
				for(int i = 0; i < inv.getSizeInventory(); ++i) {
					ItemStack stack = inv.getStackInSlot(i);
					if(stack.getItem() == TItems.TELE_STRUCTURE_UPGRADE) {
						this.canUseStructures = true;
						break;
					}
				}
			});
		
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(11 / 16.0, 7 / 16.0, 6 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(0.4108330938524687, 0.59375, -0.2363353443240117);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(0.38624815436766946, 0.59375, 0.6710918341301058);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.6110511516872056, 0.53125, -0.5789513542057286);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(0.8527096658149658, 0.46875, -0.49991530343616664);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(0.8035256253649128, 0.5, 0.5317747001824875);

		return new Vec3d(11 / 16.0, 7 / 16.0, 6 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.TELEPATHIC_CIRCUIT;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("structures", this.canUseStructures);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.canUseStructures = nbt.getBoolean("structures");
	}

	public boolean canSeeStructures() {
		return this.canUseStructures;
	}

}
