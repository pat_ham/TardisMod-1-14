package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class RefuelerControl extends BaseControl{

	private static final TranslationTextComponent ON = new TranslationTextComponent("message." + Tardis.MODID + ".control.refuel.true");
	private static final TranslationTextComponent OFF = new TranslationTextComponent("message." + Tardis.MODID + ".control.refuel.false");

	private boolean isRefueling = false;
	
	public RefuelerControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(3 / 16.0F, 3 / 16.0F);

		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.1875F, 0.1875F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		return EntitySize.flexible(4 / 16.0F, 4 / 16.0F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			this.isRefueling = !this.isRefueling;
			console.updateClient();
			player.sendStatusMessage(this.isRefueling ? ON : OFF, true);
		}
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(4 / 16.0, 6 / 16.0, -12 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(0.813087980367591, 0.375, 0.19558907625845867);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(0.9537031342233848, 0.4375, -0.2257180672665517);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.16990376273676883, 0.625, 0.6310469709936382);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(-0.383934650177183, 0.34375, 1.2364918206122213);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.8431621092124639, 0.53125, -1.072665990024325);

		return new Vec3d(5.5 / 16.0, 6 / 16.0, -16 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return isRefueling ? TSounds.REFUEL_START : TSounds.REFUEL_STOP;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("is_fueling", this.isRefueling);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.isRefueling = nbt.getBoolean("is_fueling");
	}
	
	public boolean isRefueling() {
		return this.isRefueling;
	}
	
	public void setRefuling(boolean refuel) {
		this.isRefueling = refuel;
		this.markDirty();
	}

}
