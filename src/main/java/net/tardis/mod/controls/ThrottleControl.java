package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class ThrottleControl extends BaseControl {

	private float throttle = 0F;

	public ThrottleControl(ConsoleTile console) {
		super(console);
	}
	
	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.throttle = tag.getFloat("throttle");
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putFloat("throttle", this.throttle);
		return tag;
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(3 / 16.0F, 3 / 16.0F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.275F, 0.4375F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.3125F, 0.3125F);

		return EntitySize.flexible(0.225F, 0.225F);
	}
	
	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(1 / 16.0, 8 / 16.0, -13 / 16.0F);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(0.2658954996435158, 0.5, 0.8247037291009587);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(0.7567711519635095, 0.5, -0.43);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(1.0070054078235704, 0.5625, -0.2252161444978215);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(0.9841104932080567, 0.34375, 0.9699464314312813);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.41347265301191566, 0.5, 1.2127743685836245);

		return new Vec3d(-0.9568176187101369, 0.48749999701976776, -0.2428564747162537);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		return this.doThrottleAction(console, player, 0.1F);
	}

	@Override
	public void onHit(ConsoleTile console, PlayerEntity player) {
		this.doThrottleAction(console, player, 1.0F);
	}
	
	private boolean doThrottleAction(ConsoleTile console, PlayerEntity player, float amt) {
		if(!player.world.isRemote) {
			this.throttle = (float)MathHelper.clamp(throttle + (player.isSneaking() ? -amt : amt), 0.0, 1.0);
			if(this.shouldTakeoff(console))
				console.takeoff();
			else if(console.isInFlight()) {
				if(this.throttle <= 0.0F) {
					if(console.getTimeLeft() > console.getSoundScheme().getLandTime())
						console.initLand();
				}
				else console.updateFlightTime();
				
			}
		}
		return true;
	}
	
	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.THROTTLE;
	}
	
	public float getAmount() {
		return this.throttle;
	}
	
	public void setAmount(float amt) {
		this.throttle = amt;
		this.markDirty();
	}
	
	private boolean shouldTakeoff(ConsoleTile console) {
		return console.getControl(HandbrakeControl.class).isFree() && this.throttle > 0 && !console.isInFlight();
	}

}
