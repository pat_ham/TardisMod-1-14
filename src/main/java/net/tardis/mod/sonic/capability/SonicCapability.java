package net.tardis.mod.sonic.capability;

import static net.tardis.mod.Tardis.MODID;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SyncSonicMessage;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.sonic.ISonicPart;
import net.tardis.mod.sonic.SonicManager;

/**
 * Created by Swirtzly
 * on 22/03/2020 @ 22:41
 */
@Mod.EventBusSubscriber
public class SonicCapability implements ISonic {

    private static final ResourceLocation CAP_ID = new ResourceLocation(MODID, "sonic_cap");
    public static Random RANDOM = new Random();
    private SonicBasePart.SonicComponentTypes[] PARTS = new SonicBasePart.SonicComponentTypes[]{
            SonicBasePart.SonicComponentTypes.MK_2, SonicBasePart.SonicComponentTypes.MK_2, SonicBasePart.SonicComponentTypes.MK_2, SonicBasePart.SonicComponentTypes.MK_2
    };
    private int mode = 0;

    private final ItemStack stack;
    private float charge = 100, progress = 0;
    private List<Schematic> schematics = new ArrayList<Schematic>();

    public SonicCapability(ItemStack stack) {
        this.stack = stack;
        for (ISonicPart.SonicPart value : ISonicPart.SonicPart.values()) {
            setSonicPart(SonicBasePart.SonicComponentTypes.MK_2, value);
        }
        SonicItem.syncCapability(stack);
    }

    public SonicCapability() {
        stack = null;
    }

    public static LazyOptional<ISonic> getForStack(ItemStack stack) {
        return stack.getCapability(Capabilities.SONIC_CAPABILITY);
    }

    public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
        int x = RANDOM.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    @SubscribeEvent
    public static void onItemCapabilities(AttachCapabilitiesEvent<ItemStack> event) {
        if (event.getObject().getItem() instanceof SonicItem) {
            event.addCapability(SonicCapability.CAP_ID, new SonicProvider(new SonicCapability(event.getObject())));
        }
    }

    @Override
    public void setSonicPart(SonicBasePart.SonicComponentTypes type, ISonicPart.SonicPart part) {
        PARTS[part.getInvID()] = type;
    }

    @Override
    public SonicBasePart.SonicComponentTypes getSonicPart(ISonicPart.SonicPart part) {
        return PARTS[part.getInvID()];
    }

    @Override
    public float getCharge() {
        return charge;
    }

    @Override
    public void setCharge(float charge) {
        this.charge = charge;
    }

    @Override
    public float progress() {
        return progress;
    }

    @Override
    public void setProgress(float progress) {
        this.progress = progress;
    }

    @Override
    public void sync(PlayerEntity player, Hand hand) {
        SonicItem.syncCapability(stack);
        Network.sendPacketToAll(new SyncSonicMessage(hand, serializeNBT(), player.getUniqueID()));
    }

    @Override
    public int getMode() {
        if (mode >= SonicManager.SONIC_TYPES_ARRAY.length) {
            setMode(0);
        }
        return mode;
    }

    @Override
    public void setMode(int mode) {
        if (mode >= SonicManager.SONIC_TYPES_ARRAY.length) {
            this.mode = 0;
            return;
        }
        this.mode = mode;
    }

    @Override
    public void randomiseParts() {
        for (ISonicPart.SonicPart value : ISonicPart.SonicPart.values()) {
            setSonicPart(randomEnum(SonicBasePart.SonicComponentTypes.class), value);
            SonicItem.syncCapability(stack);
        }
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT compoundNBT = new CompoundNBT();
        compoundNBT.putInt("emmiter", PARTS[0].getId());
        compoundNBT.putInt("activator", PARTS[1].getId());
        compoundNBT.putInt("handle", PARTS[2].getId());
        compoundNBT.putInt("end", PARTS[3].getId());
        compoundNBT.putInt("mode", getMode());
        compoundNBT.putFloat("charge", charge);
        compoundNBT.putFloat("progress", progress);
        ListNBT schem = new ListNBT();
        for(Schematic s : this.getSchematics())
        	schem.add(new StringNBT(s.getRegistryName().toString()));
        compoundNBT.put("schematics", schem);
        return compoundNBT;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        setSonicPart(findPartByID(nbt.getInt("emmiter")), ISonicPart.SonicPart.EMITTER);
        setSonicPart(findPartByID(nbt.getInt("activator")), ISonicPart.SonicPart.ACTIVATOR);
        setSonicPart(findPartByID(nbt.getInt("handle")), ISonicPart.SonicPart.HANDLE);
        setSonicPart(findPartByID(nbt.getInt("end")), ISonicPart.SonicPart.END);
        setCharge(nbt.getFloat("charge"));
        setProgress(nbt.getFloat("progress"));
        setMode(nbt.getInt("mode"));
        this.schematics.clear();
        ListNBT schematicList = nbt.getList("schematics", Constants.NBT.TAG_STRING);
        for(INBT schem : schematicList) {
        	this.schematics.add(TardisRegistries.SCHEMATICS.getValue(new ResourceLocation(((StringNBT)schem).getString())));
        }
    }

    public SonicBasePart.SonicComponentTypes findPartByID(int id) {
        for (SonicBasePart.SonicComponentTypes value : SonicBasePart.SonicComponentTypes.values()) {
            if (value.getId() == id) {
                return value;
            }
        }
        return SonicBasePart.SonicComponentTypes.MK_1;
    }

	@Override
	public List<Schematic> getSchematics() {
		return this.schematics;
	}
}
