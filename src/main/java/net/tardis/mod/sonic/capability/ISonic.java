package net.tardis.mod.sonic.capability;

import java.util.List;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.sonic.ISonicPart;

/**
 * Created by Swirtzly
 * on 22/03/2020 @ 22:32
 */
public interface ISonic extends INBTSerializable<CompoundNBT> {
    void setSonicPart(SonicBasePart.SonicComponentTypes type, ISonicPart.SonicPart part);

    SonicBasePart.SonicComponentTypes getSonicPart(ISonicPart.SonicPart part);

    float getCharge();

    void setCharge(float charge);

    float progress();

    void setProgress(float progress);

    void sync(PlayerEntity player, Hand hand);

    int getMode();

    void setMode(int mode);

    void randomiseParts();
    
    List<Schematic> getSchematics();
   // void addSchematic(Schematic sce);
}