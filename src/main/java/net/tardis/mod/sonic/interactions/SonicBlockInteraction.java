package net.tardis.mod.sonic.interactions;

import static net.minecraft.block.Blocks.REDSTONE_LAMP;
import static net.minecraft.block.DoorBlock.OPEN;
import static net.minecraft.block.RedstoneLampBlock.LIT;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.DoorBlock;
import net.minecraft.block.RedstoneLampBlock;
import net.minecraft.block.RedstoneWireBlock;
import net.minecraft.block.TNTBlock;
import net.minecraft.block.TrapDoorBlock;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.TNTEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.sonic.SonicManager;

/**
 * Created by Swirtzly
 * on 16/03/2020 @ 21:39
 */
public class SonicBlockInteraction implements SonicManager.ISonicMode {

    private static final ResourceLocation LOCATION = new ResourceLocation(Tardis.MODID, "block_interactions");
    private Method dispenseMethod = ObfuscationReflectionHelper.findMethod(DispenserBlock.class, "func_176439_d", World.class, BlockPos.class);

    @Override
    public boolean processBlock(PlayerEntity player, BlockState blockState, ItemStack sonic, BlockPos pos) {
        World world = player.world;
        Block block = blockState.getBlock();
        
        
      //isHandActive checks if player hand is active (active hand refers to when the player has let go of right click)
      //This makes it so that i.e. player lets go of right click, onStoppedUsing will be called, so this method will be called, but ONLY if they let go of right click, instead of on right click
        if (!world.isRemote() && player.isHandActive()) { 
        	//It doesn't work on wood!
            if (isWood(blockState)) {
                PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.tardis.wood_fail"), true);
                return false;
            }

            if (block instanceof DispenserBlock) {
                try {
                    dispenseMethod.invoke(block, world, pos);
                    return true;
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                    return false;
                }
            }

            //Explodes TNT
            if (block instanceof TNTBlock && TConfig.CONFIG.detonateTnt.get()) {
                //TODO Cost 20
                    PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.tardis.detonate_tnt"), true);
                    player.world.removeBlock(pos, true);
                    TNTEntity tnt = new TNTEntity(world, (double) ((float) pos.getX() + 0.5F), (double) pos.getY(), (double) ((float) pos.getZ() + 0.5F), player);
                    world.addEntity(tnt);
                    world.playSound(null, tnt.posX, tnt.posY, tnt.posZ, SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.BLOCKS, 1.0F, 1.0F);
                    return true;
            }

            //Toggle Redstone lamp
            if (block instanceof RedstoneLampBlock && TConfig.CONFIG.redstoneLamps.get()) {
                if (block == REDSTONE_LAMP) {
                    //TODO Cost 10
                        world.setBlockState(pos, blockState.with(LIT, !blockState.get(LIT)), 2); //Fixes neighbouring lamps from being turned off
                        return true;
                }
            }

            //Forcibly open a door
            if (block instanceof DoorBlock && TConfig.CONFIG.openDoors.get()) {
                //TODO Cost 5
                    world.setBlockState(pos, blockState.with(OPEN, !blockState.get(OPEN)), 10);
                    world.playEvent(player, blockState.get(DoorBlock.OPEN) ? blockState.getMaterial() == Material.IRON ? 1005 : 1006 : blockState.getMaterial() == Material.IRON ? 1011 : 1012, pos, 0);
                    return true;
            }

            //Force open a trap door
            if (block instanceof TrapDoorBlock && TConfig.CONFIG.openTrapDoors.get()) {
                //TODO Cost 5
                    world.setBlockState(pos, blockState.with(OPEN, !blockState.get(OPEN)));
                    return true;
            }

//            if (block instanceof RedstoneWireBlock && TConfig.CONFIG.toggleRedstone.get()) {
//                //TODO Cost 40 although I'm not even sure this works properly
//                    Integer power = blockState.get(RedstoneWireBlock.POWER);
//                    world.setBlockState(pos, blockState.with(RedstoneWireBlock.POWER, power > 0 ? 0 : 15));
//                    return true;
//            }
        }
        return false;
    }

    @Override
    public boolean processEntity(PlayerEntity user, Entity targeted, ItemStack sonic) {
        return false;
    }

    @Override
    public ArrayList<TranslationTextComponent> getAdditionalInfo() {
        ArrayList<TranslationTextComponent> list = new ArrayList<>();
        list.add(new TranslationTextComponent("sonic.modes.info.interactable_blocks"));
        for (Block interactableBlock : getInteractableBlocks()) {
            list.add((TranslationTextComponent) new TranslationTextComponent("- ").appendSibling(new TranslationTextComponent(interactableBlock.getTranslationKey())));
        }
        return list;
    }

    public ArrayList<Block> getInteractableBlocks() { //TODO OPTIMISE, CACHE THIS!! May have to move to server starting event, although that isn't on the client...
        ArrayList<Block> list = new ArrayList<Block>();
        for (Block block : ForgeRegistries.BLOCKS) {
//            if (block instanceof RedstoneWireBlock || block instanceof TrapDoorBlock || block instanceof DoorBlock || block instanceof RedstoneLampBlock || block instanceof TNTBlock) {
        		if (block instanceof TrapDoorBlock || block instanceof DoorBlock || block instanceof RedstoneLampBlock || block instanceof TNTBlock) {
                if (!isWood(block.getDefaultState())) {
                    list.add(block);
                }
            }
        }
        return list;
    }

    @Override
    public boolean hasAdditionalInfo() {
        return true;
    }

    @Override
    public void updateHeld(PlayerEntity playerEntity, ItemStack stack) {

    }

    @Override
    public ResourceLocation getRegistryName() {
        return LOCATION;
    }

    public boolean isWood(BlockState blockState) {
        return blockState.getMaterial() == Material.WOOD || blockState.getMaterial() == Material.BAMBOO || blockState.getMaterial() == Material.BAMBOO_SAPLING;
    }

    @Override
    public void processSpecialBlocks(PlayerInteractEvent.RightClickBlock event) {
        World world = event.getWorld();
        BlockState blockState = world.getBlockState(event.getPos());
    	 if (blockState.getBlock() instanceof DispenserBlock) {
             event.setCanceled(true);
         }
        processBlock(event.getPlayer(), blockState, event.getItemStack(), event.getPos());
        
    }
}
