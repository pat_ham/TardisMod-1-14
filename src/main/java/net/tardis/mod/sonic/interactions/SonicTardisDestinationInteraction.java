package net.tardis.mod.sonic.interactions;

import java.util.ArrayList;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.sonic.SonicManager;
import net.tardis.mod.tileentities.ConsoleTile;

/**
 * Created by Swirtzly
 * on 17/03/2020 @ 11:02
 */
public class SonicTardisDestinationInteraction implements SonicManager.ISonicMode {

    private static final ResourceLocation LOCATION = new ResourceLocation(Tardis.MODID, "set_destination");


    @Override
    public boolean processBlock(PlayerEntity player, BlockState blockState, ItemStack sonic, BlockPos pos) {
        if (!TConfig.CONFIG.coordinateTardis.get()) return false;
        if (!player.world.isRemote && Helper.canTravelToDimension(player.world.getDimension().getType())) {
            ConsoleTile console = TardisHelper.getConsole(player.getServer(), player.getUniqueID()).orElse(null);
            if (console != null) {
                //TODO Cost 25
                console.setDestination(player.dimension, pos);
                console.setDirection(player.getHorizontalFacing());
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean processEntity(PlayerEntity user, Entity targeted, ItemStack sonic) {
        return false;
    }

    @Override
    public ResourceLocation getRegistryName() {
        return LOCATION;
    }


    @Override
    public ArrayList<TranslationTextComponent> getAdditionalInfo() {
        ArrayList<TranslationTextComponent> list = new ArrayList<TranslationTextComponent>();
        list.add(new TranslationTextComponent("sonic.modes.info.set_coords"));
        return list;
    }

    @Override
    public boolean hasAdditionalInfo() {
        return true;
    }

    @Override
    public void updateHeld(PlayerEntity playerEntity, ItemStack stack) {

    }

}
