package net.tardis.mod.sonic.interactions;

import static net.tardis.mod.helper.PlayerHelper.getPosLookingAt;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.misc.TDamage;
import net.tardis.mod.sonic.SonicManager;
import net.tardis.mod.sonic.capability.SonicCapability;

/**
 * Created by Swirtzly
 * on 25/03/2020 @ 10:45
 */
public class SonicLaserInteraction implements SonicManager.ISonicMode {

    private static final ResourceLocation LOCATION = new ResourceLocation(Tardis.MODID, "laser_interaction");


    @Override
    public boolean processBlock(PlayerEntity player, BlockState blockState, ItemStack sonic, BlockPos pos) {
        if (player.world.isRemote) return false;
        //We can remove the activehand check here because we want the user to be able to use the laser as soon as they hold down right click (i.e. not when they stop using)
        if (TConfig.CONFIG.laserFire.get()) {
            //TODO Cost I will let you decide
                    SonicCapability.getForStack(sonic).ifPresent((data) -> {
                        if (data.getCharge() > 0) {
                            for (Direction dir : Direction.values()) {
                                if (player.world.isAirBlock(pos.add(dir.getDirectionVec()))) {
                                    player.world.setBlockState(pos.add(dir.getDirectionVec()), Blocks.FIRE.getDefaultState());
                                }
                            }
                        }
                    });
        }
        return false;
    }

    @Override
    public boolean processEntity(PlayerEntity user, Entity targeted, ItemStack sonic) {
        if (user.world.isRemote) return false;
        //TODO Cost I will let you decide
            SonicCapability.getForStack(sonic).ifPresent((data) -> {
                if (data.getCharge() > 0) {
                    targeted.attackEntityFrom(TDamage.LASER_SONIC, 5);
                }
            });
        return false;
    }

    @Override
    public ResourceLocation getRegistryName() {
        return LOCATION;
    }

    @Override
    public boolean hasAdditionalInfo() {
        return false;
    }

    @Override
    public void updateHeld(PlayerEntity playerEntity, ItemStack stack) {
        RayTraceResult raytraceresult = getPosLookingAt(playerEntity, SonicItem.getCurrentMode(stack).getReachDistance());
        if (raytraceresult != null) {
            if (raytraceresult.getType() == RayTraceResult.Type.ENTITY) {
                EntityRayTraceResult entityRayTraceResult = (EntityRayTraceResult) raytraceresult;
                processEntity(playerEntity, entityRayTraceResult.getEntity(), stack);
                SonicCapability.getForStack(stack).ifPresent(cap -> cap.sync(playerEntity, Hand.MAIN_HAND));
            }
            if (raytraceresult.getType() == RayTraceResult.Type.BLOCK) {
                BlockRayTraceResult blockRayTraceResult = (BlockRayTraceResult) raytraceresult;
                processBlock(playerEntity, playerEntity.world.getBlockState(blockRayTraceResult.getPos()), stack, blockRayTraceResult.getPos());
                SonicCapability.getForStack(stack).ifPresent(cap -> cap.sync(playerEntity, Hand.MAIN_HAND));
            }
        }
    }
}
