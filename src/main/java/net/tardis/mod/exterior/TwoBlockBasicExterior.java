
package net.tardis.mod.exterior;

import java.util.function.Supplier;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.fluid.Fluids;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.upgrades.AtriumUpgrade;

public class TwoBlockBasicExterior extends ExteriorBase{

	private Supplier<BlockState> top;
	private boolean inChameleon = false;
	private ResourceLocation blueprint = null;
	private IDoorType type;
	private TexVariant[] variants = null;
	
	public TwoBlockBasicExterior(Supplier<BlockState> state, boolean inChameleon, IDoorType type, ResourceLocation blueprint) {
		this.top = state;
		this.inChameleon = inChameleon;
		this.blueprint = blueprint;
		this.type = type;
	}
	
	public TwoBlockBasicExterior(Supplier<BlockState> state, boolean inChameleon, IDoorType type, ResourceLocation blueprint, TexVariant[] variants) {
		this(state, inChameleon, type, blueprint);
		this.variants = variants;
	}

	@Override
	public void demat(ConsoleTile console) {
		ServerWorld world = console.getWorld().getServer().getWorld(console.getDimension());
		if(world != null) {
			if(this.getExterior(console) != null)
				this.getExterior(console).demat();
		}
		else System.err.println("Tryed to demat, put TARDIS location dimension is null!");
	}

	@Override
	public void remat(ConsoleTile console) {
		ServerWorld world = console.getWorld().getServer().getWorld(console.getDestinationDimension());
		if(world != null) {
			BlockPos pos = console.getDestination().up();
			boolean isWater = world.getFluidState(pos).isTagged(FluidTags.WATER);
			BlockState state = top.get().with(BlockStateProperties.HORIZONTAL_FACING, console.getDirection());
			if(state.has(BlockStateProperties.WATERLOGGED))
				state = state.with(BlockStateProperties.WATERLOGGED, isWater);
			world.setBlockState(pos, state);
			world.setBlockState(pos.down(), Blocks.BARRIER.getDefaultState());
			
			TileEntity ext = world.getTileEntity(pos);
			if(ext instanceof ExteriorTile) {
				ExteriorTile exter = (ExteriorTile)ext;
				exter.copyConsoleData(console);
				exter.remat();
			}
		}
		else System.err.println("Tryed to Remat, put TARDIS destination dimension is null!");
	}

	@Override
	public boolean isDefault() {
		return inChameleon;
	}

	@Override
	public int getWidth(ConsoleTile console) {
		AtriumUpgrade atrium = console.getUpgrade(AtriumUpgrade.class).orElse(null);
		if(atrium != null) {
			if(atrium.isActive())
				return atrium.getWidth();
		}
		return 0;
	}

	@Override
	public int getHeight(ConsoleTile console) {
		AtriumUpgrade atrium = console.getUpgrade(AtriumUpgrade.class).orElse(null);
		if(atrium != null) {
			if(atrium.isActive())
				return atrium.getHeight();
		}
		return 2;
	}

	@Override
	public ExteriorTile getExterior(ConsoleTile console) {
		if(!console.getWorld().isRemote) {
			ServerWorld world = console.getWorld().getServer().getWorld(console.getDimension());
			if(world != null) {
				TileEntity te = world.getTileEntity(console.getLocation().up());
				if(te instanceof ExteriorTile)
					return (ExteriorTile)te;
			}
		}
		return null;
	}

	@Override
	public ResourceLocation getBlueprintTexture() {
		return this.blueprint;
	}

	@Override
	public IDoorType getDoorType() {
		return this.type;
	}

	@Override
	public void remove(ConsoleTile tile) {
		ServerWorld world = tile.getWorld().getServer().getWorld(tile.getDimension());
		if(world != null) {
			world.setBlockState(tile.getLocation(), Blocks.AIR.getDefaultState());
			world.setBlockState(tile.getLocation().up(),
					world.getFluidState(tile.getLocation().up()).getFluid() == Fluids.WATER ? Blocks.WATER.getDefaultState() : Blocks.AIR.getDefaultState());
		}
		else System.err.println("World was not loadable!");
	}

	@Override
	public void place(ConsoleTile tile, DimensionType type, BlockPos pos) {
		ServerWorld world = tile.getWorld().getServer().getWorld(type);
		if(world != null) {
			
			boolean isInWater = world.getFluidState(pos.up()).isTagged(FluidTags.WATER);
			BlockState state = this.top.get().with(BlockStateProperties.HORIZONTAL_FACING, tile.getDirection());
			if(state.has(BlockStateProperties.WATERLOGGED))
				state = state.with(BlockStateProperties.WATERLOGGED, isInWater);
			world.setBlockState(pos.up(), state);
			world.setBlockState(pos, Blocks.AIR.getDefaultState());
			
			tile.setLocation(type, pos);
			if(world.getTileEntity(pos.up()) instanceof ExteriorTile)
				((ExteriorTile)world.getTileEntity(pos.up())).copyConsoleData(tile);
		}
		else System.err.println("World was not loadable!");
	}

	@Override
	public TranslationTextComponent getDisplayName() {
		return new TranslationTextComponent("exterior.tardis." + this.getRegistryName().getPath());
	}

	@Override
	public TexVariant[] getVariants() {
		return this.variants;
	}
}
