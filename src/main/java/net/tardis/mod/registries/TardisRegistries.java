package net.tardis.mod.registries;

import java.util.ArrayList;
import java.util.List;

import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.client.animation.IExteriorAnimation.ExteriorAnimationEntry;
import net.tardis.mod.entity.ai.dalek.types.DalekType;
import net.tardis.mod.flight.FlightEvent;
import net.tardis.mod.protocols.Protocol;
import net.tardis.mod.registries.consoles.Console;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.sounds.InteriorHum;
import net.tardis.mod.subsystem.SubsystemEntry;
import net.tardis.mod.upgrades.UpgradeEntry;

/**
 * 
 * @author Spectre0987
 * 
 * Register things during {@link FMLCommonSetupEvent}
 *
 */
@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TardisRegistries {

	public static final Registry<DalekType> DALEK_TYPE = new Registry<DalekType>();
	public static final Registry<Protocol> PROTOCOL_REGISTRY = new Registry<Protocol>();
	public static final Registry<Console> CONSOLE_REGISTRY = new Registry<Console>();
	public static final Registry<SubsystemEntry<?>> SUBSYSTEM_REGISTRY = new Registry<SubsystemEntry<?>>();
	public static final Registry<UpgradeEntry<?>> UPGRADES = new Registry<UpgradeEntry<?>>();
	public static final Registry<ExteriorAnimationEntry<?>> EXTERIOR_ANIMATIONS = new Registry<ExteriorAnimationEntry<?>>();
	public static final Registry<InteriorHum> HUM_REGISTRY = new Registry<InteriorHum>();
    public static final Registry<FlightEvent> FLIGHT_EVENT = new Registry<FlightEvent>();
    public static final Registry<Schematic> SCHEMATICS = new Registry<Schematic>();
    public static final Registry<ARSPiece> ARS_PIECES = new Registry<ARSPiece>();

	private static List<Runnable> REGISTRIES = new ArrayList<Runnable>();

	@SubscribeEvent
	public static void init(FMLCommonSetupEvent event) {
		DeferredWorkQueue.runLater(() -> doRegisters());
	}
	
	public static void init() {}
	
	private static void doRegisters() {
		for (Runnable run : REGISTRIES) {
			run.run();
		}
	}
	
	/**
	 * 
	 * @param run - the runnable to execute after all registries are set up
	 */
	public static void registerRegisters(Runnable run) {
		//REGISTRIES.add(run);
		run.run();
	}
}
