package net.tardis.mod.registries.consoles;

import java.util.function.Supplier;

import net.minecraft.block.BlockState;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.registries.TardisRegistries;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class Console implements IRegisterable<Console>{
	
	private ResourceLocation registryName;
	private ResourceLocation imageLocation;
	private Supplier<BlockState> state;
	
	public static Console STEAM;
	public static Console NEMO;
	public static Console GALVANIC;
	public static Console CORAL;
	public static Console HARTNEL;
	public static Console ART_DECO;
	public static Console TOYOTA;

	public Console(Supplier<BlockState> state, ResourceLocation imageLocation) {
		this.state = state;
		this.imageLocation = imageLocation;
	}
	
	public Console(Supplier<BlockState> state, String imageLocation) {
		this(state, new ResourceLocation(Tardis.MODID, "textures/gui/consoles/" + imageLocation + ".png"));
	}

	@Override
	public Console setRegistryName(ResourceLocation regName) {
		this.registryName = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.registryName;
	}
	
	public BlockState getState() {
		return state.get();
	}
	
	public ResourceLocation getTexture() {
		return this.imageLocation;
	}
	
	@SubscribeEvent
	public static void eventBusSubscriber(FMLCommonSetupEvent event) {
		TardisRegistries.registerRegisters(() -> {
			STEAM = register(new Console(() -> TBlocks.console_steam.getDefaultState(), "steam"), "steam");
			NEMO = register(new Console(() -> TBlocks.console_nemo.getDefaultState(), "nemo"), "nemo");
			GALVANIC = register(new Console(() -> TBlocks.console_galvanic.getDefaultState(), "galvanic"), "galvanic");
			CORAL = register(new Console(() -> TBlocks.console_coral.getDefaultState(), "coral"), "coral");
			HARTNEL = register(new Console(() -> TBlocks.console_hartnel.getDefaultState(), "hartnel"), "hartnel");
			ART_DECO = register(new Console(() -> TBlocks.console_art_deco.getDefaultState(), "art_deco"), "art_deco");
			TOYOTA = register(new Console(() -> TBlocks.console_toyota.getDefaultState(), "toyota"), "toyota");
		});
	}
	
	public static Console register(Console console, ResourceLocation name) {
		console.setRegistryName(name);
		return console;
	}
	
	private static Console register(Console console, String name) {
		console.setRegistryName(new ResourceLocation(Tardis.MODID, name));
		TardisRegistries.CONSOLE_REGISTRY.register(console.getRegistryName(), console);
		return console;
	}

}
