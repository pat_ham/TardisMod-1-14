package net.tardis.mod.helper;

import java.util.Iterator;
import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.exceptions.NoDimensionFoundException;
import net.tardis.mod.exceptions.NoPlayerFoundException;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

/*
 * A helper for Tardis - specific things.
 */
public class TardisHelper {
	
	public static final BlockPos TARDIS_POS = new BlockPos(0, 128, 0).toImmutable();
	
	public static boolean isInOwnedTardis(PlayerEntity player) {
		ResourceLocation loc = DimensionType.getKey(player.world.dimension.getType());
		return loc != null ? loc.getPath().equals(player.getUniqueID().toString()) : false;
	}
	
	public static boolean isInATardis(PlayerEntity player) {
		return player.dimension.getModType() == TDimensions.TARDIS;
	}
	
	public static DimensionType setupPlayersTARDIS(ServerPlayerEntity player) {
		DimensionType tardis = TDimensions.registerOrGet(player.getUniqueID().toString(), TDimensions.TARDIS);
		ServerWorld world = ServerLifecycleHooks.getCurrentServer().getWorld(tardis);
		if(world != null && !(world.getTileEntity(TARDIS_POS) instanceof ConsoleTile)) {
			world.setBlockState(TARDIS_POS, TBlocks.console_steam.getDefaultState());
		}
		ConsoleRoom.BROKEN_STEAM.spawnConsoleRoom(world, true);
		
		//Randomly contain artron caps
		world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
			PanelInventory inv = cap.getEngineInventoryForSide(Direction.WEST);
			for(int i = 0; i < inv.getSizeInventory(); ++i) {
				inv.setInventorySlotContents(i, world.rand.nextDouble() < 0.1 ? new ItemStack(TItems.LEAKY_ARTRON_CAPACITOR) : ItemStack.EMPTY);
			}
		});
		
		return tardis;
	}
	
	public static boolean hasTARDIS(MinecraftServer server, UUID id) {
		Iterator<DimensionType> it = DimensionType.getAll().iterator();
		while(it.hasNext()) {
			DimensionType type = it.next();
			if(DimensionType.getKey(type).getPath().equals(id.toString())) {
				ServerWorld world = server.getWorld(type);
				if(world != null && world.getTileEntity(TARDIS_POS) instanceof ConsoleTile)
					return true;
			}
		}
		return false;
	}
	

	public static LazyOptional<ConsoleTile> getConsole(MinecraftServer server, UUID uuid) {
		Iterator<DimensionType> it = DimensionType.getAll().iterator();
		while(it.hasNext()) {
			DimensionType type = it.next();
			ResourceLocation name = DimensionType.getKey(type);
			if(name != null && name.getPath().toString().contentEquals(uuid.toString())) {
				ServerWorld world = server.getWorld(type);
				if(world != null) {
					TileEntity te = world.getTileEntity(TardisHelper.TARDIS_POS);
					if(te instanceof ConsoleTile)
						return LazyOptional.of(() -> (ConsoleTile) te);
				}
			}
		}
		return LazyOptional.empty();
	}
	
	public static LazyOptional<ConsoleTile> getConsole(MinecraftServer server, DimensionType type) {
		ServerWorld world = server.getWorld(type);
		if(world != null) {
			TileEntity te = world.getTileEntity(TARDIS_POS);
			if(te instanceof ConsoleTile)
				return LazyOptional.of(() -> (ConsoleTile)te);
		}
		return LazyOptional.empty();
	}
	
	@Nullable
	public static DimensionType getTardisByUUID(UUID id) {
		Iterator<DimensionType> it = DimensionType.getAll().iterator();
		while(it.hasNext()) {
			DimensionType type = it.next();
			ResourceLocation loc = DimensionType.getKey(type);
			if(loc != null && loc.getPath().toString().contentEquals(id.toString()))
				return type;
		}
		return null;
	}

    public static DimensionType getTardisDimension(String username) throws NoDimensionFoundException, NoPlayerFoundException {
        String playerUUID = PlayerHelper.getOnlinePlayerUUID(username).toString();
        DimensionType dimension = DimensionType.byName(new ResourceLocation(Tardis.MODID, playerUUID));
        if (dimension == null)
            throw new NoDimensionFoundException(username);
        return dimension;
    }


	public static LazyOptional<ConsoleTile> getConsoleInWorld(World world) {
		TileEntity te = world.getTileEntity(TARDIS_POS);
		if(te instanceof ConsoleTile)
			return LazyOptional.of(() -> (ConsoleTile)te);
		return LazyOptional.empty();
	}

}
