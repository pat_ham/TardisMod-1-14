package net.tardis.mod.helper;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.UUID;

import com.google.common.collect.ImmutableList;
import com.mojang.authlib.GameProfile;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.management.PlayerProfileCache;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.util.NonNullConsumer;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.IVortexCap;
import net.tardis.mod.exceptions.NoPlayerFoundException;
import net.tardis.mod.items.TItems;

public class PlayerHelper {

    public static UUID getOnlinePlayerUUID(String username) throws NoPlayerFoundException {
        Optional<ServerPlayerEntity> player = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayers().stream().filter(p -> p.getName().getFormattedText().equals(username)).findFirst();
        if (player.isPresent())
            return player.get().getUniqueID();
        throw new NoPlayerFoundException(username);
    }

    public static String getPlayerUsername(UUID uuid) {
        if (ServerLifecycleHooks.getCurrentServer() == null || uuid == null) return "Unknown Timelord";
        PlayerProfileCache cache = ServerLifecycleHooks.getCurrentServer().getPlayerProfileCache();
        GameProfile profile = cache.getProfileByUUID(uuid);
        if (profile != null) {
            return profile.getName();
        }
        return "Unknown Timelord";
    }
    
    public static ItemStack getHeldStack(LivingEntity holder, Hand hand) {
    	return holder.getHeldItem(hand);
    }

    public static boolean isInHand(Hand hand, LivingEntity holder, Item item) {
        ItemStack heldItem = holder.getHeldItem(hand);
        return heldItem.getItem() == item;
    }

    public static boolean isInMainHand(LivingEntity holder, Item item) {
        return isInHand(Hand.MAIN_HAND, holder, item);
    }

    /**
     * Checks if player has item in offhand
     */
    public static boolean isInOffHand(LivingEntity holder, Item item) {
        return isInHand(Hand.OFF_HAND, holder, item);
    }

    /**
     * Checks if player has item in either hand
     */
    public static boolean isInEitherHand(LivingEntity holder, Item item) {
        return isInMainHand(holder, item) || isInOffHand(holder, item);
    }

    // MAIN_HAND xor OFF_HAND
    public static boolean isInOneHand(LivingEntity holder, Item item) {
        boolean mainHand = (isInMainHand(holder, item) && !isInOffHand(holder, item));
        boolean offHand = (isInOffHand(holder, item) && !isInMainHand(holder, item));
        return mainHand || offHand;
    }

    public static void sendMessageToPlayer(PlayerEntity player, TranslationTextComponent textComponent, boolean isHotBar) {
        if (player.world.isRemote) return;
        player.sendStatusMessage(textComponent, isHotBar);
    }
    
    /**
     * Sets the Vortex Manipulator Item Model to closed
     * @implSpec Used only when VM Gui is closed
     * @param player
     */
    public static void closeVMModel(PlayerEntity player) {
		if(player != null) {
			NonNullConsumer<IVortexCap> cons = cap -> cap.setOpen(false);
			ItemStack vm = new ItemStack(TItems.VORTEX_MANIP);
			ItemStack stack = getHeldOrNearestStack(player, vm);
			stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cons);
//			stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cons);
		}
    }
    
    /**
     * Gets slot index of an itemstack in the player's inventory closet to slot 0.
     * @param player
     * @param stack
     * @return
     */
    public static int getSlotIndexOfStackInInventory(PlayerEntity player, ItemStack stack) {
      PlayerInventory inv = player.inventory;
      List<NonNullList<ItemStack>> allInventories = ImmutableList.of(inv.mainInventory, inv.offHandInventory);
      for(NonNullList<ItemStack> nonnulllist : allInventories) {
    	  Iterator<ItemStack> iterator = nonnulllist.iterator();
          while(true) {
             if (!iterator.hasNext()) {
                continue;
             }
             ItemStack itemstack = (ItemStack)iterator.next();
             if (!itemstack.isEmpty()) {
            	 if (itemstack == player.getHeldItemMainhand()) {
            		 return EquipmentSlotType.MAINHAND.getSlotIndex(); //Choose mainhand slot if item is in main hand
            	 }
            	 if (itemstack == player.getHeldItemOffhand()) {
            		 return EquipmentSlotType.OFFHAND.getSlotIndex(); //Choose oofhand slot if item is in offhand
            	 }
            	 if (itemstack.isItemEqual(stack)) {
            		 return nonnulllist.indexOf(itemstack); //Get stack closets to slot 0 if not in main or offhand
            	 }
             }
          }
       }
      return 0;
    }
    
    /**
     * More lightweight method of searching for an Item
     * @param player
     * @param stack
     * @return
     */
    public static OptionalInt findItem(PlayerEntity player, ItemStack stack) {
    	PlayerInventory inv = player.inventory;
	    for (int i = 0; i <= inv.getSizeInventory(); i++) {
	      if (inv.getStackInSlot(i).isItemEqual(stack))
	        return OptionalInt.of(i);
	    }
	    return OptionalInt.empty();
	  }
    
	/**
	 * Used to get the held itemstack or find an instance of the stack closest to slot 0
	 * @implNote Used for VM
	 * @param player
	 * @param stack
	 * @return
	 */
	public static ItemStack getHeldOrNearestStack(PlayerEntity player, ItemStack stack) {
		if (player.inventory.hasItemStack(stack)) {
			int index = findItem(player, stack).getAsInt();
			return isInMainHand(player, stack.getItem()) ? player.getHeldItemMainhand() : (isInOffHand(player, stack.getItem()) ? player.getHeldItemOffhand() : player.inventory.getStackInSlot(index));
		}
		return isInMainHand(player, stack.getItem()) ? player.getHeldItemMainhand() : player.getHeldItemOffhand();
	}
	
	/**
	 * Gets the Raytrace result of the entity's look vector
	 * @param entity
	 * @param distance
	 * @return
	 */
	public static RayTraceResult getPosLookingAt(Entity entity, double distance) {
	       Vec3d lookVec = entity.getLookVec();
	       for (int i = 0; i < distance * 2; i++) {
	           float scale = i / 2F;
	           Vec3d pos = entity.getPositionVector().add(0, entity.getEyeHeight(), 0).add(lookVec.scale(scale));
	           if (entity.world.getBlockState(new BlockPos(pos)).getCollisionShape(entity.world, new BlockPos(pos)) != VoxelShapes.empty() && !entity.world.isAirBlock(new BlockPos(pos))) {
	               return new BlockRayTraceResult(pos, Direction.getFacingFromVector(pos.x, pos.y, pos.z), new BlockPos(pos), false);
	           } else {
	               Vec3d min = pos.add(0.25F, 0.25F, 0.25F);
	               Vec3d max = pos.add(-0.25F, -0.25F, -0.25F);
	               for (Entity e : entity.world.getEntitiesWithinAABBExcludingEntity(entity, new AxisAlignedBB(min.x, min.y, min.z, max.x, max.y, max.z))) {
	                   return new EntityRayTraceResult(e);
	               }
	           }
	       }
	       return null;
	   }

}

