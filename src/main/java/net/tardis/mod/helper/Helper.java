package net.tardis.mod.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.advancements.Advancement;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.minecart.MinecartEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.FishingBobberEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.SPlayEntityEffectPacket;
import net.minecraft.network.play.server.SSetExperiencePacket;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.server.TicketType;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootParameterSets;
import net.minecraft.world.storage.loot.LootParameters;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants.Translations;
import net.tardis.mod.containers.BaseContainer;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.tileentities.ConsoleTile;

public class Helper {

    private static Random rand = new Random();

	public static String formatBlockPos(BlockPos pos) {
		return pos.getX() + ", " + pos.getY() + ", " + pos.getZ();
	}

	public static String formatDimName(DimensionType dim) {
		if(dim == null)
			return "UNKNOWN";
		return DimensionType.getKey(dim).getPath().trim().replace("	", "").replace("_", " ");
	}
	
	public static float getAngleFromFacing(Direction dir) {
		if(dir == Direction.NORTH)
			return 0F;
		else if(dir == Direction.EAST)
			return 90;
		else if(dir == Direction.SOUTH)
			return 180;
		return 270F;
	}
	
	/**
	 * Used to determine if an object is able to travel to the target dimension
	 * @implNote Usage: Config defined values
	 * @implSpec Will blacklist dimensions
	 * @param dimension
	 * @return Boolean value
	 */

	public static boolean canTravelToDimension(DimensionType dimension) {
		ResourceLocation key = DimensionType.getKey(dimension);
		if(key == null)
			return false;
		List<? extends String> blacklist = TConfig.CONFIG.blacklistedDims.get();
		for(String s : blacklist) {
			if(key.toString().contentEquals(s))
				return false;
		}
        return dimension.getModType() != TDimensions.TARDIS &&
        		dimension.getModType() != TDimensions.VORTEX &&
        		dimension != DimensionType.THE_END;
	}
	
	/**
	 * Used to determine if the VM is able to travel to the target dimension
	 * @implNote Usage: Config defined values
	 * @implSpec Will require a config option to blacklist dimensions
	 * @param dimension
	 * @return Boolean value
	 */
	public static boolean canVMTravelToDimension(DimensionType dimension) {
		if (TConfig.CONFIG.toggleVMWhitelistDims.get()) { //If using whitelist
			return TConfig.CONFIG.whitelistedVMDims.get().stream().anyMatch(dimName ->
				dimension.getRegistryName().toString().contentEquals(dimName));
		} //uses anyMatch to allow for VM to not be able be used in ANY player's tardis dimension, because every player's tardis name contains uuid which is different
		else if (!TConfig.CONFIG.toggleVMWhitelistDims.get()){ //If using blacklist
			return TConfig.CONFIG.blacklistedVMDims.get().stream().anyMatch(dimName ->
				!dimension.getRegistryName().toString().contains(dimName));
		}
		return dimension.getModType() != TDimensions.TARDIS || dimension.getModType() != TDimensions.VORTEX;
	}
	/**
	 * Determines if VM can be opened/used in a particular dimension. More lightweight method than the aboveW
	 * @param dimension
	 * @return
	 */
	public static boolean canVMBeUsedInDim(DimensionType dimension) {
		return dimension.getModType() != TDimensions.TARDIS || dimension.getModType() != TDimensions.VORTEX;
	}
	
	/**
	 * A more performance efficient version of the above to check if an object can be used in the current dimension
	 * @implNote Used for items/blocks that can only be used in the Tardis dimension
	 * @param dimension
	 * @return
	 */
	public static boolean isDimensionBlocked(DimensionType dimension) {
		return dimension.getModType() == TDimensions.TARDIS;
	}

	public static boolean isInBounds(int testX, int testY, int x, int y, int u, int v) {
		return (testX > x &&
				testX < u &&
				testY > y &&
				testY < v);
	}

	public static ResourceLocation getKeyFromDimType(DimensionType dimension) {
		ResourceLocation rl = DimensionType.getKey(dimension);
		return rl != null ? rl : new ResourceLocation("overworld");
	}
	
	/**
	 * Checks if Tardis owner is online in the dedicated server
	 * @param world
	 * @param type
	 * @return
	 */
	public static boolean isOwnerOn(World world, DimensionType type) {
		if(!world.isRemote) {
			return world.getServer().getPlayerList().getPlayerByUUID(getPlayerFromTARDIS(type)) != null;
		}
		return false;
	}
	
	public static UUID getPlayerFromTARDIS(DimensionType type) {
		ResourceLocation loc = DimensionType.getKey(type);
		return UUID.fromString(loc.getPath());
	}
	
	public static void teleportEntities(Entity e, ServerWorld world, double x, double y, double z, float yaw, float pitch) {
		if(e instanceof ServerPlayerEntity) {
			ServerPlayerEntity player = (ServerPlayerEntity)e;
			ChunkPos chunkpos = new ChunkPos(new BlockPos(x, y, z));
			world.getChunkProvider().registerTicket(TicketType.POST_TELEPORT, chunkpos, 1, player.getEntityId());
			if(player.world == world) {
				player.connection.setPlayerLocation(x, y, z, yaw, pitch);
			}
			if (player.isSleeping()) {
				player.wakeUpPlayer(true, true, false);
			}
			else {
				player.teleport(world, x, y, z, yaw, pitch);
				//Handle player active potion effects
				for(EffectInstance effectinstance : player.getActivePotionEffects()) {
					player.connection.sendPacket(new SPlayEntityEffectPacket(player.getEntityId(), effectinstance));
		         }
				//Handle player experience not getting received on client after interdimensional teleport
				player.connection.sendPacket(new SSetExperiencePacket(player.experience, player.experienceTotal, player.experienceLevel));
			}
		}
		else {
			//In in same dimension
			if(e.world == world) {
				e.setPosition(x, y, z);
				e.rotationYaw = yaw;
				e.rotationPitch = pitch;
			}
			else {
				//If interdimensional
				DimensionType targetDim = world.getDimension().getType();
				e.dimension = targetDim;
				Entity old = e;
				final ServerWorld sourceWorld = old.getServer().getWorld(old.world.getDimension().getType());
				old.detach(); //Dismounts from entities and dismounts other entities riding the entity
				Entity newE = e.getType().create(world);
				if (old instanceof FishingBobberEntity) { //Explicit handling of fishing rod because it doesn't have an entity type, hence it will cause an NPE
					FishingBobberEntity bobber = (FishingBobberEntity)old;
					ServerPlayerEntity player = (ServerPlayerEntity)bobber.getAngler();
					newE = new FishingBobberEntity(world,player, x, y, z); //This means bobber didn't "catch" anything and player's fishing rod won't lose durability
				}
				//Handle Minecarts
				if (old.isAlive() && old instanceof MinecartEntity) {
					old.remove(true); //equivalent of old.removed = true;
					old.changeDimension(targetDim);
					old.revive(); //equivalent of old.removed = false;
		        }
				if(old.world instanceof ServerWorld) {
					
					sourceWorld.removeEntity(old);
				}
				if (newE != null) { //Sanity check
					newE.copyDataFromOld(old);
					newE.setLocationAndAngles(x, y, z, yaw, pitch);
					world.func_217460_e(newE);
				}
				else {
					System.err.println("Error teleporting entity: " + old);
					System.err.println("Entity that was teleported: " + newE);
				}
				
			}
			//Handle Elytra flying
			 if (!(e instanceof LivingEntity) || !((LivingEntity)e).isElytraFlying()) {
		         e.setMotion(e.getMotion().mul(1.0D, 0.0D, 1.0D));
		         e.onGround = true;
		      }
		}
	}


    public static void dropEntityLoot(Entity target, PlayerEntity attacker) {
    	LivingEntity targeted = (LivingEntity) target;
    	ResourceLocation resourcelocation = targeted.getLootTableResourceLocation();
        LootTable loot_table = target.world.getServer().getLootTableManager().getLootTableFromLocation(resourcelocation);
        LootContext.Builder lootcontext$builder = getLootContextBuilder(true, DamageSource.GENERIC, targeted, attacker);
        LootContext ctx = lootcontext$builder.build(LootParameterSets.ENTITY);
        loot_table.generate(ctx).forEach(target::entityDropItem);
    }

    public static LootContext.Builder getLootContextBuilder(boolean p_213363_1_, DamageSource damageSourceIn, LivingEntity entity, PlayerEntity attacker) {
        LootContext.Builder lootcontext$builder = (new LootContext.Builder((ServerWorld) entity.world)).withRandom(rand).withParameter(LootParameters.THIS_ENTITY, entity).withParameter(LootParameters.POSITION, new BlockPos(entity)).withParameter(LootParameters.DAMAGE_SOURCE, damageSourceIn).withNullableParameter(LootParameters.KILLER_ENTITY, damageSourceIn.getTrueSource()).withNullableParameter(LootParameters.DIRECT_KILLER_ENTITY, damageSourceIn.getImmediateSource());
         if (p_213363_1_ && entity.getAttackingEntity() != null) {
        	 attacker = (PlayerEntity) entity.getAttackingEntity();
            lootcontext$builder = lootcontext$builder.withParameter(LootParameters.LAST_DAMAGE_PLAYER, attacker).withLuck(attacker.getLuck());
         }
         return lootcontext$builder;
    }
    
    public static void doIfAdvancementPresent(String name, ServerPlayerEntity ent, Runnable run) {
    	Advancement adv = ent.getServer().getAdvancementManager().getAdvancement(new ResourceLocation(Tardis.MODID, name));
    	if(adv != null)
    		if(ent.getAdvancements().getProgress(adv).isDone()) {
    			run.run();
    		}
    }

	public static boolean unlockInterior(ConsoleTile console, @Nullable ServerPlayerEntity player, ConsoleRoom room) {
		if(!console.getUnlockedInteriors().contains(room)) {
			console.getUnlockedInteriors().add(room);
			if(player != null) {
				player.sendStatusMessage(new TranslationTextComponent(Translations.UNLOCKED_INTERIOR, room.getDisplayName()), true);
			}
			return true;
		}
		return false;
	}
	
	public static boolean unlockExterior(ConsoleTile console, @Nullable ServerPlayerEntity player, IExterior ext) {
		if(!console.getExteriors().contains(ext)) {
			console.getExteriors().add(ext);
			if(player != null) {
				player.sendStatusMessage(new TranslationTextComponent("status.tardis.exterior.unlock", ext.getDisplayName().getFormattedText()), true);
			}
			return true;
		}
		return false;
	}

	/**
	 * Converts from Direction to Rotation, assuming the default facing is north
	 * @param dir
	 * @return
	 */
	public static Rotation getRotationFromDirection(Direction dir) {
		switch(dir) {
		case NORTH: return Rotation.NONE;
		case EAST: return Rotation.CLOCKWISE_90;
		case SOUTH: return Rotation.CLOCKWISE_180;
		case WEST: return Rotation.COUNTERCLOCKWISE_90;
		default: return Rotation.NONE;
		}
	}
	
	/**
	 * Only supports Horrizontal directions, rotates around 0, 0, 0 
	 * @param pos - The position to rotate
	 * @param dir - The direction to rotate it, assumes facing north by default
	 * @return - rotated block pos
	 */
	public static BlockPos rotateBlockPos(BlockPos pos, Direction dir) {
		switch(dir) {
			case NORTH: return pos;
			case EAST: return new BlockPos(-pos.getZ(), pos.getY(), pos.getX());
			case SOUTH: return new BlockPos(-pos.getX(), pos.getY(), -pos.getZ());
			case WEST: return new BlockPos(pos.getZ(), pos.getY(), -pos.getX());
			default: return pos;
		}
	}
	
	public static Effect getEffectFromRL(ResourceLocation loc) {
		return ForgeRegistries.POTIONS.getValue(loc);
	}
	
	public static ResourceLocation getKeyFromEffect(Effect effect) {
		return ForgeRegistries.POTIONS.getKey(effect);
	}

	public static void addPlayerInvContainer(BaseContainer container, PlayerInventory player, int x, int y) {
		
		//Player Main
		for(int i = 0; i < player.mainInventory.size() - 9; ++i) {
			container.addSlot(new Slot(player, i + 9, x + 8 + (i % 9) * 18, 86 + y + (i / 9) * 18));
		}
		
		//hotbar
		for(int i = 0; i < 9; ++i) {
			container.addSlot(new Slot(player, i, 8 + x + (i * 18), y + 144));
		}
	}
	
	public static void dropInventoryItems(World worldIn, BlockPos pos, IItemHandler inventory)
    {
        for (int i = 0; i < inventory.getSlots(); ++i)
        {
            ItemStack itemstack = inventory.getStackInSlot(i);

            if (itemstack.getCount() > 0)
            {
                InventoryHelper.spawnItemStack(worldIn, (double) pos.getX(), (double) pos.getY(), (double) pos.getZ(), itemstack);
            }
        }
    }

	public static List<DimensionType> getAllValidDimensionTypes() {
		
		List<DimensionType> dims = new ArrayList<DimensionType>();
		
		for(DimensionType type : DimensionType.getAll()) {
			if(Helper.canTravelToDimension(type))
				dims.add(type);
		}
		
		return dims;
	}
}
