package net.tardis.mod.compat.jei;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.ITickTimer;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.items.TItems;

public class AlembicRecipeCategory implements IRecipeCategory<AlembicRecipe> {

	public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "alembic");
	private IDrawable background;
	private IDrawable icon;
	private ITickTimer ticker;
	
	public AlembicRecipeCategory(IGuiHelper help){
		background = help.createDrawable(new ResourceLocation(Tardis.MODID, "textures/gui/containers/alembic.png"),
				31, 5, 180, 121);
		icon = help.createDrawableIngredient(new ItemStack(TBlocks.alembic));
		ticker = help.createTickTimer(60, 60, false);
	}
	
	@Override
	public ResourceLocation getUid() {
		return NAME;
	}

	@Override
	public Class<AlembicRecipe> getRecipeClass() {
		return AlembicRecipe.class;
	}

	@Override
	public String getTitle() {
		return "Alembic";
	}

	@Override
	public IDrawable getBackground() {
		return this.background;
	}

	@Override
	public IDrawable getIcon() {
		return this.icon;
	}

	@Override
	public void setIngredients(AlembicRecipe recipe, IIngredients ingre) {
		ingre.setInput(VanillaTypes.ITEM, new ItemStack(TItems.CINNABAR));
		ingre.setOutput(VanillaTypes.ITEM, new ItemStack(TItems.MERCURY_BOTTLE));
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, AlembicRecipe recipe, IIngredients ingre) {
		recipeLayout.getItemStacks().init(1, true, 54, 46);
		recipeLayout.getItemStacks().set(1, new ItemStack(TItems.CINNABAR));

		recipeLayout.getItemStacks().init(2, false, 25, 17);
		recipeLayout.getItemStacks().set(2, new ItemStack(Items.WATER_BUCKET));
		
		recipeLayout.getItemStacks().init(3, false, 25, 83);
		recipeLayout.getItemStacks().set(3, new ItemStack(Items.BUCKET));
		
		recipeLayout.getItemStacks().init(4, false, 54, 90);
		recipeLayout.getItemStacks().set(4, new ItemStack(Items.COAL));
		
		recipeLayout.getItemStacks().init(5, false, 141, 18);
		recipeLayout.getItemStacks().set(5, new ItemStack(Items.GLASS_BOTTLE));
		
		recipeLayout.getItemStacks().init(6, false, 141, 81);
		recipeLayout.getItemStacks().set(6, new ItemStack(TItems.MERCURY_BOTTLE));
	}

}
