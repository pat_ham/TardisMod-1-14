package net.tardis.mod.compat.jei;

import java.util.ArrayList;
import java.util.List;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.recipe.WeldRecipe;

public class WeldRecipeCategory implements IRecipeCategory<WeldRecipe> {

	public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "qunatiscope_weld");
	private IDrawable background;
	private IDrawable icon;
	
	public WeldRecipeCategory(IGuiHelper gui) {
		background = gui.createDrawable(new ResourceLocation(Tardis.MODID, "textures/gui/containers/quantiscope/weld.png"), 41, 5, 170, 121);
		icon = gui.createDrawableIngredient(new ItemStack(TBlocks.quantiscope_brass));
	}

	@Override
	public ResourceLocation getUid() {
		return NAME;
	}

	@Override
	public Class<WeldRecipe> getRecipeClass() {
		return WeldRecipe.class;
	}

	@Override
	public String getTitle() {
		return "Quantiscope Welding";
	}

	@Override
	public IDrawable getBackground() {
		return this.background;
	}

	@Override
	public IDrawable getIcon() {
		return this.icon;
	}

	@Override
	public void setIngredients(WeldRecipe recipe, IIngredients ingredients) {
		List<ItemStack> ins = new ArrayList<ItemStack>();
		for(Item it : recipe.getInputs()) {
			ins.add(new ItemStack(it));
		}
		ingredients.setInputs(VanillaTypes.ITEM, ins);
		ingredients.setOutput(VanillaTypes.ITEM, new ItemStack(recipe.getOutput()));
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, WeldRecipe recipe, IIngredients ingredients) {
		
		ItemStack[] item = new ItemStack[5];
		for(int i = 0; i < item.length; ++i) {
			if(i < recipe.getInputs().size())
				item[i] = new ItemStack(recipe.getInputs().get(i));
			else item[i] = ItemStack.EMPTY;
		}
		
		//Inputs
		recipeLayout.getItemStacks().init(0, true, 18, 18);
		recipeLayout.getItemStacks().set(0, item[0]);
		
		addSlot(recipeLayout, 0, 18, 18, item[0]);
		addSlot(recipeLayout, 1, 44, 18, item[1]);
		addSlot(recipeLayout, 2, 18, 44, item[2]);
		addSlot(recipeLayout, 3, 18, 69, item[3]);
		addSlot(recipeLayout, 4, 44, 69, item[4]);
		
		//Repair
		if(recipe.isRepair()) {
			recipeLayout.getItemStacks().init(5, true, 56, 44);
			ItemStack repair = new ItemStack(recipe.getOutput());
			repair.setDamage(repair.getMaxDamage() - 1);
			recipeLayout.getItemStacks().set(5, repair);
		}
		
		recipeLayout.getItemStacks().init(6, true, 131, 44);
		recipeLayout.getItemStacks().set(6, new ItemStack(recipe.getOutput()));
	}
	
	public static void addSlot(IRecipeLayout layout, int id, int x, int y, ItemStack stack) {
		if(!stack.isEmpty()) {
			layout.getItemStacks().init(id, true, x, y);
			layout.getItemStacks().set(id, stack);
		}
	}
}
