package net.tardis.mod.compat.jei;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.compat.jei.ARSRecipeCategory.ARSRecipe;

public class ARSRecipeCategory implements IRecipeCategory<ARSRecipe> {

	public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "ars");
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/jei/ars.png");
	private IDrawable background;
	private IDrawable icon;
	
	public ARSRecipeCategory(IGuiHelper gui) {
		background = gui.createDrawable(TEXTURE, 0, 0, 64, 64);
		icon = gui.createDrawableIngredient(new ItemStack(TBlocks.ars_egg));
	}
	
	@Override
	public ResourceLocation getUid() {
		return NAME;
	}

	@Override
	public Class<? extends ARSRecipe> getRecipeClass() {
		return ARSRecipe.class;
	}

	@Override
	public String getTitle() {
		return "ARS System";
	}

	@Override
	public IDrawable getBackground() {
		return background;
	}

	@Override
	public IDrawable getIcon() {
		return icon;
	}

	@Override
	public void setIngredients(ARSRecipe rec, IIngredients ingre) {
		ingre.setOutput(VanillaTypes.ITEM, rec.getOutput());
	}

	@Override
	public void setRecipe(IRecipeLayout recLay, ARSRecipe recipe, IIngredients ingredients) {
		recLay.getItemStacks().init(0, false, 21, 22);
		recLay.getItemStacks().set(0, recipe.getOutput());
	}
	
	public static class ARSRecipe{
		
		private ItemStack output;
		
		public ARSRecipe(ItemStack output) {
			this.output = output;
		}
		
		public ItemStack getOutput() {
			return this.output;
		}
	}
}
