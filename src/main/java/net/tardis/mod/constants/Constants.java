package net.tardis.mod.constants;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;

public class Constants {

    public static float halfPI = 1.57079633f;

    public static class Gui {
    	
		public static final int MONITOR_MAIN_STEAM = 1;
		public static final int MONITOR_ECHANGE = 2;
		
		public static final int NONE = 3;
		public static final int MONITOR_EDIT_INTERIOR = 4;
		
		public static final int VORTEX_MAIN = 5;
		public static final int VORTEX_TELE = 6;

		public static final int MONITOR_MAIN_EYE = 7;
		public static final int MONITOR_MAIN_GALVANIC = 8;
		
		public static final int MANUAL = 9;
		
		public static final int ARS_EGG = 10;
		
		public static final int TELEPATHIC = 11;
		
		public static final int MONITOR_MAIN_RCA = 12;
		public static final int ARS_TABLET = 13;
		public static final int COMMUNICATOR = 14;
		
	}
	
	public static class Protocols{
		public static final ResourceLocation EDIT_INTERIOR = new ResourceLocation(Tardis.MODID, "edit_interior");
		public static final ResourceLocation LIFE_SCAN = new ResourceLocation(Tardis.MODID, "life_scan");
		public static final ResourceLocation TOGGLE_ALARM = new ResourceLocation(Tardis.MODID, "toggle_alarm");
    }

    public static class DalekTypes {
		public static final ResourceLocation DALEK_TIMEWAR = new ResourceLocation(Tardis.MODID, "time_war");
		public static final ResourceLocation DALEK_SW = new ResourceLocation(Tardis.MODID, "special_weapons");
		public static final ResourceLocation DALEK_CLASSIC = new ResourceLocation(Tardis.MODID, "classic");
		public static final ResourceLocation DALEK_SEC = new ResourceLocation(Tardis.MODID, "sec");
		public static final ResourceLocation DALEK_SUPREME = new ResourceLocation(Tardis.MODID, "supreme");
        public static final ResourceLocation DALEK_IMPERIAL = new ResourceLocation(Tardis.MODID, "imperial");
    }

    public static class Translations{
        public static final TranslationTextComponent NO_USE_OUTSIDE_TARDIS = new TranslationTextComponent("error." + Tardis.MODID + ".use.outside_tardis");
        public static final TranslationTextComponent CANT_USE_IN_TARDIS = new TranslationTextComponent("error." + Tardis.MODID + ".use.in_tardis");
        public static final TranslationTextComponent CANT_USE_IN_DIM = new TranslationTextComponent("error." + Tardis.MODID + ".use.in_dim");
        public static final String NOT_ENOUGH_ARTRON = "status." + Tardis.MODID + ".not_enough_artron";
        public static final TranslationTextComponent OUTSIDE_BORDER = new TranslationTextComponent("error.tardis.outside_border");
        public static final TranslationTextComponent CORRIDOR_BLOCKED = new TranslationTextComponent("error.tardis.corridor.blocked");
        
        public static final String NO_COMPONENT = "error." + Tardis.MODID + ".no_subsystem";
        public static final TranslationTextComponent NOT_ADMIN = new TranslationTextComponent("error.tardis.not_admin");
		public static final String UNLOCKED_INTERIOR = "status.tardis.unlocked_interior";
		
		public static final TranslationTextComponent TOOLTIP_HOLD_SHIFT = new TranslationTextComponent("item.info.shift");
    }

    public static class Message {
        public static final String NO_PERMISSION = "You do not have the permission to do that.";
        public static final String NO_TARDIS = "No TARDIS Found for this player. Is the Tardis Console TileEntity invalid?";
    }
}
