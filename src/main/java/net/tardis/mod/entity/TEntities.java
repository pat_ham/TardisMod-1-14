package net.tardis.mod.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EntityType.Builder;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.projectiles.LaserEntity;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TEntities {
	
	public static final EntityType<ControlEntity> CONTROL = registerStatic(ControlEntity::new, ControlEntity::new, EntityClassification.MISC, 1F, 1F, "control");
	public static final EntityType<DoorEntity> DOOR = registerStatic(DoorEntity::new, DoorEntity::new, EntityClassification.MISC, 1F, 2F, "interior_door");
	public static final EntityType<ChairEntity> CHAIR = registerStatic(ChairEntity::new, ChairEntity::new, EntityClassification.MISC, 1F, 1F, "chair");
	public static final EntityType<TardisEntity> TARDIS = registerBase(TardisEntity::new, TardisEntity::new, EntityClassification.MISC, 1F, 2F, 64, 1, true, "tardis");
	public static final EntityType<HoloPilotEntity> HOLO_PILOT = registerStatic(HoloPilotEntity::new, HoloPilotEntity::new, EntityClassification.AMBIENT, 0.75F, 1.75F, "holo_pilot");
	public static final EntityType<SecDroidEntity> SECURITY_DROID = registerBase(SecDroidEntity::new, SecDroidEntity::new, EntityClassification.CREATURE, 0.8F, 0.8F, 64, 1, false, "sec_drone");

    public static final EntityType<DalekEntity> DALEK = registerFireResistantMob(DalekEntity::new, DalekEntity::new, EntityClassification.MONSTER, 1F, 1.75F, "dalek", false);
    public static final EntityType<LaserEntity> LASER = registerMob(LaserEntity::new, LaserEntity::new, EntityClassification.MISC, 0.5F, 0.5F, "laser", true);
    public static final EntityType<BessieEntity> BESSIE = registerFireResistantMob(BessieEntity::new, BessieEntity::new, EntityClassification.MISC, 1.75F, 1.75F, "bessie", false);

	@SubscribeEvent
	public static void registerEntities(RegistryEvent.Register<EntityType<?>> event) {
		event.getRegistry().registerAll(
				CONTROL,
				DOOR,
				CHAIR,
				TARDIS,
				HOLO_PILOT,
				SECURITY_DROID,
				
				//Mobs
				DALEK,
				LASER,
				BESSIE
		);
	}
	
	public static <T extends Entity> EntityType<T> registerBase(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, int trackingRange, int updateFreq, boolean sendUpdate, String name){
		ResourceLocation loc = new ResourceLocation(Tardis.MODID, name);
		Builder<T> builder = EntityType.Builder.create(factory, classification);
		builder.setShouldReceiveVelocityUpdates(sendUpdate);
		builder.setTrackingRange(trackingRange);
		builder.setUpdateInterval(updateFreq);
		builder.size(width, height);
		builder.setCustomClientFactory((spawnEntity, world) -> client.spawn(world));
		EntityType<T> type = builder.build(loc.toString());
		type.setRegistryName(loc);
		return type;
	}
	
	public static <T extends Entity> EntityType<T> registerFireResistantBase(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, int trackingRange, int updateFreq, boolean sendUpdate, String name){
		ResourceLocation loc = new ResourceLocation(Tardis.MODID, name);
		Builder<T> builder = EntityType.Builder.create(factory, classification);
		builder.setShouldReceiveVelocityUpdates(sendUpdate);
		builder.setTrackingRange(trackingRange);
		builder.setUpdateInterval(updateFreq);
		builder.size(width, height);
		builder.setCustomClientFactory((spawnEntity, world) -> client.spawn(world));
		builder.immuneToFire();
		EntityType<T> type = builder.build(loc.toString());
		type.setRegistryName(loc);
		return type;
	}
	
	public static <T extends Entity> EntityType<T> registerStatic(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, String name){
		return TEntities.registerBase(factory, client, classification, width, height, 64, 40, false, name);
	}

    public static <T extends Entity> EntityType<T> registerMob(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, String name, boolean velocity) {
		return TEntities.registerBase(factory, client, classification, width, height, 80, 3, velocity, name);
    }
    
    public static <T extends Entity> EntityType<T> registerFireResistantMob(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, String name, boolean velocity) {
		return TEntities.registerFireResistantBase(factory, client, classification, width, height, 80, 3, velocity, name);
    }


    public static <T extends Entity> EntityType<T> registerMob(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, String name, boolean velocity, int trackingRange, int updateFreq) {
        return TEntities.registerBase(factory, client, classification, width, height, trackingRange, updateFreq, velocity, name);
	}

	public interface IClientSpawner<T> {
		T spawn(World world);
	}

}
