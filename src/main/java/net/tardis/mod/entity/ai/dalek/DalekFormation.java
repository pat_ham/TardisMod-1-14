package net.tardis.mod.entity.ai.dalek;

import java.util.ArrayList;

import net.minecraft.util.math.Vec3d;
import net.tardis.mod.entity.DalekEntity;

/**
 * Singleton - Spectre0987
 * @param command
 */
public class DalekFormation {
	
	private ArrayList<Vec3d> offsets = new ArrayList<Vec3d>();
	
	public DalekFormation(ArrayList<Vec3d> offsets) {
		offsets.add(0, new Vec3d(0, 0, 0));
		this.offsets = offsets;
	}
	
	public Vec3d getPlace(DalekEntity commander, DalekEntity drone) {
		int index = 0;
		for(DalekEntity dal : commander.getDrones()) {
			if(dal.getUniqueID().equals(drone.getUniqueID())) {
				if(offsets.size() > index)
					return offsets.get(index);
			}
			++index;
		}
		return null;
	}

}
