package net.tardis.mod.entity.ai.dalek;

import java.util.EnumSet;

import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.entity.DalekEntity;

public class DalekFollowLeaderGoal extends Goal{

	private DalekEntity dalek;
	
	public DalekFollowLeaderGoal(DalekEntity dalek) {
		this.dalek = dalek;
		this.setMutexFlags(EnumSet.of(Flag.MOVE));
	}
	
	@Override
	public boolean shouldExecute() {
		return !dalek.isCommander() && dalek.getCommander() != null;
	}

	@Override
	public boolean shouldContinueExecuting() {
		return this.shouldExecute();
	}

	@Override
	public void tick() {
		super.tick();
		if(dalek.getCommander() != null && !dalek.getCommander().removed && dalek.getCommander() != dalek) {
			DalekFormation form = dalek.getCommander().getFormation();
			if(form != null) {
				Vec3d pos = form.getPlace(dalek.getCommander(), dalek);
				if(pos != null) {
					double xRot = Math.sin(Math.toRadians(dalek.getCommander().rotationYaw)) * pos.x;
					double zRot = -Math.cos(Math.toRadians(dalek.getCommander().rotationYaw) * pos.z);
					double x = dalek.getCommander().posX, y = dalek.getCommander().posY, z = dalek.getCommander().posZ;
					
					if(dalek.getPositionVec().distanceTo(new Vec3d(xRot + x, y, zRot + z)) > 0.5)
						dalek.getNavigator().tryMoveToXYZ(xRot + x, y, zRot + z, 1.2D);
				}
			}
		}
	}

}
