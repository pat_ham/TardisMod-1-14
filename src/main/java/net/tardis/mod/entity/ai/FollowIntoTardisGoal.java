package net.tardis.mod.entity.ai;

import java.util.EnumSet;
import java.util.Optional;

import javax.annotation.Nonnull;

import com.google.common.collect.Lists;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class FollowIntoTardisGoal extends Goal{

	private CreatureEntity entity;
	private BlockPos target = BlockPos.ZERO;
	private double speed;
	private int timeout = 300;

	public FollowIntoTardisGoal(CreatureEntity entity, double speed) {
		this.entity = entity;
		this.speed = speed;
		this.setMutexFlags(EnumSet.of(Flag.MOVE, Flag.LOOK));
	}
	
	@Override
	public boolean shouldExecute() {
		return target != BlockPos.ZERO && this.timeout > 0;
	}

	@Override
	public boolean shouldContinueExecuting() {
		return this.shouldExecute();
	}

	@Override
	public void startExecuting() {
		super.startExecuting();
		this.timeout = 300;
	}

	@Override
	public void tick() {
		super.tick();
		
		if(entity.getNavigator().noPath()) {
			entity.getNavigator().tryMoveToXYZ(this.target.getX() + 0.5, this.target.getY() + 1, this.target.getZ() + 0.5, 1.2);
		}
		
		if(this.entity.getPosition().withinDistance(this.target, 2)) {
			this.target = BlockPos.ZERO;
			
			Optional<TileEntity> optTile = entity.world.loadedTileEntityList.stream().filter(tile -> tile instanceof ExteriorTile && tile.getPos().withinDistance(entity.getPosition(), 3)).findFirst();
			entity.world.getServer().enqueue(new TickDelayedTask(0, () -> optTile.ifPresent(tile -> ((ExteriorTile)tile).transferEntities(Lists.newArrayList(entity)))));
		}
		
		if(this.timeout > 0)
			--this.timeout;
	}
	
	@Override
	public void resetTask() {
		super.resetTask();
		this.target = BlockPos.ZERO;
		this.timeout = 300;
	}

	public void setTarget(@Nonnull BlockPos pos) {
		this.target = pos;
	}

	
}
