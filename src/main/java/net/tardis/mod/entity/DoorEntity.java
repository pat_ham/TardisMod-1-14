package net.tardis.mod.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class DoorEntity extends Entity{

	public static DataParameter<Integer> STATE = EntityDataManager.createKey(DoorEntity.class, DataSerializers.VARINT);
	public IDoorType doorType = EnumDoorType.STEAM;
	private List<UUID> teleportImmune = new ArrayList<>();
	private boolean isLocked = false;
	private float health = 10;
	
	public DoorEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
	}
	
	public DoorEntity(World worldIn) {
		this(TEntities.DOOR, worldIn);
	}

	@Override
	protected void registerData() {
		this.dataManager.register(STATE, EnumDoorState.CLOSED.ordinal());
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.getDataManager().set(STATE, compound.getInt("door_state"));
		this.isLocked = compound.getBoolean("locked");
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.putInt("door_state", this.dataManager.get(STATE));
		compound.putBoolean("locked", this.isLocked);
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}
	
	public void setOpenState(EnumDoorState open) {
		this.dataManager.set(STATE, open.ordinal());
		if(open != EnumDoorState.CLOSED)
			this.setLocked(false);
	}

	public EnumDoorState getOpenState() {
		return EnumDoorState.values()[this.dataManager.get(STATE)];
	}
	
	public void openOther() {
		if(!world.isRemote) {
			ConsoleTile console = this.getConsole();
			if(console != null) {
				ExteriorTile ext = console.getExterior().getExterior(console);
				if(ext != null) {
					ext.setDoorState(this.getOpenState());
					if(this.getOpenState() != EnumDoorState.CLOSED) {
						ext.setLocked(false);
					}
				}
			}
		}
	}
	
	@Override
	public void onKillCommand() {
		return;
	}
	
	@Override
	public ItemStack getPickedResult(RayTraceResult target) {
		return new ItemStack(TItems.INT_DOOR);
	}

	@Override
	public void tick() {
		super.tick();
		if(!world.isRemote) {
			if(this.getOpenState() != EnumDoorState.CLOSED && this.ticksExisted > 5) {
				List<Entity> entities = world.getEntitiesWithinAABB(Entity.class, this.getBoundingBox());
				this.teleportEntity(entities);
				List<UUID> list = new ArrayList<UUID>();
				for(Entity e : entities) {
					if(this.teleportImmune.contains(e.getUniqueID()))
						list.add(e.getUniqueID());
				}
				this.teleportImmune = list;
			}
		}
		if(this.getConsole() != null) {
			this.doorType = this.getConsole().getExterior().getDoorType();
		}
		
		if(this.getConsole() != null && this.getConsole().isInFlight() && this.getOpenState() != EnumDoorState.CLOSED)
			this.suckIntoVoid();
	}
	
	public void addEntityToTeleportedList(UUID id) {
		this.teleportImmune.add(id);
	}

	private void teleportEntity(List<Entity> entity) {
		ConsoleTile console = this.getConsole();
		if(console == null)
			return;
		
		world.getServer().enqueue(new TickDelayedTask(1, () -> {
			
			for(Entity e : entity) {
				if(!(e instanceof DoorEntity)) {
					
					if(console.isInFlight()) {
						
						if(e instanceof PlayerEntity && console.getOwner().equals(e.getUniqueID())) {
							if(console.getEmotionHandler().getLoyalty() > 50) {
								this.setOpenState(EnumDoorState.CLOSED);
								return;
							}
							else if(console.getEmotionHandler().getLoyalty() > 25) {
								console.initLand();
							}
						}
						
						e.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
							BlockPos diff = console.getDestination().subtract(console.getLocation());
							double scale = console.flightTicks / console.getMaxFlightTime();
							DimensionType type = scale > 0.5 ? console.getDestinationDimension() : console.getDimension();
							cap.setDestination(new SpaceTimeCoord(type, world.getServer().getWorld(type).getHeight(Type.WORLD_SURFACE, console.getLocation().add(new BlockPos(diff.getX() * scale, diff.getY() * scale, diff.getZ() * scale)))));
							Helper.teleportEntities(e, world.getServer().getWorld(TDimensions.VORTEX_TYPE), 0, 128, 0, e.rotationYaw, e.rotationPitch);
						});
						return;
					}
					
					if(this.teleportImmune.contains(e.getUniqueID()))
						break;
					
					ExteriorTile ext = console.getExterior().getExterior(console);
					if(ext != null)
						ext.addTeleportedEntity(e.getUniqueID());
					
					Direction dir = console.getExteriorDirection();
					BlockPos pos = console.getLocation().offset(dir);
					e.rotationYaw = Helper.getAngleFromFacing(dir.getOpposite());
					Helper.teleportEntities(e, world.getServer().getWorld(console.getDimension()), pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5, Helper.getAngleFromFacing(dir.getOpposite()), 0);
				}
			}
			
		}));
	}
	
	public void suckIntoVoid() {
		for(LivingEntity e : world.getEntitiesWithinAABB(LivingEntity.class, this.getBoundingBox().grow(40))) {
			if(e.canEntityBeSeen(this)) {
				e.stopRiding();
				Vec3d motion = this.getPositionVec().subtract(e.getPositionVec()).normalize().scale(0.1);
				e.setMotion(e.getMotion().add(motion));
			}
		}
	}
	
	@Override
	public boolean processInitialInteract(PlayerEntity player, Hand hand) {
		if(!world.isRemote) {
			ExteriorTile ext = this.getConsole().getExterior().getExterior(this.getConsole());
			if(player.isSneaking()) {
				if(this.isLocked) {
					this.isLocked = false;
					world.playSound(null, this.getPosition(), TSounds.DOOR_UNLOCK, SoundCategory.BLOCKS, 1F, 1F);
					this.playSoundAtExterior(ext, TSounds.DOOR_UNLOCK, SoundCategory.BLOCKS, 1F);
                    if (ext != null)
                        ext.setLocked(false);
					player.sendStatusMessage(ExteriorBlock.UNLOCKED, true);
					return true;
				}
				else {
					this.isLocked = true;
					this.setOpenState(EnumDoorState.CLOSED);
					this.openOther();
					if(ext != null) {
						ext.setLocked(isLocked);
					}
					world.playSound(null, this.getPosition(), TSounds.DOOR_LOCK, SoundCategory.BLOCKS, 1F, 1F);
					this.playSoundAtExterior(ext, TSounds.DOOR_LOCK, SoundCategory.BLOCKS, 1F);
					player.sendStatusMessage(ExteriorBlock.LOCKED, true);
					return true;
				}
			}
			
			if(this.isLocked) {
				player.sendStatusMessage(ExteriorBlock.LOCKED, true);
				return true;
			}
			
			EnumDoorState[] valid = this.doorType.getValidStates();
			int index = this.getOpenState().ordinal() + 1;
			if(index >= valid.length)
				index = 0;
			this.setOpenState(valid[index]);
			this.openOther();
			if (this.getOpenState() == EnumDoorState.CLOSED) {
				world.playSound(null, player.getPosition(), TSounds.DOOR_CLOSE, SoundCategory.BLOCKS, 1F, 1F);
				this.playSoundAtExterior(ext, TSounds.DOOR_CLOSE, SoundCategory.BLOCKS, 1F);
			}
			else {
				world.playSound(null, player.getPosition(), TSounds.DOOR_OPEN, SoundCategory.BLOCKS, 1F, 1F);
				this.playSoundAtExterior(ext, TSounds.DOOR_OPEN, SoundCategory.BLOCKS, 1F);
			}
		}
		return true;
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return false;
	}
	@Override
	public boolean attackEntityFrom(DamageSource source, float amount) {
		if(!world.isRemote) {
			if (source.getTrueSource() instanceof PlayerEntity) {
				PlayerEntity playerIn = (PlayerEntity) source.getTrueSource();
				if (playerIn.abilities.isCreativeMode) {
					this.health = 0;
				}
				else{
					this.health -= amount;
				}
			}
			if(this.health <= 0) {
				ItemEntity entity = new ItemEntity(world, posX, posY, posZ, this.getPickedResult(null));
				world.addEntity(entity);
				remove();
			}
			this.world.playSound(null, getPosition(), SoundEvents.ITEM_SHIELD_BLOCK, SoundCategory.NEUTRAL, 1F, 1F);
		}
		return true;
	}
	
	@Nullable
	public ConsoleTile getConsole() {
		TileEntity te = this.world.getTileEntity(TardisHelper.TARDIS_POS);
		if(te instanceof ConsoleTile)
			return (ConsoleTile) te;
		return null;
	}
	
	public void setLocked(boolean locked) {
		this.isLocked = locked;
	}
	
	public boolean isLocked() {
		return this.isLocked;
	}
	
	public void playSoundAtExterior(@Nullable ExteriorTile tile, SoundEvent event, SoundCategory cat, float vol) {
		if(tile != null && tile.hasWorld())
			tile.getWorld().playSound(null, tile.getPos(), event, cat, vol, 1F);
	}
	
	public void updateOther() {
		ConsoleTile tile = this.getConsole();
		if(tile != null) {
			ExteriorTile ext = tile.getExterior().getExterior(tile);
			if(ext != null) {
				ext.setDoorState(this.getOpenState());
				ext.setLocked(this.isLocked);
				ext.updateClient();
			}
		}
	}
	
}
