package net.tardis.mod.entity;

import javax.annotation.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class TardisEntity extends Entity{

	public static final DataParameter<String> EXTERIOR = EntityDataManager.createKey(TardisEntity.class, DataSerializers.STRING);
	public static final DataParameter<CompoundNBT> EXTERIOR_DATA = EntityDataManager.createKey(TardisEntity.class, DataSerializers.COMPOUND_NBT);
	private ConsoleTile console;
	private int sneakTicks = 0;
    private IExterior exterior;
    
    private boolean hasLanded = false;
	
    //Readonly
    private ExteriorTile tile;
    
	public TardisEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
	}
	
	public TardisEntity(World worldIn) {
		this(TEntities.TARDIS, worldIn);
	}

	@Override
	protected void registerData() {
		this.getDataManager().register(EXTERIOR, ExteriorRegistry.TRUNK.getRegistryName().toString());
		this.getDataManager().register(EXTERIOR_DATA, new CompoundNBT());
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.getDataManager().set(EXTERIOR, compound.getString("exterior"));
		
		if(compound.contains("exterior_data")) {
			CompoundNBT tag = compound.getCompound("exterior_data");
			
			this.readExteriorFromData(tag);
		}
		
		if(!world.isRemote && compound.contains("interior_dimension")) {
			TardisHelper.getConsole(world.getServer(), DimensionType.byName(new ResourceLocation(compound.getString("interior_dimension"))))
				.ifPresent(console -> this.console = console);
		}
	}
	
	public void readExteriorFromData(CompoundNBT tag) {
		TileEntityType<?> type = ForgeRegistries.TILE_ENTITIES.getValue(new ResourceLocation(tag.getString("id")));
		if(type != null) {
			this.tile = (ExteriorTile) type.create();
			tile.deserializeNBT(tag);
		}
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.putString("exterior", this.getDataManager().get(EXTERIOR));
		
		if(tile != null)
			compound.put("exterior_data", tile.serializeNBT());
		
		if(this.console != null)
			compound.putString("interior_dimension", DimensionType.getKey(this.console.getWorld().dimension.getType()).toString());
	}

    @Override
    public void onRemovedFromWorld() {
        super.onRemovedFromWorld();
        if (!world.isRemote && !hasLanded)
            land();
    }

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void tick() {
		super.tick();
		
		this.move(MoverType.SELF, this.getMotion());
		if(!this.hasNoGravity())
			this.setMotion(this.getMotion().add(0, -0.08, 0));
		
		if(!world.isRemote) {
			
			if(this.getConsole() != null && world.getGameTime() % 20 == 0) {
				console.setLocation(world.getDimension().getType(), this.getPosition());
			}
			
			//Damage
			if(this.getPassengers().isEmpty()) {
				boolean hasWackeed = false;
				for(Entity ent : world.getEntitiesWithinAABB(Entity.class, this.getBoundingBox())) {
					ent.attackEntityFrom(DamageSource.ANVIL, 5);
				}
				if(hasWackeed)
					world.playSound(null, getPosition(), SoundEvents.ITEM_SHIELD_BLOCK, SoundCategory.NEUTRAL, 1F, 1F);
			}
			
			if(!world.getBlockState(this.getPosition().down()).isAir())
				land();
		}
	}
	
	@Override
	public void updateRidden() {
		super.updateRidden();
		/*this.prevPosX = this.posX;
		this.prevPosY = this.posY;
		this.prevPosZ = this.posZ;
		
		this.posX = this.getRidingEntity().posX;
		this.posY = this.getRidingEntity().posY;
		this.posZ = this.getRidingEntity().posZ;*/

        if (this.onGround) {
        	
            if (this.getRidingEntity().isSneaking())
                ++sneakTicks;
            else this.sneakTicks = 0;
            if (sneakTicks > 30)
                land();
        }
        else this.sneakTicks = 0;
	}
	
	@Override
	public boolean processInitialInteract(PlayerEntity player, Hand hand) {
		return super.processInitialInteract(player, hand);
	}

	public void land() {
        if (!world.isRemote && console != null && !hasLanded) {
        	
			console.getExterior().place(console, world.getDimension().getType(), this.getPosition());
			console.setLocation(world.getDimension().getType(), this.getPosition());
        }
        remove();
        hasLanded = true;
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return true;
	}

    public IExterior getExterior() {
        if (exterior == null) {
            this.exterior = ExteriorRegistry.getExterior(new ResourceLocation(this.getDataManager().get(EXTERIOR)));
        }
        return this.exterior;
    }
    
    @Nullable
    public ExteriorTile getExteriorTile() {
    	if(this.tile == null)
    		this.readExteriorFromData(this.getDataManager().get(EXTERIOR_DATA));
    	return this.tile;
    }
    
    public void setExteriorTile(ExteriorTile tile) {
    	this.tile = tile;
    	this.dataManager.set(EXTERIOR_DATA, tile.serializeNBT());
    }
    
    public ConsoleTile getConsole() {
    	return console;
    }

    public void setConsole(ConsoleTile tile) {
        this.console = tile;
        this.exterior = tile.getExterior();
        if (!world.isRemote)
        	this.getDataManager().set(EXTERIOR, this.exterior.getRegistryName().toString());
    }

}
