package net.tardis.mod.entity;

import javax.annotation.Nullable;

import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.controller.FlyingMovementController;
import net.minecraft.entity.ai.goal.FollowOwnerFlyingGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.NonTamedTargetGoal;
import net.minecraft.entity.ai.goal.OwnerHurtByTargetGoal;
import net.minecraft.entity.ai.goal.OwnerHurtTargetGoal;
import net.minecraft.entity.ai.goal.RangedAttackGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomFlyingGoal;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.passive.IFlyingAnimal;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.pathfinding.FlyingPathNavigator;
import net.minecraft.pathfinding.PathNavigator;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.world.World;

public class SecDroidEntity extends TameableEntity implements IFlyingAnimal, IRangedAttackMob{
	
	public NearestAttackableTargetGoal<PlayerEntity> attackPlayerGoal;

	public SecDroidEntity(EntityType<? extends TameableEntity> type, World worldIn) {
		super(type, worldIn);
		this.moveController = new FlyingMovementController(this);
		this.setNoGravity(true);
	}
	
	public SecDroidEntity(World worldIn) {
		this(TEntities.SECURITY_DROID, worldIn);
	}
	
	@Override
	protected void registerGoals() {
		super.registerGoals();
		
		this.goalSelector.addGoal(3, new FollowOwnerFlyingGoal(this, 1.0, 4, 10));
		this.goalSelector.addGoal(6, new WaterAvoidingRandomFlyingGoal(this, 1.0D));
		
		this.goalSelector.addGoal(5, new LookRandomlyGoal(this));
		
		this.goalSelector.addGoal(0, new RangedAttackGoal(this, 1.5, 60, 6));
		
		this.goalSelector.addGoal(0, new NonTamedTargetGoal<PlayerEntity>(this, PlayerEntity.class, true, null));
		this.goalSelector.addGoal(2, new OwnerHurtTargetGoal(this));
		this.goalSelector.addGoal(2, new OwnerHurtByTargetGoal(this));
		this.goalSelector.addGoal(3, new NearestAttackableTargetGoal<MonsterEntity>(this, MonsterEntity.class, true));
	}

	@Override
	public boolean shouldAttackEntity(LivingEntity target, LivingEntity owner) {
		return target instanceof TameableEntity && ((TameableEntity)target).getOwner() == owner ? false : super.shouldAttackEntity(target, owner);
	}

	@Override
	protected PathNavigator createNavigator(World worldIn) {
		FlyingPathNavigator flyingpathnavigator = new FlyingPathNavigator(this, worldIn);
	    flyingpathnavigator.setCanOpenDoors(false);
	    flyingpathnavigator.setCanSwim(true);
	    flyingpathnavigator.setCanEnterDoors(true);
	    return flyingpathnavigator;
	}

	@Override
	protected void registerAttributes() {
		super.registerAttributes();
		this.getAttributes().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE);
		this.getAttributes().registerAttribute(SharedMonsterAttributes.FLYING_SPEED);
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20);
		this.getAttribute(SharedMonsterAttributes.FLYING_SPEED).setBaseValue((double)0.4F);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue((double)0.2F);
		this.getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(2);
	}

	@Override
	public void livingTick() {
		super.livingTick();
	}
	
	@Override
	public void writeAdditional(CompoundNBT compound) {
		super.writeAdditional(compound);
	}

	@Override
	public void readAdditional(CompoundNBT compound) {
		super.readAdditional(compound);
	}

	@Nullable
	@Override
	public AgeableEntity createChild(AgeableEntity ageable) {
		return null;
	}

	@Override
	public boolean processInteract(PlayerEntity player, Hand hand) {
		this.setTamedBy(player);//Temporary debug functionality to prevent it from killing you during debug
		return super.processInteract(player, hand);
	}

	@Override
	public boolean canDespawn(double distanceToClosestPlayer) {
		return false;
	}

	@Override
	public void fall(float distance, float damageMultiplier) {}

	@Override
	public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor) {
		if(target.canEntityBeSeen(this))
			target.attackEntityFrom(DamageSource.MAGIC, (float)this.getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getValue());
	}
}
