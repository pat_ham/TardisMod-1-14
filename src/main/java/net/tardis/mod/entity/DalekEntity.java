package net.tardis.mod.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.RangedAttackGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.merchant.villager.AbstractVillagerEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.monster.SkeletonEntity;
import net.minecraft.entity.monster.SpiderEntity;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.entity.ai.dalek.DalekFollowLeaderGoal;
import net.tardis.mod.entity.ai.dalek.DalekFormation;
import net.tardis.mod.entity.ai.dalek.FlyingAttackGoal;
import net.tardis.mod.entity.ai.dalek.types.DalekType;
import net.tardis.mod.misc.TDamage;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.sounds.TSounds;

public class DalekEntity extends MonsterEntity implements IRangedAttackMob {

	private static AxisAlignedBB SCAN_RANGE = new AxisAlignedBB(0, 0, 0, 1, 1, 1).grow(20);
	private DalekEntity commander = null;

	//Data Parameters
	private static final DataParameter<Boolean> IS_COMMANDER = EntityDataManager.createKey(DalekEntity.class, DataSerializers.BOOLEAN);
    private static final DataParameter<String> TYPE = EntityDataManager.createKey(DalekEntity.class, DataSerializers.STRING);
    private static final DataParameter<Boolean> FLYING = EntityDataManager.createKey(DalekEntity.class, DataSerializers.BOOLEAN);

	private static final Class[] MOBS_TO_ATTACK = new Class[]{ZombieEntity.class, SkeletonEntity.class, SpiderEntity.class, PlayerEntity.class, AbstractVillagerEntity.class};


	//May be resource intensive, we'll see
	private ArrayList<DalekEntity> drones = new ArrayList<DalekEntity>();
	
	private DalekFormation formation;

	public DalekEntity(EntityType<? extends MonsterEntity> type, World world) {
		super(type, world);
		
		//Testing
		ArrayList<Vec3d> list = new ArrayList<Vec3d>();
		list.add(new Vec3d(2, 0, -2)); //Arrow formation
		list.add(new Vec3d(-2, 0, -2));
		formation = new DalekFormation(list);
		
	}
	
	public DalekEntity(World world) {
		this(TEntities.DALEK, world);
	}

	@Override
	protected void registerData() {
		super.registerData();
		getDataManager().register(IS_COMMANDER, false);
		getDataManager().register(TYPE, getRandomDalekType(this).getRegistryName().toString()); //Fix the NBT saving
		this.getDataManager().register(FLYING, false);
    }

	@Override
	protected void registerAttributes() {
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(35.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.23D);
		this.getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(3.0D);
        this.getAttribute(SharedMonsterAttributes.ARMOR).setBaseValue(20.0D);
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(40D);
	}

	@Override
	protected void registerGoals() {
		super.registerGoals();

		this.targetSelector.addGoal(0, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, true));
		
		for (Class<? extends MobEntity> mob : MOBS_TO_ATTACK) {
			this.targetSelector.addGoal(0, new NearestAttackableTargetGoal<>(this, mob, true));
		}
		
		this.goalSelector.addGoal(0, new RangedAttackGoal(this, 1.0D, 45, 15.0F));
		this.goalSelector.addGoal(0, new FlyingAttackGoal(this));
		this.goalSelector.addGoal(0, new LookAtGoal(this, PlayerEntity.class, 8.0F));
		
		this.goalSelector.addGoal(1, new DalekFollowLeaderGoal(this));

		this.goalSelector.addGoal(2, new MoveTowardsTargetGoal(this, 1.0, 10));
		this.goalSelector.addGoal(3, new WaterAvoidingRandomWalkingGoal(this, 0.4D));
		this.goalSelector.addGoal(3, new LookRandomlyGoal(this));
	}

	@Override
	protected void collideWithEntity(Entity entityIn) {
		if(!(entityIn instanceof DalekEntity))
			super.collideWithEntity(entityIn);
	}

	@Override
	protected SoundEvent getAmbientSound() {
		return getDalekType().getAmbientSounds(this);
	}

	@Override
	protected SoundEvent getDeathSound() {
		return getDalekType().getDeathSound(this);
	}

	@Override
	public void tick() {
		super.tick();
        if (world == null)
			return;

        setInFlight(world.getBlockState(getPosition().down()).getMaterial().isLiquid());

		getDalekType().specialUpdate(this);

        if (!world.isRemote) {
            this.commander = this.scanForLeader();
            
            if(this.ticksExisted % 100 == 0)
            	this.verifyDroneList();
		}
        else {
	        if (world.isRemote && ticksExisted == 5) {
//	        	if (this.isInFlight()) {
                Tardis.proxy.playMovingSound(this, TSounds.DALEK_HOVER, SoundCategory.HOSTILE, 0.7F, true);
//	        	}
//	        	else {
//	        		Tardis.proxy.playMovingSound(this, TSounds.DALEK_MOVES, SoundCategory.HOSTILE, 0.7F, true);
//	        	}
			}
	
			//Spawn particles if flying
			if (this.isInFlight()) {
	            for (int x = 0; x <= 13; x++) {
	                world.addParticle(ParticleTypes.COMPOSTER, posX + (world.rand.nextDouble() - 0.5D) * 0.5D - 0.3D, this.posY, this.posZ + (world.rand.nextDouble() - 0.5D) * 0.5D - 0.2D, 1D, 1D, 2D);
	                world.addParticle(ParticleTypes.COMPOSTER, posX + (world.rand.nextDouble() + 0.5D) * 0.5D - 0.3D, this.posY, this.posZ + (world.rand.nextDouble() + 0.5D) * 0.5D - 0.2D, 1D, 1D, 2D);
	            }
	        }
        }
        
        if(this.isCommander()) {
        	if(this.getAllClear(this.getHorizontalFacing(), 2)) {
        		ArrayList<Vec3d> list = new ArrayList<Vec3d>();
        		list.add(new Vec3d(2, 0, -2)); //Arrow formation
        		list.add(new Vec3d(-2, 0, -2));
        		formation = new DalekFormation(list);
        	}
        	else {
        		ArrayList<Vec3d> list = new ArrayList<Vec3d>();
        		list.add(new Vec3d(0, 0, -2));
        		list.add(new Vec3d(0, 0, -4));
        		formation = new DalekFormation(list);
        	}
        }
	}

	//Dalek shit
	public DalekEntity scanForLeader() {
		List<DalekEntity> daleks = world.getEntitiesWithinAABB(DalekEntity.class, SCAN_RANGE.offset(this.getPositionVec()));
		//Search range for a commander
		for(DalekEntity dalek : daleks) {
			if(dalek.isCommander()) {
				if(!dalek.getDrones().contains(this))
					dalek.addDrone(this);
				return dalek;
			}
		}
		//If we can't find one, promote ourselves
		this.setIsCommander(true);
		
		for(DalekEntity dalek : daleks) {
			dalek.setCommander(this);
			this.addDrone(dalek);
		}
		return this;
	}
	
	//Dalek getters and setters

	public boolean isCommander() {
		return getDataManager().get(IS_COMMANDER);
	}

	public void setIsCommander(boolean command) {
		getDataManager().set(IS_COMMANDER, command);
	}
	
	public void setCommander(DalekEntity dalek) {
		this.commander = dalek;
	}

	public DalekEntity getCommander() {
		return this.commander;
	}


	public DalekType getDalekType() {
		ResourceLocation location = new ResourceLocation(getDataManager().get(TYPE));
		if (TardisRegistries.DALEK_TYPE.getRegistry().containsKey(location)) {
			return TardisRegistries.DALEK_TYPE.getValue(location);
		}
		return TardisRegistries.DALEK_TYPE.getValue(Constants.DalekTypes.DALEK_TIMEWAR);
	}

	public void setDalekType(ResourceLocation dalekType) {
		getDataManager().set(TYPE, dalekType.toString());
	}
	
	//Will return an empty list if not a commander
	public ArrayList<DalekEntity> getDrones(){
		if(!this.isCommander())
			this.drones.clear();
		return this.drones;
	}
	
	public void addDrone(DalekEntity drone) {
		this.drones.add(drone);
	}

	//Should *not* be done frequently
	public void verifyDroneList() {
		for (Iterator<DalekEntity> iterator = drones.iterator(); iterator.hasNext(); ) {
			DalekEntity dalek = iterator.next();
			if (!dalek.isAlive()) {
				dalek.setCommander(null);
				iterator.remove();
			}
		}
	}

	public DalekFormation getFormation() {
		return this.formation;
	}

	public boolean isInFlight() {
		return this.getDataManager().get(FLYING);
	}
	
	public void setInFlight(boolean flying) {
		this.getDataManager().set(FLYING, flying);
	}
	
	//NBT
	@Override
	public void writeAdditional(CompoundNBT dalekNbt) {
		super.writeAdditional(dalekNbt);
		dalekNbt.putBoolean("isCommander", isCommander());
		dalekNbt.putString("dalekType", getDataManager().get(TYPE));
	}

	@Override
	public void readAdditional(CompoundNBT dalekNbt) {
		super.readAdditional(dalekNbt);
		setIsCommander(dalekNbt.getBoolean("isCommander"));
		setDalekType(new ResourceLocation(dalekNbt.getString("dalekType")));
	}

	@Override
	public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor) {
		getDalekType().onAttack(this, target, distanceFactor);
	}

	@Override
	public void fall(float distance, float damageMultiplier) {}

	@Override
	protected void updateFallState(double y, boolean onGroundIn, BlockState state, BlockPos pos) {}

	@Override
	public ITextComponent getName() {
		if (getCustomName() != null) {
			return super.getName();
		} else {
			return this.getDalekType().getName();
		}
	}

	public static DalekType getRandomDalekType(DalekEntity dalek){
		List<ResourceLocation> keysAsArray = new ArrayList<>(TardisRegistries.DALEK_TYPE.getRegistry().keySet());
		return TardisRegistries.DALEK_TYPE.getRegistry().get(keysAsArray.get(dalek.world.rand.nextInt(keysAsArray.size())));
	}

	@Override
	public ItemStack getPickedResult(RayTraceResult target) {
		return new ItemStack(getDalekType().getSpawnItem());
	}
	
	public boolean getAllClear(Direction dir, int blocks) {
		BlockPos start = this.getPosition().offset(dir.getOpposite(), blocks);
		BlockPos end = this.getPosition().offset(dir, blocks);
		Iterator<BlockPos> poses = BlockPos.getAllInBox(start, end).iterator();
		
		while(poses.hasNext()) {
			BlockPos pos = poses.next();
			if(!world.getBlockState(pos).isAir(world, pos))
				return false;
		}
		return true;
	}

	@Override
	public boolean canBreatheUnderwater() {
		return true;
	}

	@Override
	public boolean isInvulnerableTo(DamageSource source) {
		super.isInvulnerableTo(source);
		Entity trueSource = source.getTrueSource();
		if (trueSource != null && trueSource instanceof LivingEntity || source.equals(TDamage.LASER) || source.equals(TDamage.CYBERMAN) || source.equals(TDamage.LASER_SONIC)) {
			return false;
		}
		return this.isDalekInvulnerableTo(source);
	}
	
	public boolean isDalekInvulnerableTo(DamageSource source) {
		return !source.equals(DamageSource.ON_FIRE)
				|| !source.equals(DamageSource.FALLING_BLOCK)
				|| !source.equals(DamageSource.FALL)
				|| !source.equals(DamageSource.ANVIL)
				|| !source.equals(DamageSource.SWEET_BERRY_BUSH)
				|| !source.equals(DamageSource.LAVA)
				|| !source.equals(DamageSource.FIREWORKS)
				|| !source.equals(DamageSource.LIGHTNING_BOLT)
				|| !source.equals(DamageSource.HOT_FLOOR)
				|| !source.equals(DamageSource.CACTUS);

	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
		return SoundEvents.ENTITY_IRON_GOLEM_HURT;
	}
	
	@Override
	public boolean canRenderOnFire() {
		return false;
	}

    @Override
    protected void playStepSound(BlockPos pos, BlockState blockIn) {
        this.playSound(TSounds.DALEK_MOVES, 0.1F, 1.0F);
    }

	@Override
	public void onKillCommand() {
		super.onKillCommand();
		this.remove();
	}
}
