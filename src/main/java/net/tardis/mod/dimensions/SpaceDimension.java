package net.tardis.mod.dimensions;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.biome.provider.SingleBiomeProvider;
import net.minecraft.world.biome.provider.SingleBiomeProviderSettings;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.ChunkGenerator;

public class SpaceDimension extends Dimension implements IDimProperties{

	private static final SingleBiomeProvider PROVIDER = new SingleBiomeProvider(new SingleBiomeProviderSettings().setBiome(Biomes.THE_VOID));
	
	public SpaceDimension(World worldIn, DimensionType typeIn) {
		super(worldIn, typeIn);
	}

	@Override
	public ChunkGenerator<?> createChunkGenerator() {
		return new SpaceChunkGenerator(this.world, PROVIDER, new SpaceGenerationSettings());
	}

	@Override
	public BlockPos findSpawn(ChunkPos chunkPosIn, boolean checkValid) {
		return null;
	}

	@Override
	public BlockPos findSpawn(int posX, int posZ, boolean checkValid) {
		return null;
	}

	@Override
	public float calculateCelestialAngle(long worldTime, float partialTicks) {
		return 0;
	}

	@Override
	public boolean isSurfaceWorld() {
		return false;
	}

	@Override
	public Vec3d getFogColor(float celestialAngle, float partialTicks) {
		return new Vec3d(0, 0, 0);
	}

	@Override
	public boolean canRespawnHere() {
		return false;
	}

	@Override
	public boolean doesXZShowFog(int x, int z) {
		return true;
	}

	@Override
	public boolean shouldMapSpin(String entity, double x, double z, double rotation) {
		return true;
	}

	@Override
	public boolean doesWaterVaporize() {
		return true;
	}

	@Override
	public Vec3d modMotion(LivingEntity ent) {
		IAttributeInstance grav = ent.getAttribute(LivingEntity.ENTITY_GRAVITY);
		if(grav != null)
			return ent.getMotion().add(0, grav.getValue(), 0);
		return ent.getMotion();
	}

}
