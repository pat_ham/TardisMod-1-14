package net.tardis.mod.dimensions;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.biome.provider.SingleBiomeProvider;
import net.minecraft.world.biome.provider.SingleBiomeProviderSettings;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.ChunkGenerator;
import net.tardis.mod.dimensions.TardisChunkGenerator.GeneratorSettingsTardis;
import net.tardis.mod.dimensions.chunkgen.MoonChunkGenerator;

public class MoonDimension extends Dimension implements IDimProperties{

	public static final SingleBiomeProvider BIOME = new SingleBiomeProvider(new SingleBiomeProviderSettings().setBiome(Biomes.DESERT));
	
	public MoonDimension(World worldIn, DimensionType typeIn) {
		super(worldIn, typeIn);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ChunkGenerator<?> createChunkGenerator() {
		return new MoonChunkGenerator(this.world, BIOME, new GeneratorSettingsTardis());
	}

	@Override
	public BlockPos findSpawn(ChunkPos chunkPosIn, boolean checkValid) {
		return null;
	}

	@Override
	public BlockPos findSpawn(int posX, int posZ, boolean checkValid) {
		return null;
	}

	@Override
	public float calculateCelestialAngle(long worldTime, float partialTicks) {
		return 0;
	}

	@Override
	public boolean isSurfaceWorld() {
		return false;
	}

	@Override
	public Vec3d getFogColor(float celestialAngle, float partialTicks) {
		return new Vec3d(0, 0, 0);
	}

	@Override
	public boolean canRespawnHere() {
		return false;
	}

	@Override
	public boolean doesXZShowFog(int x, int z) {
		return false;
	}
	
	@Override
	public Vec3d modMotion(LivingEntity mot) {
		return mot.getMotion().add(0, 0.04, 0);
	}

}
