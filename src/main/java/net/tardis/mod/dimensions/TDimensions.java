package net.tardis.mod.dimensions;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.util.ResourceLocation;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.ModDimension;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.world.data.TardisWorldData;

@Mod.EventBusSubscriber	(modid = Tardis.MODID, bus = Bus.MOD)
public class TDimensions {

	/// Add the new Dimension to the list
	public static List<ResourceLocation> TARDIS_DIMENSIONS = new ArrayList<ResourceLocation>();

	public static ModDimension TARDIS;
	public static ModDimension VORTEX;
	public static ModDimension SPACE;
	public static ModDimension MOON;
	
	public static DimensionType VORTEX_TYPE;
	
	public static DimensionType SPACE_TYPE;
	public static DimensionType MOON_TYPE;

	public static DimensionType registerOrGet(String name, ModDimension mod) {
		ResourceLocation loc = new ResourceLocation(Tardis.MODID, name);
		if(DimensionType.byName(loc) != null)
			return DimensionType.byName(loc);
		
		DimensionType type = DimensionManager.registerDimension(loc, mod, null, true);
		TARDIS_DIMENSIONS.add(loc);
		if(Events.DATA != null)
			Events.DATA.markDirty();
		return type;
	}

	/// Register the Dimension ID and generation type here
	@SubscribeEvent
	public static void register(RegistryEvent.Register<ModDimension> event) {
		event.getRegistry().registerAll(
				TARDIS = new SingleModDimension(TardisDimension::new).setRegistryName(Tardis.MODID, "tardis"),
				VORTEX = new SingleModDimension(VortexDimension::new).setRegistryName(Tardis.MODID, "vortex"),
				SPACE = new SingleModDimension(SpaceDimension::new).setRegistryName(Tardis.MODID, "space"),
				MOON = new SingleModDimension(MoonDimension::new).setRegistryName(new ResourceLocation(Tardis.MODID, "moon"))
		);

	}

	/// Takes previously created TARDISs and updates them to the new system.
	public static void registerOldTardises() {
		for(ResourceLocation loc : TARDIS_DIMENSIONS){
			if(DimensionType.byName(loc) == null) {
				DimensionManager.registerDimension(loc, TARDIS, null, true);
				if(!TDimensions.TARDIS_DIMENSIONS.contains(loc))
					TDimensions.TARDIS_DIMENSIONS.add(loc);
			}
		}
	}
	
	@Mod.EventBusSubscriber(modid = Tardis.MODID)
	public static class Events{
		
		static TardisWorldData DATA;
		
		@SubscribeEvent
		public static void saveTardisDimensions(WorldEvent.Save event) {
			if(!(event.getWorld() instanceof ServerWorld))
				return;
			
			if(event.getWorld().getDimension().getType() == DimensionType.OVERWORLD) {
				((ServerWorld)event.getWorld()).getSavedData().set(DATA);
			}
		}
		
		@SubscribeEvent
		public static void load(WorldEvent.Load event) {
			if(!(event.getWorld() instanceof ServerWorld))
				return;
			
			if(event.getWorld().getDimension().getType() == DimensionType.OVERWORLD) {
				DATA = ((ServerWorld)event.getWorld()).getSavedData().getOrCreate(TardisWorldData::new, "tardis");
				registerOldTardises();
			}
		}
		
		@SubscribeEvent
		public static void unload(WorldEvent.Unload event) {
			if(event.getWorld().getDimension().getType() == DimensionType.OVERWORLD) {
				TDimensions.TARDIS_DIMENSIONS.clear();
			}
		}
	}
}