package net.tardis.mod.dimensions;

import java.util.function.BiFunction;

import net.minecraft.world.World;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.common.ModDimension;

public class SingleModDimension extends ModDimension {

	DimFactory<?> fact;
	
	public SingleModDimension(DimFactory<?> fact) {
		this.fact = fact;
	}
	
	@Override
	public BiFunction<World, DimensionType, ? extends Dimension> getFactory() {
		return (BiFunction<World, DimensionType, Dimension>) (t, u) -> fact.create(t, u);
	}

	public static interface DimFactory<T extends Dimension>{
		
		T create(World world, DimensionType type);
	}
}
