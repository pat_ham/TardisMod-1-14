package net.tardis.mod.dimensions;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.biome.provider.SingleBiomeProvider;
import net.minecraft.world.biome.provider.SingleBiomeProviderSettings;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.IRenderHandler;
import net.tardis.mod.client.ModelRegistry;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;

public class TardisDimension extends Dimension{

	public static SingleBiomeProvider PROVIDER = new SingleBiomeProvider(new SingleBiomeProviderSettings().setBiome(Biomes.THE_VOID));
	
	public TardisDimension(World worldIn, DimensionType typeIn) {
		super(worldIn, typeIn);
	}

	@Override
	public ChunkGenerator<?> createChunkGenerator() {
		TardisChunkGenerator gen = new TardisChunkGenerator(this.world, PROVIDER, new TardisChunkGenerator.GeneratorSettingsTardis());
		return gen;
	}

	@Override
	public BlockPos findSpawn(ChunkPos chunkPosIn, boolean checkValid) {
		return BlockPos.ZERO;
	}

	@Override
	public BlockPos findSpawn(int posX, int posZ, boolean checkValid) {
		return BlockPos.ZERO;
	}

	@Override
	public float calculateCelestialAngle(long worldTime, float partialTicks) {
		return 0.5F;
	}

	@Override
	public boolean isSurfaceWorld() {
		return true;
	}

	@Override
	public Vec3d getFogColor(float celestialAngle, float partialTicks) {
		return new Vec3d(0, 0, 0);
	}

	@Override
	public boolean canRespawnHere() {
		return false;
	}

	@Override
	public boolean doesXZShowFog(int x, int z) {
		return false;
	}

	@Override
	public SleepResult canSleepAt(PlayerEntity player, BlockPos pos) {
		return SleepResult.ALLOW;
	}

	@Override
	public float getCloudHeight() {
		return -80.0F;
	}

	@Override
	public boolean doesWaterVaporize() {
		return false;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void getLightmapColors(float partialTicks, float sunBrightness, float skyLight, float blockLight, float[] colors) {

		if(this.getWorld().isBlockLoaded(TardisHelper.TARDIS_POS)) {
			TileEntity te = world.getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				ConsoleTile console = (ConsoleTile)te;
				if(((ConsoleTile)te).getInteriorManager().isAlarmOn()){
					colors[0] = 1F;
					colors[1] *= 1 - ((float)(Math.sin(world.getGameTime() * 0.1)) * 0.5F);
					colors[2] *= 1 - ((float)(Math.sin(world.getGameTime() * 0.1)) * 0.5F);
				}
				else {
					if(console.getEmotionHandler().getMood() <= EnumHappyState.SAD.getTreshold()) {
						colors[0] *= 0.4;
						colors[1] *= 0.4;
						colors[2] *= 0.75F;
					}
					else if(console.getEmotionHandler().getMood() >= EnumHappyState.HAPPY.getTreshold()) {
						colors[0] *= 1.5F;
						colors[1] *= 1.5F;
						colors[2] *= 1.5F;
					}
					//Cutomization takes last priority
					float[] color = console.getInteriorManager().getColor();
					colors[0] *= color[0] + 0.1;
					colors[1] *= color[1] + 0.1;
					colors[2] *= color[2] + 0.1;
					
				}
				
			}
		}
		super.getLightmapColors(partialTicks, sunBrightness, skyLight, blockLight, colors);
	}

	@Override
	public boolean hasSkyLight() {
		return false;
	}

	@Override
	public boolean canDoRainSnowIce(Chunk chunk) {
		return false;
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public IRenderHandler getSkyRenderer() {
		return ModelRegistry.EMPTY_RENDER;
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public IRenderHandler getCloudRenderer() {
		return ModelRegistry.EMPTY_RENDER;
	}
	
	@OnlyIn(Dist.CLIENT)
	@Override
	public IRenderHandler getWeatherRenderer() {
		return ModelRegistry.EMPTY_RENDER;
	}

	@Override
	public DimensionType getRespawnDimension(ServerPlayerEntity player) {
		return super.getRespawnDimension(player);
	}
	
}
