package net.tardis.mod.dimensions.chunkgen;

import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.Heightmap.Type;
import net.tardis.mod.dimensions.TardisChunkGenerator.GeneratorSettingsTardis;

public class MoonChunkGenerator extends ChunkGenerator<GeneratorSettingsTardis> {

	public MoonChunkGenerator(IWorld worldIn, BiomeProvider biomeProviderIn, GeneratorSettingsTardis generationSettingsIn) {
		super(worldIn, biomeProviderIn, generationSettingsIn);
	}

	@Override
	public void generateSurface(IChunk chunkIn) {
		
	}

	@Override
	public int getGroundHeight() {
		return 64;
	}

	@Override
	public void makeBase(IWorld worldIn, IChunk chunkIn) {
		for(int x = 0; x < 16; ++x) {
			for(int z = 0; z < 16; ++z) {
				for(int y = 0; y < 64; ++y) {
					chunkIn.setBlockState(new BlockPos(x, y, z), Blocks.STONE.getDefaultState(), false);
				}
				chunkIn.setBlockState(new BlockPos(x, 0, z), Blocks.BEDROCK.getDefaultState(), false);
			}
		}
	}

	@Override
	public int func_222529_a(int x, int z, Type type) {
		return 0;
	}

}
