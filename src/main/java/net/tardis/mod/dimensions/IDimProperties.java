package net.tardis.mod.dimensions;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.Vec3d;

public interface IDimProperties {

	Vec3d modMotion(LivingEntity mot);
}
