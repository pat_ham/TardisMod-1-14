package net.tardis.mod;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.tardis.mod.cap.ChunkLoaderCapability;
import net.tardis.mod.cap.IChunkLoader;
import net.tardis.mod.cap.ILightCap;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.cap.LightCapability;
import net.tardis.mod.cap.TardisWorldCapability;
import net.tardis.mod.cap.entity.IPlayerData;
import net.tardis.mod.cap.entity.PlayerDataCapability;
import net.tardis.mod.cap.items.IRemote;
import net.tardis.mod.cap.items.IVortexCap;
import net.tardis.mod.cap.items.IWatch;
import net.tardis.mod.cap.items.RemoteCapability;
import net.tardis.mod.cap.items.VortexCapability;
import net.tardis.mod.cap.items.WatchCapability;
import net.tardis.mod.client.animation.ExteriorAnimation;
import net.tardis.mod.commands.TardisCommand;
import net.tardis.mod.commands.permissions.PermissionManager;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.entity.ai.dalek.types.DalekType;
import net.tardis.mod.experimental.advancement.TTriggers;
import net.tardis.mod.flight.FlightEvent;
import net.tardis.mod.misc.vm.VortexMFunctions;
import net.tardis.mod.network.Network;
import net.tardis.mod.protocols.Protocol;
import net.tardis.mod.proxy.ClientProxy;
import net.tardis.mod.proxy.IProxy;
import net.tardis.mod.proxy.ServerProxy;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.schematics.Schematics;
import net.tardis.mod.sonic.capability.ISonic;
import net.tardis.mod.sonic.capability.SonicCapability;
import net.tardis.mod.sonic.capability.SonicStorage;
import net.tardis.mod.sounds.InteriorHum;
import net.tardis.mod.upgrades.Upgrades;
import net.tardis.mod.world.WorldGen;

@Mod(Tardis.MODID)
public class Tardis
{
	public static final String MODID = "tardis";
	
	public static IProxy proxy;
	
    public Tardis()
    {
    	FMLJavaModLoadingContext.get().getModEventBus().addListener(this::commonSetup);

        MinecraftForge.EVENT_BUS.register(this);
        MinecraftForge.EVENT_BUS.register(new WorldGen());
        TTriggers.init();
    }
    
	private void commonSetup(FMLCommonSetupEvent event) {

        Network.init();
        TardisRegistries.init();
        ControlRegistry.register();
        
        DistExecutor.runWhenOn(Dist.DEDICATED_SERVER, () -> {

        	return () -> proxy = new ServerProxy();

        });
        
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> proxy = new ClientProxy());

        TardisRegistries.registerRegisters(Protocol::registerAll);
        TardisRegistries.registerRegisters(DalekType::registerAll);
        TardisRegistries.registerRegisters(Upgrades::registerAll);
        TardisRegistries.registerRegisters(ExteriorAnimation::registerAll);
        TardisRegistries.registerRegisters(InteriorHum::registerAll);
        TardisRegistries.registerRegisters(FlightEvent::registerAll);
        TardisRegistries.registerRegisters(Schematics::registerAll);
        WorldGen.applyFeatures();
        VortexMFunctions.init();

        CapabilityManager.INSTANCE.register(ILightCap.class, new ILightCap.LightStorage(), LightCapability::new);
        CapabilityManager.INSTANCE.register(IChunkLoader.class, new ChunkLoaderCapability.LoaderStorage(), () -> new ChunkLoaderCapability(null));
        CapabilityManager.INSTANCE.register(ITardisWorldData.class, new ITardisWorldData.TardisWorldStorage(), () -> new TardisWorldCapability(null));

        //ItemCaps
        CapabilityManager.INSTANCE.register(IVortexCap.class, new IVortexCap.Storage(), VortexCapability::new);
        CapabilityManager.INSTANCE.register(ISonic.class, new SonicStorage(), SonicCapability::new);
        CapabilityManager.INSTANCE.register(IWatch.class, new IWatch.Storage(), WatchCapability::new);
        CapabilityManager.INSTANCE.register(IRemote.class, new IRemote.Storage(), () -> new RemoteCapability(null));

        //Entity Caps
        CapabilityManager.INSTANCE.register(IPlayerData.class, new IPlayerData.Storage(), () -> new PlayerDataCapability(null));
        
    }

    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
        PermissionManager.init();
        TardisCommand.register(event.getCommandDispatcher());
    }

}
