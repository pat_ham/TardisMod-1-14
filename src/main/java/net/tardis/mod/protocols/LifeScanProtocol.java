package net.tardis.mod.protocols;

import net.minecraft.entity.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.tileentities.ConsoleTile;

public class LifeScanProtocol extends Protocol {

	private static AxisAlignedBB BOX = new AxisAlignedBB(-20, -20, -20, 20, 20, 20);
	
	@Override
	public void call(World world, ConsoleTile console) {
		if(!world.isRemote) {
			IntWrapper wrap = new IntWrapper();
			((ServerWorld)world).getEntities().forEach((entity) -> {
				if(entity instanceof MobEntity || entity instanceof ServerPlayerEntity)
					wrap.addInt();
			});
			for(PlayerEntity entity : world.getEntitiesWithinAABB(PlayerEntity.class, BOX.offset(console.getPos()))) {
				entity.sendStatusMessage(new StringTextComponent("There are " + wrap.val + " life signs"), true);
			}
		}
		else Tardis.proxy.openGUI(Constants.Gui.NONE, new GuiContext());
	}

	@Override
	public String getDisplayName() {
		return "Scan for life signs";
	}
	
	
	
	@Override
	public String getSubmenu() {
		return "security";
	}



	public static class IntWrapper{
		
		private int val;
		
		public void addInt() {
			++val;
		}
		
		public int getInt() {
			return this.val;
		}
	}

}
