package net.tardis.mod.misc;

import java.util.function.Function;
import java.util.function.Supplier;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.client.models.interiordoors.IInteriorDoorRenderer;
import net.tardis.mod.enums.EnumDoorState;

public interface IDoorType {

	EnumDoorState[] getValidStates();
	
	//For rendering mostly, in Degrees
	double getRotationForState(EnumDoorState state);
	
	
	@OnlyIn(Dist.CLIENT)
	void setInteriorDoorModel(IInteriorDoorRenderer renderer);
	
	@OnlyIn(Dist.CLIENT)
	IInteriorDoorRenderer getInteriorDoorRenderer();
	
	public static enum EnumDoorType implements IDoorType{
		
		STEAM((state) -> {
			switch(state) {
				case CLOSED: return 0.0;
				case ONE: return 55.0;
				default: return 85.0;
			}
		}, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),
		
		TRUNK(state -> {
			switch(state) {
			case CLOSED: return 0.0;
			case ONE: return -55.0;
			default: return -85.0;
			}
		}, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),
		
		RED(state -> {
			switch(state) {
			case CLOSED: return 0.0;
			case ONE: return 55.0;
			default: return 85.0;
			}
		}, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),
		
		POLICE_BOX(state -> {
			switch(state) {
			case CLOSED: return 0.0;
			case ONE: return 90.0; //This returns the rotation value for the right door
			case BOTH: return -90.0; //This returns the rotation value of the left door
			default: return 0.0;
			}
		}, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),

		MODERN_POLICE_BOX(state -> {
			switch(state) {
				case CLOSED: return 0.0;
				case ONE: return 75.0; //This returns the rotation value for the right door
				case BOTH: return -75.0; //This returns the rotation value of the left door
				default: return 0.0;
			}
		}, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),
		
		FORTUNE(state -> {
			switch(state) {
			case CLOSED: return 0.0;
			case ONE: return -55.0;
			default: return -85.0;
			}
		}, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),
		
		SAFE(state -> {
				switch(state) {
					case CLOSED: return 0.0;
					case ONE: return -55.0;
					default: return -85.0;
				}
			}, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),
		
		TT_CAPSULE(state -> {
				switch(state) {
				case CLOSED: return 0.0;
				default: return 85.0;
			}
		}, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH);
		
		
		Function<EnumDoorState, Double> func;
		EnumDoorState[] validStates;
		Supplier<Supplier<IInteriorDoorRenderer>> renderer;
		
		EnumDoorType(Function<EnumDoorState, Double> func, EnumDoorState... states){
			this.validStates = states;
			this.func = func;
		}

		@Override
		public EnumDoorState[] getValidStates() {
			return this.validStates;
		}

		@Override
		public double getRotationForState(EnumDoorState state) {
			return func.apply(state);
		}
		
		@OnlyIn(Dist.CLIENT)
		public void setInteriorDoorModel(IInteriorDoorRenderer renderer) {
			this.renderer = () -> () -> renderer;
		}
		
		@OnlyIn(Dist.CLIENT)
		public IInteriorDoorRenderer getInteriorDoorRenderer() {
			return this.renderer.get().get();
		}
		
	}
}
