package net.tardis.mod.misc;

import net.tardis.mod.tileentities.ConsoleTile;

public interface ITickable {

	void tick(ConsoleTile console);
}
