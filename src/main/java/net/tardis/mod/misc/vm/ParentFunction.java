package net.tardis.mod.misc.vm;

import java.util.Map;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.tardis.mod.sounds.TSounds;
/**
 * Parent VM Function
 * @implSpec Extend this class, and use same parameters for first constructor, leave second constructor blank
 * @implNote Ensure to call super
 */
public abstract class ParentFunction implements IVortexMFunction{
	private Map<Integer, IVortexMFunction> subFunctions;
	
	public ParentFunction(Map<Integer, IVortexMFunction> subFunctions) {
		this.subFunctions = subFunctions;
	}
	
	public ParentFunction() {
		
	}
	
	@Override
	public void onActivated(World world, PlayerEntity player) {
		world.playSound(player, player.getPosition(), TSounds.VM_BUTTON, SoundCategory.PLAYERS, 0.5F, 1F);
	}
	
	

	@Override
	public void sendActionToServer(World world, ServerPlayerEntity player) {
		world.playSound(null, player.getPosition(), TSounds.VM_BUTTON, SoundCategory.PLAYERS, 0.5F, 1F);
	}

	public Map<Integer, IVortexMFunction> getSubFunctionList() {
		return this.subFunctions;
	}
	
	public IVortexMFunction getSubFunctionById(int id) {
		return this.subFunctions.get(id);
	}
	
	@Override
	public String getNameKey() {
		return "parent_function";
	}

	@Override
	public Boolean stateComplete() {
		return true;
	}
	
	@Override
	public Boolean isServerSide() {
		return false;
	}
	
}
