package net.tardis.mod.misc.vm;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class VortexMFunctions {
	private static Map<Integer, ParentFunction> FUNCTIONS = new HashMap<Integer, ParentFunction>();
	
	//This is meant to be unmodifiable
	private static Map<Integer, IVortexMFunction> BATTERY_SUB_FUNCTIONS = Collections.unmodifiableMap(new HashMap<Integer, IVortexMFunction>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			putIfAbsent(0, new BatteryFunction());
			putIfAbsent(1, new ScannerSubFunction(new BatteryFunction()));
		}
	});
	private static Map<Integer, IVortexMFunction> TELEPORT_SUB_FUNCTIONS = Collections.unmodifiableMap(new HashMap<Integer, IVortexMFunction>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			putIfAbsent(0, new TeleportFunction());
		}
	});
	
	/**
	 * Initalises maps. This is called in FMLCommonSetup
	 * Also call this in the VM GUI to initalise the functions on the client
	 */
	public static void init() {
		FUNCTIONS.put(0, new TeleportFunction(TELEPORT_SUB_FUNCTIONS));
		FUNCTIONS.put(1, new BatteryFunction(BATTERY_SUB_FUNCTIONS));
	}
	
	/**
	 * Used to get only the Parent Function
	 * @param id
	 * @return
	 */
    public static ParentFunction getFunction(Integer id) {
        if (FUNCTIONS.containsKey(id)) {
            return FUNCTIONS.get(id);
        }
        return FUNCTIONS.get(id);
    }
    
    /**
     * Convience method to get either parent or child function
     * @param id - Index value of Parent Function
     * @param subFunction - id of the sub function, is only used if isSubFunction is true
     * @param isSubFunction - determines if we are trying to get a sub function
     * @return IVortexMFunction
     */
    public static IVortexMFunction getVMFunction(int id, int subFunction, boolean isSubFunction) {
    	if (isSubFunction) {
    		if ((getFunction(id).getSubFunctionList().containsKey(subFunction))) {
    			return getFunction(id).getSubFunctionById(subFunction);
    		}
    	}
    	else {
                return getFunction(id);
        }
    	return getFunction(id);
    }
    
    /**
     * Used to get the function map if we need to use it to check for valid key etc.
     * @return Map<Integer, ParentFunction>
     */
    public static Map<Integer, ParentFunction> getFunctionList() {
    	return FUNCTIONS;
    }

}
