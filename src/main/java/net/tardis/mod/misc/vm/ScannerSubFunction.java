package net.tardis.mod.misc.vm;


import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.protocols.LifeScanProtocol.IntWrapper;


/**
 * Created by 50ap5ud5
 * on 5 May 2020 @ 8:04:17 pm
 */
public class ScannerSubFunction extends SubFunction {
	
	public ScannerSubFunction(ParentFunction parent) {
		super(parent);
	}

	@Override
	public void onActivated(World world, PlayerEntity player) {
		Tardis.proxy.openGUI(Constants.Gui.NONE, null);
		PlayerHelper.closeVMModel(player);
	}
	
	@Override
	public void sendActionToServer(World world, ServerPlayerEntity player) {
		IntWrapper wrapPlayer = new IntWrapper();
		IntWrapper warpMonster = new IntWrapper();
		((ServerWorld)world).getEntities().forEach((entity) -> {
			if(entity instanceof PlayerEntity && entity != player) {
				wrapPlayer.addInt();
			}
			if (entity instanceof MonsterEntity) {
				warpMonster.addInt();
			}
				
		});
		PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.vm.scanner.result", wrapPlayer.getInt(), warpMonster.getInt()), false);
		super.sendActionToServer(world, player);
	}

	@Override
	public String getNameKey() {
		return new TranslationTextComponent("subfunction.vm.scanner").getFormattedText();
	}

	@Override
	public Boolean stateComplete() {
		return true;
	}

	@Override
	public Boolean isServerSide() {
		return true;
	}


}
