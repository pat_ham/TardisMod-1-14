package net.tardis.mod.tileentities.monitors;

import java.text.DecimalFormat;

import javax.annotation.Nullable;

import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.boti.BlockStore;
import net.tardis.mod.boti.IBotiEnabled;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BOTIMessage;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class MonitorTile extends TileEntity implements ITickableTileEntity, IBotiEnabled{

	private WorldShell shell;
	private String[] info;
	public double startX = 0;
	public double startY = 0;
	public double startZ = 0;
	public double width = 0, height = 0;
	public MonitorMode mode = MonitorMode.INFO;
	private int timeToSync = 20;
	
	public static DecimalFormat format = new DecimalFormat("###");
	
	public MonitorTile() {
		this(TTiles.STEAMPUNK_MONITOR);
	}
	
	public MonitorTile(TileEntityType<?> tile) {
		super(tile);
	}

	@Override
	public void tick() {
		ConsoleTile console = this.getConsole();
		if(console != null) {
			this.info = new String[] {
			"Location: " + Helper.formatBlockPos(console.getLocation()),
			"Dimension: " + Helper.formatDimName(console.getDimension()),
			"Exterior Facing: " + console.getExteriorDirection().getName().toUpperCase(),
			"Target: " + Helper.formatBlockPos(console.getDestination()),
			"Target Dim: " + Helper.formatDimName(console.getDestinationDimension()),
			"Artron Units: " + format.format(console.getArtron()) + "AU",
			"Journey: " + format.format(console.getPercentageJourney() * 100.0F) + "%"
			};
			
		}
		
		if(this.timeToSync > 0) {
			--this.timeToSync;
			if(this.timeToSync <= 0)
				this.updateBoti();
		}
	}
	
	@Nullable
	private ConsoleTile getConsole() {
		TileEntity te = this.world.getTileEntity(TardisHelper.TARDIS_POS);
		if(te instanceof ConsoleTile)
			return (ConsoleTile)te;
		return null;
	}
	
	public void setGuiXYZ(double x, double y, double z, double width, double height) {
		this.startX = x;
		this.startY = y;
		this.startZ = z;
		this.width = width;
		this.height = height;
	}
	
	public String[] getInfo() {
		return info;
	}

	@Override
	public WorldShell getBotiWorld() {
		return this.shell;
	}

	@Override
	public void setBotiWorld(WorldShell shell) {
		this.shell = shell;
		this.shell.setNeedsUpdate(true);
	}

	public void updateBoti() {
		if(world.isRemote)
			return;
		ConsoleTile console = this.getConsole();
		if(console != null) {
			shell = new WorldShell(console.getLocation());
			ServerWorld w = world.getServer().getWorld(console.getDimension());
			int radius = 10;
			BlockPos start = console.getLocation().up().toImmutable();
			shell.setBiome(w.getBiome(start));
			for(int x = -radius; x < radius; ++x) {
				for(int z = -radius; z < radius; ++z) {
					for(int y = -radius; y < radius; ++y) {
						BlockPos pos = start.add(x, y, z);
						BlockState state = w.getBlockState(pos);
						TileEntity te = w.getTileEntity(pos);
						
						if(state.getRenderType() != BlockRenderType.INVISIBLE || te != null || state.getMaterial().isLiquid())
							shell.put(pos, new BlockStore(state, world.getLight(pos), te != null ? te.serializeNBT() : null));
					}
				}
			}
			//Entities
			
			for(ServerPlayerEntity ent : world.getEntitiesWithinAABB(ServerPlayerEntity.class, new AxisAlignedBB(this.getPos()).grow(60))) {
				Network.sendTo(new BOTIMessage(this.getPos(), shell), ent);
			}
		}
	}
	
	public MonitorMode getMode() {
		return this.mode;
	}
	
	public void setMode(MonitorMode mode) {
		this.mode = mode;
		this.markDirty();
		if(!world.isRemote)
			world.markAndNotifyBlock(getPos(), world.getChunkAt(this.getPos()), world.getBlockState(this.getPos()), world.getBlockState(this.getPos()), 2);
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.mode = MonitorMode.values()[pkt.getNbtCompound().getInt("mode")];
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.getUpdateTag());
	}
	
	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		super.handleUpdateTag(tag);
		this.mode = MonitorMode.values()[tag.getInt("mode")];
	}

	@Override
	public CompoundNBT getUpdateTag() {
		CompoundNBT tag = this.write(new CompoundNBT());
		tag.putInt("mode", this.mode.ordinal());
		return tag;
	}


	@Override
	public void onLoad() {
		super.onLoad();
		this.timeToSync = 20;
	}

	public static enum MonitorMode{
		INFO,
		SCANNER
	}

}
