package net.tardis.mod.tileentities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.items.CapabilityItemHandler;

public class ReclamationTile extends TileEntity implements IInventory, ITickableTileEntity{
	
	private List<ItemStack> inv = new ArrayList<ItemStack>();
	
	public ReclamationTile() {
		super(TTiles.RECLAMATION_UNIT);
	}

	@Override
	public void tick() {
		if(!world.isRemote && this.isEmpty()) {
			world.setBlockState(this.getPos(), Blocks.AIR.getDefaultState());
			world.playSound(null, this.getPos(), SoundEvents.UI_TOAST_OUT, SoundCategory.BLOCKS, 1F, 1F);
		}
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return slot < inv.size() ? inv.get(slot) : ItemStack.EMPTY;
	}

	@Override
	public void clear() {
		inv.clear();
		this.markDirty();
	}

	@Override
	public int getSizeInventory() {
		return inv.size();
	}

	@Override
	public boolean isEmpty() {
		for(ItemStack stack : inv) {
			if(!stack.isEmpty())
				return false;
		}
		return true;
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		this.markDirty();
		return this.getStackInSlot(index).split(count);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		ItemStack stack = this.getStackInSlot(index);
		this.setInventorySlotContents(index, ItemStack.EMPTY);
		this.markDirty();
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if(index < inv.size())
			this.inv.set(index, stack);
		else inv.add(stack);
	}

	@Override
	public boolean isUsableByPlayer(PlayerEntity player) {
		return true;
	}
	
	public void addItemStack(ItemStack stack) {
		this.markDirty();
		this.inv.add(stack);
	}
	
	public void sort() {
		Iterator<ItemStack> it = inv.iterator();
		while(it.hasNext()) {
			ItemStack stack = it.next();
			if(stack.isEmpty())
				it.remove();
		}
		if(!this.world.isRemote) {
			this.world.notifyBlockUpdate(this.getPos(), this.getBlockState(), this.getBlockState(), 3);
		}
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		
		ListNBT items = compound.getList("items", NBT.TAG_COMPOUND);
		for(INBT tag : items) {
			inv.add(ItemStack.read((CompoundNBT)tag));
		}
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		ListNBT list = new ListNBT();
		this.sort();
		for(ItemStack stack : inv) {
			list.add(stack.serializeNBT());
		}
		compound.put("items", list);
		return super.write(compound);
	}
	
}