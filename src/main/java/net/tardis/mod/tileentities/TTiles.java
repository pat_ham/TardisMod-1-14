package net.tardis.mod.tileentities;


import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Supplier;

import net.minecraft.block.Block;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.blocks.TileBlock;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.SteamConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.exteriors.ClockExteriorTile;
import net.tardis.mod.tileentities.exteriors.FortuneExteriorTile;
import net.tardis.mod.tileentities.exteriors.ModernPoliceBoxExteriorTile;
import net.tardis.mod.tileentities.exteriors.PoliceBoxExteriorTile;
import net.tardis.mod.tileentities.exteriors.RedExteriorTile;
import net.tardis.mod.tileentities.exteriors.SafeExteriorTile;
import net.tardis.mod.tileentities.exteriors.SteampunkExteriorTile;
import net.tardis.mod.tileentities.exteriors.TTCapsuleExteriorTile;
import net.tardis.mod.tileentities.exteriors.TrunkExteriorTile;
import net.tardis.mod.tileentities.machines.ArtronTapTile;
import net.tardis.mod.tileentities.monitors.MonitorTile;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TTiles {
	
	public static List<TileEntityType<?>> TYPES = new ArrayList<TileEntityType<?>>();
	
	public static TileEntityType<SteamConsoleTile> CONSOLE_STEAM = register(SteamConsoleTile::new, "console_steam", TBlocks.console_steam);
	public static TileEntityType<NemoConsoleTile> CONSOLE_NEMO = register(NemoConsoleTile::new, "console_nemo", TBlocks.console_nemo);
	public static TileEntityType<GalvanicConsoleTile> CONSOLE_GALVANIC = register(GalvanicConsoleTile::new, "console_galvanic", TBlocks.console_galvanic);
    public static TileEntityType<CoralConsoleTile> CONSOLE_CORAL = register(CoralConsoleTile::new, "coral", TBlocks.console_coral);
    public static TileEntityType<HartnelConsoleTile> CONSOLE_HARTNEL = register(HartnelConsoleTile::new, "hartnel", TBlocks.console_hartnel);
    public static TileEntityType<ArtDecoConsoleTile> CONSOLE_ART_DECO = register(ArtDecoConsoleTile::new, "art_deco", TBlocks.console_art_deco);
    public static TileEntityType<ToyotaConsoleTile> CONSOLE_TOYOTA = register(ToyotaConsoleTile::new, "toyota", TBlocks.console_toyota);

	public static TileEntityType<ClockExteriorTile> EXTERIOR_CLOCK = register(ClockExteriorTile::new, "exterior_clock", TBlocks.exterior_clock);
	public static TileEntityType<SteampunkExteriorTile> EXTERIOR_STEAMPUNK = register(SteampunkExteriorTile::new, "exterior_steampunk", TBlocks.exterior_steampunk);
	public static TileEntityType<TrunkExteriorTile> EXTERIOR_TRUNK = register(TrunkExteriorTile::new, "exterior_trunk", TBlocks.exterior_trunk);
	public static TileEntityType<RedExteriorTile> EXTERIOR_RED = register(RedExteriorTile::new, "exterior_red", TBlocks.exterior_red);
	public static TileEntityType<PoliceBoxExteriorTile> EXTERIOR_POLICE_BOX = register(PoliceBoxExteriorTile::new, "exterior_police_box", TBlocks.exterior_police_box);
	public static TileEntityType<FortuneExteriorTile> EXTEIROR_FORTUNE = register(FortuneExteriorTile::new, "exterior_fortune", TBlocks.exterior_fortune);
	public static TileEntityType<ModernPoliceBoxExteriorTile> EXTERIOR_MODERN_POLICE_BOX = register(ModernPoliceBoxExteriorTile::new, "exterior_modern_police_box", TBlocks.exterior_modern_police_box);
	public static TileEntityType<SafeExteriorTile> EXTERIOR_SAFE = register(SafeExteriorTile::new, "exterior_safe", TBlocks.exterior_safe);
	public static TileEntityType<TTCapsuleExteriorTile> EXTERIOR_TT_CAPSULE = register(TTCapsuleExteriorTile::new, "exterior_tt_capsule", TBlocks.exterior_tt_capsule);

	public static TileEntityType<MonitorTile> STEAMPUNK_MONITOR = register(MonitorTile::new, "steampunk_monitor", TBlocks.steampunk_monitor, TBlocks.eye_monitor, TBlocks.rca_monitor);
	public static TileEntityType<BrokenExteriorTile> BROKEN_TARDIS = register(BrokenExteriorTile::new, "broken_tardis", TBlocks.broken_exterior);
	public static TileEntityType<AlembicTile> ALEMBIC = register(AlembicTile::new, "alembic",TBlocks.alembic);
	public static TileEntityType<TardisEngineTile> ENGINE = register(TardisEngineTile::new, "engine", TBlocks.engine);
	public static TileEntityType<QuantiscopeTile> QUANTISCOPE = register(QuantiscopeTile::new, "quantiscope", TBlocks.quantiscope_brass, TBlocks.quantiscope_iron);
	public static TileEntityType<AntiGravityTile> ANTI_GRAV = register(AntiGravityTile::new, "anti_grav", TBlocks.anti_grav);
    public static TileEntityType<SquarenessChamelonTile> SQUARENESS = register(SquarenessChamelonTile::new, "squareness", TBlocks.squareness);
    public static TileEntityType<ShipComputerTile> SHIP_COMPUTER = register(ShipComputerTile::new, "ship_computer", TBlocks.ship_computer);
    public static TileEntityType<ReclamationTile> RECLAMATION_UNIT = register(ReclamationTile::new, "reclaimation_unit", TBlocks.reclamation_unit);
    public static TileEntityType<ArtronConverterTile> ARTRON_CONVERTER = register(ArtronConverterTile::new, "artron_converter", TBlocks.artron_converter);
    public static TileEntityType<ToyotaSpinnyTile> TOYOTA_SPIN = register(ToyotaSpinnyTile::new, "spinny_thing", TBlocks.spinny_block);
    public static TileEntityType<ArtronTapTile> ARTRON_TAP = register(ArtronTapTile::new, "artron_tap", TBlocks.artron_tap);
    
    public static TileEntityType<SuperStructureTile> SUPER_STRUCTURE = register(SuperStructureTile::new, "structure_block", TBlocks.structure_block);

	@SubscribeEvent
	public static void register(RegistryEvent.Register<TileEntityType<?>> event) {
		for(TileEntityType<?> type : TYPES) {
			event.getRegistry().register(type);
		}
	}

	public static <T extends TileEntity> TileEntityType<T> register(Supplier<T> tile, String name, Block... validBlock) {
		TileEntityType<T> type = TileEntityType.Builder.create(tile, validBlock).build(null);
		type.setRegistryName(Tardis.MODID, name);
		TYPES.add(type);
		for(Block block : validBlock) {
			if(block instanceof TileBlock) {
				((TileBlock)block).setTileEntity(type);
			}
		}
		return type;
	}

}
