package net.tardis.mod.tileentities.exteriors;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.goal.PrioritizedGoal;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.boti.BlockStore;
import net.tardis.mod.boti.IBotiEnabled;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.animation.IExteriorAnimation;
import net.tardis.mod.client.animation.IExteriorAnimation.ExteriorAnimationEntry;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.entity.ai.FollowIntoTardisGoal;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class ExteriorTile extends TileEntity implements ITickableTileEntity, IBotiEnabled, IEnergyStorage{
	
	private boolean locked = false;
	private EnumDoorState openState = EnumDoorState.CLOSED;
	//MUST contain at least one state
	private EnumDoorState[] validDoorStates = new EnumDoorState[]{
			EnumDoorState.CLOSED,
			EnumDoorState.ONE,
			EnumDoorState.BOTH
	};
	private DimensionType interiorDimension;
	private EnumMatterState matterState = EnumMatterState.SOLID;
	private List<UUID> teleportedIDs = new ArrayList<>();
	private String customName = "";
	private IExteriorAnimation animation;
	private EnergyStorage energyStorage;
	private WorldShell shell;
	private boolean crashed = false;
	
	private List<TexVariant> variants = new ArrayList<TexVariant>();
	private int variantIndex = 0;
	
	//Render variables
	public float alpha = 1F;
	public float lightLevel = 1F;
	
	public ExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
		animation = TardisRegistries.EXTERIOR_ANIMATIONS.getValue(new ResourceLocation(Tardis.MODID, "classic"))
				.create(this);
	}

	@Override
	public void tick() {
		
		this.transferEntities(world.getWorld().getEntitiesWithinAABB(Entity.class, this.getDoorAABB().offset(this.getPos())));

		if(this.matterState != EnumMatterState.SOLID) {
			this.animation.tick();
			
			//Teleport what we land on
			if(!world.isRemote) {
				for(Entity ent : world.getEntitiesWithinAABB(Entity.class, new AxisAlignedBB(this.getPos().down()).expand(0, 1, 0))) {
					ServerWorld serverWorld = world.getServer().getWorld(interiorDimension);
					if(serverWorld != null) {
						BlockPos pos = TardisHelper.TARDIS_POS.south(4);
						Helper.teleportEntities(ent, serverWorld, pos.getX(), pos.getY(), pos.getZ(), ent.rotationYaw, ent.rotationPitch);
					}
				}
			}
		}
		
		if(!world.isRemote && world.getBlockState(this.getPos().down(2)).isAir())
			this.fall();
		
		if(this.getMatterState() == EnumMatterState.DEMAT) {
			if(this.alpha <= 0.0) {
				this.deleteExteriorBlocks();
			}
		}
		
		else if(this.getMatterState() == EnumMatterState.REMAT) {
			if(alpha >= 1.0)
				this.setMatterState(EnumMatterState.SOLID);
		}
		
		if(!world.isRemote && this.interiorDimension != null && this.energyStorage == null) {
			world.getServer().getWorld(interiorDimension).getCapability(Capabilities.TARDIS_DATA)
				.ifPresent(cap -> this.energyStorage = cap.getEnergy());
		}
		
		this.pushPower();
		
		if(world.isRemote && this.crashed) {
			world.addParticle(ParticleTypes.LARGE_SMOKE, this.getPos().getX() + 0.5, this.getPos().getY() + 1, this.getPos().getZ() + 0.5, 0, 0.5, 0);
		}
		
		if(world.getGameTime() % 200 == 0)
			this.updateBoti();
	}
	
	//Functions
	
	public void deleteExteriorBlocks() {
		world.setBlockState(getPos(), Blocks.AIR.getDefaultState());
		world.setBlockState(getPos().down(), Blocks.AIR.getDefaultState());
	}
	
	public void updateClient() {
		if(!world.isRemote)
			world.notifyBlockUpdate(this.getPos(), this.world.getBlockState(getPos()), this.world.getBlockState(getPos()), 3);
	}

	public void open(Entity opening) {
		if(this.locked) {
			if(opening instanceof PlayerEntity)
				((PlayerEntity)opening).sendStatusMessage(ExteriorBlock.LOCKED, true);
			return;
		}
		
		this.setDoorState(this.getNextDoorState());
		this.setOther();
		if (getOpen().equals(EnumDoorState.CLOSED)) {
			world.playSound(null, getPos(), TSounds.DOOR_CLOSE, SoundCategory.BLOCKS, 0.5F, 1F);
		}
		else {
			world.playSound(null, getPos(), TSounds.DOOR_OPEN, SoundCategory.BLOCKS, 0.5F, 1F);
		}
	}
	
	public void demat() {
		this.setMatterState(EnumMatterState.DEMAT);
		this.setDoorState(EnumDoorState.CLOSED);
        this.alpha = 1.0F;
        this.animation.reset();
		
	}
	
	public void remat() {
		this.setMatterState(EnumMatterState.REMAT);
		this.setDoorState(EnumDoorState.CLOSED);
		this.alpha = 0.0F;
        this.animation.reset();
	}
	
	public boolean isKeyValid(ItemStack stack) {
		return stack.hasTag() &&
				stack.getTag().contains("tardis_key") &&
				Helper.getPlayerFromTARDIS(interiorDimension).toString().equals(stack.getTag().getString("tardis_key"));
	}
	
	public void setOther() {
		if(!world.isRemote) {
			ServerWorld server = world.getServer().getWorld(interiorDimension);
			if(server != null) {
				TileEntity te = server.getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile) {
					ChunkPos pos = server.getChunk(TardisHelper.TARDIS_POS).getPos();
					for(int x = -3; x < 3; ++x) {
						for(int z = -3; z < 3; ++z) {
							server.forceChunk(pos.x + x, pos.z + z, true);
						}
					}
					((ConsoleTile)te).getDoor().ifPresent(door -> {
						door.setOpenState(this.getOpen());
						door.setLocked(this.locked);
						door.world.playSound(null, door.getPosition(), door.getOpenState().equals(EnumDoorState.CLOSED)  ? TSounds.DOOR_CLOSE : TSounds.DOOR_OPEN, SoundCategory.BLOCKS, 0.5F, 1F);
						door.world.playSound(null, door.getPosition(), door.isLocked() ? TSounds.DOOR_LOCK : TSounds.DOOR_UNLOCK, SoundCategory.BLOCKS, 0.5F, 1F);
					});
				}
			}
		}
	}
	
	public void transferEntities(List<Entity> entityList) {
		if(!world.isRemote && this.getOpen() != EnumDoorState.CLOSED) {
			if(this.interiorDimension == null)
				return;
			
			//Stop if no entities to move
			
			List<UUID> tempIDs = new ArrayList<>();
			for(Entity e : entityList) {
				if(this.teleportedIDs.contains(e.getUniqueID())) {
					tempIDs.add(e.getUniqueID());
				}
			}
			this.teleportedIDs = tempIDs;
			
			if(entityList.isEmpty())
				return;
			
			double x = 0, y = TardisHelper.TARDIS_POS.getY(), z = 0;
			ConsoleTile console = null;
			ServerWorld ws = this.world.getServer().getWorld(this.interiorDimension);
			
			//Get Console
			if(ws != null) {
				TileEntity te = ws.getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile)
					console = (ConsoleTile)te;
			}
			
			//If an interior door exists, put the player near it
			DoorEntity door = console != null ? console.getDoor().orElse(null) : null;
			
			if(door != null) {
				x = door.posX;
				z = door.posZ;
				y = door.posY;
			}
			
			for(Entity e : entityList) {
				
				if(this.teleportedIDs.contains(e.getUniqueID()) || e.removed)
					break;
				
				if(door != null) {
					e.rotationYaw = door.rotationYaw - 180;
					door.addEntityToTeleportedList(e.getUniqueID());
				}
				
				Helper.teleportEntities(e, ws, x, y, z, door != null ? (door.rotationYaw - 180) : 0, e.rotationPitch);
				
				//Follow into TARDIS
				if(e instanceof PlayerEntity) {
					for(MonsterEntity ent : world.getEntitiesWithinAABB(MonsterEntity.class, new AxisAlignedBB(this.getPos()).grow(20))) {
						if(ent.getAttackTarget() == e) {
							for(PrioritizedGoal goal : ent.goalSelector.goals) {
								if(goal.getGoal() instanceof FollowIntoTardisGoal) {
									((FollowIntoTardisGoal)goal.getGoal()).setTarget(this.getPos());
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void pushPower() {
		for(Direction dir : Direction.values()) {
			TileEntity te = world.getTileEntity(getPos().offset(dir));
			if(te != null)
				te.getCapability(CapabilityEnergy.ENERGY, dir.getOpposite()).ifPresent(cap -> {
					int power = this.getEnergyStored();
					int accepted = cap.receiveEnergy(power, false);
					this.extractEnergy(accepted, false);
				});
		}
	}
	
	public void addTeleportedEntity(UUID id) {
		this.teleportedIDs.add(id);
	}
	
	public EnumDoorState getNextDoorState() {
		int index = 0;
		for(EnumDoorState state : this.validDoorStates) {
			if(state == this.openState) {
				if(index + 1 < this.validDoorStates.length) 
					return this.validDoorStates[index + 1];
				 else return this.validDoorStates[0];
			}
			++index;
		}
		return EnumDoorState.CLOSED;
	}
	
	//Getters and setters
	
	public void setDoorState(EnumDoorState state) {
		this.openState = state;
		this.markDirty();
		this.updateClient();
	}
	
	public EnumDoorState getOpen() {
		return this.openState;
	}
	
	public void setLocked(boolean locked) {
		this.locked = locked;
		this.markDirty();
	}
	
	public boolean getLocked() {
		return this.locked;
	}
	
	public void toggleLocked() {
		this.locked = !this.locked;
		if(locked) {
			this.setDoorState(EnumDoorState.CLOSED);
			this.setOther();
		}
	}
	
	public void setInterior(DimensionType type) {
		this.interiorDimension = type;
		this.markDirty();
	}
	
	public DimensionType getInterior() {
		return this.interiorDimension;
	}

	public EnumMatterState getMatterState() {
		return this.matterState;
	}
	
	public void setMatterState(EnumMatterState state) {
		this.matterState = state;
		this.markDirty();
		if(state != EnumMatterState.SOLID)
			this.setDoorState(EnumDoorState.CLOSED);
		this.updateClient();
	}
	
	public void setLightLevel(float percent) {
		this.lightLevel = percent;
		this.markDirty();
		this.updateClient();
	}
	
	public float getLightLevel() {
		return this.lightLevel;
	}
	
	public String getCustomName() {
		return this.customName;
	}
	
	public void setCustomName(String name) {
		this.customName = name;
		this.markDirty();
		this.updateClient();
		
		if(!world.isRemote) {
			TardisHelper.getConsole(world.getServer(), this.interiorDimension)
				.ifPresent(tile -> tile.setCustomName(name));
		}
	}
	
	public void updateConsoleName() {
		if(!world.isRemote) {
			TardisHelper.getConsole(world.getServer(), this.interiorDimension)
				.ifPresent(tile -> tile.setCustomName(getCustomName()));
		}
	}
	
	public IExteriorAnimation getExteriorAnimation() {
		return this.animation;
	}
	
	public void setExteriorAnimation(ExteriorAnimationEntry<?> anim) {
		this.animation = anim.create(this);
		this.markDirty();
		this.updateClient();
	}
	
	//Scrape the necessary Data, including dimension from console
	public void copyConsoleData(ConsoleTile console) {
		this.interiorDimension = console.getWorld().getDimension().getType();
		
		//Set door stuff
		this.locked = false;
		this.openState = EnumDoorState.CLOSED;
		
		//Override if door exists
		console.getDoor().ifPresent(ent -> {
			this.locked = ent.isLocked();
			this.openState = ent.getOpenState();
		});
		
		this.customName = console.getCustomName();
        ExteriorAnimationEntry<?> entry = TardisRegistries.EXTERIOR_ANIMATIONS.getValue(console.getExteriorManager().getExteriorAnimation());
        if (entry != null)
            this.animation = entry.create(this);
		this.lightLevel = console.getInteriorManager().getLight() / 15.0F;
		this.variantIndex = console.getExteriorManager().getExteriorVariant();
		this.updateClient();
		
	}
	
	@Override
	public WorldShell getBotiWorld() {
		return this.shell;
	}

	@Override
	public void setBotiWorld(WorldShell shell) {
		this.shell = shell;
	}
	
	public void updateBoti() {
		this.shell = new WorldShell(this.getPos());
		int radius = 10;
		for(int x = -radius; x < radius; ++x) {
			for(int y = -radius; y < radius; ++y) {
				for(int z = -radius; z < radius; ++z) {
					shell.put(this.getPos().add(x, 0, z), new BlockStore(TBlocks.atrium_block.getDefaultState(), 15, null));
				}
			}
		}
	}

	public void setVaraints(TexVariant... variants) {
		this.variants.clear();
		for(TexVariant t : variants) {
			this.variants.add(t);
		}
	}
	
	public void setVariant(int i) {
		this.variantIndex = i < this.variants.size() ? i : 0;
		this.markDirty();
		this.updateClient();
	}
	
	@Nullable
	public TexVariant getVariant() {
		return this.variantIndex < this.variants.size() ? this.variants.get(variantIndex) : null;
	}
	
	/*
	 * If an entity is inside this it will be transfered to the TARDIS
	 */
	public abstract AxisAlignedBB getDoorAABB();
	
	//Required Minecraft shit
	
	@Override
	public void read(CompoundNBT compound) {
		this.locked = compound.getBoolean("locked");
		this.openState = EnumDoorState.valueOf(compound.getString("state"));
		if(compound.contains("interior"))
			this.interiorDimension = DimensionType.byName(new ResourceLocation(compound.getString("interior")));
		if(compound.contains("matter_state"))
			this.matterState = EnumMatterState.values()[compound.getInt("matter_state")];
		this.lightLevel = compound.getFloat("light_level");
		this.customName = compound.getString("custom_name");
		this.animation = TardisRegistries.EXTERIOR_ANIMATIONS.getValue(new ResourceLocation(compound.getString("animation")))
				.create(this);
		this.variantIndex = compound.getInt("variant_index");
		super.read(compound);
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putBoolean("locked", this.locked);
		compound.putString("state", this.openState.name());
		if(this.interiorDimension != null)
			compound.putString("interior", this.interiorDimension.getRegistryName().toString());
		compound.putInt("matter_state", this.matterState.ordinal());
		compound.putFloat("light_level", this.lightLevel);
		compound.putString("custom_name", customName);
		compound.putString("animation", this.animation.getType().getRegistryName().toString());
		compound.putInt("variant_index", this.variantIndex);
		return super.write(compound);
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.getUpdateTag());
	}

	@Override
	public CompoundNBT getUpdateTag() {
		CompoundNBT tag = this.serializeNBT();
		tag.putFloat("alpha", this.alpha);
		tag.putInt("door_state", this.openState.ordinal());
		return tag;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.openState = EnumDoorState.values()[pkt.getNbtCompound().getInt("door_state")];
		this.locked = pkt.getNbtCompound().getBoolean("locked");
		this.matterState = EnumMatterState.values()[pkt.getNbtCompound().getInt("matter_state")];
		this.alpha = pkt.getNbtCompound().getFloat("alpha");
		this.lightLevel = pkt.getNbtCompound().getFloat("light_level");
		this.customName = pkt.getNbtCompound().getString("custom_name");
		this.animation = TardisRegistries.EXTERIOR_ANIMATIONS.getValue(new ResourceLocation(pkt.getNbtCompound().getString("animation")))
				.create(this);
		this.variantIndex = pkt.getNbtCompound().getInt("variant_index");
	}

	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		super.handleUpdateTag(tag);
		this.alpha = tag.getFloat("alpha");
	}
	
	//Power
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityEnergy.ENERGY ? LazyOptional.of(() -> (T)this) : super.getCapability(cap, side);
	}

	@Override
	public int receiveEnergy(int maxReceive, boolean simulate) {
		return this.getEnergy().receiveEnergy(maxReceive, simulate);
	}

	@Override
	public int extractEnergy(int maxExtract, boolean simulate) {
		return this.getEnergy().extractEnergy(maxExtract, simulate);
	}

	@Override
	public int getEnergyStored() {
		return this.getEnergy().getEnergyStored();
	}

	@Override
	public int getMaxEnergyStored() {
		return this.getEnergy().getMaxEnergyStored();
	}

	@Override
	public boolean canExtract() {
		return this.getEnergy().canExtract();
	}

	@Override
	public boolean canReceive() {
		return this.getEnergy().canReceive();
	}
	
	public EnergyStorage getEnergy() {
		return this.energyStorage == null ? new EnergyStorage(1) : this.energyStorage;
	}

	@Override
	public double getMaxRenderDistanceSquared() {
		return 16384.0;
	}
	
	@Nullable
	public TardisEntity createEntity() {
		if(!world.isRemote) {
			TardisEntity entity = TEntities.TARDIS.create(world);
			TardisHelper.getConsole(world.getServer(), interiorDimension).ifPresent(tile -> entity.setConsole(tile));
			entity.setPosition(this.getPos().getX() + 0.5, this.getPos().getY() - 1, this.getPos().getZ() + 0.5);
			entity.rotationYaw = Helper.getAngleFromFacing(this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING)) - 180;
			return entity;
		}
		return null;
	}
	
	public void fall() {
		TardisEntity ent = this.createEntity();
		if(ent != null) {
			ent.setExteriorTile(this);
			if(world.addEntity(ent))
				this.deleteExteriorBlocks();
		}
		
	}

	public void setCrashed(boolean crashed) {
		this.crashed = crashed;
		this.updateClient();
		this.markDirty();
	}
	
}
