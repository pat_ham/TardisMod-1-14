package net.tardis.mod.tileentities;

import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.tardis.mod.blocks.ArtronConverterBlock;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.helper.TardisHelper;

public class ArtronConverterTile extends TileEntity implements ITickableTileEntity{

	private static int TRANSFER = 10;
	
	private EnergyStorage energy = new EnergyStorage(256);
	
	private ConsoleTile tile;
	
	public ArtronConverterTile() {
		super(TTiles.ARTRON_CONVERTER);
	}

	@Override
	public void tick() {
		this.getCapability(CapabilityEnergy.ENERGY).ifPresent(cap -> {
			
			if(tile == null)
				return;
			
			if(this.getBlockState().get(ArtronConverterBlock.EMIT)) {
				if(tile.getArtron() < tile.getMaxArtron()) {
					
					//float rate = TConfig.CONFIG.artron_convert;
					
					int amt = cap.extractEnergy(1000, false);
					if(amt > 0) {
						tile.setArtron(tile.getArtron() + (amt / 1000.0F));
						tile.updateClient();
					}
				}
			}
			else {
				//Convert Artron to FE
				if(tile != null && tile.getArtron() > 1) {
					int amt = cap.receiveEnergy(100, false);
					tile.setArtron(tile.getArtron() - (amt / 100.0F));
				}
				
				//Push FE to valid tiles
				for(Direction dir : Direction.values()) {
					TileEntity te = world.getTileEntity(getPos().offset(dir));
					if(te != null)
						te.getCapability(CapabilityEnergy.ENERGY, dir.getOpposite()).ifPresent(power -> {
							int amt = cap.extractEnergy(TRANSFER, false);
							power.receiveEnergy(amt, false);
						});
				}
			}
			
		});
		
		if(tile == null || tile.isRemoved())
			TardisHelper.getConsoleInWorld(world).ifPresent(console -> this.tile = console);
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityEnergy.ENERGY ? LazyOptional.of(() -> (T)this.energy) : super.getCapability(cap, side);
	}

}
