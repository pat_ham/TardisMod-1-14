package net.tardis.mod.tileentities.console.misc;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class TraitTag implements IRegisterable<TraitTag>{

	private ResourceLocation key;
	
	private int mood = 0;
	private int loyalty = 0;
	private double weight = 1.0;
	
	public int getMoodMod() {
		return mood;
	}
	
	public int getLoyaltyMod() {
		return this.loyalty;
	}
	
	public void setMoodMod(int mood) {
		this.mood = mood;
	}
	
	public void setLoyaltyMod(int loy) {
		this.loyalty = loy;
	}
	
	public double getWeight() {
		return this.weight;
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	abstract void onTardisAction(TardisAction act, ConsoleTile tile);

	@Override
	public TraitTag setRegistryName(ResourceLocation regName) {
		this.key = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.key;
	}
	
}
