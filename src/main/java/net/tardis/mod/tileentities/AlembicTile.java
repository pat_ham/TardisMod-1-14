package net.tardis.mod.tileentities;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tags.TardisItemTags;

public class AlembicTile extends TileEntity implements ITickableTileEntity, IInventory, IItemHandlerModifiable{

	public static final int MAX_TIME = 200;
	private ItemStackHandler handler = new ItemStackHandler(6);
	private FluidTank waterTank = new FluidTank(1000, stack -> stack.getFluid() == Fluids.WATER);
	private int progress = 0;
	private int burnTime = 0;
	private int maxBurnTime = 1;
	private int mercury = 0;
	
	public AlembicTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public AlembicTile() {
		super(TTiles.ALEMBIC);
	}

	@Override
	public void tick() {
		if (!world.isRemote) {
		if(this.burnTime > 0) {
			--this.burnTime;
		}
		
		//Consume Fuel
		if(this.burnTime <= 0 && !this.getStackInSlot(2).isEmpty() && isFuel(this.getStackInSlot(3))) {
			this.burnTime = this.maxBurnTime = ForgeHooks.getBurnTime(this.getStackInSlot(3));
			this.getStackInSlot(3).shrink(1);
			this.markDirty();
		}
		
		//If has fuel and has something to process
		int emptySpace = 1000 - this.mercury;
		if(this.burnTime > 0 && this.getStackInSlot(2).getItem().isIn(TardisItemTags.CINNABAR) && waterTank.getFluidAmount() > 0 && emptySpace > 0) {
			++progress;
			world.setBlockState(pos, this.getBlockState().with(BlockStateProperties.ENABLED, true), 2);
			this.markDirty();
			//If it's done
			if(this.progress >= MAX_TIME) {
				this.progress = 0;
				this.getStackInSlot(2).shrink(1);
				
				int amt = 1000 - this.mercury;
				waterTank.drain(amt, FluidAction.EXECUTE);
				this.mercury += amt;
				world.playSound(null, getPos(), SoundEvents.BLOCK_BREWING_STAND_BREW, SoundCategory.AMBIENT, 0.25F, 1F);
				world.setBlockState(pos, this.getBlockState().with(BlockStateProperties.ENABLED, false), 2);
				this.markDirty();
			}
		}
		else this.progress = 0;
		
		if(this.mercury >= 333 && this.getStackInSlot(4).getItem() == Items.GLASS_BOTTLE) {
			this.mercury -= 333;
			this.getStackInSlot(4).shrink(1);
			this.handler.insertItem(5, new ItemStack(TItems.MERCURY_BOTTLE), false);
		}
		
		this.getStackInSlot(0).getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY).ifPresent(cap -> {
			FluidStack fluid = cap.getFluidInTank(0);
			if(fluid.getFluid() == Fluids.WATER) {
				int amtToTake = this.waterTank.getSpace();
				if(amtToTake > 0) {
					ItemStack stack = cap.getContainer();
					this.setInventorySlotContents(0, this.handler.insertItem(1, stack.getContainerItem(), false));
					this.waterTank.fill(cap.drain(amtToTake, FluidAction.EXECUTE), FluidAction.EXECUTE);
					this.markDirty();
				}
			}
		});
		}
	}

	@Override
	public void clear() {
		for(int i = 0; i < handler.getSlots(); ++i) {
			handler.setStackInSlot(i, ItemStack.EMPTY);
		}
		this.markDirty();
	}

	@Override
	public int getSizeInventory() {
		return handler.getSlots();
	}

	@Override
	public boolean isEmpty() {
		for(int i = 0; i < handler.getSlots(); ++i) {
			if(!handler.getStackInSlot(i).isEmpty())
				return false;
		}
		return true;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return handler.getStackInSlot(index);
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		this.markDirty();
		return handler.getStackInSlot(index).split(count);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		ItemStack stack = handler.getStackInSlot(index);
		handler.setStackInSlot(index, ItemStack.EMPTY);
		this.markDirty();
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		handler.setStackInSlot(index, stack);
	}

	@Override
	public boolean isUsableByPlayer(PlayerEntity player) {
		return true;
	}
	
	public FluidTank getWaterTank() {
		return this.waterTank;
	}
	
	public int getMercury() {
		return this.mercury;
	}
	
	public int getBurnTime() {
		return this.burnTime;
	}
	
	public int getMaxBurnTime() {
		return this.maxBurnTime;
	}
	
	public float getPercent() {
		return this.progress / 200.0F;
	}
	
	public static boolean isFuel(ItemStack stack) {
	      return ForgeHooks.getBurnTime(stack) > 0;
	   }
	
	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		this.burnTime = compound.getInt("burn_time");
		this.maxBurnTime = compound.getInt("max_burn_time");
		this.progress = compound.getInt("progress");
		this.mercury = compound.getInt("mercury");
		this.waterTank.readFromNBT(compound.getCompound("water_tank"));
		this.handler.deserializeNBT(compound.getCompound("inv_handler"));
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putInt("burn_time", this.burnTime);
		compound.putInt("max_burn_time", this.maxBurnTime);
		compound.putInt("progress", this.progress);
		compound.putInt("mercury", this.mercury);
		compound.put("water_tank", this.waterTank.writeToNBT(new CompoundNBT()));
		compound.put("inv_handler", this.handler.serializeNBT());
		return super.write(compound);
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.serializeNBT());
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		super.handleUpdateTag(tag);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.write(new CompoundNBT());
	}

	@Override
	public int getSlots() {
		return this.handler.getSlots();
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
		return this.handler.insertItem(slot, stack, simulate);
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate) {
		return this.handler.extractItem(slot, amount, simulate);
	}

	@Override
	public int getSlotLimit(int slot) {
		return this.handler.getSlotLimit(slot);
	}

	@Override
	public boolean isItemValid(int slot, ItemStack stack) {
		return this.handler.isItemValid(slot, stack);
	}

	@Override
	public void setStackInSlot(int slot, ItemStack stack) {
		this.handler.setStackInSlot(slot, stack);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? LazyOptional.of(() -> (T)this) : super.getCapability(cap, side);
	}
	
}
