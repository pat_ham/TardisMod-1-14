package net.tardis.mod.tileentities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.Constants.Translations;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.controls.ControlRegistry.ControlEntry;
import net.tardis.mod.controls.IControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.StabilizerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.events.TardisEvent;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.flight.FlightEvent;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.LandingSystem;
import net.tardis.mod.items.ArtronCapacitorItem;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.MissControlMessage;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.sounds.ISoundScheme;
import net.tardis.mod.sounds.SoundSchemeBasic;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemEntry;
import net.tardis.mod.tileentities.console.misc.EmotionHandler;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.tileentities.console.misc.ExteriorPropertyManager;
import net.tardis.mod.tileentities.console.misc.InteriorManager;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;
import net.tardis.mod.upgrades.Upgrade;
import net.tardis.mod.upgrades.UpgradeEntry;

public class ConsoleTile extends TileEntity implements ITickableTileEntity{
	
	private static final AxisAlignedBB CONRTROL_HITBOX = new AxisAlignedBB(-1, 0, -1, 2, 2, 2);
	public static final int TARDIS_MAX_SPEED = 10;
	//Ten AU a tick at max
	public static final float BASIC_FUEL_USEAGE = 1;
	public static Random rand = new Random();
	
	public int flightTicks = 0;
	private int maxFlightTicks = 0;
	private EmotionHandler emotionHandler;
	private InteriorManager interiorManager;
	private List<ITickable> tickers = new ArrayList<ITickable>();
	private HashMap<ResourceLocation, INBTSerializable<CompoundNBT>> dataHandlers = new HashMap<ResourceLocation, INBTSerializable<CompoundNBT>>();
	private ArrayList<ControlEntity> controls = new ArrayList<ControlEntity>();
	private ArrayList<ControlEntry<?>> controlEntries = new ArrayList<ControlEntry<?>>();
	private ArrayList<IExterior> unlockedExteriors = new ArrayList<IExterior>();
	private ArrayList<ConsoleRoom> unlockedInteriors = new ArrayList<ConsoleRoom>();
	private IExterior exterior;
	private ISoundScheme scheme;
	private BlockPos location = BlockPos.ZERO;
	private BlockPos destination = BlockPos.ZERO;
	private DimensionType dimension;
	private DimensionType destinationDimension;
	private Direction facing = Direction.NORTH;
	public int coordIncr = 10;
	private float max_artron = 0;
	private float artron = 0;
	private float rechargeMod = 1F;
	private ConsoleRoom consoleRoom = ConsoleRoom.STEAM;
	private List<Subsystem> subsystems = new ArrayList<>();
	private List<Upgrade> upgrades = new ArrayList<>();
	private String customName = "";
	private ExteriorPropertyManager exteriorProps;
	private NonNullList<SpaceTimeCoord> waypoints = NonNullList.withSize(16, SpaceTimeCoord.UNIVERAL_CENTER);
	private SpaceTimeCoord returnLocation = SpaceTimeCoord.UNIVERAL_CENTER;
	private HashMap<UUID, BlockPos> bedPositions = new HashMap<UUID, BlockPos>();
	private List<Class<? extends IControl>> avoidControls = new ArrayList<Class<? extends IControl>>();
	private FlightEvent currentEvent = null;
	private List<SpaceTimeCoord> distressSignal = new ArrayList<SpaceTimeCoord>();
	private ItemStack sonic = ItemStack.EMPTY;
	protected TexVariant[] variants = {};
	private int variant = 0;
	
	//What ever you do do not save / sync this (If you use onLoad() the world won't load)
	private int timeUntilControlSpawn = 10;
	
	//Last player to interact with a control, used for loyalty
	private PlayerEntity pilot;

	public ConsoleTile(TileEntityType<?> type) {
		super(type);
		this.emotionHandler = new EmotionHandler(this);
		this.interiorManager = new InteriorManager(this);
		this.exteriorProps = new ExteriorPropertyManager(this);
		this.exterior = ExteriorRegistry.STEAMPUNK;
		this.dimension = DimensionType.OVERWORLD;
		this.destinationDimension = DimensionType.OVERWORLD;
		this.unlockedExteriors = ExteriorRegistry.getDefaultExteriors();
		//TODO: Add a sound scheme registry
		this.scheme = new SoundSchemeBasic();
		this.registerControlEntry(ControlRegistry.DEMAT);
		this.registerControlEntry(ControlRegistry.THROTTLE);
		this.registerControlEntry(ControlRegistry.RANDOM);
		this.registerControlEntry(ControlRegistry.DIMENSION);
		this.registerControlEntry(ControlRegistry.FACING);
		this.registerControlEntry(ControlRegistry.X);
		this.registerControlEntry(ControlRegistry.Y);
		this.registerControlEntry(ControlRegistry.Z);
		this.registerControlEntry(ControlRegistry.INC_MOD);
		this.registerControlEntry(ControlRegistry.LAND_TYPE);
		this.registerControlEntry(ControlRegistry.REFUELER);
		this.registerControlEntry(ControlRegistry.FAST_RETURN);
		this.registerControlEntry(ControlRegistry.TELEPATHIC);
		this.registerControlEntry(ControlRegistry.STABILIZERS);
		this.registerControlEntry(ControlRegistry.SONIC_PORT);
		this.registerControlEntry(ControlRegistry.COMMUNICATOR);
		this.registerControlEntry(ControlRegistry.DOOR);
		
		for(SubsystemEntry<?> entry : TardisRegistries.SUBSYSTEM_REGISTRY.getRegistry().values()) {
			this.subsystems.add(entry.create(this));
		}
		
		for(UpgradeEntry<?> entry : TardisRegistries.UPGRADES.getRegistry().values()) {
			this.upgrades.add(entry.create(this));
		}
		
		for(ConsoleRoom r : ConsoleRoom.REGISTRY.values()) {
			if(r.isDefault())
				this.unlockedInteriors.add(r);
		}
		
	}
	
	//Fly loop, called every tick
	public void fly() {
		//This is not a duplicate of isInFlight(), I have a plan
		//If this is true, this is a "real flight", as in it's going somewhere
		if(this.isInFlight()) {
			
			--this.flightTicks;
			if(!world.isRemote && this.flightTicks == 0)
				land();
			
			if(!world.isRemote && !this.canFly())
				crash();
			
			//Artron usage
			this.artron -= this.calcFuelUse();

			if (this.flightTicks % 20 == 0) {
				for (Subsystem sub : this.getSubSystems()) {
					sub.onFlightSecond();
				}
				for (Upgrade up : this.getUpgrades()) {
					up.onFlightSecond();
				}
			}

			if (!world.isRemote && !this.getControl(StabilizerControl.class).isStabilized()) {
				int interval = (int)(100 + (((1.0F - this.getControl(ThrottleControl.class).getAmount())) * 100));
				if (this.flightTicks % interval == 0 && this.flightTicks > 10 && this.flightTicks < this.maxFlightTicks - 10) {
					if (!this.avoidControls.isEmpty()) {
						//Boom -boom
						world.playSound(null, this.getPos(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 0.5F, 1F);
						for (LivingEntity ent : this.world.getEntitiesWithinAABB(LivingEntity.class, new AxisAlignedBB(this.getPos()).grow(20))) {
							ent.setMotion(ent.getMotion().add(rand.nextDouble() - 0.5, rand.nextDouble(), rand.nextDouble() - 0.5));
							if (ent instanceof ServerPlayerEntity) {
								Network.sendTo(new MissControlMessage(), (ServerPlayerEntity) ent);
							}
						}
						this.getEmotionHandler().setMood(this.getEmotionHandler().getMood() - 10);
						this.getEmotionHandler().setLoyalty(this.getEmotionHandler().getLoyalty() - 5);
						for(Subsystem s : this.subsystems) {
							s.explode(true);
						}
						this.destination = this.randomizeCoords(destination, 20);
						this.currentEvent.getCrashAction().onCrash(this);
						this.avoidControls.clear();
					}

					this.currentEvent = FlightEvent.getRandomEvent(ConsoleTile.rand);

					this.avoidControls.addAll(currentEvent.getControls());
					for (PlayerEntity ent : this.world.getEntitiesWithinAABB(PlayerEntity.class, new AxisAlignedBB(this.getPos()).grow(20))) {
						ent.sendStatusMessage(currentEvent.getTranslation(), true);
					}
				}
			}
			
			//shake
			if(!world.isRemote) {
				if(!this.avoidControls.isEmpty())
					for(PlayerEntity player : world.getPlayers()) {
						player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
							cap.setShaking(5);
							cap.update();
						});
					}
			}
		}

		this.playFlightLoop();
		this.playLandSound();
	}
	
	//Landing code, handles exiting flight and seting up the exterior
	public void land() {
		
		this.flightTicks = this.maxFlightTicks = 0;
		
		if(!world.isRemote) {
			
			MinecraftForge.EVENT_BUS.post(new TardisEvent.Land(this));
			
			ServerWorld ws = world.getServer().getWorld(this.destinationDimension);
			this.dimension = this.destinationDimension;

			//Clear the flight mini-game controls
			this.avoidControls.clear();
			
			//Emotional- induced inaccuracy
			if(this.getEmotionHandler() != null && this.getEmotionHandler().getMood() < EnumHappyState.APATHETIC.getTreshold()) {
				this.destination = this.randomizeCoords(this.destination, 100);
			}
			
			//World border- redirect
			if(!ws.getWorldBorder().contains(this.destination)) {
				BlockPos pos = ws.getSpawnPoint();
				if(pos == null)
					pos = BlockPos.ZERO;
				this.destination = this.randomizeCoords(ws.getHeight(Type.MOTION_BLOCKING_NO_LEAVES, pos), 50);
				for(PlayerEntity ent : world.getEntitiesWithinAABB(PlayerEntity.class, new AxisAlignedBB(this.getPos()).grow(30))) {
					ent.sendStatusMessage(Translations.OUTSIDE_BORDER, true);
				}
			}

			//Get landing type, up or down
			EnumLandType landType = EnumLandType.DOWN;
			LandingTypeControl landControl = this.getControl(LandingTypeControl.class);
			if(landControl != null)
				landType = landControl.getLandType();
			
			
			BlockPos landSpot = LandingSystem.getLand(ws, destination, landType, this);
			if(landSpot.equals(BlockPos.ZERO)) {
				for(int i = 0; i < 30; ++i) {
					 if(landSpot.equals(BlockPos.ZERO))
						 landSpot = LandingSystem.getLand(ws, this.randomizeCoords(destination, 30), landType, this);
					 else break;
				}
			}
			if(landSpot.equals(BlockPos.ZERO))
				landSpot = this.destination;
			
			this.location = this.destination = landSpot.toImmutable();
			
			this.exterior.remat(this);
			this.getSoundScheme().playExteriorLand(this);
			
			//Name the exterior
			if(this.exterior.getExterior(this) != null) {
				ExteriorTile ext = this.exterior.getExterior(this);
				ext.setCustomName(getCustomName());
			}
			this.getControl(ThrottleControl.class).setAmount(0.0F);
			this.updateClient();
		}

		for (Subsystem sub : this.getSubSystems()) {
			sub.onLand();
		}
		for (Upgrade up : this.getUpgrades()) {
			up.onLand();
		}
	}
	
	//Take off, removes exterior, sets up flight and starts flight loop
	public boolean takeoff() {
		if(this.isInFlight())
			return false;
		if(!this.canFly()) {
			this.world.playSound(null, this.getPos(), TSounds.CANT_START, SoundCategory.BLOCKS, 1F, 1F);
			return false;
		}
		MinecraftForge.EVENT_BUS.post(new TardisEvent.Takeoff(this));

		this.avoidControls.clear();
		
		this.returnLocation = new SpaceTimeCoord(this.getDimension(), this.getLocation(), this.getExteriorDirection());
		
		this.getEmotionHandler().addMood(10);
		this.getEmotionHandler().addLoyaltyIfOwner(1, this.getPilot());
		this.maxFlightTicks = this.calcFlightTicks() +
				this.getSoundScheme().getLandTime() +
				this.getSoundScheme().getTakeoffTime();
		this.flightTicks = this.maxFlightTicks;
		this.exterior.demat(this);
		this.scheme.playTakeoffSounds(this);
		System.out.println("TARDIS: Started a flight to last " + this.maxFlightTicks);
		this.getControl(RefuelerControl.class).setRefuling(false);
		
		
		this.updateClient();

		for (Subsystem sub : this.getSubSystems()) {
			sub.onTakeoff();
		}
		for (Upgrade up : this.getUpgrades()) {
			up.onTakeoff();
		}
		
		//Shake player's screens
		if(!world.isRemote) {
			for(PlayerEntity player : world.getPlayers()) {
				player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
					cap.setShaking(this.getSoundScheme().getTakeoffTime());
					cap.update();
				});
			}
		}
		
		return true;
	}
	
	public void initLand() {
		this.scaleDestination();
		this.flightTicks = this.getSoundScheme().getLandTime() + 1;
		this.updateClient();
	}
	
	//Violently fall out of flight
	public void crash() {
		world.playSound(null, this.getPos(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 1F, 0.25F);
		this.destination = this.randomizeCoords(this.destination, 50);
		this.land();
		Tardis.proxy.shutTheFuckUp(null, SoundCategory.BLOCKS);
		ExteriorTile tile = this.getExterior().getExterior(this);
		if(tile != null) {
			//TODO: Spawn particles
			tile.setCrashed(true);
		}
	}
	
	//Attacked
	public void damage(float damage) {
		int dam = (int)Math.ceil(damage / this.subsystems.size());
		for(Subsystem sub : this.getSubSystems()) {
			sub.damage(null, dam);
		}
	}
	
	public boolean isInFlight() {
		return this.flightTicks > 0;
	}
	
	public int getTimeLeft() {
		return this.flightTicks;
	}
	
	public void setDestination(DimensionType type, BlockPos pos) {
		this.destination = pos.toImmutable();
		this.destinationDimension = type;
		this.markDirty();
		if(this.isInFlight())
			this.updateFlightTime();
		this.updateClient();
	}
	
	public void setConsoleRoom(ConsoleRoom room) {
		this.consoleRoom = room;
		this.markDirty();
		this.updateClient();
	}
	
	public void playFlightLoop() {
		if(!world.isRemote) {
			if(this.flightTicks % this.scheme.getLoopTime() == 0 && this.flightTicks > this.scheme.getTakeoffTime())
				this.scheme.playFlightLoop(this);
		}
	}
	
	private void playLandSound() {
		if(!world.isRemote && this.scheme.getLandTime() == this.flightTicks) {
			this.scheme.playInteriorLand(this);
			
			for(PlayerEntity player : world.getPlayers()) {
				player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
					cap.setShaking(this.getSoundScheme().getLandTime());
					cap.update();
				});
			}
		}
	}
	
	private void playAmbiantNoises() {
		if(!world.isRemote && this.getInteriorManager().isAlarmOn() && this.world.getGameTime() % 70 == 0) {
			world.playSound(null, this.getPos(), TSounds.SINGLE_CLOISTER, SoundCategory.BLOCKS, 1F, 0.5F);
		}
		
		//Client managed Sounds
		if(world.isRemote) {
			PlayerEntity player = Tardis.proxy.getClientPlayer();
			//Creaks
			if(player.ticksExisted % 2400 == 0)
                Tardis.proxy.playMovingSound(player, TSounds.AMBIENT_CREAKS, SoundCategory.AMBIENT, 0.7F, false);
			if(this.consoleRoom == ConsoleRoom.NAUTILUS) {
				if(player.ticksExisted % 600 == 0)
                    Tardis.proxy.playMovingSound(player, SoundEvents.AMBIENT_UNDERWATER_LOOP_ADDITIONS_ULTRA_RARE, SoundCategory.AMBIENT, 1F, false);
			}
		}
	}
	
	public IExterior getExterior() {
		return this.exterior;
	}
	
	public void setExterior(IExterior ext) {
		this.exterior = ext;
		this.markDirty();
		this.updateClient();
	}
	
	//Getters 'n' such
	
	public EmotionHandler getEmotionHandler() {
		return this.emotionHandler;
	}
	
	public BlockPos getLocation() {
		return this.location;
	}
	
	public BlockPos getDestination() {
		return this.destination;
	}
	
	public DimensionType getDimension() {
		return this.dimension;
	}
	
	public DimensionType getDestinationDimension() {
		return this.destinationDimension;
	}
	
	public Direction getDirection() {
		return this.facing == Direction.DOWN || this.facing == Direction.UP ? facing = Direction.NORTH : facing;
	}
	
	public boolean canFly() {
		for(Subsystem s : this.subsystems) {
			if(s.stopsFlight()) {
				return false;
			}
		}
		return this.artron > 0;
	}
	
	public void setDirection(Direction dir) {
		if(dir != Direction.DOWN && dir != Direction.UP) {
			this.facing = dir;
			this.markDirty();
			this.updateClient();
		}
	}
	
	public void setLocation(DimensionType type, BlockPos location) {
		this.dimension = type;
		this.location = location.toImmutable();
		this.markDirty();
		this.updateClient();
	}
	
	@SuppressWarnings("unchecked")
	public <T extends IControl> T getControl(Class<T> clazz){
		for(ControlEntity control : controls) {
			if(control.getControl().getClass() == clazz)
				return (T)control.getControl();
		}
		//Make it not crash if this is null for some reason
		
		//this.sendControls();
		try {
			return clazz.getConstructor(ConsoleTile.class).newInstance(this);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * Returns: a float between 0 and 1
	 */
	public float getPercentageJourney() {
		return this.flightTicks == 0 ? 0F : 1.0F - (this.flightTicks / (float)this.maxFlightTicks);
	}
	
	public ISoundScheme getSoundScheme() {
		return this.scheme;
	}
	
	public void setSonicItem(ItemStack sonic) {
		this.sonic = sonic;
		this.markDirty();
		this.updateClient();
	}
	
	public ItemStack getSonicItem() {
		return this.sonic;
	}
	
	//Register stuff
	
	public void registerTicker(ITickable ticker) {
		this.tickers.add(ticker);
	}
	
	public void registerDataHandler(ResourceLocation loc, INBTSerializable<CompoundNBT> handler) {
		this.dataHandlers.put(loc, handler);
	}
	
	public void registerControlEntry(ControlEntry<?> entry) {
		this.controlEntries.add(entry);
	}
	public void setCoordIncr(int incr) {
		this.coordIncr = incr;
		this.markDirty();
		this.updateClient();
	}
	
	public int getCoordIncr() {
		return this.coordIncr;
	}
	
	public float getArtron() {
		return this.artron;
	}
	
	public float getMaxArtron(){
		return this.max_artron;
	}
	
	public void setArtron(float artron) {
		if (artron < this.max_artron) {
			if(artron > 0)
				this.artron = artron;
			else this.artron = 0;
		}
		else {
			this.artron = this.max_artron;
		}
		this.markDirty();
		this.updateClient();
	}
	
	public BlockPos randomizeCoords(BlockPos pos, int radius) {
		int x = -radius + (rand.nextInt(radius * 2));
		int y = -radius + (rand.nextInt(radius * 2));
		int z = -radius + (rand.nextInt(radius * 2));
		return pos.add(x, y < 0 ? 5 : y, z).toImmutable();
	}
	
	public void scaleDestination() {
		float per = 1.0F - (this.flightTicks / (float)this.maxFlightTicks);
		if(per < 0)
			this.destination = this.getLocation();
		BlockPos diff = this.getDestination().subtract(this.getLocation());
		this.destination = this.getLocation().add(new BlockPos(diff.getX() * per, diff.getY() * per, diff.getZ() * per)).toImmutable();
	}
	
	/*
	 * This gets the first door in this dimension
	 */
	public LazyOptional<DoorEntity> getDoor() {
		if(world instanceof ServerWorld) {
			Iterator<Entity> it = ((ServerWorld)world).getEntities().iterator();
			while(it.hasNext()) {
				Entity e = it.next();
				if(e instanceof DoorEntity) 
					return LazyOptional.of(() -> (DoorEntity) e);
			}
		}
		return LazyOptional.empty();
	}
	
	public Direction getExteriorDirection() {
		ExteriorTile ext = this.getExterior().getExterior(this);
		if(ext != null) {
			BlockState state = ext.getWorld().getBlockState(ext.getPos());
			if(state.has(BlockStateProperties.HORIZONTAL_FACING))
				return state.get(BlockStateProperties.HORIZONTAL_FACING);
		}
		return this.facing;
	}
	
	public ConsoleRoom getConsoleRoom() {
		return this.consoleRoom;
	}
	
	public List<IExterior> getExteriors(){
		return this.unlockedExteriors;
	}
	
	public List<ConsoleRoom> getUnlockedInteriors(){
		return this.unlockedInteriors;
	}
	
	public UUID getOwner() {
		return Helper.getPlayerFromTARDIS(this.getWorld().getDimension().getType());
	}
	
	public int getMaxFlightTime() {
		return this.maxFlightTicks;
	}
	
	public float calcFuelUse() {
		float use = BASIC_FUEL_USEAGE * (this.getControl(ThrottleControl.class).getAmount() * 0.025F);
		if(this.getControl(StabilizerControl.class) != null)
			use *= this.getControl(StabilizerControl.class).isStabilized() ? 1 : 0.5F;
		return use;
	}
	
	public float calcSpeed() {
		return 1.0F - this.getControl(ThrottleControl.class).getAmount();
	}
	
	public void updateArtronValues() {
		
		this.world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
			
			this.max_artron = 0;
			rechargeMod = 1;
			
			PanelInventory inv = cap.getEngineInventoryForSide(Direction.WEST);
			for(int i = 0; i < inv.getSizeInventory(); ++i) {
				ItemStack stack = inv.getStackInSlot(i);
				if(stack.getItem() instanceof ArtronCapacitorItem) {
					ArtronCapacitorItem item = (ArtronCapacitorItem)stack.getItem();
					this.max_artron += item.getMaxStorage();
					rechargeMod /= item.getRechangeModifier();
				}
			}
			
			if(artron > this.max_artron)
				this.artron = this.max_artron;
			
		});
	}
	
	public boolean areAdminFunctionsLocked() {
		return this.getEmotionHandler().getLoyalty() > 50;
	}
	
	public boolean canDoAdminFunction(PlayerEntity entity) {
		if(!this.areAdminFunctionsLocked())
			return true;
		ResourceLocation loc = DimensionType.getKey(this.world.getDimension().getType());
		if(loc != null) {
			return loc.getPath().contentEquals(entity.getUniqueID().toString());
		}
		return false;
	}
	
	//Doesn't include landing or take off
	public int calcFlightTicks() {
		float dist = (float) Math.sqrt(this.location.distanceSq(this.destination));
		float mod = ConsoleTile.TARDIS_MAX_SPEED * this.calcSpeed() + 0.1F;
		int time = (int)(dist * mod);
		return time < 0 ? 0 : time;
	}
	
	//Only to be used in flight
	public void updateFlightTime() {
		int elapsedTime = this.flightTicks;
		this.flightTicks = this.calcFlightTicks() + this.getSoundScheme().getLandTime();
		this.maxFlightTicks = this.flightTicks + elapsedTime;
		this.markDirty();
		this.updateClient();
	}
	
	
	public InteriorManager getInteriorManager() {
		return this.interiorManager;
	}
	
	public ExteriorPropertyManager getExteriorManager() {
		return this.exteriorProps;
	}
	
	public void setCustomName(String name) {
		this.customName = name;
		this.markDirty();
	}
	
	public String getCustomName() {
		return this.customName;
	}

	//Packet shit
	
	/*
	 * To be used by packets *ONLY*, so help me God...
	 */
	public void setFlightTicks(int ticks) {
		this.flightTicks = ticks;
		this.updateClient();
	}
	
	public ArrayList<ControlEntity> getControlList(){
		return this.controls;
	}
	
	public List<SpaceTimeCoord> getWaypoints() {
		return this.waypoints;
	}
	
	public boolean addWaypoint(SpaceTimeCoord coord) {
		int index = 0;
		for(SpaceTimeCoord test : waypoints) {
			if(test.equals(SpaceTimeCoord.UNIVERAL_CENTER))
				break;
			++index;
		}
		
		if(index + 1 < waypoints.size()) {
			this.waypoints.set(index + 1, coord);
			return true;
		}
		return false;
		
	}
	
	public void addDistressSignal(SpaceTimeCoord coord) {
		this.distressSignal.add(coord);
		this.markDirty();
		this.updateClient();
	}
	
	public SpaceTimeCoord getReturnLocation() {
		return this.returnLocation;
	}

	public List<Class<? extends IControl>> getAvoidanceControls() {
		return this.avoidControls;
	}
	
	public List<SpaceTimeCoord> getDistressSignals(){
		return this.distressSignal;
	}
	
	public void setPilot(PlayerEntity player) {
		this.pilot = player;
	}
	
	public PlayerEntity getPilot() {
		return this.pilot;
	}
	
	public TexVariant[] getVariants() {
		return this.variants;
	}
	
	@Nullable
	public TexVariant getVariant() {
		if(this.variant < this.variants.length)
			return this.variants[this.variant];
		return null;
	}
	
	public void setVariant(int index) {
		if(index < this.variants.length)
			this.variant = index;
		this.markDirty();
	}
	
	//Minecraft Shit
	
	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		for(Entry<ResourceLocation, INBTSerializable<CompoundNBT>> saved : this.dataHandlers.entrySet()) {
			saved.getValue().deserializeNBT(compound.getCompound(saved.getKey().toString()));
		}
		
		if(compound.contains("unlocked_exteriors")) {
			ListNBT unlockedList = compound.getList("unlocked_exteriors", NBT.TAG_STRING);
			for(INBT tag : unlockedList) {
                IExterior ext = ExteriorRegistry.getExterior(new ResourceLocation(tag.getString()));
				if(ext != null && !this.unlockedExteriors.contains(ext))
					this.unlockedExteriors.add(ext);
			}
		}
		
		if(compound.contains("waypoints")) {
			int wayIndex = 0;
			for(INBT way : compound.getList("waypoints", Constants.NBT.TAG_COMPOUND)) {
				this.waypoints.set(wayIndex, SpaceTimeCoord.deserialize((CompoundNBT)way));
				++wayIndex;
			}
		}
		
		this.location = BlockPos.fromLong(compound.getLong("location"));
		this.destination = BlockPos.fromLong(compound.getLong("destination"));
		this.dimension = DimensionType.byName(new ResourceLocation(compound.getString("dimension")));
		this.destinationDimension = DimensionType.byName(new ResourceLocation(compound.getString("dest_dim")));
		this.flightTicks = compound.getInt("flight_ticks");
		this.maxFlightTicks = compound.getInt("max_flight_ticks");
		this.exterior = ExteriorRegistry.getExterior(new ResourceLocation(compound.getString("exterior")));
		this.artron = compound.getFloat("artron");
		this.consoleRoom = ConsoleRoom.REGISTRY.get(new ResourceLocation(compound.getString("console_room")));
		this.customName = compound.getString("custom_name");
		this.returnLocation = SpaceTimeCoord.deserialize(compound.getCompound("return_pos"));
		this.facing = Direction.values()[compound.getInt("facing")];
		ListNBT bedList = compound.getList("bed_list",Constants.NBT.TAG_COMPOUND);
		for(INBT  base : bedList) {
			CompoundNBT bedTag = ((CompoundNBT)base);
			this.bedPositions.put(UUID.fromString(bedTag.getString("player_id")), BlockPos.fromLong(bedTag.getLong("pos")));
		}
		this.sonic = ItemStack.read(compound.getCompound("sonic_item"));
		ListNBT distressList = compound.getList("distress_list", Constants.NBT.TAG_COMPOUND);
		this.distressSignal.clear();
		for(INBT dis : distressList) {
			this.distressSignal.add(SpaceTimeCoord.deserialize((CompoundNBT)dis));
		}
		
		ListNBT interiors = compound.getList("interiors", NBT.TAG_STRING);
		for(INBT in : interiors) {
			ConsoleRoom room = ConsoleRoom.REGISTRY.get(new ResourceLocation(((StringNBT)in).getString()));
			if(!this.unlockedInteriors.contains(room))
				this.unlockedInteriors.add(room);
		}
		
		this.max_artron = compound.getFloat("max_artron");
		this.rechargeMod = compound.getFloat("recharge_modifier");
		this.variant = compound.getInt("texture_variant");
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		for(Entry<ResourceLocation, INBTSerializable<CompoundNBT>> entry : this.dataHandlers.entrySet()) {
			compound.put(entry.getKey().toString(), entry.getValue().serializeNBT());
		}
		
		ListNBT unlockedList = new ListNBT();
		for(IExterior unlocked : unlockedExteriors) {
			unlockedList.add(new StringNBT(unlocked.getRegistryName().toString()));
		}
		compound.put("unlocked_exteriors", unlockedList);
		
		ListNBT waypoints = new ListNBT();
		for(SpaceTimeCoord coord : this.waypoints) {
			waypoints.add(coord.serialize());
		}
		compound.put("waypoints", waypoints);
		
		compound.putLong("location", this.location.toLong());
		compound.putLong("destination", this.destination.toLong());
		compound.putInt("flight_ticks", this.flightTicks);
		compound.putInt("max_flight_ticks", this.maxFlightTicks);
		compound.putString("exterior", this.exterior.getRegistryName().toString());
		compound.putString("dimension", Helper.getKeyFromDimType(this.dimension).toString());
		compound.putString("dest_dim", Helper.getKeyFromDimType(this.destinationDimension).toString());
		compound.putFloat("artron", this.artron);
		compound.putString("console_room", this.consoleRoom.getRegistryName().toString());
		compound.putString("custom_name", this.customName);
		compound.put("return_pos", this.returnLocation.serialize());
		compound.putInt("facing", this.facing.ordinal());

		//Bed locations
		ListNBT bedList = new ListNBT();
		for(Entry<UUID, BlockPos> entry : this.bedPositions.entrySet()) {
			CompoundNBT bedTag = new CompoundNBT();
			bedTag.putString("player_id", entry.getKey().toString());
			bedTag.putLong("pos", entry.getValue().toLong());
			bedList.add(bedTag);
		}
		compound.put("bed_list", bedList);
		compound.put("sonic_item", this.sonic.serializeNBT());
		
		//SOSes
		ListNBT distress = new ListNBT();
		for(SpaceTimeCoord dis : this.distressSignal)
			distress.add(dis.serialize());
		compound.put("distress_list", distress);
		
		//Interiors
		ListNBT interiors = new ListNBT();
		for(ConsoleRoom room : this.unlockedInteriors) {
			if(room != null)
				interiors.add(new StringNBT(room.getRegistryName().toString()));
		}
		compound.put("interiors", interiors);
		compound.putFloat("max_artron", this.max_artron);
		compound.putFloat("recharge_modifier", this.rechargeMod);
		compound.putInt("texture_variant", this.variant);
		return super.write(compound);
	}

	@Override
	public void tick() {
		//Cycle through tickable objects
		for(ITickable tick : this.tickers) {
			tick.tick(this);
		}

		if(this.isInFlight()) {
			fly();
		}
		this.playAmbiantNoises();
		
		if(!this.isInFlight() && this.getControl(RefuelerControl.class).isRefueling() && artron < this.max_artron)
			this.artron += (0.1F * this.rechargeMod);
		
		if(world.getGameTime() % 200 == 0) {
			if(world.isRemote || controls.isEmpty())
				this.getOrCreateControls();
		}
		
		if(timeUntilControlSpawn > 0) {
			--timeUntilControlSpawn;
			if(timeUntilControlSpawn == 0)
				this.getOrCreateControls();
		}
		
		if(!world.isRemote && world.getGameTime() % 40 == 0)
			this.updateArtronValues();
		
		if(world.isRemote) {
			boolean spark = false;
			for(Subsystem s : this.subsystems) {
				if(s.shouldSpark()) {
					spark = true;
					break;
				}
			}
			
			if(spark)
				world.addParticle(ParticleTypes.LAVA, pos.getX(), pos.getY(), pos.getZ(), 0, 0, 0);
		}
	}

	public void getOrCreateControls() {
		this.gatherOldControls();
		if(!world.isRemote && this.controls.size() < this.controlEntries.size()) {
				this.removeControls();
				for(ControlEntry<?> controlEntry : this.controlEntries) {
				IControl control = controlEntry.spawn(this);
				ControlEntity entity = TEntities.CONTROL.create(this.world);
				entity.setPosition(
						this.getPos().getX() + 0.5 + control.getPos().x,
						this.getPos().getY() + 0.5 + control.getPos().y,
						this.getPos().getZ() + 0.5 + control.getPos().z);
				entity.setControl(control);
				entity.setConsole(this);
				((ServerWorld)world).addEntityIfNotDuplicate(entity);
				this.controls.add(entity);
			}
		}
		this.updateClient();
	}
	
	/*
	 * Gets controls after a world reload
	 */
	private void gatherOldControls() {
		this.controls.clear();
		for(ControlEntity control : world.getEntitiesWithinAABB(ControlEntity.class, CONRTROL_HITBOX.offset(getPos()).grow(2))) {
			if(!control.removed) {
				control.setConsole(this);
				this.controls.add(control);
			}
		}
	}

	public void removeControls() {
		for(ControlEntity control : world.getEntitiesWithinAABB(ControlEntity.class, CONRTROL_HITBOX.offset(this.getPos()).grow(5))) {
			control.remove();
		}
		this.controls.clear();
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.read(pkt.getNbtCompound());
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), 99, this.getUpdateTag());
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.write(new CompoundNBT());
	}

	public void updateClient() {
		if(world.isRemote) return;
		
		BlockState state = world.getBlockState(this.getPos());
		world.markAndNotifyBlock(this.getPos(), world.getChunkAt(getPos()), state, state, 2);
	}

	@Override
	public void onLoad() {
		super.onLoad();
		this.timeUntilControlSpawn = 10;
	}
	
	public List<Subsystem> getSubSystems(){
		return this.subsystems;
	}
	
	public List<Upgrade> getUpgrades(){
		return this.upgrades;
	}
	

	@SuppressWarnings("unchecked")
	public <T extends Subsystem> LazyOptional<T> getSubsystem(Class<T> clazz) {
		for(Subsystem sys : this.getSubSystems()) {
			if(sys.getClass() == clazz)
				return LazyOptional.of(() -> (T)sys);
		}
		return LazyOptional.empty();
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Upgrade> LazyOptional<T> getUpgrade(Class<T> clazz) {
		for(Upgrade upgrade : upgrades) {
			if(upgrade.getClass() == clazz)
				return (LazyOptional<T>)LazyOptional.of(() -> upgrade);
		}
		return LazyOptional.empty();
	}
	/**
	 * Adds or updates the player respawn point if they slept in a bed in their tardis
	 * If the entry doesn't exist, it creates one, otherwise it updates the existing one
	 * @param player
	 * @param pos
	 */
	public void addOrUpdateBedLoc(PlayerEntity player, BlockPos pos) {
		this.bedPositions.put(player.getUniqueID(), pos);
	}
	
	public BlockPos getBedPosForPlayer(PlayerEntity player) {
		return this.bedPositions.get(player.getUniqueID());
	}
	/**
	 * Removes the player respawn point position if the object exists
	 * @param player
	 * @param pos
	 */
	public void removePlayerBedLoc(PlayerEntity player) {
		this.bedPositions.remove(player.getUniqueID());
	}

	public void playSoundAtExterior(SoundEvent sound, SoundCategory cat, float vol, float pitch) {
		ExteriorTile tile = this.exterior.getExterior(this);
		if(tile != null)
			tile.getWorld().playSound(null, tile.getPos(), sound, cat, vol, pitch);
	}
}
