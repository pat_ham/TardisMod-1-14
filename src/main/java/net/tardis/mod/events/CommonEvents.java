package net.tardis.mod.events;


import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.minecraft.block.BedBlock;
import net.minecraft.block.BellBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.merchant.villager.VillagerTrades.ITrade;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.FilledMapItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.MerchantOffer;
import net.minecraft.nbt.INBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.MapData;
import net.minecraft.world.storage.MapDecoration.Type;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.TickEvent.WorldTickEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerRespawnEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.RegisterDimensionsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ChunkLoaderCapability;
import net.tardis.mod.cap.IChunkLoader;
import net.tardis.mod.cap.ILightCap;
import net.tardis.mod.cap.ITardisWorldData.TardisWorldProvider;
import net.tardis.mod.cap.LightCapability;
import net.tardis.mod.cap.entity.IPlayerData;
import net.tardis.mod.cap.entity.PlayerDataCapability;
import net.tardis.mod.cap.items.IRemote;
import net.tardis.mod.cap.items.IVortexCap;
import net.tardis.mod.cap.items.IWatch;
import net.tardis.mod.cap.items.RemoteCapability;
import net.tardis.mod.cap.items.VortexCapability;
import net.tardis.mod.cap.items.WatchCapability;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.dimensions.IDimProperties;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.dimensions.TardisDimension;
import net.tardis.mod.entity.ai.FollowIntoTardisGoal;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.recipe.Recipes;
import net.tardis.mod.recipe.WeldRecipe;
import net.tardis.mod.sonic.SonicManager;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.TemporalGraceSubsystem;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.trades.ItemTrade;
import net.tardis.mod.trades.Villager;
import net.tardis.mod.world.WorldGen;

@Mod.EventBusSubscriber(modid = Tardis.MODID)
public class CommonEvents {
	
	public static final ResourceLocation LIGHT_CAP = new ResourceLocation(Tardis.MODID, "light");
	public static final ResourceLocation CHUNK_CAP = new ResourceLocation(Tardis.MODID, "loader");
	public static final ResourceLocation TARDIS_CAP = new ResourceLocation(Tardis.MODID, "tardis_data");
	public static final ResourceLocation WATCH_CAP = new ResourceLocation(Tardis.MODID, "watch");
    public static final ResourceLocation PLAYER_DATA_CAP = new ResourceLocation(Tardis.MODID, "player_data");
    public static final ResourceLocation REMOTE_CAP = new ResourceLocation(Tardis.MODID, "remote");
    public static final ResourceLocation VORTEX = new ResourceLocation(Tardis.MODID, "vortex");

	@SubscribeEvent
	public static void attachChunkCaps(AttachCapabilitiesEvent<Chunk> event) {
		if(event.getObject().getWorld().getDimension().getType().getModType() == TDimensions.TARDIS) {
			event.addCapability(LIGHT_CAP, new ILightCap.LightProvider(new LightCapability(event.getObject())));
		}
	}
	
	@SubscribeEvent
	public static void attachItemStackCap(AttachCapabilitiesEvent<ItemStack> event) {
		if(event.getObject().getItem() == TItems.POCKET_WATCH)
			event.addCapability(WATCH_CAP, new IWatch.Provider(new WatchCapability()));
		if(event.getObject().getItem() == TItems.STATTENHEIM_REMOTE)
			event.addCapability(REMOTE_CAP, new IRemote.Provider(new RemoteCapability(event.getObject())));
		if (event.getObject().getItem() == TItems.VORTEX_MANIP)
			event.addCapability(VORTEX, new IVortexCap.Provider(new VortexCapability(event.getObject(), 3)));
	}

    @SubscribeEvent
    public static void attachPlayerCap(AttachCapabilitiesEvent<Entity> event) {
        if (event.getObject() instanceof PlayerEntity)
            event.addCapability(PLAYER_DATA_CAP, new IPlayerData.Provider(new PlayerDataCapability((PlayerEntity) event.getObject())));
    }
	
    @SubscribeEvent
	public static void registerTrades(VillagerTradesEvent event) {
		
		if(event.getType() == Villager.STORY_TELLER) {
			event.getTrades().get(2).add(new ITrade() {

				@Override
				public MerchantOffer getOffer(Entity trader, Random rand) {
					BlockPos pos = WorldGen.getClosestBrokenExterior((int)trader.posX, (int)trader.posZ);
					if(pos != null && trader != null && trader.world != null) {
						ItemStack map = FilledMapItem.setupNewMap(trader.world, pos.getX(), pos.getZ(), (byte)0, true, true);
						MapData.addTargetDecoration(map, pos, "Artifact", Type.RED_X);
						map.setDisplayName(new StringTextComponent("Ancient Artifact Map"));
						if (map != null) {
							FilledMapItem.getMapData(map, trader.world).scale = (byte)0;
							FilledMapItem.renderBiomePreviewMap(trader.world, map);
							return new MerchantOffer(new ItemStack(Items.EMERALD, 10), map, 2, 1, 0);
						}
					}
					return null;
				}});
			event.getTrades().get(1).add(new ItemTrade(new ItemStack(Items.PAPER, 24), new ItemStack(Items.EMERALD), 3, 3));
			event.getTrades().get(1).add(new ItemTrade(new ItemStack(Items.GLASS_PANE, 10), new ItemStack(Items.EMERALD), 3, 3));
			ItemStack manual = new ItemStack(TItems.MANUAL);
			manual.setDisplayName(new StringTextComponent("Strange Journal"));
			event.getTrades().get(1).add(new ItemTrade(new ItemStack(Items.EMERALD, 3), manual, 5, 3));
		}

		
	}
    
	@SubscribeEvent
	public static void attachChunkLoadCaps(AttachCapabilitiesEvent<World> event) {
		if (event.getObject().isRemote) return;
		
		if(event.getObject().getDimension() instanceof TardisDimension)
			event.addCapability(TARDIS_CAP, new TardisWorldProvider(event.getObject()));
		
		 final LazyOptional<IChunkLoader> inst = LazyOptional.of(() -> new ChunkLoaderCapability((ServerWorld)event.getObject()));
	        final ICapabilitySerializable<INBT> provider = new ICapabilitySerializable<INBT>() {
	            @Override
	            public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
	                return Capabilities.CHUNK_LOADER.orEmpty(cap, inst);
	            }

	            @Override
	            public INBT serializeNBT() {
	                return Capabilities.CHUNK_LOADER.writeNBT(inst.orElse(null), null);
	            }

	            @Override
	            public void deserializeNBT(INBT nbt) {
	            	Capabilities.CHUNK_LOADER.readNBT(inst.orElse(null), null, nbt);
	            }
	        };
        event.addCapability(CHUNK_CAP, provider);
        event.addListener(() -> inst.invalidate());
	}


	@SubscribeEvent
	public static void onBlockBreak(BlockEvent.BreakEvent event) {
		if(event.getState().getBlock() instanceof IDontBreak) {
			event.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public static void onBlockClicked(PlayerInteractEvent.RightClickBlock event) {
		if(event.getWorld().getBlockState(event.getPos()).getBlock() instanceof BellBlock) {
			ChunkPos pos = event.getWorld().getChunk(event.getEntity().getPosition()).getPos();
			for(int x = -3; x < 3; ++x) {
				for(int z = -3; z < 3; ++z) {
					for(TileEntity te : event.getWorld().getChunk(pos.x + x, pos.z + z).getTileEntityMap().values()) {
						if(te instanceof ExteriorTile || te instanceof BrokenExteriorTile) {
							event.getWorld().playSound(null, te.getTileEntity().getPos(), TSounds.SINGLE_CLOISTER, SoundCategory.BLOCKS, 5F, 0.5F);
						}
					}
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void onWorldTick(WorldTickEvent event) {
		//Set Phase to Start to ensure our event is only called once
		if(event.phase == Phase.START) {
			event.world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> cap.tick());
			if(!event.world.isRemote && event.world.getGameTime() % 200 == 0 && event.world.getDimension().getType().getModType() == TDimensions.TARDIS) {
				for(PlayerEntity player : event.world.getPlayers()) {
					ChunkPos pos = new ChunkPos(player.getPosition());
					for(int x = -3; x < 3; ++x) {
						for(int z = -3; z < 3; ++z) {
							event.world.getChunk(pos.x + x, pos.z + z).getCapability(Capabilities.LIGHT).ifPresent(cap -> cap.onLoad());
						}
					}
				}
			}
		}
	}


	@SubscribeEvent
	public static void onEntityJoin(EntityJoinWorldEvent event){
		if(event.getWorld().getDimension().getType().getModType() == TDimensions.TARDIS && !event.getWorld().isRemote) {

            //Ring alarm for naughty mobs
			if(event.getEntity() instanceof IMob) {
				TileEntity te = event.getWorld().getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile)
					((ConsoleTile)te).getInteriorManager().setAlarmOn(true);
			}
			
			//If a player enters the TARDIS
			if(event.getEntity() instanceof ServerPlayerEntity) {
				ServerPlayerEntity player = (ServerPlayerEntity)event.getEntity();
				
				if(TardisHelper.isInOwnedTardis(player)){
					//Unlock things based on Achievement
					Helper.doIfAdvancementPresent("fortune_exterior", player, () -> {
						TileEntity te = player.getServerWorld().getTileEntity(TardisHelper.TARDIS_POS);
						if(te instanceof ConsoleTile) {
							IExterior ext = ExteriorRegistry.FORTUNE;
							ConsoleTile console = (ConsoleTile)te;
							if(!console.getExteriors().contains(ext)) {
								console.getExteriors().add(ext);
								player.sendStatusMessage(new TranslationTextComponent("status.tardis.exterior.unlock", ext.getDisplayName().getFormattedText()), true);
							}
						}
					});
					
					Helper.doIfAdvancementPresent("nemo_interior", player, () -> {
						TardisHelper.getConsole(player.server, player.dimension)
								.ifPresent(console -> Helper.unlockInterior(console, player, ConsoleRoom.NAUTILUS));
					});
					
					Helper.doIfAdvancementPresent("trunk_exterior", player, () -> {
						TardisHelper.getConsole(player.server, player.dimension).ifPresent(tile -> Helper.unlockExterior(tile, player, ExteriorRegistry.TRUNK));
					});
					
					Helper.doIfAdvancementPresent("safe_exterior", player, () -> {
						TardisHelper.getConsoleInWorld(player.world).ifPresent(tile -> Helper.unlockExterior(tile, player, ExteriorRegistry.SAFE));
					});
				}
			}
		}
		
		//Follow into TARDIS
		if(event.getEntity() instanceof MonsterEntity) {
			MonsterEntity ent = (MonsterEntity)event.getEntity();
			ent.goalSelector.addGoal(0, new FollowIntoTardisGoal(ent, ent.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getValue()));
		}
	}
	
	//Only works in player's own tardis at the moment
	@SubscribeEvent
	public static void onSleep(PlayerSleepInBedEvent event) {
		//Checks if player have a Tardis
		if (TardisHelper.hasTARDIS(event.getPlayer().getServer(), event.getPlayer().getUniqueID())) {
		
			TardisHelper.getConsole(event.getPlayer().getServer(), event.getPlayer().getUniqueID()).ifPresent(console -> {
				if(TardisHelper.isInATardis(event.getPlayer())) {
					console.addOrUpdateBedLoc(event.getPlayer(), event.getPos()); //Sets player spawn to Tardis dim
					event.getPlayer().startSleeping(event.getPos()); //Starts sleep timer so we can reduce phantom spawn rates
				}
				else if (!TardisHelper.isInOwnedTardis(event.getPlayer())){
					//If the player sleeps successfully in a non Tardis dimension, remove the bed pos so vanilla will overwrite the tardis one
					//Prevents you from always spawning in the tardis even if you sleep in another dimension
					console.removePlayerBedLoc(event.getPlayer()); 
				}
			});
		}
		
		//Emotional
		if(TardisHelper.isInOwnedTardis(event.getPlayer())) {
			PlayerEntity player = event.getPlayer();
			player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
				if(cap.hasUsedTelepathics()) {
					TardisHelper.getConsoleInWorld(player.getEntityWorld()).ifPresent(tile -> {
						String emotionName = "";
						
						int mood = tile.getEmotionHandler().getMood();
						
						for(EnumHappyState state : EnumHappyState.values()) {
							System.out.println(state);
							System.out.println(mood);
							if(mood >= state.getTreshold()) {
								emotionName = state.name().toLowerCase();
								System.out.println(emotionName);
								break;
							}
							else if (mood < EnumHappyState.SAD.getTreshold()) {
								emotionName = "miserable!";
								break;
							}
						}
						
						System.out.println(player.getName().getFormattedText() + "'s TARDIS " + "Feeling: " + emotionName + ", Mood: " + mood);
						//Send status to chat so it won't get lost when on multiplayer
						event.getPlayer().sendStatusMessage(new TranslationTextComponent("status.tardis.mood", emotionName), false);
						cap.setHasUsedTelepathics(false);
					});
				}
			});
		}
	}
	
	@SubscribeEvent
	public static void onPlayerRespawn(PlayerRespawnEvent event) {
		if (!event.getPlayer().world.isRemote && TardisHelper.hasTARDIS(event.getPlayer().getServer(), event.getPlayer().getUniqueID())) {
			TardisHelper.getConsole(event.getPlayer().getServer(), event.getPlayer().getUniqueID()).ifPresent(console -> {
				if (console.getBedPosForPlayer(event.getPlayer()) != null) {
					BlockPos pos = console.getBedPosForPlayer(event.getPlayer());
					if(!(console.getWorld().getBlockState(pos).getBlock() instanceof BedBlock) || console.getBedPosForPlayer(event.getPlayer()).equals(null)) {
							return; //should respawn them at overworld if they break bed in tardis and die
					}
					event.getPlayer().setPosition(pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5);
					if (!TardisHelper.isInATardis(event.getPlayer())) {
						Helper.teleportEntities(event.getPlayer(), (ServerWorld)console.getWorld(), pos.getX()  + 0.5, pos.getY() + 1, pos.getZ() + 0.5, event.getPlayer().rotationYaw, event.getPlayer().rotationPitch);
					}
				}
			});
		}
	}
	
	@SubscribeEvent
	public static void registerPremDimensions(RegisterDimensionsEvent event) {
		TDimensions.VORTEX_TYPE = DimensionManager.registerOrGetDimension(new ResourceLocation(Tardis.MODID, "vortex"), TDimensions.VORTEX, null, false);
		//TDimensions.SPACE_TYPE = DimensionManager.registerOrGetDimension(new ResourceLocation(Tardis.MODID, "space"), TDimensions.SPACE, null, false);
		TDimensions.MOON_TYPE = DimensionManager.registerOrGetDimension(new ResourceLocation(Tardis.MODID,  "moon"), TDimensions.MOON, null, true);
	}

    @SubscribeEvent
    public static void onLivingTick(LivingEvent.LivingUpdateEvent event) {
        if (event.getEntityLiving() instanceof PlayerEntity)
            event.getEntityLiving().getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> cap.tick());
        
        //Gravity mod
        if(event.getEntity().world.dimension instanceof IDimProperties)
        	event.getEntityLiving().setMotion(((IDimProperties)event.getEntityLiving().world.dimension).modMotion(event.getEntityLiving()));
    }


    //=== This is where I will handle special cases for blocks and entities that do something other than the intended sonic result
    @SubscribeEvent
    public static void sonicOnEntity(PlayerInteractEvent.EntityInteract event) {
        if (event.getItemStack().getItem() instanceof SonicItem) {
            ItemStack stack = event.getItemStack();
            SonicManager.ISonicMode mode = SonicItem.getCurrentMode(stack);
            mode.processSpecialEntity(event);
        }
    }

    @SubscribeEvent
    public static void sonicOnSpecialBlock(PlayerInteractEvent.RightClickBlock event) {
        if (event.getItemStack().getItem() instanceof SonicItem) {
            ItemStack stack = event.getItemStack();
            SonicManager.ISonicMode mode = SonicItem.getCurrentMode(stack);
            mode.processSpecialBlocks(event);
        }
    }
    
    @SubscribeEvent
    public static void onHurt(LivingHurtEvent event) {
    	if(event.getEntity() instanceof PlayerEntity && !event.getEntity().world.isRemote) {
    		PlayerEntity player = (PlayerEntity)event.getEntity();
    		if(TardisHelper.hasTARDIS(player.world.getServer(), player.getUniqueID()) && player.getHealth() < 6) {
    			if(Helper.canTravelToDimension(player.dimension)) {
    				TardisHelper.getConsole(player.world.getServer(), player.getUniqueID()).ifPresent(tile -> {
    					HandbrakeControl brake = tile.getControl(HandbrakeControl.class);
        				if(tile.getEmotionHandler().getLoyalty() > 100 && brake != null && brake.isFree()) {
        					tile.getExterior().remove(tile);
        					tile.setLocation(player.dimension, player.getPosition());
        					tile.getExterior().place(tile, player.dimension, player.getPosition());
        					ExteriorTile ext = tile.getExterior().getExterior(tile);
        					if(ext != null) {
        						ext.remat();
        					}
        				}
    				});
    			}
    		}
    		
    		//Temporal Grace
    		
    		if(event.getEntityLiving().dimension.getModType() == TDimensions.TARDIS && !event.getSource().canHarmInCreative()) {
    			TardisHelper.getConsoleInWorld(event.getEntityLiving().world).ifPresent(tile -> {
    				tile.getSubsystem(TemporalGraceSubsystem.class).ifPresent(grace -> {
    					if(grace.canBeUsed()) {
    						grace.damage(null, 1);
    						event.setCanceled(true);
    					}
    				});
    			});
    		}
    		
    	}
    }
    
    @SubscribeEvent
    public static void readRecipes(FMLServerStartingEvent event) {
    	Recipes.WELD_RECIPE.clear();
    	Collection<ResourceLocation> locations = event.getServer().getResourceManager().getAllResourceLocations("quantiscope", str -> str.endsWith(".json"));
    	for(ResourceLocation loc : locations) {
    		try {
    			JsonObject obj = new JsonParser().parse(new InputStreamReader(event.getServer().getResourceManager().getResource(loc).getInputStream())).getAsJsonObject();
    			if(!obj.get("repair").getAsBoolean()) {
    				List<Item> itemList = new ArrayList<Item>();
    				for(JsonElement items : obj.get("ingredients").getAsJsonArray()) {
    					itemList.add(ForgeRegistries.ITEMS.getValue(new ResourceLocation(items.getAsString())));
    				}
    				Item result = null;
    				JsonObject resultObj = obj.get("result").getAsJsonObject();
    				if(resultObj.has("item"))
    					result = ForgeRegistries.ITEMS.getValue(new ResourceLocation(resultObj.get("item").getAsString()));
    				
    				Recipes.WELD_RECIPE.add(new WeldRecipe(result, false, itemList.toArray(new Item[0])));
    			}
    			else{
    				Item focalPoint = ForgeRegistries.ITEMS.getValue(new ResourceLocation(obj.get("repair_item").getAsString()));
    				List<Item> items = new ArrayList<Item>();
    				for(JsonElement ele : obj.get("ingredients").getAsJsonArray()) {
    					items.add(ForgeRegistries.ITEMS.getValue(new ResourceLocation(ele.getAsString())));
    				}
    				Recipes.WELD_RECIPE.add(new WeldRecipe(focalPoint, true, items.toArray(new Item[0])));
    			}
    		}
    		catch(Exception e) {
    			System.err.println("Error parsing Quantiscope recipe for " + loc.toString());
    			e.printStackTrace();
    		}
    	}
    }
    
    @SubscribeEvent
    public static void useVortexM(PlayerInteractEvent.RightClickEmpty e) {
		if (e.getPlayer().getHeldItemMainhand().isEmpty()) {
    		ItemStack vm = new ItemStack(TItems.VORTEX_MANIP);
    		if (e.getPlayer().inventory.hasItemStack(vm)) {
	    		int index = PlayerHelper.findItem(e.getPlayer(), vm).getAsInt();
				ItemStack stack = e.getPlayer().inventory.getStackInSlot(index);
				//Mimic right click behaviour of item, fixes issue where capability cannot be found for the stack if it's not in hand
				if(e.getWorld().isRemote) {
					if (!e.getPlayer().getCooldownTracker().hasCooldown(stack.getItem())) {
						Tardis.proxy.openGUI(Constants.Gui.VORTEX_MAIN, null);
						if (!PlayerHelper.isInEitherHand(e.getPlayer(), vm.getItem())) //Prevents stack from being opened when another stack is in the offhand
							stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> cap.setOpen(true));
					}
				}
    		}
		}
    }
    
    @SubscribeEvent
    public static void onKilled(LivingDeathEvent event) {
    	//Sad Boi Noises
		if(event.getEntityLiving() instanceof PlayerEntity && event.getEntityLiving().isServerWorld()) {
			if(TardisHelper.hasTARDIS(event.getEntityLiving().world.getServer(), event.getEntityLiving().getUniqueID())) {
				TardisHelper.getConsole(event.getEntityLiving().world.getServer(), event.getEntityLiving().getUniqueID()).ifPresent(console -> {
					if(console.getEmotionHandler().getLoyalty() > 10) {
						console.playSoundAtExterior(TSounds.SINGLE_CLOISTER, SoundCategory.BLOCKS, 1F, 1F);
    					console.getWorld().playSound(null, console.getPos(), TSounds.SINGLE_CLOISTER, SoundCategory.BLOCKS, 1F, 1F);
					}
				});
			}
		}
		
    }
}
