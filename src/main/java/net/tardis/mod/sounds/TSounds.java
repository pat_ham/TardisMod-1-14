package net.tardis.mod.sounds;

import java.util.HashMap;

import javax.annotation.CheckForNull;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ObjectHolder;
import net.tardis.mod.Tardis;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
@ObjectHolder(Tardis.MODID)
public class TSounds {

    public static final SoundEvent BESSIE_HORN = null;
    /**
     * SOUND REGISTRY
     * This class is responsible for registering all sounds within the mod
     * Each sound requires a field below with a matching name in the registry event
     * ~ Swirtzly
     */

    public static final SoundEvent TARDIS_LAND = null;
    public static final SoundEvent TARDIS_TAKEOFF = null;
    public static final SoundEvent TARDIS_FLY_LOOP = null;
    
    public static final SoundEvent ROTOR_START = null;
    public static final SoundEvent ROTOR_TICK = null;
    public static final SoundEvent ROTOR_END = null;
    
    public static final SoundEvent DOOR_OPEN = null;
    public static final SoundEvent DOOR_CLOSE = null;
    public static final SoundEvent DOOR_LOCK = null;
    public static final SoundEvent DOOR_UNLOCK = null;
    public static final SoundEvent DOOR_KNOCK = null;
    public static final SoundEvent HANDBRAKE_ENGAGE = null;
    public static final SoundEvent HANDBRAKE_RELEASE = null;
    public static final SoundEvent THROTTLE = null;
    public static final SoundEvent RANDOMISER = null;
    public static final SoundEvent GENERIC_ONE = null;
    public static final SoundEvent GENERIC_TWO = null;
    public static final SoundEvent GENERIC_THREE = null;
    public static final SoundEvent DIMENSION = null;
    public static final SoundEvent DIRECTION = null;
    public static final SoundEvent LANDING_TYPE_UP = null;
    public static final SoundEvent LANDING_TYPE_DOWN = null;
    public static final SoundEvent CANT_START = null;
    public static final SoundEvent REFUEL_START = null;
    public static final SoundEvent REFUEL_STOP = null;
    public static final SoundEvent STABILIZER_ON = null;
    public static final SoundEvent STABILIZER_OFF = null;
    public static final SoundEvent TELEPATHIC_CIRCUIT = null;
    
    public static final SoundEvent SINGLE_CLOISTER = null;
    public static final SoundEvent SONIC_GENERIC = null;
    public static final SoundEvent VM_TELEPORT = null;
    public static final SoundEvent VM_BUTTON = null;
    public static final SoundEvent VM_TELEPORT_DEST = null;
    public static final SoundEvent WATCH_MALFUNCTION = null;
    public static final SoundEvent WATCH_TICK = null;
    public static final SoundEvent REMOTE_ACCEPT = null;
    
    public static final SoundEvent PAPER_DROP = null;

    public static final SoundEvent DALEK_EXTERMINATE = null;
    public static final SoundEvent DALEK_FIRE = null;
    public static final SoundEvent DALEK_HOVER = null;
    public static final SoundEvent DALEK_DEATH = null;
    public static final SoundEvent DALEK_MOVES = null;
    public static final SoundEvent DALEK_SW_AIM = null;
    public static final SoundEvent DALEK_SW_FIRE = null;
    public static final SoundEvent DALEK_SW_HIT_EXPLODE = null;

    public static final SoundEvent TARDIS_HUM_63 = null;
    public static final SoundEvent TARDIS_HUM_70 = null;
    public static final SoundEvent TARDIS_HUM_80 = null;
    public static final SoundEvent TARDIS_HUM_COPPER = null;
    public static final SoundEvent TARDIS_HUM_CORAL = null;
    public static final SoundEvent TARDIS_HUM_TOYOTA = null;
    
    public static final SoundEvent AMBIENT_CREAKS = null;
    public static final SoundEvent SONIC_TUNING = null;
    public static final SoundEvent REACHED_DESTINATION = null;
    public static final SoundEvent SONIC_FAIL = null;
    public static final SoundEvent SONIC_BROKEN = null;
    public static final SoundEvent BESSIE_DRIVE = null;
    public static final SoundEvent SHIELD_HUM = null;
    public static final SoundEvent COMMUNICATOR_BEEP = null;
    public static final SoundEvent COMMUNICATOR_STEAM = null;

    public static final SoundEvent ELECTRIC_SPARK = null;

    public static final SoundEvent EYE_MONITOR_INTERACT = null;
    public static final SoundEvent STEAMPUNK_MONITOR_INTERACT = null;

    private static HashMap<ResourceLocation, ISoundScheme> SCHEMES = new HashMap<ResourceLocation, ISoundScheme>();

    @SubscribeEvent
    public static void registerSounds(RegistryEvent.Register<SoundEvent> event) {
        event.getRegistry().registerAll(
        		
        	// TARDIS
	        setUpSound("tardis_takeoff"),
	        setUpSound("tardis_land"),
	        setUpSound("tardis_fly_loop"),
	        setUpSound("rotor_start"),
	        setUpSound("rotor_tick"),
	        setUpSound("rotor_end"),
	        setUpSound("door_open"),
	        setUpSound("door_close"),
	        setUpSound("door_lock"),
	        setUpSound("door_unlock"),
	        setUpSound("door_knock"),
            setUpSound("handbrake_engage"),
            setUpSound("handbrake_release"),
            setUpSound("throttle"),
            setUpSound("randomiser"),
            setUpSound("single_cloister"),
            setUpSound("generic_one"),
            setUpSound("generic_two"),
	        setUpSound("generic_three"),
            setUpSound("dimension"),
            setUpSound("direction"),
            setUpSound("landing_type_up"),
            setUpSound("landing_type_down"),
            setUpSound("cant_start"),
            setUpSound("reached_destination"),
            setUpSound("refuel_start"),
            setUpSound("refuel_stop"),
            setUpSound("communicator_beep"),
            setUpSound("stabilizer_on"),
            setUpSound("stabilizer_off"),
            setUpSound("telepathic_circuit"),
            setUpSound("communicator_steam"),

            // Sonic Devices
            setUpSound("sonic_generic"),
            setUpSound("sonic_tuning"),
            setUpSound("sonic_fail"),
            setUpSound("sonic_broken"),
            
            // Vortex Manipulator
            setUpSound("vm_teleport"),
            setUpSound("vm_button"),
            setUpSound("vm_teleport_dest"),
            
            //Stattenheim Remote
            setUpSound("remote_accept"),
            
            // Blocks
            setUpSound("paper_drop"),
            //Entity
            setUpSound("dalek_exterminate"),
            setUpSound("dalek_fire"),
            setUpSound("dalek_hover"),
            setUpSound("dalek_death"),
            setUpSound("dalek_moves"),
            setUpSound("bessie_drive"),
            setUpSound("bessie_horn"),
            setUpSound("shield_hum"),

            //Special Weapon
            setUpSound("dalek_sw_fire"),
            setUpSound("dalek_sw_aim"),
            setUpSound("dalek_sw_hit_explode"),

            //Hums
            setUpSound("tardis_hum_63"),
            setUpSound("tardis_hum_70"),
            setUpSound("tardis_hum_80"),
            setUpSound("tardis_hum_copper"),
            setUpSound("tardis_hum_coral"),
            setUpSound("tardis_hum_toyota"),
            
            //Ambient noises
            setUpSound("ambient_creaks"),
            setUpSound("electric_spark"),

            //Monitor Sounds
            setUpSound("eye_monitor_interact"),
            setUpSound("steampunk_monitor_interact"),

            //Items
            setUpSound("watch_malfunction"),
            setUpSound("watch_tick")

        );
    }

    private static SoundEvent setUpSound(String soundName) {
        return new SoundEvent(new ResourceLocation(Tardis.MODID, soundName)).setRegistryName(soundName);
    }
    
    public static void registerSoundScheme(ResourceLocation loc, ISoundScheme scheme) {
    	scheme.setRegistryName(loc);
    	SCHEMES.put(loc, scheme);
    }
    
    @CheckForNull
    public static ISoundScheme getSoundByRegistryName(ISoundScheme scheme) {
    	return SCHEMES.getOrDefault(scheme, null);
    }

}
