package net.tardis.mod.world;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.Category;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.GenerationStage.Decoration;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.ProbabilityConfig;
import net.minecraft.world.gen.placement.ChanceConfig;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.world.feature.TardisGenFeature;
import net.tardis.mod.world.feature.structures.CrashedStructure;
import net.tardis.mod.world.feature.structures.ObservatoryStructure;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class WorldGen{
	
	public static final Feature<NoFeatureConfig> TARDIS_GEN = new TardisGenFeature(NoFeatureConfig::deserialize);
	public static final Feature<ProbabilityConfig> XION_CRYSTAL = new XionCrystalFeature(ProbabilityConfig::deserialize);
	public static final Feature<ProbabilityConfig> OBSERVATORY = new ObservatoryStructure(ProbabilityConfig::deserialize);
	public static final Feature<NoFeatureConfig> CRASHED_SHIP = new CrashedStructure(NoFeatureConfig::deserialize);
	
	public static List<BlockPos> BROKEN_EXTERIORS = new ArrayList<>();
	
	@SubscribeEvent
	public static void registerFeature(RegistryEvent.Register<Feature<?>> event) {
		event.getRegistry().registerAll(
				TARDIS_GEN.setRegistryName(new ResourceLocation(Tardis.MODID, "broken_exterior")),
				OBSERVATORY.setRegistryName(new ResourceLocation(Tardis.MODID, "observatory"))
		);
		
	}
	/**
	 * Used to actually apply features to the world
	 * @implNote This is called in the FMLCommonSetup in the main Tardis class
	 * 
	 */
	 public static void applyFeatures() { 
		//Generates under ground
		 for(Biome biome : ForgeRegistries.BIOMES.getValues()) { 
			//Won't show in the Nether and End or TARDIS
			 if (!biome.equals(Biomes.THE_VOID) && biome.getCategory() != Biome.Category.NETHER && biome.getCategory() != Biome.Category.THEEND) { 
				 biome.addFeature(Decoration.UNDERGROUND_DECORATION, Biome.createDecoratedFeature(TARDIS_GEN, IFeatureConfig.NO_FEATURE_CONFIG, Placement.CHANCE_HEIGHTMAP, new ChanceConfig(100)));
				 biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE,
						 new OreFeatureConfig(
						 	OreFeatureConfig.FillerBlockType.NATURAL_STONE, 
							TBlocks.cinnabar_ore.getDefaultState(), 
							5), 	
						Placement.COUNT_RANGE, 
						new CountRangeConfig(2, 0, 0, 24))); //Cinnabar ore in veins of 1-5, two veins per chunk, generates between y level 0 and 24
				 
				 biome.addFeature(Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(XION_CRYSTAL, new ProbabilityConfig(0.2F),
						 Placement.COUNT_RANGE, new CountRangeConfig(2, 0, 0, 24)));
				 
				 if(biome.getCategory() != Category.OCEAN)
					 biome.addFeature(Decoration.SURFACE_STRUCTURES, Biome.createDecoratedFeature(CRASHED_SHIP, NoFeatureConfig.NO_FEATURE_CONFIG, Placement.CHANCE_HEIGHTMAP, new ChanceConfig(1300)));
			 }
		}
		 
		Biomes.DESERT.addFeature(Decoration.SURFACE_STRUCTURES, Biome.createDecoratedFeature(OBSERVATORY, new ProbabilityConfig(0.01F), Placement.CHANCE_HEIGHTMAP, new ChanceConfig(1000)));
		Biomes.SAVANNA.addFeature(Decoration.SURFACE_STRUCTURES, Biome.createDecoratedFeature(OBSERVATORY, new ProbabilityConfig(0.01F), Placement.CHANCE_HEIGHTMAP, new ChanceConfig(1000)));
		Biomes.SAVANNA_PLATEAU.addFeature(Decoration.SURFACE_STRUCTURES, Biome.createDecoratedFeature(OBSERVATORY, new ProbabilityConfig(0.01F), Placement.CHANCE_HEIGHTMAP, new ChanceConfig(1000)));
		Biomes.PLAINS.addFeature(Decoration.SURFACE_STRUCTURES, Biome.createDecoratedFeature(OBSERVATORY, new ProbabilityConfig(0.01F), Placement.CHANCE_HEIGHTMAP, new ChanceConfig(1000)));
		 //Feature.STRUCTURES.put("tardis:broken", TARDIS_GEN);
	 }
	 
	 @Nullable
	 public static BlockPos getClosestBrokenExterior(int x, int y) {
		 int dist = Integer.MAX_VALUE;
		 BlockPos ext = null;
		 for(BlockPos pos : BROKEN_EXTERIORS) {
			 if(pos.distanceSq(new BlockPos(x, 0, y)) < dist)
				 ext = pos;
				 
		 }
		 if(BROKEN_EXTERIORS.contains(ext))
			 BROKEN_EXTERIORS.remove(ext);
		 return ext;
	 }
}
