package net.tardis.mod.world.feature.structures;

import java.util.List;
import java.util.Random;
import java.util.function.Function;

import com.mojang.datafixers.Dynamic;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.merchant.villager.VillagerEntity;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.WorldGenRegion;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.ProbabilityConfig;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.Template.BlockInfo;
import net.tardis.mod.Tardis;

public class ObservatoryStructure extends Feature<ProbabilityConfig> {
	
	public static final ResourceLocation DESERT = new ResourceLocation(Tardis.MODID, "tardis/structures/worldgen/observ_desert");
	public static final ResourceLocation SAVANNA = new ResourceLocation(Tardis.MODID, "tardis/structures/worldgen/observ_savanna");
	public static final ResourceLocation PLAINS = new ResourceLocation(Tardis.MODID, "tardis/structures/worldgen/observ_plains");
	
	public ObservatoryStructure(Function<Dynamic<?>, ? extends ProbabilityConfig> configFactoryIn) {
		super(configFactoryIn);
	}
	
	
	@Override
	public boolean place(IWorld worldIn, ChunkGenerator<? extends GenerationSettings> generator, Random rand, BlockPos pos, ProbabilityConfig config) {
		if(worldIn instanceof WorldGenRegion) {
			WorldGenRegion region = (WorldGenRegion)worldIn;
			pos = worldIn.getHeight(Type.WORLD_SURFACE_WG, pos).down();
			Biome b = region.getBiome(pos);
			Template temp = region.getWorld().getStructureTemplateManager().getTemplate(b == Biomes.DESERT ? DESERT : (b == Biomes.SAVANNA || b == Biomes.SAVANNA_PLATEAU ? SAVANNA : PLAINS));
			System.out.println(pos);
			temp.addBlocksToWorld(worldIn, pos, new PlacementSettings());
			List<BlockInfo> infos = temp.func_215381_a(pos, new PlacementSettings(), Blocks.STRUCTURE_BLOCK);
			for(BlockInfo info : infos) {
				if(info.nbt.contains("metadata") && info.nbt.getString("metadata").contentEquals("chest")) {
					//region.setBlockState(info.pos.down(), Blocks.CHEST.getDefaultState(), 2);
					TileEntity te = region.getTileEntity(info.pos.down());
					if(te instanceof ChestTileEntity) {
						if (b == Biomes.DESERT)
							((ChestTileEntity)te).setLootTable(new ResourceLocation("chests/village/village_desert_house"), region.getSeed());
						if (b == Biomes.PLAINS)
							((ChestTileEntity)te).setLootTable(new ResourceLocation("chests/village/village_plains_house"), region.getSeed());
						else if (b == Biomes.SAVANNA || b == Biomes.SAVANNA_PLATEAU)
							((ChestTileEntity)te).setLootTable(new ResourceLocation("chests/village/village_savanna_house"), region.getSeed());
					}
					region.setBlockState(info.pos, Blocks.AIR.getDefaultState(), 2);
				}
			}
			
			VillagerEntity vil = EntityType.VILLAGER.create(region.getWorld());
			vil.setPosition(pos.getX() + 8, pos.getY() + 3, pos.getZ() + 8);
			region.addEntity(vil);
			
			return true;
		}
		return false;
	}

}
