package net.tardis.mod.world.feature.structures;

import java.util.List;
import java.util.Random;
import java.util.function.Function;

import com.mojang.datafixers.Dynamic;

import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.WorldGenRegion;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.Template.BlockInfo;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootParameterSets;
import net.minecraft.world.storage.loot.LootParameters;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraft.world.storage.loot.LootTables;
import net.tardis.mod.Tardis;
import net.tardis.mod.schematics.Schematics;
import net.tardis.mod.tileentities.ShipComputerTile;
import net.tardis.mod.world.WorldGen;

public class CrashedStructure extends Feature<NoFeatureConfig> {

	public static final ResourceLocation CRASHED_SHIP = new ResourceLocation(Tardis.MODID, "tardis/structures/worldgen/crashed_shuttle");
	
	
	public CrashedStructure(Function<Dynamic<?>, ? extends NoFeatureConfig> configFactoryIn) {
		super(configFactoryIn);
	}

	@Override
	public boolean place(IWorld worldIn, ChunkGenerator<? extends GenerationSettings> generator, Random rand, BlockPos pos, NoFeatureConfig config) {
		if(worldIn.getDimension().getType() != DimensionType.OVERWORLD)
			return false;
		
		if(worldIn instanceof WorldGenRegion) {
			WorldGenRegion reg = (WorldGenRegion)worldIn;
			Template temp = reg.getWorld().getStructureTemplateManager().getTemplate(CRASHED_SHIP);
			if(temp != null) {
				pos = worldIn.getHeight(Type.WORLD_SURFACE_WG, pos).down(7);
				int lowest = 1000;
				for(int x = 0; x < temp.getSize().getX(); ++x) {
					for(int z = 0; z < temp.getSize().getZ(); ++z) {
						BlockPos test = reg.getHeight(Type.WORLD_SURFACE_WG, pos.add(x, 0, z));
						if(test.getY() < lowest)
							lowest = test.getY();
					}
				}
				pos = new BlockPos(pos.getX(), lowest, pos.getZ()).down(3);
				
				PlacementSettings set = new PlacementSettings();
				temp.addBlocksToWorld(reg, pos, set);
				System.out.println("Crashed Ship " + pos);
				WorldGen.BROKEN_EXTERIORS.add(pos);
				
				List<BlockInfo> infos = temp.func_215381_a(pos, set, Blocks.STRUCTURE_BLOCK);
				for(BlockInfo info : infos) {
					if(info.nbt.contains("metadata") && info.nbt.getString("metadata").contentEquals("computer")) {
						TileEntity te = reg.getTileEntity(info.pos.down());
						if(te instanceof ShipComputerTile) {
							ShipComputerTile computer = (ShipComputerTile)te;
							computer.setSchematic(worldIn.getRandom().nextBoolean() ? Schematics.POLICE_BOX : Schematics.POLICE_BOX_MODERN);
							//TODO: Loot tables
							LootTable table = reg.getWorld().getServer().getLootTableManager().getLootTableFromLocation(LootTables.CHESTS_SIMPLE_DUNGEON);
							List<ItemStack> stacks = table.generate(new LootContext.Builder(reg.getWorld())
									.withParameter(LootParameters.POSITION, te.getPos())
									.build(LootParameterSets.CHEST));
							
							if(stacks.size() < computer.getSlots()) {
								for(ItemStack stack : stacks) {
									int i = 0;
									while(!computer.getStackInSlot(i).isEmpty()) {
										i = reg.getRandom().nextInt(computer.getSlots() - 1);
									}
									computer.setStackInSlot(i, stack);
								}
							}
						}
						reg.setBlockState(info.pos, Blocks.AIR.getDefaultState(), 3);
					}
				}
				
				return true;
			}
		}
		return false;
	}

}
