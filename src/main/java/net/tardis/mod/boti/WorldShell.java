package net.tardis.mod.boti;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.fluid.Fluids;
import net.minecraft.fluid.IFluidState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IEnviromentBlockReader;
import net.minecraft.world.LightType;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.client.renderers.boti.BotiManager;

public class WorldShell implements IEnviromentBlockReader{

	private HashMap<BlockPos, BlockStore> blocks = new HashMap<>();
	private HashMap<BlockPos, TileEntity> tileEntities = new HashMap<>();
	private boolean update = true;
	private BlockPos offset = BlockPos.ZERO;
	private Biome biome = Biomes.THE_VOID;
	
	//Object type to not make servers sad -- on client will be a BotiWorld
	private Object botiWorld;
	private Object botiManager;
	
	public WorldShell(BlockPos offset){
		this.offset = offset.toImmutable();
	}
	
	public void put(BlockPos pos, BlockStore store) {
		blocks.put(pos.toImmutable(), store);
	}
	
	public BlockStore get(BlockPos pos) {
		return blocks.get(pos);
	}
	
	public Map<BlockPos, BlockStore> getMap(){
		return this.blocks;
	}

	@Override
	public TileEntity getTileEntity(BlockPos pos) {
		return tileEntities.get(pos);
	}

	@Override
	public BlockState getBlockState(BlockPos pos) {
		return get(pos) != null ? get(pos).getState() : Blocks.AIR.getDefaultState();
	}

	@Override
	public IFluidState getFluidState(BlockPos pos) {
		return get(pos) != null ? get(pos).getState().getFluidState() : Fluids.EMPTY.getDefaultState();
	}

	@Override
	public Biome getBiome(BlockPos pos) {
		return biome;
	}
	
	public void setBiome(Biome b) {
		this.biome = b;
	}

	@Override
	public int getLightFor(LightType type, BlockPos pos) {
		return get(pos) != null ? get(pos).getLight() : 15;
	}
	
	@OnlyIn(Dist.CLIENT)
	public void setupTEs(){
		tileEntities.clear();
		this.setWorld(new BotiWorld(DimensionType.OVERWORLD, this, Minecraft.getInstance().worldRenderer));
		for(Entry<BlockPos, BlockStore> entry : blocks.entrySet()) {
			TileEntity te = entry.getValue().getTile();
			if(te != null) {
				te.setWorld(this.getWorld());
				tileEntities.put(entry.getKey(), te);
			}
		}
	}
	
	@Nullable
	@OnlyIn(Dist.CLIENT)
	public BotiWorld getWorld() {
		return (BotiWorld)this.botiWorld;
	}
	
	@OnlyIn(Dist.CLIENT)
	public void setWorld(BotiWorld world) {
		this.botiWorld = world;
	}
	
	@OnlyIn(Dist.CLIENT)
	public BotiManager getRenderManager() {
		return (BotiManager)this.botiManager;
	}
	
	public Map<BlockPos, TileEntity> getTiles(){
		return tileEntities;
	}
	
	public boolean needsUpdate() {
		return this.update;
	}
	
	public void setNeedsUpdate(boolean update) {
		this.update = update;
	}
	
	public BlockPos getOffset() {
		return this.offset;
	}
	
	public void writeToBuffer(PacketBuffer buf) {
		buf.writeBlockPos(offset);
		buf.writeResourceLocation(this.biome.getRegistryName());
		buf.writeInt(this.blocks.size());
		for(Entry<BlockPos, BlockStore> entry : this.getMap().entrySet()) {
			buf.writeBlockPos(entry.getKey());
			buf.writeCompoundTag(entry.getValue().serialize());
		}
	}
	
	public static WorldShell readFromBuffer(PacketBuffer buffer) {
		WorldShell shell = new WorldShell(buffer.readBlockPos());
		shell.setBiome(ForgeRegistries.BIOMES.getValue(buffer.readResourceLocation()));
		int size = buffer.readInt();
		for(int i = 0; i < size; ++i) {
			BlockPos pos = buffer.readBlockPos();
			CompoundNBT store = buffer.readCompoundTag();
			shell.put(pos, BlockStore.deserialize(store));
		}
		return shell;
	}
}
