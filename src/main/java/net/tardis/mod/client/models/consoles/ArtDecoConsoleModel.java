package net.tardis.mod.client.models.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.StabilizerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.controls.XControl;
import net.tardis.mod.controls.YControl;
import net.tardis.mod.controls.ZControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.ConsoleTile;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


@SuppressWarnings("deprecation")
public class ArtDecoConsoleModel extends Model {
	private final RendererModel bone35;
	private final RendererModel basepanels;
	private final RendererModel bone8;
	private final RendererModel bone2;
	private final RendererModel bone3;
	private final RendererModel bone4;
	private final RendererModel bone5;
	private final RendererModel bone6;
	private final RendererModel bone7;
	private final RendererModel bone9;
	private final RendererModel bone10;
	private final RendererModel bone11;
	private final RendererModel bone12;
	private final RendererModel paneledges;
	private final RendererModel bone14;
	private final RendererModel bone15;
	private final RendererModel bone16;
	private final RendererModel bone17;
	private final RendererModel bone18;
	private final RendererModel bone19;
	private final RendererModel bone20;
	private final RendererModel bone21;
	private final RendererModel bone22;
	private final RendererModel bone23;
	private final RendererModel bone24;
	private final RendererModel components;
	private final RendererModel NorthWest;
	private final RendererModel rotation;
	private final RendererModel refuel_rotate_y;
	private final RendererModel rotate_Y3;
	private final RendererModel incmodcontrol;
	private final RendererModel door_rotate_y;
	private final RendererModel rotate_Y2;
	private final RendererModel North;
	private final RendererModel rotation2;
	private final RendererModel bone130;
	private final RendererModel bone131;
	private final RendererModel bone129;
	private final RendererModel fastreturn;
	private final RendererModel communicator;
	private final RendererModel bone40;
	private final RendererModel bone44;
	private final RendererModel bone138;
	private final RendererModel bone71;
	private final RendererModel bone137;
	private final RendererModel bone72;
	private final RendererModel bone136;
	private final RendererModel bone73;
	private final RendererModel bone135;
	private final RendererModel bone76;
	private final RendererModel bone133;
	private final RendererModel bone79;
	private final RendererModel bone134;
	private final RendererModel bone139;
	private final RendererModel bone140;
	private final RendererModel bone141;
	private final RendererModel bone142;
	private final RendererModel bone143;
	private final RendererModel bone144;
	private final RendererModel bone145;
	private final RendererModel bone146;
	private final RendererModel bone147;
	private final RendererModel bone148;
	private final RendererModel bone149;
	private final RendererModel bone150;
	private final RendererModel landtype_translate_z;
	private final RendererModel SouthEast;
	private final RendererModel rotation3;
	private final RendererModel bone87;
	private final RendererModel ycontrol;
	private final RendererModel zcontrol;
	private final RendererModel xcontrol;
	private final RendererModel bone81;
	private final RendererModel throttle_rotate_x;
	private final RendererModel handbrake_rotate_x;
	private final RendererModel South;
	private final RendererModel rotation4;
	private final RendererModel stabiliser_rotate_y;
	private final RendererModel dimension_translate_z;
	private final RendererModel bone33;
	private final RendererModel bone34;
	private final RendererModel NorthEast;
	private final RendererModel rotation5;
	private final RendererModel bone41;
	private final RendererModel sonicport;
	private final RendererModel bone88;
	private final RendererModel telepathiccircuit;
	private final RendererModel bone32;
	private final RendererModel bone80;
	private final RendererModel bone85;
	private final RendererModel bone166;
	private final RendererModel SouthWest;
	private final RendererModel rotation6;
	private final RendererModel bone86;
	private final RendererModel bone83;
	private final RendererModel bone84;
	private final RendererModel facing_rotate_y;
	private final RendererModel bone36;
	private final RendererModel underconsole;
	private final RendererModel base;
	private final RendererModel bone116;
	private final RendererModel bone117;
	private final RendererModel bone118;
	private final RendererModel bone119;
	private final RendererModel bone120;
	private final RendererModel basestruts2;
	private final RendererModel bone111;
	private final RendererModel bone112;
	private final RendererModel bone113;
	private final RendererModel bone114;
	private final RendererModel bone115;
	private final RendererModel basestruts1;
	private final RendererModel bone106;
	private final RendererModel bone107;
	private final RendererModel bone108;
	private final RendererModel bone109;
	private final RendererModel bone110;
	private final RendererModel underedges;
	private final RendererModel bone95;
	private final RendererModel bone96;
	private final RendererModel bone97;
	private final RendererModel bone98;
	private final RendererModel bone99;
	private final RendererModel bone100;
	private final RendererModel bone101;
	private final RendererModel bone102;
	private final RendererModel bone103;
	private final RendererModel bone104;
	private final RendererModel bone105;
	private final RendererModel underborder2;
	private final RendererModel bone90;
	private final RendererModel bone91;
	private final RendererModel bone92;
	private final RendererModel bone93;
	private final RendererModel bone94;
	private final RendererModel underborder;
	private final RendererModel bone26;
	private final RendererModel bone27;
	private final RendererModel bone28;
	private final RendererModel bone29;
	private final RendererModel bone30;
	private final RendererModel underpanels;
	private final RendererModel bone37;
	private final RendererModel bone38;
	private final RendererModel bone39;
	private final RendererModel bone121;
	private final RendererModel bone122;
	private final RendererModel bone123;
	private final RendererModel bone124;
	private final RendererModel bone125;
	private final RendererModel bone126;
	private final RendererModel bone127;
	private final RendererModel bone128;
	private final RendererModel centre;
	private final RendererModel base2;
	private final RendererModel bone25;
	private final RendererModel bone69;
	private final RendererModel bone74;
	private final RendererModel bone75;
	private final RendererModel bone89;
	private final RendererModel glow;
	private final RendererModel rotor_translate_y_rotate_y;
	private final RendererModel base5;
	private final RendererModel bone161;
	private final RendererModel bone162;
	private final RendererModel bone163;
	private final RendererModel bone164;
	private final RendererModel bone165;
	private final RendererModel base4;
	private final RendererModel bone156;
	private final RendererModel bone157;
	private final RendererModel bone158;
	private final RendererModel bone159;
	private final RendererModel bone160;
	private final RendererModel base3;
	private final RendererModel bone151;
	private final RendererModel bone152;
	private final RendererModel bone153;
	private final RendererModel bone154;
	private final RendererModel bone155;
	private final RendererModel lamp;
	private final RendererModel rotation7;
	private final RendererModel redbutton;
	private final RendererModel rotation8;
	private final RendererModel decals;
	private final RendererModel bone66;
	private final RendererModel bone61;
	private final RendererModel bone68;
	private final RendererModel bone70;
	private final RendererModel bone62;
	private final RendererModel bone77;
	private final RendererModel bone78;
	private final RendererModel bone63;
	private final RendererModel bone65;
	private final RendererModel bone58;
	private final RendererModel bone59;
	private final RendererModel bone64;
	private final RendererModel bone53;
	private final RendererModel bone54;
	private final RendererModel bone55;
	private final RendererModel bone56;
	private final RendererModel bone31;
	private final RendererModel bone51;
	private final RendererModel bone;
	private final RendererModel bone46;
	private final RendererModel bone47;
	private final RendererModel bone48;
	private final RendererModel bone49;
	private final RendererModel bone13;
	private final RendererModel bone42;
	private final RendererModel bone43;

	public ArtDecoConsoleModel() {
		textureWidth = 256;
		textureHeight = 256;

		bone35 = new RendererModel(this);
		bone35.setRotationPoint(0.0F, 3.0F, 0.0F);
		

		basepanels = new RendererModel(this);
		basepanels.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone35.addChild(basepanels);
		

		bone8 = new RendererModel(this);
		bone8.setRotationPoint(0.0F, -3.0F, 45.0F);
		basepanels.addChild(bone8);
		setRotationAngle(bone8, -0.2618F, 0.0F, 0.0F);
		bone8.cubeList.add(new ModelBox(bone8, 150, 0, -11.0F, -4.0F, -31.95F, 22, 2, 4, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 150, 0, -8.0F, -4.0F, -36.95F, 16, 2, 5, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 144, 32, -9.0F, -3.0F, -33.0F, 18, 2, 6, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 144, 24, -12.0F, -3.0F, -27.0F, 24, 2, 6, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 144, 16, -15.0F, -3.0F, -21.0F, 30, 2, 6, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 144, 8, -18.0F, -3.0F, -15.0F, 36, 2, 6, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 144, 0, -25.0F, -3.0F, -3.0F, 50, 2, 6, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 144, 0, -22.0F, -3.0F, -9.0F, 44, 2, 6, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		basepanels.addChild(bone2);
		setRotationAngle(bone2, 0.0F, -1.0472F, 0.0F);
		

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.0F, -3.0F, 45.0F);
		bone2.addChild(bone3);
		setRotationAngle(bone3, -0.2618F, 0.0F, 0.0F);
		bone3.cubeList.add(new ModelBox(bone3, 144, 0, -25.0F, -3.0F, -3.0F, 50, 2, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 144, 0, -22.0F, -3.0F, -9.0F, 44, 2, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 144, 8, -18.0F, -3.0F, -15.0F, 36, 2, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 144, 16, -15.0F, -3.0F, -21.0F, 30, 2, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 144, 24, -12.0F, -3.0F, -27.0F, 24, 2, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 144, 32, -9.0F, -3.0F, -33.0F, 18, 2, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 150, 0, -11.0F, -4.0F, -31.95F, 22, 2, 4, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 150, 0, -8.0F, -4.0F, -36.95F, 16, 2, 5, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone2.addChild(bone4);
		setRotationAngle(bone4, 0.0F, -1.0472F, 0.0F);
		

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(0.0F, -3.0F, 45.0F);
		bone4.addChild(bone5);
		setRotationAngle(bone5, -0.2618F, 0.0F, 0.0F);
		bone5.cubeList.add(new ModelBox(bone5, 144, 0, -25.0F, -3.0F, -3.0F, 50, 2, 6, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 144, 0, -22.0F, -3.0F, -9.0F, 44, 2, 6, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 144, 8, -18.0F, -3.0F, -15.0F, 36, 2, 6, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 144, 16, -15.0F, -3.0F, -21.0F, 30, 2, 6, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 144, 24, -12.0F, -3.0F, -27.0F, 24, 2, 6, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 144, 32, -9.0F, -3.0F, -33.0F, 18, 2, 6, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 150, 0, -11.0F, -4.0F, -31.95F, 22, 2, 4, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 150, 0, -8.0F, -4.0F, -36.95F, 16, 2, 5, 0.0F, false));

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone4.addChild(bone6);
		setRotationAngle(bone6, 0.0F, -1.0472F, 0.0F);
		

		bone7 = new RendererModel(this);
		bone7.setRotationPoint(0.0F, -3.0F, 45.0F);
		bone6.addChild(bone7);
		setRotationAngle(bone7, -0.2618F, 0.0F, 0.0F);
		bone7.cubeList.add(new ModelBox(bone7, 144, 0, -25.0F, -3.0F, -3.0F, 50, 2, 6, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 144, 0, -22.0F, -3.0F, -9.0F, 44, 2, 6, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 144, 8, -18.0F, -3.0F, -15.0F, 36, 2, 6, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 144, 16, -15.0F, -3.0F, -21.0F, 30, 2, 6, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 144, 24, -12.0F, -3.0F, -27.0F, 24, 2, 6, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 144, 32, -9.0F, -3.0F, -33.0F, 18, 2, 6, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 150, 0, -11.0F, -4.0F, -31.95F, 22, 2, 4, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 150, 0, -8.0F, -4.0F, -36.95F, 16, 2, 5, 0.0F, false));

		bone9 = new RendererModel(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone6.addChild(bone9);
		setRotationAngle(bone9, 0.0F, -1.0472F, 0.0F);
		

		bone10 = new RendererModel(this);
		bone10.setRotationPoint(0.0F, -3.0F, 45.0F);
		bone9.addChild(bone10);
		setRotationAngle(bone10, -0.2618F, 0.0F, 0.0F);
		bone10.cubeList.add(new ModelBox(bone10, 144, 0, -25.0F, -3.0F, -3.0F, 50, 2, 6, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 144, 0, -22.0F, -3.0F, -9.0F, 44, 2, 6, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 144, 8, -18.0F, -3.0F, -15.0F, 36, 2, 6, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 144, 16, -15.0F, -3.0F, -21.0F, 30, 2, 6, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 144, 24, -12.0F, -3.0F, -27.0F, 24, 2, 6, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 144, 32, -9.0F, -3.0F, -33.0F, 18, 2, 6, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 150, 0, -11.0F, -4.0F, -31.95F, 22, 2, 4, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 150, 0, -8.0F, -4.0F, -36.95F, 16, 2, 5, 0.0F, false));

		bone11 = new RendererModel(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone9.addChild(bone11);
		setRotationAngle(bone11, 0.0F, -1.0472F, 0.0F);
		

		bone12 = new RendererModel(this);
		bone12.setRotationPoint(0.0F, -3.0F, 45.0F);
		bone11.addChild(bone12);
		setRotationAngle(bone12, -0.2618F, 0.0F, 0.0F);
		bone12.cubeList.add(new ModelBox(bone12, 144, 0, -25.0F, -3.0F, -3.0F, 50, 2, 6, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 144, 0, -22.0F, -3.0F, -9.0F, 44, 3, 6, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 144, 8, -18.0F, -3.0F, -15.0F, 36, 2, 6, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 144, 16, -15.0F, -3.0F, -21.0F, 30, 2, 6, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 144, 24, -12.0F, -3.0F, -27.0F, 24, 2, 6, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 144, 32, -9.0F, -3.0F, -33.0F, 18, 2, 6, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 150, 0, -11.0F, -4.0F, -31.95F, 22, 2, 4, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 150, 0, -8.0F, -4.0F, -36.95F, 16, 2, 5, 0.0F, false));

		paneledges = new RendererModel(this);
		paneledges.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone35.addChild(paneledges);
		setRotationAngle(paneledges, 0.0F, -0.5236F, 0.0F);
		

		bone14 = new RendererModel(this);
		bone14.setRotationPoint(0.0F, -3.0F, 45.0F);
		paneledges.addChild(bone14);
		setRotationAngle(bone14, -0.2313F, 0.0F, 0.0F);
		bone14.cubeList.add(new ModelBox(bone14, 136, 49, -3.0F, -4.65F, -34.6F, 6, 4, 44, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 177, 90, -3.0F, -0.65F, -34.6F, 6, 5, 3, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 171, 82, -2.0F, -5.8234F, -35.9292F, 4, 2, 11, 0.0F, false));

		bone15 = new RendererModel(this);
		bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
		paneledges.addChild(bone15);
		setRotationAngle(bone15, 0.0F, -1.0472F, 0.0F);
		

		bone16 = new RendererModel(this);
		bone16.setRotationPoint(0.0F, -3.0F, 45.0F);
		bone15.addChild(bone16);
		setRotationAngle(bone16, -0.2313F, 0.0F, 0.0F);
		bone16.cubeList.add(new ModelBox(bone16, 136, 49, -3.0F, -4.65F, -34.6F, 6, 4, 44, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 177, 90, -7.0F, -0.65F, -34.6F, 12, 5, 3, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 171, 82, -2.0F, -5.8234F, -35.9292F, 4, 2, 11, 0.0F, false));

		bone17 = new RendererModel(this);
		bone17.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone15.addChild(bone17);
		setRotationAngle(bone17, 0.0F, -1.0472F, 0.0F);
		

		bone18 = new RendererModel(this);
		bone18.setRotationPoint(0.0F, -3.0F, 45.0F);
		bone17.addChild(bone18);
		setRotationAngle(bone18, -0.2313F, 0.0F, 0.0F);
		bone18.cubeList.add(new ModelBox(bone18, 136, 49, -3.0F, -4.65F, -34.6F, 6, 4, 44, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 177, 90, -7.0F, -0.65F, -34.6F, 12, 5, 3, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 171, 82, -2.0F, -5.8234F, -35.9292F, 4, 2, 11, 0.0F, false));

		bone19 = new RendererModel(this);
		bone19.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone17.addChild(bone19);
		setRotationAngle(bone19, 0.0F, -1.0472F, 0.0F);
		

		bone20 = new RendererModel(this);
		bone20.setRotationPoint(0.0F, -3.0F, 45.0F);
		bone19.addChild(bone20);
		setRotationAngle(bone20, -0.2313F, 0.0F, 0.0F);
		bone20.cubeList.add(new ModelBox(bone20, 136, 49, -3.0F, -4.65F, -34.6F, 6, 4, 44, 0.0F, false));
		bone20.cubeList.add(new ModelBox(bone20, 177, 90, -7.0F, -0.65F, -34.6F, 12, 5, 3, 0.0F, false));
		bone20.cubeList.add(new ModelBox(bone20, 171, 82, -2.0F, -5.8234F, -35.9292F, 4, 2, 11, 0.0F, false));

		bone21 = new RendererModel(this);
		bone21.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone19.addChild(bone21);
		setRotationAngle(bone21, 0.0F, -1.0472F, 0.0F);
		

		bone22 = new RendererModel(this);
		bone22.setRotationPoint(0.0F, -3.0F, 45.0F);
		bone21.addChild(bone22);
		setRotationAngle(bone22, -0.2313F, 0.0F, 0.0F);
		bone22.cubeList.add(new ModelBox(bone22, 136, 49, -3.0F, -4.65F, -34.6F, 6, 4, 44, 0.0F, false));
		bone22.cubeList.add(new ModelBox(bone22, 177, 90, -7.0F, -0.65F, -34.6F, 12, 5, 3, 0.0F, false));
		bone22.cubeList.add(new ModelBox(bone22, 171, 82, -2.0F, -5.8234F, -35.9292F, 4, 2, 11, 0.0F, false));

		bone23 = new RendererModel(this);
		bone23.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone21.addChild(bone23);
		setRotationAngle(bone23, 0.0F, -1.0472F, 0.0F);
		

		bone24 = new RendererModel(this);
		bone24.setRotationPoint(0.0F, -3.0F, 45.0F);
		bone23.addChild(bone24);
		setRotationAngle(bone24, -0.2313F, 0.0F, 0.0F);
		bone24.cubeList.add(new ModelBox(bone24, 136, 49, -3.0F, -4.65F, -34.6F, 6, 4, 44, 0.0F, false));
		bone24.cubeList.add(new ModelBox(bone24, 177, 90, -7.0F, -0.65F, -34.6F, 12, 5, 3, 0.0F, false));
		bone24.cubeList.add(new ModelBox(bone24, 171, 82, -2.0F, -5.8234F, -35.9292F, 4, 2, 11, 0.0F, false));

		components = new RendererModel(this);
		components.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone35.addChild(components);
		

		NorthWest = new RendererModel(this);
		NorthWest.setRotationPoint(0.0F, 0.0F, 0.0F);
		components.addChild(NorthWest);
		setRotationAngle(NorthWest, 0.0F, -1.0472F, 0.0F);
		

		rotation = new RendererModel(this);
		rotation.setRotationPoint(-0.5F, -12.5F, -23.5F);
		NorthWest.addChild(rotation);
		setRotationAngle(rotation, 0.2618F, 0.0F, 0.0F);
		rotation.cubeList.add(new ModelBox(rotation, 0, 6, -14.7405F, -0.2042F, -21.0635F, 4, 1, 4, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 18, 6, -14.7405F, 0.2958F, -21.0635F, 4, 1, 4, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 39, 2, 11.7595F, 0.2958F, -22.0635F, 5, 1, 5, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 16, 13, 6.7595F, 0.2958F, -24.0635F, 2, 1, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 16, 13, -7.2405F, 0.2958F, -24.0635F, 2, 1, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 0, 13, 9.7595F, 0.2958F, -24.0635F, 6, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 0, 13, -14.2405F, 0.2958F, -24.0635F, 6, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 39, 9, 12.0095F, -0.4542F, -21.8135F, 1, 1, 1, 0.0F, false));

		refuel_rotate_y = new RendererModel(this);
		refuel_rotate_y.setRotationPoint(0.5138F, 0.7958F, -1.5263F);
		rotation.addChild(refuel_rotate_y);
		setRotationAngle(refuel_rotate_y, 0.0F, -0.7854F, 0.0F);
		refuel_rotate_y.cubeList.add(new ModelBox(refuel_rotate_y, 0, 0, 0.375F, -0.5F, 0.375F, 2, 1, 2, 0.0F, false));
		refuel_rotate_y.cubeList.add(new ModelBox(refuel_rotate_y, 0, 0, -1.875F, -0.5F, -1.875F, 2, 1, 2, 0.0F, false));

		rotate_Y3 = new RendererModel(this);
		rotate_Y3.setRotationPoint(-7.246F, 0.5641F, -10.0194F);
		rotation.addChild(rotate_Y3);
		setRotationAngle(rotate_Y3, 0.0F, -0.7854F, 0.0F);
		rotate_Y3.cubeList.add(new ModelBox(rotate_Y3, 19, 0, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		incmodcontrol = new RendererModel(this);
		incmodcontrol.setRotationPoint(-12.7405F, -0.2042F, -19.0635F);
		rotation.addChild(incmodcontrol);
		setRotationAngle(incmodcontrol, 0.0F, -0.6981F, 0.0F);
		incmodcontrol.cubeList.add(new ModelBox(incmodcontrol, 32, 5, -0.25F, 0.0F, -1.0F, 1, 0, 3, 0.0F, false));

		door_rotate_y = new RendererModel(this);
		door_rotate_y.setRotationPoint(14.1762F, -0.3709F, -19.5635F);
		rotation.addChild(door_rotate_y);
		setRotationAngle(door_rotate_y, 0.0F, 0.3491F, 0.0F);
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 44, 9, -0.4167F, 0.1667F, -2.5F, 1, 1, 2, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 51, 9, -0.4167F, -0.3333F, -0.5F, 1, 1, 1, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 36, 14, -0.9167F, -1.3333F, -0.5F, 8, 1, 1, 0.0F, false));

		rotate_Y2 = new RendererModel(this);
		rotate_Y2.setRotationPoint(8.354F, 0.5641F, -10.3694F);
		rotation.addChild(rotate_Y2);
		setRotationAngle(rotate_Y2, 0.0F, 2.3562F, 0.0F);
		rotate_Y2.cubeList.add(new ModelBox(rotate_Y2, 19, 0, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

		North = new RendererModel(this);
		North.setRotationPoint(0.0F, 0.0F, -6.0F);
		components.addChild(North);
		

		rotation2 = new RendererModel(this);
		rotation2.setRotationPoint(-0.5F, -12.5F, -23.5F);
		North.addChild(rotation2);
		setRotationAngle(rotation2, 0.2618F, 0.0F, 0.0F);
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 35, -12.5F, 2.0458F, -17.2635F, 1, 1, 8, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 14, 40, -9.5F, 1.0458F, -3.2635F, 3, 1, 6, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, -0.75F, 1.0458F, -17.2635F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 29, 39, -0.75F, 1.7958F, -10.2635F, 3, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 29, 39, -0.75F, 1.7958F, -4.2635F, 3, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 33, 47, 0.25F, 1.5458F, -4.5135F, 1, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 27, 50, -0.25F, 1.7958F, -8.2635F, 2, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 13, 50, -0.75F, 1.5458F, -17.2635F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 33, 42, -8.25F, 1.2958F, -3.0135F, 1, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 46, -9.5F, 2.0458F, -4.7635F, 3, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 46, 8.0F, 2.0458F, -4.7635F, 3, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 38, 42, 8.0F, 1.0458F, -3.2635F, 3, 1, 6, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 19, 55, 11.5F, 1.2958F, -13.5135F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 55, 12.5F, 2.0458F, -15.0135F, 1, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 5, 55, 11.5F, 2.0458F, -18.0135F, 1, 1, 2, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 12, 55, 13.5F, 2.0458F, -18.0135F, 1, 1, 2, 0.0F, false));

		bone130 = new RendererModel(this);
		bone130.setRotationPoint(-8.0F, 2.5458F, -4.2635F);
		rotation2.addChild(bone130);
		setRotationAngle(bone130, 0.0F, 0.0F, -0.5236F);
		bone130.cubeList.add(new ModelBox(bone130, 9, 45, -0.5F, -2.0F, -0.5F, 1, 2, 1, 0.0F, false));

		bone131 = new RendererModel(this);
		bone131.setRotationPoint(9.5F, 2.5458F, -4.2635F);
		rotation2.addChild(bone131);
		setRotationAngle(bone131, 0.0F, 0.0F, 0.5236F);
		bone131.cubeList.add(new ModelBox(bone131, 9, 45, -0.5F, -2.0F, -0.5F, 1, 2, 1, 0.0F, false));

		bone129 = new RendererModel(this);
		bone129.setRotationPoint(13.0F, 1.7958F, -12.0135F);
		rotation2.addChild(bone129);
		setRotationAngle(bone129, -0.3491F, 0.0F, 0.0F);
		bone129.cubeList.add(new ModelBox(bone129, 32, 55, -0.5F, -2.0F, -0.5F, 1, 2, 1, 0.0F, false));

		fastreturn = new RendererModel(this);
		fastreturn.setRotationPoint(0.75F, 2.2958F, -6.7635F);
		rotation2.addChild(fastreturn);
		setRotationAngle(fastreturn, 0.3491F, 0.0F, 0.0F);
		fastreturn.cubeList.add(new ModelBox(fastreturn, 38, 51, -0.5F, -2.0F, -0.5F, 1, 2, 1, 0.0F, false));

		communicator = new RendererModel(this);
		communicator.setRotationPoint(8.75F, 7.6479F, 18.1183F);
		rotation2.addChild(communicator);
		setRotationAngle(communicator, 0.0F, 0.5236F, 0.0F);
		communicator.cubeList.add(new ModelBox(communicator, 0, 59, 2.25F, -6.3521F, -12.3817F, 2, 3, 2, 0.0F, false));

		bone40 = new RendererModel(this);
		bone40.setRotationPoint(3.25F, -20.6818F, -10.0876F);
		communicator.addChild(bone40);
		setRotationAngle(bone40, 0.5236F, 0.0F, 0.0F);
		bone40.cubeList.add(new ModelBox(bone40, 0, 59, -1.0F, 9.2628F, -9.4195F, 2, 3, 2, 0.0F, false));
		bone40.cubeList.add(new ModelBox(bone40, 18, 60, -2.0F, 8.2628F, -10.4195F, 4, 1, 4, 0.0F, false));
		bone40.cubeList.add(new ModelBox(bone40, 31, 55, -4.0F, 7.5128F, -11.9195F, 8, 1, 7, 0.0F, false));

		bone44 = new RendererModel(this);
		bone44.setRotationPoint(0.0F, 7.0128F, -8.4195F);
		bone40.addChild(bone44);
		

		bone138 = new RendererModel(this);
		bone138.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone44.addChild(bone138);
		setRotationAngle(bone138, -0.4363F, 0.0F, 0.0F);
		bone138.cubeList.add(new ModelBox(bone138, 35, 64, -2.5F, -2.5F, -0.5F, 5, 3, 1, 0.0F, false));
		bone138.cubeList.add(new ModelBox(bone138, 35, 64, -3.5F, -5.5F, -0.5F, 7, 1, 1, 0.0F, false));
		bone138.cubeList.add(new ModelBox(bone138, 35, 64, -3.0F, -4.5F, -0.5F, 6, 2, 1, 0.0F, false));

		bone71 = new RendererModel(this);
		bone71.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone44.addChild(bone71);
		setRotationAngle(bone71, 0.0F, -1.0472F, 0.0F);
		

		bone137 = new RendererModel(this);
		bone137.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone71.addChild(bone137);
		setRotationAngle(bone137, -0.4363F, 0.0F, 0.0F);
		bone137.cubeList.add(new ModelBox(bone137, 35, 64, -2.5F, -2.5F, -0.5F, 5, 3, 1, 0.0F, false));
		bone137.cubeList.add(new ModelBox(bone137, 35, 64, -3.5F, -5.5F, -0.5F, 7, 1, 1, 0.0F, false));
		bone137.cubeList.add(new ModelBox(bone137, 35, 64, -3.0F, -4.5F, -0.5F, 6, 2, 1, 0.0F, false));

		bone72 = new RendererModel(this);
		bone72.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone71.addChild(bone72);
		setRotationAngle(bone72, 0.0F, -1.0472F, 0.0F);
		

		bone136 = new RendererModel(this);
		bone136.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone72.addChild(bone136);
		setRotationAngle(bone136, -0.4363F, 0.0F, 0.0F);
		bone136.cubeList.add(new ModelBox(bone136, 35, 64, -2.5F, -2.5F, -0.5F, 5, 3, 1, 0.0F, false));
		bone136.cubeList.add(new ModelBox(bone136, 35, 64, -3.5F, -5.5F, -0.5F, 7, 1, 1, 0.0F, false));
		bone136.cubeList.add(new ModelBox(bone136, 35, 64, -3.0F, -4.5F, -0.5F, 6, 2, 1, 0.0F, false));

		bone73 = new RendererModel(this);
		bone73.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone72.addChild(bone73);
		setRotationAngle(bone73, 0.0F, -1.0472F, 0.0F);
		

		bone135 = new RendererModel(this);
		bone135.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone73.addChild(bone135);
		setRotationAngle(bone135, -0.4363F, 0.0F, 0.0F);
		bone135.cubeList.add(new ModelBox(bone135, 35, 64, -2.5F, -2.5F, -0.5F, 5, 3, 1, 0.0F, false));
		bone135.cubeList.add(new ModelBox(bone135, 35, 64, -3.5F, -5.5F, -0.5F, 7, 1, 1, 0.0F, false));
		bone135.cubeList.add(new ModelBox(bone135, 35, 64, -3.0F, -4.5F, -0.5F, 6, 2, 1, 0.0F, false));

		bone76 = new RendererModel(this);
		bone76.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone73.addChild(bone76);
		setRotationAngle(bone76, 0.0F, -1.0472F, 0.0F);
		

		bone133 = new RendererModel(this);
		bone133.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone76.addChild(bone133);
		setRotationAngle(bone133, -0.4363F, 0.0F, 0.0F);
		bone133.cubeList.add(new ModelBox(bone133, 35, 64, -2.5F, -2.5F, -0.5F, 5, 3, 1, 0.0F, false));
		bone133.cubeList.add(new ModelBox(bone133, 35, 64, -3.5F, -5.5F, -0.5F, 7, 1, 1, 0.0F, false));
		bone133.cubeList.add(new ModelBox(bone133, 35, 64, -3.0F, -4.5F, -0.5F, 6, 2, 1, 0.0F, false));

		bone79 = new RendererModel(this);
		bone79.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone76.addChild(bone79);
		setRotationAngle(bone79, 0.0F, -1.0472F, 0.0F);
		

		bone134 = new RendererModel(this);
		bone134.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone79.addChild(bone134);
		setRotationAngle(bone134, -0.4363F, 0.0F, 0.0F);
		bone134.cubeList.add(new ModelBox(bone134, 35, 64, -2.5F, -2.5F, -0.5F, 5, 3, 1, 0.0F, false));
		bone134.cubeList.add(new ModelBox(bone134, 35, 64, -3.5F, -5.5F, -0.5F, 7, 1, 1, 0.0F, false));
		bone134.cubeList.add(new ModelBox(bone134, 35, 64, -3.0F, -4.5F, -0.5F, 6, 2, 1, 0.0F, false));

		bone139 = new RendererModel(this);
		bone139.setRotationPoint(0.0F, 7.7628F, -8.4195F);
		bone40.addChild(bone139);
		setRotationAngle(bone139, 0.0F, -0.5236F, 0.0F);
		

		bone140 = new RendererModel(this);
		bone140.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone139.addChild(bone140);
		setRotationAngle(bone140, -0.4363F, 0.0F, 0.0F);
		bone140.cubeList.add(new ModelBox(bone140, 52, 64, -0.5F, -6.6F, 0.05F, 1, 6, 1, 0.0F, false));

		bone141 = new RendererModel(this);
		bone141.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone139.addChild(bone141);
		setRotationAngle(bone141, 0.0F, -1.0472F, 0.0F);
		

		bone142 = new RendererModel(this);
		bone142.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone141.addChild(bone142);
		setRotationAngle(bone142, -0.4363F, 0.0F, 0.0F);
		bone142.cubeList.add(new ModelBox(bone142, 52, 64, -0.5F, -6.6F, 0.05F, 1, 6, 1, 0.0F, false));

		bone143 = new RendererModel(this);
		bone143.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone141.addChild(bone143);
		setRotationAngle(bone143, 0.0F, -1.0472F, 0.0F);
		

		bone144 = new RendererModel(this);
		bone144.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone143.addChild(bone144);
		setRotationAngle(bone144, -0.4363F, 0.0F, 0.0F);
		bone144.cubeList.add(new ModelBox(bone144, 52, 64, -0.5F, -6.6F, 0.05F, 1, 6, 1, 0.0F, false));

		bone145 = new RendererModel(this);
		bone145.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone143.addChild(bone145);
		setRotationAngle(bone145, 0.0F, -1.0472F, 0.0F);
		

		bone146 = new RendererModel(this);
		bone146.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone145.addChild(bone146);
		setRotationAngle(bone146, -0.4363F, 0.0F, 0.0F);
		bone146.cubeList.add(new ModelBox(bone146, 52, 64, -0.5F, -6.6F, 0.05F, 1, 6, 1, 0.0F, false));

		bone147 = new RendererModel(this);
		bone147.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone145.addChild(bone147);
		setRotationAngle(bone147, 0.0F, -1.0472F, 0.0F);
		

		bone148 = new RendererModel(this);
		bone148.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone147.addChild(bone148);
		setRotationAngle(bone148, -0.4363F, 0.0F, 0.0F);
		bone148.cubeList.add(new ModelBox(bone148, 52, 64, -0.5F, -6.6F, 0.05F, 1, 6, 1, 0.0F, false));

		bone149 = new RendererModel(this);
		bone149.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone147.addChild(bone149);
		setRotationAngle(bone149, 0.0F, -1.0472F, 0.0F);
		

		bone150 = new RendererModel(this);
		bone150.setRotationPoint(0.0F, 0.0F, 3.85F);
		bone149.addChild(bone150);
		setRotationAngle(bone150, -0.4363F, 0.0F, 0.0F);
		bone150.cubeList.add(new ModelBox(bone150, 52, 64, -0.5F, -6.6F, 0.05F, 1, 6, 1, 0.0F, false));

		landtype_translate_z = new RendererModel(this);
		landtype_translate_z.setRotationPoint(-11.9405F, 3.0458F, -11.2635F);
		rotation2.addChild(landtype_translate_z);
		landtype_translate_z.cubeList.add(new ModelBox(landtype_translate_z, 11, 40, -1.0595F, -1.25F, 0.0F, 2, 1, 1, 0.0F, false));

		SouthEast = new RendererModel(this);
		SouthEast.setRotationPoint(0.0F, 0.0F, 0.0F);
		components.addChild(SouthEast);
		setRotationAngle(SouthEast, 0.0F, 2.0944F, 0.0F);
		

		rotation3 = new RendererModel(this);
		rotation3.setRotationPoint(-0.5F, -12.5F, -29.5F);
		SouthEast.addChild(rotation3);
		setRotationAngle(rotation3, 0.2618F, 0.0F, 0.0F);
		rotation3.cubeList.add(new ModelBox(rotation3, 10, 141, -14.0F, 1.7958F, -5.5135F, 7, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 10, 141, -3.0F, 1.8458F, 8.4865F, 7, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 140, -0.5F, 1.5458F, 5.9865F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 140, 2.5F, 1.5458F, 5.9865F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 145, 6.5F, 1.2958F, 0.9865F, 3, 1, 3, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 140, 5.5F, 1.5458F, 5.9865F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 140, -3.5F, 1.5458F, 5.9865F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 140, -6.5F, 1.5458F, 5.9865F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 10, 144, -9.5F, 2.0458F, -2.0135F, 2, 1, 6, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 10, 141, 8.0F, 1.7958F, -5.5135F, 7, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 32, 154, 12.025F, 0.2958F, -13.2885F, 1, 2, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 11, 155, 12.0F, 1.5458F, -16.2635F, 4, 1, 4, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 38, 138, -13.0F, 1.2958F, -18.2635F, 4, 1, 9, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 61, 150, -12.75F, 0.3208F, -17.2635F, 1, 1, 6, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 46, 150, -10.25F, 0.3208F, -17.2635F, 1, 1, 6, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 44, 159, -12.5F, 1.0458F, -10.7635F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 44, 159, -10.5F, 1.0458F, -10.7635F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 23, 157, -14.25F, 1.7958F, -17.2635F, 1, 1, 7, 0.0F, false));

		bone87 = new RendererModel(this);
		bone87.setRotationPoint(8.0F, 1.5458F, 2.4865F);
		rotation3.addChild(bone87);
		setRotationAngle(bone87, -0.3491F, 0.0F, 0.0F);
		bone87.cubeList.add(new ModelBox(bone87, 10, 144, -0.5F, -1.75F, -0.5F, 1, 2, 1, 0.0F, false));

		ycontrol = new RendererModel(this);
		ycontrol.setRotationPoint(11.5595F, 2.7958F, 0.9865F);
		rotation3.addChild(ycontrol);
		ycontrol.cubeList.add(new ModelBox(ycontrol, 28, 141, -1.0595F, -1.25F, -5.0F, 2, 1, 1, 0.0F, false));

		zcontrol = new RendererModel(this);
		zcontrol.setRotationPoint(14.0595F, 2.7958F, 0.9865F);
		rotation3.addChild(zcontrol);
		zcontrol.cubeList.add(new ModelBox(zcontrol, 28, 141, -1.0595F, -1.25F, -5.0F, 2, 1, 1, 0.0F, false));

		xcontrol = new RendererModel(this);
		xcontrol.setRotationPoint(9.0595F, 2.7958F, 0.9865F);
		rotation3.addChild(xcontrol);
		xcontrol.cubeList.add(new ModelBox(xcontrol, 28, 141, -1.0595F, -1.25F, -5.0F, 2, 1, 1, 0.0F, false));

		bone81 = new RendererModel(this);
		bone81.setRotationPoint(-8.5F, 1.0458F, 0.9865F);
		rotation3.addChild(bone81);
		setRotationAngle(bone81, 0.0F, 0.6981F, 0.0F);
		bone81.cubeList.add(new ModelBox(bone81, 27, 145, -0.5F, -1.0F, -1.0F, 1, 1, 5, 0.0F, false));
		bone81.cubeList.add(new ModelBox(bone81, 21, 147, -0.5F, 0.0F, -0.5F, 1, 1, 1, 0.0F, false));

		throttle_rotate_x = new RendererModel(this);
		throttle_rotate_x.setRotationPoint(-11.0833F, 1.6208F, -13.7635F);
		rotation3.addChild(throttle_rotate_x);
		setRotationAngle(throttle_rotate_x, -0.7854F, 0.0F, 0.0F);
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 56, 150, -1.1667F, -3.325F, -0.5F, 1, 4, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 56, 150, 0.3333F, -3.325F, -0.5F, 1, 4, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 52, 161, -2.1667F, -4.35F, -0.5F, 7, 1, 1, 0.0F, false));

		handbrake_rotate_x = new RendererModel(this);
		handbrake_rotate_x.setRotationPoint(14.0F, 1.0458F, -14.2635F);
		rotation3.addChild(handbrake_rotate_x);
		setRotationAngle(handbrake_rotate_x, 0.0F, 0.5236F, 0.0F);
		handbrake_rotate_x.cubeList.add(new ModelBox(handbrake_rotate_x, 8, 155, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));
		handbrake_rotate_x.cubeList.add(new ModelBox(handbrake_rotate_x, 39, 156, 4.5F, -2.0F, -0.525F, 1, 1, 1, 0.0F, false));
		handbrake_rotate_x.cubeList.add(new ModelBox(handbrake_rotate_x, 2, 163, -2.0F, -1.0F, -0.525F, 8, 1, 1, 0.0F, false));

		South = new RendererModel(this);
		South.setRotationPoint(0.0F, 0.0F, 0.0F);
		components.addChild(South);
		setRotationAngle(South, 0.0F, 3.1416F, 0.0F);
		

		rotation4 = new RendererModel(this);
		rotation4.setRotationPoint(-0.5F, -12.5F, -29.5F);
		South.addChild(rotation4);
		setRotationAngle(rotation4, 0.2618F, 0.0F, 0.0F);
		rotation4.cubeList.add(new ModelBox(rotation4, 18, 204, 5.0F, 2.1458F, 5.9865F, 2, 1, 2, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 18, 204, -5.0F, 2.1458F, 5.9865F, 2, 1, 2, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 0, 195, -13.0F, 1.1458F, -13.0135F, 3, 1, 3, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 25, 195, -3.0F, 1.3958F, -11.0135F, 7, 1, 3, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 13, 197, -2.5F, 1.6458F, -12.5135F, 2, 1, 1, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 13, 197, 0.0F, 1.6458F, -12.5135F, 2, 1, 1, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 20, 197, 2.5F, 1.6458F, -12.5135F, 1, 1, 1, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 0, 201, 11.5F, 2.1458F, -12.5135F, 2, 1, 2, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 40, 195, 7.0F, 1.6458F, -5.0135F, 2, 1, 8, 0.0F, false));

		stabiliser_rotate_y = new RendererModel(this);
		stabiliser_rotate_y.setRotationPoint(12.5F, 1.6458F, -11.5135F);
		rotation4.addChild(stabiliser_rotate_y);
		stabiliser_rotate_y.cubeList.add(new ModelBox(stabiliser_rotate_y, 4, 200, -0.5F, 0.0F, -2.5F, 1, 1, 5, 0.0F, false));
		stabiliser_rotate_y.cubeList.add(new ModelBox(stabiliser_rotate_y, 18, 201, -0.5F, -1.0F, 1.5F, 1, 1, 1, 0.0F, false));
		stabiliser_rotate_y.cubeList.add(new ModelBox(stabiliser_rotate_y, 13, 201, -0.5F, -0.5F, -2.5F, 1, 1, 1, 0.0F, false));

		dimension_translate_z = new RendererModel(this);
		dimension_translate_z.setRotationPoint(7.5595F, 1.2958F, 3.9865F);
		rotation4.addChild(dimension_translate_z);
		dimension_translate_z.cubeList.add(new ModelBox(dimension_translate_z, 34, 201, -0.0595F, -1.65F, -3.0F, 1, 2, 1, 0.0F, false));
		dimension_translate_z.cubeList.add(new ModelBox(dimension_translate_z, 28, 202, -0.0595F, -2.65F, -5.0F, 1, 1, 3, 0.0F, false));

		bone33 = new RendererModel(this);
		bone33.setRotationPoint(6.0F, 2.0458F, 6.9865F);
		rotation4.addChild(bone33);
		setRotationAngle(bone33, 0.0F, -2.0944F, 0.0F);
		bone33.cubeList.add(new ModelBox(bone33, 37, 205, -1.5F, -0.25F, -0.5F, 3, 1, 1, 0.0F, false));
		bone33.cubeList.add(new ModelBox(bone33, 28, 208, -2.5F, -0.75F, -0.5F, 1, 1, 1, 0.0F, false));

		bone34 = new RendererModel(this);
		bone34.setRotationPoint(-4.0F, 2.0458F, 6.9865F);
		rotation4.addChild(bone34);
		setRotationAngle(bone34, 0.0F, 2.0944F, 0.0F);
		bone34.cubeList.add(new ModelBox(bone34, 37, 205, -1.5F, -0.25F, -0.5F, 3, 1, 1, 0.0F, false));
		bone34.cubeList.add(new ModelBox(bone34, 28, 208, -2.5F, -0.75F, -0.5F, 1, 1, 1, 0.0F, false));

		NorthEast = new RendererModel(this);
		NorthEast.setRotationPoint(0.0F, 0.0F, 0.0F);
		components.addChild(NorthEast);
		setRotationAngle(NorthEast, 0.0F, 1.0472F, 0.0F);
		

		rotation5 = new RendererModel(this);
		rotation5.setRotationPoint(-0.5F, -12.5F, -29.5F);
		NorthEast.addChild(rotation5);
		setRotationAngle(rotation5, 0.2618F, 0.0F, 0.0F);
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 75, -17.5F, 1.5458F, -17.2635F, 7, 1, 7, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 28, 78, -17.25F, 1.7958F, -14.2635F, 1, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 28, 78, -11.75F, 1.7958F, -14.2635F, 1, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 23, 78, -14.5F, 1.7958F, -17.0135F, 1, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 84, -6.0F, 2.1458F, -16.5135F, 3, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 9, 84, 1.0F, 2.1458F, -17.0135F, 2, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 84, 4.0F, 2.1458F, -16.5135F, 3, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 95, 10.0F, 2.1458F, -15.5135F, 10, 1, 2, 0.0F, false));

		bone41 = new RendererModel(this);
		bone41.setRotationPoint(-14.0F, 2.0458F, -14.0135F);
		rotation5.addChild(bone41);
		setRotationAngle(bone41, 0.0F, -0.5236F, 0.0F);
		bone41.cubeList.add(new ModelBox(bone41, 29, 77, -0.5F, -0.5F, -0.5F, 1, 0, 4, 0.0F, false));

		sonicport = new RendererModel(this);
		sonicport.setRotationPoint(0.0595F, 2.7958F, -13.0135F);
		rotation5.addChild(sonicport);
		sonicport.cubeList.add(new ModelBox(sonicport, 18, 84, -2.0595F, -0.65F, -4.0F, 2, 1, 2, 0.0F, false));

		bone88 = new RendererModel(this);
		bone88.setRotationPoint(15.0F, 2.1458F, -14.5135F);
		rotation5.addChild(bone88);
		setRotationAngle(bone88, -1.3963F, 0.0F, 0.0F);
		bone88.cubeList.add(new ModelBox(bone88, 49, 79, -4.0F, -6.0F, 0.0F, 8, 6, 0, 0.0F, false));

		telepathiccircuit = new RendererModel(this);
		telepathiccircuit.setRotationPoint(0.5F, 33.0F, 29.5F);
		rotation5.addChild(telepathiccircuit);
		setRotationAngle(telepathiccircuit, 0.0F, 3.1416F, 0.0F);
		telepathiccircuit.cubeList.add(new ModelBox(telepathiccircuit, 26, 108, -7.5F, -33.1F, 30.0F, 15, 4, 4, 0.0F, false));
		telepathiccircuit.cubeList.add(new ModelBox(telepathiccircuit, 27, 101, -6.5F, -32.1F, 34.0F, 13, 2, 4, 0.0F, false));
		telepathiccircuit.cubeList.add(new ModelBox(telepathiccircuit, 23, 95, -8.5F, -31.1F, 37.0F, 17, 1, 4, 0.0F, false));

		bone32 = new RendererModel(this);
		bone32.setRotationPoint(0.0F, -32.6F, 36.0F);
		telepathiccircuit.addChild(bone32);
		setRotationAngle(bone32, -0.3491F, -0.3491F, 0.0F);
		bone32.cubeList.add(new ModelBox(bone32, 0, 100, -0.5F, -0.5F, -3.0F, 1, 1, 5, 0.0F, false));

		bone80 = new RendererModel(this);
		bone80.setRotationPoint(-2.5F, -26.0F, 34.0F);
		telepathiccircuit.addChild(bone80);
		setRotationAngle(bone80, 0.1745F, 0.0F, 0.0F);
		bone80.cubeList.add(new ModelBox(bone80, 0, 107, 9.0F, -11.1F, -1.5F, 2, 8, 1, 0.0F, false));
		bone80.cubeList.add(new ModelBox(bone80, 0, 119, -2.5F, -12.1F, -3.5F, 10, 9, 1, 0.0F, false));
		bone80.cubeList.add(new ModelBox(bone80, 0, 107, -6.0F, -11.1F, -1.5F, 2, 8, 1, 0.0F, false));
		bone80.cubeList.add(new ModelBox(bone80, 0, 117, -4.0F, -9.1F, -0.5F, 13, 1, 0, 0.0F, false));

		bone85 = new RendererModel(this);
		bone85.setRotationPoint(-4.5F, -3.35F, 3.0F);
		bone80.addChild(bone85);
		setRotationAngle(bone85, 0.3491F, 0.0F, 0.0F);
		bone85.cubeList.add(new ModelBox(bone85, 14, 100, -0.5F, -4.5F, -0.5F, 1, 5, 1, 0.0F, false));

		bone166 = new RendererModel(this);
		bone166.setRotationPoint(0.0F, -35.1F, 34.5F);
		telepathiccircuit.addChild(bone166);
		setRotationAngle(bone166, 0.4363F, 0.0F, 0.0F);
		bone166.cubeList.add(new ModelBox(bone166, 48, 78, -5.0F, -7.0F, -3.0F, 10, 9, 0, 0.0F, false));

		SouthWest = new RendererModel(this);
		SouthWest.setRotationPoint(0.0F, 0.0F, 0.0F);
		components.addChild(SouthWest);
		setRotationAngle(SouthWest, 0.0F, -2.0944F, 0.0F);
		

		rotation6 = new RendererModel(this);
		rotation6.setRotationPoint(-0.5F, -12.5F, -29.5F);
		SouthWest.addChild(rotation6);
		setRotationAngle(rotation6, 0.2618F, 0.0F, 0.0F);
		rotation6.cubeList.add(new ModelBox(rotation6, 14, 226, -0.5F, 1.6458F, 1.9865F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 5, 226, 2.0F, 1.6458F, 0.9865F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 5, 226, -3.0F, 1.6458F, 0.9865F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 231, -3.5F, 1.6458F, -6.0135F, 8, 1, 4, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 239, -4.5F, 1.8958F, -17.5135F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 239, -2.5F, 1.8958F, -17.5135F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 8, 239, -0.5F, 1.6458F, -17.5135F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 17, 239, 2.5F, 1.8958F, -17.5135F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 24, 239, 4.5F, 1.8958F, -17.5135F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 245, 11.75F, 1.8958F, -13.7635F, 3, 1, 3, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 14, 246, 12.25F, 1.1458F, -13.2635F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 21, 245, 12.75F, 1.3958F, -12.7635F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 18, 244, -16.5F, 1.8958F, -16.0135F, 9, 1, 9, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 54, 248, -12.5F, 0.8958F, -12.0135F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 26, 231, 5.5F, 1.3958F, -6.0135F, 2, 1, 4, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 24, 227, -2.5F, 2.1458F, -1.5135F, 6, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 24, 227, -2.5F, 2.1458F, -7.5135F, 6, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 227, -5.0F, 1.8958F, -0.0135F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 227, 5.0F, 1.8958F, -0.0135F, 1, 1, 1, 0.0F, false));

		bone86 = new RendererModel(this);
		bone86.setRotationPoint(5.0F, 2.1458F, -16.5135F);
		rotation6.addChild(bone86);
		setRotationAngle(bone86, -0.6109F, 0.0F, 0.0F);
		bone86.cubeList.add(new ModelBox(bone86, 32, 239, -0.75F, -1.25F, -0.25F, 1, 2, 1, 0.0F, false));

		bone83 = new RendererModel(this);
		bone83.setRotationPoint(-6.0F, 1.6458F, -15.2635F);
		rotation6.addChild(bone83);
		setRotationAngle(bone83, 0.0F, 0.4363F, 0.0F);
		bone83.cubeList.add(new ModelBox(bone83, 48, 248, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

		bone84 = new RendererModel(this);
		bone84.setRotationPoint(0.0F, -1.0F, 0.0F);
		bone83.addChild(bone84);
		setRotationAngle(bone84, 0.0F, 0.0F, -0.1745F);
		bone84.cubeList.add(new ModelBox(bone84, 48, 244, -3.5F, -0.5F, -0.5F, 4, 1, 1, 0.0F, false));

		facing_rotate_y = new RendererModel(this);
		facing_rotate_y.setRotationPoint(-12.0F, 1.8958F, -11.5135F);
		rotation6.addChild(facing_rotate_y);
		facing_rotate_y.cubeList.add(new ModelBox(facing_rotate_y, 39, 233, -3.5F, -0.5F, -3.5F, 7, 1, 7, 0.0F, false));

		bone36 = new RendererModel(this);
		bone36.setRotationPoint(6.5F, 1.8958F, -4.0135F);
		rotation6.addChild(bone36);
		setRotationAngle(bone36, -0.6109F, 0.0F, 0.0F);
		bone36.cubeList.add(new ModelBox(bone36, 40, 232, -0.5F, -2.5F, -0.5F, 1, 3, 1, 0.0F, false));

		underconsole = new RendererModel(this);
		underconsole.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone35.addChild(underconsole);
		

		base = new RendererModel(this);
		base.setRotationPoint(0.0F, 1.25F, 0.0F);
		underconsole.addChild(base);
		base.cubeList.add(new ModelBox(base, 210, 164, -5.0F, 0.725F, -0.6F, 10, 19, 13, 0.0F, false));

		bone116 = new RendererModel(this);
		bone116.setRotationPoint(0.0F, 0.0F, 0.0F);
		base.addChild(bone116);
		setRotationAngle(bone116, 0.0F, -1.0472F, 0.0F);
		bone116.cubeList.add(new ModelBox(bone116, 210, 164, -5.0F, 0.725F, -0.6F, 10, 19, 13, 0.0F, false));

		bone117 = new RendererModel(this);
		bone117.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone116.addChild(bone117);
		setRotationAngle(bone117, 0.0F, -1.0472F, 0.0F);
		bone117.cubeList.add(new ModelBox(bone117, 210, 164, -5.0F, 0.725F, -0.6F, 10, 19, 13, 0.0F, false));

		bone118 = new RendererModel(this);
		bone118.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone117.addChild(bone118);
		setRotationAngle(bone118, 0.0F, -1.0472F, 0.0F);
		bone118.cubeList.add(new ModelBox(bone118, 210, 164, -5.0F, 0.725F, -0.6F, 10, 19, 13, 0.0F, false));

		bone119 = new RendererModel(this);
		bone119.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone118.addChild(bone119);
		setRotationAngle(bone119, 0.0F, -1.0472F, 0.0F);
		bone119.cubeList.add(new ModelBox(bone119, 210, 164, -5.0F, 0.725F, -0.6F, 10, 19, 13, 0.0F, false));

		bone120 = new RendererModel(this);
		bone120.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone119.addChild(bone120);
		setRotationAngle(bone120, 0.0F, -1.0472F, 0.0F);
		bone120.cubeList.add(new ModelBox(bone120, 210, 164, -5.0F, 0.725F, -0.6F, 10, 19, 13, 0.0F, false));

		basestruts2 = new RendererModel(this);
		basestruts2.setRotationPoint(0.0F, -1.75F, 0.0F);
		underconsole.addChild(basestruts2);
		basestruts2.cubeList.add(new ModelBox(basestruts2, 161, 172, -2.0F, 5.75F, 12.4F, 4, 14, 3, 0.0F, false));

		bone111 = new RendererModel(this);
		bone111.setRotationPoint(0.0F, 0.0F, 0.0F);
		basestruts2.addChild(bone111);
		setRotationAngle(bone111, 0.0F, -1.0472F, 0.0F);
		bone111.cubeList.add(new ModelBox(bone111, 161, 172, -2.0F, 5.75F, 12.4F, 4, 14, 3, 0.0F, false));

		bone112 = new RendererModel(this);
		bone112.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone111.addChild(bone112);
		setRotationAngle(bone112, 0.0F, -1.0472F, 0.0F);
		bone112.cubeList.add(new ModelBox(bone112, 161, 172, -2.0F, 5.75F, 12.4F, 4, 14, 3, 0.0F, false));

		bone113 = new RendererModel(this);
		bone113.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone112.addChild(bone113);
		setRotationAngle(bone113, 0.0F, -1.0472F, 0.0F);
		bone113.cubeList.add(new ModelBox(bone113, 161, 172, -2.0F, 5.75F, 12.4F, 4, 14, 3, 0.0F, false));

		bone114 = new RendererModel(this);
		bone114.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone113.addChild(bone114);
		setRotationAngle(bone114, 0.0F, -1.0472F, 0.0F);
		bone114.cubeList.add(new ModelBox(bone114, 161, 172, -2.0F, 5.75F, 12.4F, 4, 14, 3, 0.0F, false));

		bone115 = new RendererModel(this);
		bone115.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone114.addChild(bone115);
		setRotationAngle(bone115, 0.0F, -1.0472F, 0.0F);
		bone115.cubeList.add(new ModelBox(bone115, 161, 172, -2.0F, 5.75F, 12.4F, 4, 14, 3, 0.0F, false));

		basestruts1 = new RendererModel(this);
		basestruts1.setRotationPoint(0.0F, -3.75F, 0.0F);
		underconsole.addChild(basestruts1);
		setRotationAngle(basestruts1, 0.0F, -0.5236F, 0.0F);
		basestruts1.cubeList.add(new ModelBox(basestruts1, 185, 163, -2.0F, 5.75F, 8.4F, 4, 19, 8, 0.0F, false));

		bone106 = new RendererModel(this);
		bone106.setRotationPoint(0.0F, 0.0F, 0.0F);
		basestruts1.addChild(bone106);
		setRotationAngle(bone106, 0.0F, -1.0472F, 0.0F);
		bone106.cubeList.add(new ModelBox(bone106, 185, 163, -2.0F, 5.75F, 8.4F, 4, 19, 8, 0.0F, false));

		bone107 = new RendererModel(this);
		bone107.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone106.addChild(bone107);
		setRotationAngle(bone107, 0.0F, -1.0472F, 0.0F);
		bone107.cubeList.add(new ModelBox(bone107, 185, 163, -2.0F, 5.75F, 8.4F, 4, 19, 8, 0.0F, false));

		bone108 = new RendererModel(this);
		bone108.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone107.addChild(bone108);
		setRotationAngle(bone108, 0.0F, -1.0472F, 0.0F);
		bone108.cubeList.add(new ModelBox(bone108, 185, 163, -2.0F, 5.75F, 8.4F, 4, 19, 8, 0.0F, false));

		bone109 = new RendererModel(this);
		bone109.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone108.addChild(bone109);
		setRotationAngle(bone109, 0.0F, -1.0472F, 0.0F);
		bone109.cubeList.add(new ModelBox(bone109, 185, 163, -2.0F, 5.75F, 8.4F, 4, 19, 8, 0.0F, false));

		bone110 = new RendererModel(this);
		bone110.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone109.addChild(bone110);
		setRotationAngle(bone110, 0.0F, -1.0472F, 0.0F);
		bone110.cubeList.add(new ModelBox(bone110, 185, 163, -2.0F, 5.75F, 8.4F, 4, 19, 8, 0.0F, false));

		underedges = new RendererModel(this);
		underedges.setRotationPoint(0.0F, -8.75F, 0.0F);
		underconsole.addChild(underedges);
		setRotationAngle(underedges, 0.0F, -0.5236F, 0.0F);
		

		bone95 = new RendererModel(this);
		bone95.setRotationPoint(0.0F, 3.0F, 45.0F);
		underedges.addChild(bone95);
		setRotationAngle(bone95, 0.144F, 0.0F, 0.0F);
		bone95.cubeList.add(new ModelBox(bone95, 150, 63, -3.0F, 2.65F, -21.6F, 6, 2, 30, 0.0F, false));

		bone96 = new RendererModel(this);
		bone96.setRotationPoint(0.0F, 0.0F, 0.0F);
		underedges.addChild(bone96);
		setRotationAngle(bone96, 0.0F, -1.0472F, 0.0F);
		

		bone97 = new RendererModel(this);
		bone97.setRotationPoint(0.0F, 3.0F, 45.0F);
		bone96.addChild(bone97);
		setRotationAngle(bone97, 0.144F, 0.0F, 0.0F);
		bone97.cubeList.add(new ModelBox(bone97, 150, 63, -3.0F, 2.65F, -21.6F, 6, 2, 30, 0.0F, false));

		bone98 = new RendererModel(this);
		bone98.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone96.addChild(bone98);
		setRotationAngle(bone98, 0.0F, -1.0472F, 0.0F);
		

		bone99 = new RendererModel(this);
		bone99.setRotationPoint(0.0F, 3.0F, 45.0F);
		bone98.addChild(bone99);
		setRotationAngle(bone99, 0.144F, 0.0F, 0.0F);
		bone99.cubeList.add(new ModelBox(bone99, 150, 63, -3.0F, 2.65F, -21.6F, 6, 2, 30, 0.0F, false));

		bone100 = new RendererModel(this);
		bone100.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone98.addChild(bone100);
		setRotationAngle(bone100, 0.0F, -1.0472F, 0.0F);
		

		bone101 = new RendererModel(this);
		bone101.setRotationPoint(0.0F, 3.0F, 45.0F);
		bone100.addChild(bone101);
		setRotationAngle(bone101, 0.144F, 0.0F, 0.0F);
		bone101.cubeList.add(new ModelBox(bone101, 150, 63, -3.0F, 2.65F, -21.6F, 6, 2, 30, 0.0F, false));

		bone102 = new RendererModel(this);
		bone102.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone100.addChild(bone102);
		setRotationAngle(bone102, 0.0F, -1.0472F, 0.0F);
		

		bone103 = new RendererModel(this);
		bone103.setRotationPoint(0.0F, 3.0F, 45.0F);
		bone102.addChild(bone103);
		setRotationAngle(bone103, 0.144F, 0.0F, 0.0F);
		bone103.cubeList.add(new ModelBox(bone103, 150, 63, -3.0F, 2.65F, -21.6F, 6, 2, 30, 0.0F, false));

		bone104 = new RendererModel(this);
		bone104.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone102.addChild(bone104);
		setRotationAngle(bone104, 0.0F, -1.0472F, 0.0F);
		

		bone105 = new RendererModel(this);
		bone105.setRotationPoint(0.0F, 3.0F, 45.0F);
		bone104.addChild(bone105);
		setRotationAngle(bone105, 0.144F, 0.0F, 0.0F);
		bone105.cubeList.add(new ModelBox(bone105, 150, 63, -3.0F, 2.65F, -21.6F, 6, 2, 30, 0.0F, false));

		underborder2 = new RendererModel(this);
		underborder2.setRotationPoint(0.0F, 7.0F, 0.0F);
		underconsole.addChild(underborder2);
		underborder2.cubeList.add(new ModelBox(underborder2, 172, 144, -13.5F, -7.4F, 11.5F, 27, 3, 12, 0.0F, false));

		bone90 = new RendererModel(this);
		bone90.setRotationPoint(0.0F, 0.0F, 0.0F);
		underborder2.addChild(bone90);
		setRotationAngle(bone90, 0.0F, -1.0472F, 0.0F);
		bone90.cubeList.add(new ModelBox(bone90, 172, 144, -13.5F, -7.4F, 11.5F, 27, 3, 12, 0.0F, false));

		bone91 = new RendererModel(this);
		bone91.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone90.addChild(bone91);
		setRotationAngle(bone91, 0.0F, -1.0472F, 0.0F);
		bone91.cubeList.add(new ModelBox(bone91, 172, 144, -13.5F, -7.4F, 11.5F, 27, 3, 12, 0.0F, false));

		bone92 = new RendererModel(this);
		bone92.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone91.addChild(bone92);
		setRotationAngle(bone92, 0.0F, -1.0472F, 0.0F);
		bone92.cubeList.add(new ModelBox(bone92, 172, 144, -13.5F, -7.4F, 11.5F, 27, 3, 12, 0.0F, false));

		bone93 = new RendererModel(this);
		bone93.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone92.addChild(bone93);
		setRotationAngle(bone93, 0.0F, -1.0472F, 0.0F);
		bone93.cubeList.add(new ModelBox(bone93, 172, 144, -13.5F, -7.4F, 11.5F, 27, 3, 12, 0.0F, false));

		bone94 = new RendererModel(this);
		bone94.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone93.addChild(bone94);
		setRotationAngle(bone94, 0.0F, -1.0472F, 0.0F);
		bone94.cubeList.add(new ModelBox(bone94, 172, 144, -13.5F, -7.4F, 11.5F, 27, 3, 12, 0.0F, false));

		underborder = new RendererModel(this);
		underborder.setRotationPoint(0.0F, 2.0F, 0.0F);
		underconsole.addChild(underborder);
		underborder.cubeList.add(new ModelBox(underborder, 130, 93, -25.0F, -6.4F, 42.5F, 50, 2, 6, 0.0F, false));

		bone26 = new RendererModel(this);
		bone26.setRotationPoint(0.0F, 0.0F, 0.0F);
		underborder.addChild(bone26);
		setRotationAngle(bone26, 0.0F, -1.0472F, 0.0F);
		bone26.cubeList.add(new ModelBox(bone26, 130, 93, -25.0F, -6.4F, 42.5F, 50, 2, 6, 0.0F, false));

		bone27 = new RendererModel(this);
		bone27.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone26.addChild(bone27);
		setRotationAngle(bone27, 0.0F, -1.0472F, 0.0F);
		bone27.cubeList.add(new ModelBox(bone27, 130, 93, -25.0F, -6.4F, 42.5F, 50, 2, 6, 0.0F, false));

		bone28 = new RendererModel(this);
		bone28.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone27.addChild(bone28);
		setRotationAngle(bone28, 0.0F, -1.0472F, 0.0F);
		bone28.cubeList.add(new ModelBox(bone28, 130, 93, -25.0F, -6.4F, 42.5F, 50, 2, 6, 0.0F, false));

		bone29 = new RendererModel(this);
		bone29.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone28.addChild(bone29);
		setRotationAngle(bone29, 0.0F, -1.0472F, 0.0F);
		bone29.cubeList.add(new ModelBox(bone29, 130, 93, -25.0F, -6.4F, 42.5F, 50, 2, 6, 0.0F, false));

		bone30 = new RendererModel(this);
		bone30.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone29.addChild(bone30);
		setRotationAngle(bone30, 0.0F, -1.0472F, 0.0F);
		bone30.cubeList.add(new ModelBox(bone30, 130, 93, -25.0F, -6.4F, 42.5F, 50, 2, 6, 0.0F, false));

		underpanels = new RendererModel(this);
		underpanels.setRotationPoint(0.0F, -8.0F, 0.0F);
		underconsole.addChild(underpanels);
		

		bone37 = new RendererModel(this);
		bone37.setRotationPoint(0.0F, 3.0F, 45.0F);
		underpanels.addChild(bone37);
		setRotationAngle(bone37, 0.1745F, 0.0F, 0.0F);
		bone37.cubeList.add(new ModelBox(bone37, 144, 24, -12.0F, 2.55F, -27.0F, 24, 0, 6, 0.0F, false));
		bone37.cubeList.add(new ModelBox(bone37, 144, 16, -15.0F, 2.55F, -21.0F, 30, 0, 6, 0.0F, false));
		bone37.cubeList.add(new ModelBox(bone37, 144, 8, -18.0F, 2.55F, -15.0F, 36, 0, 6, 0.0F, false));
		bone37.cubeList.add(new ModelBox(bone37, 144, 0, -22.0F, 2.55F, -9.0F, 44, 0, 6, 0.0F, false));
		bone37.cubeList.add(new ModelBox(bone37, 144, 0, -25.0F, 2.55F, -3.0F, 50, 0, 6, 0.0F, false));

		bone38 = new RendererModel(this);
		bone38.setRotationPoint(0.0F, 0.0F, 0.0F);
		underpanels.addChild(bone38);
		setRotationAngle(bone38, 0.0F, -1.0472F, 0.0F);
		

		bone39 = new RendererModel(this);
		bone39.setRotationPoint(0.0F, 3.0F, 45.0F);
		bone38.addChild(bone39);
		setRotationAngle(bone39, 0.1745F, 0.0F, 0.0F);
		bone39.cubeList.add(new ModelBox(bone39, 144, 0, -22.0F, 2.55F, -9.0F, 44, 0, 6, 0.0F, false));
		bone39.cubeList.add(new ModelBox(bone39, 144, 8, -18.0F, 2.55F, -15.0F, 36, 0, 6, 0.0F, false));
		bone39.cubeList.add(new ModelBox(bone39, 144, 16, -15.0F, 2.55F, -21.0F, 30, 0, 6, 0.0F, false));
		bone39.cubeList.add(new ModelBox(bone39, 144, 24, -12.0F, 2.55F, -27.0F, 24, 0, 6, 0.0F, false));
		bone39.cubeList.add(new ModelBox(bone39, 144, 0, -25.0F, 2.55F, -3.0F, 50, 0, 6, 0.0F, false));

		bone121 = new RendererModel(this);
		bone121.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone38.addChild(bone121);
		setRotationAngle(bone121, 0.0F, -1.0472F, 0.0F);
		

		bone122 = new RendererModel(this);
		bone122.setRotationPoint(0.0F, 3.0F, 45.0F);
		bone121.addChild(bone122);
		setRotationAngle(bone122, 0.1745F, 0.0F, 0.0F);
		bone122.cubeList.add(new ModelBox(bone122, 144, 0, -22.0F, 2.55F, -9.0F, 44, 0, 6, 0.0F, false));
		bone122.cubeList.add(new ModelBox(bone122, 144, 8, -18.0F, 2.55F, -15.0F, 36, 0, 6, 0.0F, false));
		bone122.cubeList.add(new ModelBox(bone122, 144, 16, -15.0F, 2.55F, -21.0F, 30, 0, 6, 0.0F, false));
		bone122.cubeList.add(new ModelBox(bone122, 144, 24, -12.0F, 2.55F, -27.0F, 24, 0, 6, 0.0F, false));
		bone122.cubeList.add(new ModelBox(bone122, 144, 0, -25.0F, 2.55F, -3.0F, 50, 0, 6, 0.0F, false));

		bone123 = new RendererModel(this);
		bone123.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone121.addChild(bone123);
		setRotationAngle(bone123, 0.0F, -1.0472F, 0.0F);
		

		bone124 = new RendererModel(this);
		bone124.setRotationPoint(0.0F, 3.0F, 45.0F);
		bone123.addChild(bone124);
		setRotationAngle(bone124, 0.1745F, 0.0F, 0.0F);
		bone124.cubeList.add(new ModelBox(bone124, 144, 24, -12.0F, 2.55F, -27.0F, 24, 0, 6, 0.0F, false));
		bone124.cubeList.add(new ModelBox(bone124, 144, 16, -15.0F, 2.55F, -21.0F, 30, 0, 6, 0.0F, false));
		bone124.cubeList.add(new ModelBox(bone124, 144, 8, -18.0F, 2.55F, -15.0F, 36, 0, 6, 0.0F, false));
		bone124.cubeList.add(new ModelBox(bone124, 144, 0, -22.0F, 2.55F, -9.0F, 44, 0, 6, 0.0F, false));
		bone124.cubeList.add(new ModelBox(bone124, 144, 0, -25.0F, 2.55F, -3.0F, 50, 0, 6, 0.0F, false));

		bone125 = new RendererModel(this);
		bone125.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone123.addChild(bone125);
		setRotationAngle(bone125, 0.0F, -1.0472F, 0.0F);
		

		bone126 = new RendererModel(this);
		bone126.setRotationPoint(0.0F, 3.0F, 45.0F);
		bone125.addChild(bone126);
		setRotationAngle(bone126, 0.1745F, 0.0F, 0.0F);
		bone126.cubeList.add(new ModelBox(bone126, 144, 24, -12.0F, 2.55F, -27.0F, 24, 0, 6, 0.0F, false));
		bone126.cubeList.add(new ModelBox(bone126, 144, 16, -15.0F, 2.55F, -21.0F, 30, 0, 6, 0.0F, false));
		bone126.cubeList.add(new ModelBox(bone126, 144, 8, -18.0F, 2.55F, -15.0F, 36, 0, 6, 0.0F, false));
		bone126.cubeList.add(new ModelBox(bone126, 144, 0, -22.0F, 2.55F, -9.0F, 44, 0, 6, 0.0F, false));
		bone126.cubeList.add(new ModelBox(bone126, 144, 0, -25.0F, 2.55F, -3.0F, 50, 0, 6, 0.0F, false));

		bone127 = new RendererModel(this);
		bone127.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone125.addChild(bone127);
		setRotationAngle(bone127, 0.0F, -1.0472F, 0.0F);
		

		bone128 = new RendererModel(this);
		bone128.setRotationPoint(0.0F, 3.0F, 45.0F);
		bone127.addChild(bone128);
		setRotationAngle(bone128, 0.1745F, 0.0F, 0.0F);
		bone128.cubeList.add(new ModelBox(bone128, 144, 24, -12.0F, 2.55F, -27.0F, 24, 0, 6, 0.0F, false));
		bone128.cubeList.add(new ModelBox(bone128, 144, 16, -15.0F, 2.55F, -21.0F, 30, 0, 6, 0.0F, false));
		bone128.cubeList.add(new ModelBox(bone128, 144, 8, -18.0F, 2.55F, -15.0F, 36, 0, 6, 0.0F, false));
		bone128.cubeList.add(new ModelBox(bone128, 144, 0, -22.0F, 2.55F, -9.0F, 44, 0, 6, 0.0F, false));
		bone128.cubeList.add(new ModelBox(bone128, 144, 0, -25.0F, 2.55F, -3.0F, 50, 0, 6, 0.0F, false));

		centre = new RendererModel(this);
		centre.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone35.addChild(centre);
		

		base2 = new RendererModel(this);
		base2.setRotationPoint(0.0F, -8.75F, 0.0F);
		centre.addChild(base2);
		base2.cubeList.add(new ModelBox(base2, 126, 132, -7.5F, -6.275F, -1.85F, 15, 8, 13, 0.0F, false));

		bone25 = new RendererModel(this);
		bone25.setRotationPoint(0.0F, 0.0F, 0.0F);
		base2.addChild(bone25);
		setRotationAngle(bone25, 0.0F, -1.0472F, 0.0F);
		bone25.cubeList.add(new ModelBox(bone25, 126, 132, -7.5F, -6.275F, -1.85F, 15, 8, 13, 0.0F, false));

		bone69 = new RendererModel(this);
		bone69.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone25.addChild(bone69);
		setRotationAngle(bone69, 0.0F, -1.0472F, 0.0F);
		bone69.cubeList.add(new ModelBox(bone69, 126, 132, -7.5F, -6.275F, -1.85F, 15, 8, 13, 0.0F, false));

		bone74 = new RendererModel(this);
		bone74.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone69.addChild(bone74);
		setRotationAngle(bone74, 0.0F, -1.0472F, 0.0F);
		bone74.cubeList.add(new ModelBox(bone74, 126, 132, -7.5F, -6.275F, -1.85F, 15, 8, 13, 0.0F, false));

		bone75 = new RendererModel(this);
		bone75.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone74.addChild(bone75);
		setRotationAngle(bone75, 0.0F, -1.0472F, 0.0F);
		bone75.cubeList.add(new ModelBox(bone75, 126, 132, -7.5F, -6.275F, -1.85F, 15, 8, 13, 0.0F, false));

		bone89 = new RendererModel(this);
		bone89.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone75.addChild(bone89);
		setRotationAngle(bone89, 0.0F, -1.0472F, 0.0F);
		bone89.cubeList.add(new ModelBox(bone89, 126, 132, -7.5F, -6.275F, -1.85F, 15, 8, 13, 0.0F, false));

		glow = new RendererModel(this);
		glow.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		rotor_translate_y_rotate_y = new RendererModel(this);
		rotor_translate_y_rotate_y.setRotationPoint(0.0F, -4.475F, 0.0F);
		glow.addChild(rotor_translate_y_rotate_y);
		

		base5 = new RendererModel(this);
		base5.setRotationPoint(0.0F, -0.75F, 0.0F);
		rotor_translate_y_rotate_y.addChild(base5);
		setRotationAngle(base5, 0.0F, -0.5236F, 0.0F);
		base5.cubeList.add(new ModelBox(base5, 235, 103, -1.0F, -31.275F, 5.4F, 2, 14, 3, 0.0F, false));

		bone161 = new RendererModel(this);
		bone161.setRotationPoint(0.0F, 0.0F, 0.0F);
		base5.addChild(bone161);
		setRotationAngle(bone161, 0.0F, -1.0472F, 0.0F);
		bone161.cubeList.add(new ModelBox(bone161, 235, 103, -1.0F, -31.275F, 5.4F, 2, 14, 3, 0.0F, false));

		bone162 = new RendererModel(this);
		bone162.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone161.addChild(bone162);
		setRotationAngle(bone162, 0.0F, -1.0472F, 0.0F);
		bone162.cubeList.add(new ModelBox(bone162, 235, 103, -1.0F, -31.275F, 5.4F, 2, 14, 3, 0.0F, false));

		bone163 = new RendererModel(this);
		bone163.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone162.addChild(bone163);
		setRotationAngle(bone163, 0.0F, -1.0472F, 0.0F);
		bone163.cubeList.add(new ModelBox(bone163, 235, 103, -1.0F, -31.275F, 5.4F, 2, 14, 3, 0.0F, false));

		bone164 = new RendererModel(this);
		bone164.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone163.addChild(bone164);
		setRotationAngle(bone164, 0.0F, -1.0472F, 0.0F);
		bone164.cubeList.add(new ModelBox(bone164, 235, 103, -1.0F, -31.275F, 5.4F, 2, 14, 3, 0.0F, false));

		bone165 = new RendererModel(this);
		bone165.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone164.addChild(bone165);
		setRotationAngle(bone165, 0.0F, -1.0472F, 0.0F);
		bone165.cubeList.add(new ModelBox(bone165, 235, 103, -1.0F, -31.275F, 5.4F, 2, 14, 3, 0.0F, false));

		base4 = new RendererModel(this);
		base4.setRotationPoint(0.0F, -0.75F, 0.0F);
		rotor_translate_y_rotate_y.addChild(base4);
		base4.cubeList.add(new ModelBox(base4, 219, 102, -1.5F, -31.275F, 6.4F, 3, 14, 3, 0.0F, false));

		bone156 = new RendererModel(this);
		bone156.setRotationPoint(0.0F, 0.0F, 0.0F);
		base4.addChild(bone156);
		setRotationAngle(bone156, 0.0F, -1.0472F, 0.0F);
		bone156.cubeList.add(new ModelBox(bone156, 219, 102, -1.5F, -31.275F, 6.4F, 3, 14, 3, 0.0F, false));

		bone157 = new RendererModel(this);
		bone157.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone156.addChild(bone157);
		setRotationAngle(bone157, 0.0F, -1.0472F, 0.0F);
		bone157.cubeList.add(new ModelBox(bone157, 219, 102, -1.5F, -31.275F, 6.4F, 3, 14, 3, 0.0F, false));

		bone158 = new RendererModel(this);
		bone158.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone157.addChild(bone158);
		setRotationAngle(bone158, 0.0F, -1.0472F, 0.0F);
		bone158.cubeList.add(new ModelBox(bone158, 219, 102, -1.5F, -31.275F, 6.4F, 3, 14, 3, 0.0F, false));

		bone159 = new RendererModel(this);
		bone159.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone158.addChild(bone159);
		setRotationAngle(bone159, 0.0F, -1.0472F, 0.0F);
		bone159.cubeList.add(new ModelBox(bone159, 219, 102, -1.5F, -31.275F, 6.4F, 3, 14, 3, 0.0F, false));

		bone160 = new RendererModel(this);
		bone160.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone159.addChild(bone160);
		setRotationAngle(bone160, 0.0F, -1.0472F, 0.0F);
		bone160.cubeList.add(new ModelBox(bone160, 219, 102, -1.5F, -31.275F, 6.4F, 3, 14, 3, 0.0F, false));

		base3 = new RendererModel(this);
		base3.setRotationPoint(0.0F, -14.75F, 0.0F);
		rotor_translate_y_rotate_y.addChild(base3);
		base3.cubeList.add(new ModelBox(base3, 153, 0, -6.0F, -3.275F, -2.6F, 12, 5, 13, 0.0F, false));
		base3.cubeList.add(new ModelBox(base3, 153, 0, -6.0F, -20.275F, -2.6F, 12, 3, 13, 0.0F, false));
		base3.cubeList.add(new ModelBox(base3, 184, 107, -4.0F, -21.275F, -6.05F, 8, 1, 13, 0.0F, false));

		bone151 = new RendererModel(this);
		bone151.setRotationPoint(0.0F, 0.0F, 0.0F);
		base3.addChild(bone151);
		setRotationAngle(bone151, 0.0F, -1.0472F, 0.0F);
		bone151.cubeList.add(new ModelBox(bone151, 153, 0, -6.0F, -3.275F, -2.6F, 12, 5, 13, 0.0F, false));
		bone151.cubeList.add(new ModelBox(bone151, 153, 0, -6.0F, -20.275F, -2.6F, 12, 3, 13, 0.0F, false));
		bone151.cubeList.add(new ModelBox(bone151, 184, 107, -4.0F, -21.275F, -6.05F, 8, 1, 13, 0.0F, false));

		bone152 = new RendererModel(this);
		bone152.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone151.addChild(bone152);
		setRotationAngle(bone152, 0.0F, -1.0472F, 0.0F);
		bone152.cubeList.add(new ModelBox(bone152, 153, 0, -6.0F, -3.275F, -2.6F, 12, 5, 13, 0.0F, false));
		bone152.cubeList.add(new ModelBox(bone152, 153, 0, -6.0F, -20.275F, -2.6F, 12, 3, 13, 0.0F, false));
		bone152.cubeList.add(new ModelBox(bone152, 184, 107, -4.0F, -21.275F, -6.05F, 8, 1, 13, 0.0F, false));

		bone153 = new RendererModel(this);
		bone153.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone152.addChild(bone153);
		setRotationAngle(bone153, 0.0F, -1.0472F, 0.0F);
		bone153.cubeList.add(new ModelBox(bone153, 153, 0, -6.0F, -3.275F, -2.6F, 12, 5, 13, 0.0F, false));
		bone153.cubeList.add(new ModelBox(bone153, 153, 0, -6.0F, -20.275F, -2.6F, 12, 3, 13, 0.0F, false));
		bone153.cubeList.add(new ModelBox(bone153, 184, 107, -4.0F, -21.275F, -6.05F, 8, 1, 13, 0.0F, false));

		bone154 = new RendererModel(this);
		bone154.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone153.addChild(bone154);
		setRotationAngle(bone154, 0.0F, -1.0472F, 0.0F);
		bone154.cubeList.add(new ModelBox(bone154, 153, 0, -6.0F, -3.275F, -2.6F, 12, 5, 13, 0.0F, false));
		bone154.cubeList.add(new ModelBox(bone154, 153, 0, -6.0F, -20.275F, -2.6F, 12, 3, 13, 0.0F, false));
		bone154.cubeList.add(new ModelBox(bone154, 184, 107, -4.0F, -21.275F, -6.05F, 8, 1, 13, 0.0F, false));

		bone155 = new RendererModel(this);
		bone155.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone154.addChild(bone155);
		setRotationAngle(bone155, 0.0F, -1.0472F, 0.0F);
		bone155.cubeList.add(new ModelBox(bone155, 153, 0, -6.0F, -3.275F, -2.6F, 12, 5, 13, 0.0F, false));
		bone155.cubeList.add(new ModelBox(bone155, 153, 0, -6.0F, -20.275F, -2.6F, 12, 3, 13, 0.0F, false));
		bone155.cubeList.add(new ModelBox(bone155, 184, 107, -4.0F, -21.275F, -6.05F, 8, 1, 13, 0.0F, false));

		lamp = new RendererModel(this);
		lamp.setRotationPoint(0.0F, -21.0F, 0.0F);
		glow.addChild(lamp);
		setRotationAngle(lamp, 0.0F, 3.1416F, 0.0F);
		

		rotation7 = new RendererModel(this);
		rotation7.setRotationPoint(-0.5F, -12.5F, -29.5F);
		lamp.addChild(rotation7);
		setRotationAngle(rotation7, 0.2618F, 0.0F, 0.0F);
		rotation7.cubeList.add(new ModelBox(rotation7, 1, 207, -8.0F, 1.6458F, -0.0135F, 4, 1, 4, 0.0F, false));
		rotation7.cubeList.add(new ModelBox(rotation7, 15, 210, -7.5F, -2.3542F, 0.4865F, 3, 4, 3, 0.0F, false));

		redbutton = new RendererModel(this);
		redbutton.setRotationPoint(0.0F, -21.0F, 0.0F);
		glow.addChild(redbutton);
		setRotationAngle(redbutton, 0.0F, -2.0944F, 0.0F);
		

		rotation8 = new RendererModel(this);
		rotation8.setRotationPoint(-0.5F, -12.5F, -29.5F);
		redbutton.addChild(rotation8);
		setRotationAngle(rotation8, 0.2618F, 0.0F, 0.0F);
		rotation8.cubeList.add(new ModelBox(rotation8, 14, 246, 12.25F, 1.1458F, -13.2635F, 2, 1, 2, 0.0F, false));
		rotation8.cubeList.add(new ModelBox(rotation8, 21, 245, 12.75F, 1.3958F, -12.7635F, 1, 1, 1, 0.0F, false));

		decals = new RendererModel(this);
		decals.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		bone66 = new RendererModel(this);
		bone66.setRotationPoint(0.0F, 0.0F, 0.0F);
		decals.addChild(bone66);
		setRotationAngle(bone66, 0.0F, 1.0472F, 0.0F);
		

		bone61 = new RendererModel(this);
		bone61.setRotationPoint(0.0F, -24.0F, 45.0F);
		bone66.addChild(bone61);
		setRotationAngle(bone61, -0.2618F, 0.0F, 0.0F);
		bone61.cubeList.add(new ModelBox(bone61, 196, 36, 0.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 162, 45, -1.0F, -3.1F, 1.2F, 2, 2, 2, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 196, 36, -24.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));

		bone68 = new RendererModel(this);
		bone68.setRotationPoint(-5.0F, -2.35F, -1.55F);
		bone61.addChild(bone68);
		setRotationAngle(bone68, 0.0F, -0.7854F, 0.0F);
		

		bone70 = new RendererModel(this);
		bone70.setRotationPoint(-5.0F, -2.35F, -3.55F);
		bone61.addChild(bone70);
		setRotationAngle(bone70, 0.0F, -0.7854F, 0.0F);
		

		bone62 = new RendererModel(this);
		bone62.setRotationPoint(1.25F, -2.025F, -17.0F);
		bone61.addChild(bone62);
		setRotationAngle(bone62, 0.0F, -0.7854F, 0.0F);
		bone62.cubeList.add(new ModelBox(bone62, 152, 72, -13.0F, -1.0F, -11.25F, 9, 0, 9, 0.0F, false));

		bone77 = new RendererModel(this);
		bone77.setRotationPoint(-8.5F, -3.025F, -21.75F);
		bone61.addChild(bone77);
		setRotationAngle(bone77, 0.0F, 0.7854F, 0.0F);
		bone77.cubeList.add(new ModelBox(bone77, 156, 72, -3.5F, 0.0F, -3.5F, 7, 0, 7, 0.0F, false));

		bone78 = new RendererModel(this);
		bone78.setRotationPoint(8.5F, -3.025F, -21.75F);
		bone61.addChild(bone78);
		setRotationAngle(bone78, 0.0F, -2.3562F, 0.0F);
		bone78.cubeList.add(new ModelBox(bone78, 156, 72, -3.5F, 0.0F, -3.5F, 7, 0, 7, 0.0F, false));

		bone63 = new RendererModel(this);
		bone63.setRotationPoint(0.0F, -3.025F, -10.875F);
		bone61.addChild(bone63);
		setRotationAngle(bone63, 0.0F, -0.7854F, 0.0F);
		bone63.cubeList.add(new ModelBox(bone63, 219, 69, -10.0F, 0.0F, 8.0F, 9, 0, 9, 0.0F, false));

		bone65 = new RendererModel(this);
		bone65.setRotationPoint(0.0F, 0.0F, 0.0F);
		decals.addChild(bone65);
		setRotationAngle(bone65, 0.0F, 2.0944F, 0.0F);
		

		bone58 = new RendererModel(this);
		bone58.setRotationPoint(0.0F, -24.0F, 45.0F);
		bone65.addChild(bone58);
		setRotationAngle(bone58, -0.2618F, 0.0F, 0.0F);
		bone58.cubeList.add(new ModelBox(bone58, 196, 36, 0.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));
		bone58.cubeList.add(new ModelBox(bone58, 162, 45, -1.0F, -3.1F, 1.2F, 2, 2, 2, 0.0F, false));
		bone58.cubeList.add(new ModelBox(bone58, 196, 36, -24.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));

		bone59 = new RendererModel(this);
		bone59.setRotationPoint(0.0F, -3.025F, -13.0F);
		bone58.addChild(bone59);
		setRotationAngle(bone59, 0.0F, -0.7854F, 0.0F);
		bone59.cubeList.add(new ModelBox(bone59, 176, 48, -10.0F, 0.0F, -10.0F, 20, 0, 20, 0.0F, false));
		bone59.cubeList.add(new ModelBox(bone59, 132, 104, 11.0F, 0.0F, -8.0F, 11, 0, 17, 0.0F, false));
		bone59.cubeList.add(new ModelBox(bone59, 183, 68, -8.0F, 0.0F, 11.0F, 17, 0, 11, 0.0F, false));

		bone64 = new RendererModel(this);
		bone64.setRotationPoint(0.0F, 0.0F, 0.0F);
		decals.addChild(bone64);
		setRotationAngle(bone64, 0.0F, 3.1416F, 0.0F);
		

		bone53 = new RendererModel(this);
		bone53.setRotationPoint(0.0F, -24.0F, 45.0F);
		bone64.addChild(bone53);
		setRotationAngle(bone53, -0.2618F, 0.0F, 0.0F);
		bone53.cubeList.add(new ModelBox(bone53, 150, 51, 9.0F, -3.025F, -9.0F, 7, 0, 10, 0.0F, false));
		bone53.cubeList.add(new ModelBox(bone53, 125, 42, -4.0F, -3.025F, -18.0F, 8, 0, 19, 0.0F, false));
		bone53.cubeList.add(new ModelBox(bone53, 150, 51, -16.0F, -3.025F, -9.0F, 7, 0, 10, 0.0F, false));
		bone53.cubeList.add(new ModelBox(bone53, 196, 36, 0.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));
		bone53.cubeList.add(new ModelBox(bone53, 162, 45, -1.0F, -3.1F, 1.2F, 2, 2, 2, 0.0F, false));
		bone53.cubeList.add(new ModelBox(bone53, 196, 36, -24.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));

		bone54 = new RendererModel(this);
		bone54.setRotationPoint(1.0F, -2.025F, -17.25F);
		bone53.addChild(bone54);
		setRotationAngle(bone54, 0.0F, -0.7854F, 0.0F);
		bone54.cubeList.add(new ModelBox(bone54, 154, 65, -4.0F, -1.0F, -2.25F, 6, 0, 6, 0.0F, false));
		bone54.cubeList.add(new ModelBox(bone54, 152, 72, -13.0F, -1.0F, -11.25F, 9, 0, 9, 0.0F, false));

		bone55 = new RendererModel(this);
		bone55.setRotationPoint(12.5F, -2.025F, -9.75F);
		bone53.addChild(bone55);
		setRotationAngle(bone55, 0.0F, -0.7854F, 0.0F);
		bone55.cubeList.add(new ModelBox(bone55, 145, 66, -2.0F, -1.0F, -2.0F, 5, 0, 5, 0.0F, false));

		bone56 = new RendererModel(this);
		bone56.setRotationPoint(-12.5F, -2.025F, -9.75F);
		bone53.addChild(bone56);
		setRotationAngle(bone56, 0.0F, -0.7854F, 0.0F);
		bone56.cubeList.add(new ModelBox(bone56, 145, 66, -2.0F, -1.0F, -2.0F, 5, 0, 5, 0.0F, false));

		bone31 = new RendererModel(this);
		bone31.setRotationPoint(0.0F, 0.0F, 0.0F);
		decals.addChild(bone31);
		setRotationAngle(bone31, 0.0F, -2.0944F, 0.0F);
		

		bone51 = new RendererModel(this);
		bone51.setRotationPoint(0.0F, -24.0F, 45.0F);
		bone31.addChild(bone51);
		setRotationAngle(bone51, -0.2618F, 0.0F, 0.0F);
		bone51.cubeList.add(new ModelBox(bone51, 196, 36, 0.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));
		bone51.cubeList.add(new ModelBox(bone51, 162, 45, -1.0F, -3.1F, 1.2F, 2, 2, 2, 0.0F, false));
		bone51.cubeList.add(new ModelBox(bone51, 196, 36, -24.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, 0.0F, 0.0F);
		decals.addChild(bone);
		setRotationAngle(bone, 0.0F, -1.0472F, 0.0F);
		

		bone46 = new RendererModel(this);
		bone46.setRotationPoint(0.0F, -24.0F, 45.0F);
		bone.addChild(bone46);
		setRotationAngle(bone46, -0.2618F, 0.0F, 0.0F);
		bone46.cubeList.add(new ModelBox(bone46, 127, 63, 6.0F, -3.075F, -11.0F, 12, 0, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 196, 36, 0.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 162, 45, -1.0F, -3.1F, 1.2F, 2, 2, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 196, 36, -24.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 127, 63, -18.0F, -3.075F, -11.0F, 12, 0, 2, 0.0F, false));

		bone47 = new RendererModel(this);
		bone47.setRotationPoint(6.0F, -2.3F, -11.0F);
		bone46.addChild(bone47);
		setRotationAngle(bone47, 0.0F, 1.309F, 0.0F);
		bone47.cubeList.add(new ModelBox(bone47, 98, 63, -14.0F, -0.725F, 0.0F, 14, 0, 2, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 99, 63, -14.0F, -0.725F, 0.0F, 14, 0, 2, 0.0F, false));

		bone48 = new RendererModel(this);
		bone48.setRotationPoint(-6.0F, -2.3F, -11.0F);
		bone46.addChild(bone48);
		setRotationAngle(bone48, 0.0F, -1.309F, 0.0F);
		bone48.cubeList.add(new ModelBox(bone48, 98, 63, 0.0F, -0.725F, 0.0F, 14, 0, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 99, 63, 0.0F, -0.725F, 0.0F, 14, 0, 2, 0.0F, false));

		bone49 = new RendererModel(this);
		bone49.setRotationPoint(0.0F, -2.25F, -6.0F);
		bone46.addChild(bone49);
		setRotationAngle(bone49, 0.0F, -0.7854F, 0.0F);
		bone49.cubeList.add(new ModelBox(bone49, 146, 82, -4.0F, -0.775F, -4.0F, 11, 0, 11, 0.0F, false));

		bone13 = new RendererModel(this);
		bone13.setRotationPoint(0.0F, -24.0F, 45.0F);
		decals.addChild(bone13);
		setRotationAngle(bone13, -0.2618F, 0.0F, 0.0F);
		bone13.cubeList.add(new ModelBox(bone13, 123, 67, -1.0137F, -3.2F, -28.8995F, 2, 0, 14, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 196, 36, 0.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 162, 45, -1.0F, -3.1F, 1.2F, 2, 2, 2, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 196, 36, -24.7F, -3.3F, 1.2F, 24, 2, 2, 0.0F, false));

		bone42 = new RendererModel(this);
		bone42.setRotationPoint(12.0F, -2.2F, -6.0F);
		bone13.addChild(bone42);
		setRotationAngle(bone42, 0.0F, -0.7854F, 0.0F);
		bone42.cubeList.add(new ModelBox(bone42, 197, 79, -5.0F, -0.85F, -5.0F, 10, 2, 10, 0.0F, false));
		bone42.cubeList.add(new ModelBox(bone42, 127, 81, -15.0F, -1.0F, 1.0F, 10, 0, 2, 0.0F, false));

		bone43 = new RendererModel(this);
		bone43.setRotationPoint(-12.0F, -2.2F, -6.0F);
		bone13.addChild(bone43);
		setRotationAngle(bone43, 0.0F, 0.7854F, 0.0F);
		bone43.cubeList.add(new ModelBox(bone43, 197, 79, -5.0F, -0.85F, -5.0F, 10, 2, 10, 0.0F, false));
		bone43.cubeList.add(new ModelBox(bone43, 127, 81, 5.0F, -1.0F, 1.0F, 10, 0, 2, 0.0F, false));
	}

	public void render(ConsoleTile tile) {
		int prev = tile.isInFlight() ? tile.flightTicks - 1 : 0;
		this.rotor_translate_y_rotate_y.rotateAngleY = (float) Math.toRadians(prev - (tile.flightTicks - prev) * Minecraft.getInstance().getRenderPartialTicks());
		this.rotor_translate_y_rotate_y.offsetY = (float) (Math.cos(tile.flightTicks * 0.1F) * 0.5F) - 0.5F;
		
		this.throttle_rotate_x.rotateAngleX = (float)Math.toRadians(80 - (tile.getControl(ThrottleControl.class).getAmount() * 160));
		
		this.handbrake_rotate_x.rotateAngleY = (float)Math.toRadians(tile.getControl(HandbrakeControl.class).isFree() ? -45 : 10);
		
		this.stabiliser_rotate_y.rotateAngleY = (float)Math.toRadians(tile.getControl(StabilizerControl.class).getAnimationTicks() * 30);
		
		this.dimension_translate_z.offsetZ = -0.35F + (tile.getControl(IncModControl.class).index / (float)(IncModControl.COORD_MODS.length - 1)) * 0.35F;
		
		this.xcontrol.offsetY = tile.getControl(XControl.class).getAnimationTicks() != 0 ? 0.03F : 0F;
		this.ycontrol.offsetY = tile.getControl(YControl.class).getAnimationTicks() != 0 ? 0.03F : 0F;
		this.zcontrol.offsetY = tile.getControl(ZControl.class).getAnimationTicks() != 0 ? 0.03F : 0F;
		
		this.landtype_translate_z.offsetZ = tile.getControl(LandingTypeControl.class).getLandType() == EnumLandType.DOWN ? -0.4F : 0F;
		
		this.bone34.rotateAngleY = (float) Math.toRadians(Helper.getAngleFromFacing(tile.getDirection()) + 90);
		
		bone35.render(0.0625F);
		ModelHelper.renderPartBrightness(1F, glow);
		decals.render(0.0625F);
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(0.3, -0.5, -1.9);
		GlStateManager.rotated(22.5, 1, 0, 0);
		Minecraft.getInstance().getItemRenderer().renderItem(tile.getSonicItem(), TransformType.NONE);
		GlStateManager.popMatrix();
		
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}