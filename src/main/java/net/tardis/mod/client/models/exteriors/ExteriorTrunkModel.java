package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;

//Made with Blockbench
//Paste this code into your mod.

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.client.models.IBotiModel;
import net.tardis.mod.client.models.IExteriorModel;
import net.tardis.mod.client.renderers.exteriors.TrunkExteriorRenderer;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ExteriorTrunkModel extends Model implements IBotiModel, IExteriorModel{
	private final RendererModel box;
	private final RendererModel walls;
	private final RendererModel back_wall;
	private final RendererModel sidewall_right;
	private final RendererModel sidewall_left;
	private final RendererModel top_wall;
	private final RendererModel bottom_wall;
	private final RendererModel trim;
	private final RendererModel strap;
	private final RendererModel leather;
	private final RendererModel buckle;
	private final RendererModel corners;
	private final RendererModel knobs1;
	private final RendererModel knobs2;
	private final RendererModel knobs3;
	private final RendererModel knobs4;
	private final RendererModel knobs5;
	private final RendererModel knobs6;
	private final RendererModel knobs7;
	private final RendererModel knobs8;
	private final RendererModel hinge;
	private final RendererModel mount;
	private final RendererModel latch;
	private final RendererModel lock;
	private final RendererModel sticker1;
	private final RendererModel sticker2;
	private final RendererModel lid_rotate_y;
	private final RendererModel walls2;
	private final RendererModel back_wall2;
	private final RendererModel sidewall_right2;
	private final RendererModel sidewall_left2;
	private final RendererModel top_wall2;
	private final RendererModel bottom_wall2;
	private final RendererModel trim2;
	private final RendererModel corners2;
	private final RendererModel knobs9;
	private final RendererModel knobs10;
	private final RendererModel knobs11;
	private final RendererModel knobs12;
	private final RendererModel knobs13;
	private final RendererModel knobs14;
	private final RendererModel knobs15;
	private final RendererModel knobs16;
	private final RendererModel mount2;
	private final RendererModel latch2;
	private final RendererModel sticker3;
	private final RendererModel sticker4;
	private final RendererModel boti;

	public ExteriorTrunkModel() {
		textureWidth = 256;
		textureHeight = 256;

		box = new RendererModel(this);
		box.setRotationPoint(0.0F, 24.0F, 0.0F);

		walls = new RendererModel(this);
		walls.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(walls);

		back_wall = new RendererModel(this);
		back_wall.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls.addChild(back_wall);
		back_wall.cubeList.add(new ModelBox(back_wall, 160, 105, -16.0F, -65.0F, 14.9F, 32, 64, 1, 0.0F, false));

		sidewall_right = new RendererModel(this);
		sidewall_right.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls.addChild(sidewall_right);
		sidewall_right.cubeList.add(new ModelBox(sidewall_right, 175, 96, 15.0F, -64.0F, -3.0F, 1, 63, 18, 0.0F, false));

		sidewall_left = new RendererModel(this);
		sidewall_left.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls.addChild(sidewall_left);
		sidewall_left.cubeList.add(new ModelBox(sidewall_left, 141, 87, -16.0F, -65.0F, -3.0F, 1, 63, 18, 0.0F, false));

		top_wall = new RendererModel(this);
		top_wall.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls.addChild(top_wall);
		top_wall.cubeList.add(new ModelBox(top_wall, 110, 98, -16.0F, -65.0F, -4.0F, 32, 1, 19, 0.0F, false));

		bottom_wall = new RendererModel(this);
		bottom_wall.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls.addChild(bottom_wall);
		bottom_wall.cubeList.add(new ModelBox(bottom_wall, 142, 160, -16.0F, -2.0F, -4.0F, 32, 1, 18, 0.0F, false));

		trim = new RendererModel(this);
		trim.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(trim);
		trim.cubeList.add(new ModelBox(trim, 68, 1, -16.5F, -64.0F, 14.5F, 2, 62, 2, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 70, 1, 14.5F, -64.0F, 14.5F, 2, 62, 2, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 74, 5, -15.0F, -65.5F, 14.5F, 30, 2, 2, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 82, 3, -15.0F, -2.5F, 14.5F, 30, 2, 2, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 99, 42, 14.5F, -2.5F, -3.0F, 2, 2, 18, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 96, 40, -16.5F, -2.5F, -3.0F, 2, 2, 18, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 104, 34, -16.5F, -65.5F, -3.0F, 2, 2, 18, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 102, 34, 14.5F, -65.5F, -3.0F, 2, 2, 18, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 68, 3, 14.5F, -62.0F, -4.5F, 2, 58, 2, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 69, 3, -16.5F, -62.0F, -4.5F, 2, 58, 2, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 85, 3, -13.0F, -65.5F, -4.5F, 26, 2, 2, 0.0F, false));
		trim.cubeList.add(new ModelBox(trim, 86, 8, -13.0F, -2.5F, -4.5F, 26, 2, 2, 0.0F, false));

		strap = new RendererModel(this);
		strap.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(strap);

		leather = new RendererModel(this);
		leather.setRotationPoint(0.0F, 0.0F, 0.0F);
		strap.addChild(leather);
		leather.cubeList.add(new ModelBox(leather, 89, 186, -16.0F, -66.0F, 5.0F, 32, 1, 3, 0.0F, false));
		leather.cubeList.add(new ModelBox(leather, 83, 185, -17.0F, -66.0F, 5.0F, 1, 66, 3, 0.0F, false));
		leather.cubeList.add(new ModelBox(leather, 89, 186, -16.0F, -1.0F, 5.0F, 32, 1, 3, 0.0F, false));
		leather.cubeList.add(new ModelBox(leather, 74, 185, 16.0F, -66.0F, 5.0F, 1, 66, 3, 0.0F, false));

		buckle = new RendererModel(this);
		buckle.setRotationPoint(0.0F, 0.0F, 0.0F);
		strap.addChild(buckle);
		buckle.cubeList.add(new ModelBox(buckle, 107, 22, -17.5F, -56.0F, 5.0F, 2, 1, 4, 0.0F, false));
		buckle.cubeList.add(new ModelBox(buckle, 107, 22, -17.5F, -51.0F, 5.0F, 2, 1, 4, 0.0F, false));
		buckle.cubeList.add(new ModelBox(buckle, 107, 22, -17.5F, -55.0F, 8.0F, 2, 4, 1, 0.0F, false));
		buckle.cubeList.add(new ModelBox(buckle, 107, 22, -17.5F, -56.0F, 4.0F, 2, 6, 1, 0.0F, false));

		corners = new RendererModel(this);
		corners.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(corners);

		knobs1 = new RendererModel(this);
		knobs1.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners.addChild(knobs1);
		knobs1.cubeList.add(new ModelBox(knobs1, 76, 71, -17.0F, -4.0F, 13.0F, 4, 4, 4, 0.0F, false));
		knobs1.cubeList.add(new ModelBox(knobs1, 76, 71, -16.0F, -3.0F, 15.5F, 2, 2, 2, 0.0F, false));
		knobs1.cubeList.add(new ModelBox(knobs1, 76, 71, -17.5F, -3.0F, 14.0F, 2, 2, 2, 0.0F, false));
		knobs1.cubeList.add(new ModelBox(knobs1, 76, 71, -16.0F, -1.5F, 14.0F, 2, 2, 2, 0.0F, false));

		knobs2 = new RendererModel(this);
		knobs2.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners.addChild(knobs2);
		knobs2.cubeList.add(new ModelBox(knobs2, 76, 71, 13.0F, -4.0F, 13.0F, 4, 4, 4, 0.0F, false));
		knobs2.cubeList.add(new ModelBox(knobs2, 76, 71, 14.0F, -3.0F, 15.5F, 2, 2, 2, 0.0F, false));
		knobs2.cubeList.add(new ModelBox(knobs2, 76, 71, 15.5F, -3.0F, 14.0F, 2, 2, 2, 0.0F, false));
		knobs2.cubeList.add(new ModelBox(knobs2, 76, 71, 14.0F, -1.5F, 14.0F, 2, 2, 2, 0.0F, false));

		knobs3 = new RendererModel(this);
		knobs3.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners.addChild(knobs3);
		knobs3.cubeList.add(new ModelBox(knobs3, 76, 71, 13.0F, -66.0F, 13.0F, 4, 4, 4, 0.0F, false));
		knobs3.cubeList.add(new ModelBox(knobs3, 76, 71, 14.0F, -65.0F, 15.5F, 2, 2, 2, 0.0F, false));
		knobs3.cubeList.add(new ModelBox(knobs3, 76, 71, 15.5F, -65.0F, 14.0F, 2, 2, 2, 0.0F, false));
		knobs3.cubeList.add(new ModelBox(knobs3, 76, 71, 14.0F, -66.5F, 14.0F, 2, 2, 2, 0.0F, false));

		knobs4 = new RendererModel(this);
		knobs4.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners.addChild(knobs4);
		knobs4.cubeList.add(new ModelBox(knobs4, 76, 71, -17.0F, -66.0F, 13.0F, 4, 4, 4, 0.0F, false));
		knobs4.cubeList.add(new ModelBox(knobs4, 76, 71, -16.0F, -65.0F, 15.5F, 2, 2, 2, 0.0F, false));
		knobs4.cubeList.add(new ModelBox(knobs4, 76, 71, -17.5F, -65.0F, 14.0F, 2, 2, 2, 0.0F, false));
		knobs4.cubeList.add(new ModelBox(knobs4, 76, 71, -16.0F, -66.5F, 14.0F, 2, 2, 2, 0.0F, false));

		knobs5 = new RendererModel(this);
		knobs5.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners.addChild(knobs5);
		knobs5.cubeList.add(new ModelBox(knobs5, 76, 71, -17.0F, -66.0F, -4.5F, 4, 4, 4, 0.0F, false));
		knobs5.cubeList.add(new ModelBox(knobs5, 76, 71, -17.5F, -65.0F, -3.5F, 2, 2, 2, 0.0F, false));
		knobs5.cubeList.add(new ModelBox(knobs5, 76, 71, -16.0F, -66.5F, -3.5F, 2, 2, 2, 0.0F, false));

		knobs6 = new RendererModel(this);
		knobs6.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners.addChild(knobs6);
		knobs6.cubeList.add(new ModelBox(knobs6, 76, 71, 13.0F, -66.0F, -4.5F, 4, 4, 4, 0.0F, false));
		knobs6.cubeList.add(new ModelBox(knobs6, 76, 71, 15.5F, -65.0F, -3.5F, 2, 2, 2, 0.0F, false));
		knobs6.cubeList.add(new ModelBox(knobs6, 76, 71, 14.0F, -66.5F, -3.5F, 2, 2, 2, 0.0F, false));

		knobs7 = new RendererModel(this);
		knobs7.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners.addChild(knobs7);
		knobs7.cubeList.add(new ModelBox(knobs7, 76, 71, 13.0F, -4.0F, -4.5F, 4, 4, 4, 0.0F, false));
		knobs7.cubeList.add(new ModelBox(knobs7, 76, 71, 15.5F, -3.0F, -3.5F, 2, 2, 2, 0.0F, false));
		knobs7.cubeList.add(new ModelBox(knobs7, 76, 71, 14.0F, -1.5F, -3.5F, 2, 2, 2, 0.0F, false));

		knobs8 = new RendererModel(this);
		knobs8.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners.addChild(knobs8);
		knobs8.cubeList.add(new ModelBox(knobs8, 76, 71, -17.0F, -4.0F, -4.5F, 4, 4, 4, 0.0F, false));
		knobs8.cubeList.add(new ModelBox(knobs8, 76, 71, -17.5F, -3.0F, -3.5F, 2, 2, 2, 0.0F, false));
		knobs8.cubeList.add(new ModelBox(knobs8, 76, 71, -16.0F, -1.5F, -3.5F, 2, 2, 2, 0.0F, false));

		hinge = new RendererModel(this);
		hinge.setRotationPoint(17.0F, -34.3333F, -4.5F);
		setRotationAngle(hinge, 0.0F, 0.7854F, 0.0F);
		box.addChild(hinge);
		hinge.cubeList.add(new ModelBox(hinge, 73, 71, -0.5F, -3.6667F, -0.5F, 1, 6, 1, 0.0F, false));
		hinge.cubeList.add(new ModelBox(hinge, 73, 71, -0.5F, -23.6667F, -0.5F, 1, 6, 1, 0.0F, false));
		hinge.cubeList.add(new ModelBox(hinge, 73, 71, -0.5F, 20.3333F, -0.5F, 1, 6, 1, 0.0F, false));

		mount = new RendererModel(this);
		mount.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(mount);
		mount.cubeList.add(new ModelBox(mount, 80, 71, 16.0F, -12.0F, -4.0F, 1, 2, 4, 0.0F, false));
		mount.cubeList.add(new ModelBox(mount, 80, 71, 16.0F, -36.0F, -4.0F, 1, 2, 4, 0.0F, false));
		mount.cubeList.add(new ModelBox(mount, 80, 71, 16.0F, -56.0F, -4.0F, 1, 2, 4, 0.0F, false));

		latch = new RendererModel(this);
		latch.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(latch);
		latch.cubeList.add(new ModelBox(latch, 61, 75, -17.0F, -36.0F, -2.75F, 1, 4, 6, 0.0F, false));

		lock = new RendererModel(this);
		lock.setRotationPoint(-16.5F, -34.0F, 0.0F);
		setRotationAngle(lock, 0.7854F, 0.0F, 0.0F);
		box.addChild(lock);
		lock.cubeList.add(new ModelBox(lock, 65, 78, -1.0F, -3.5F, -3.5F, 2, 4, 4, 0.0F, false));

		sticker1 = new RendererModel(this);
		sticker1.setRotationPoint(-16.0F, -47.0F, 0.0F);
		setRotationAngle(sticker1, 0.6981F, 0.0F, 0.0F);
		box.addChild(sticker1);
		sticker1.cubeList.add(new ModelBox(sticker1, 5, 152, -0.1F, -3.0F, -2.0F, 1, 6, 8, 0.0F, false));

		sticker2 = new RendererModel(this);
		sticker2.setRotationPoint(2.0F, -20.0F, 16.0F);
		setRotationAngle(sticker2, 0.0F, 0.0F, 0.5236F);
		box.addChild(sticker2);
		sticker2.cubeList.add(new ModelBox(sticker2, 4, 154, 2.0F, -4.0F, -0.95F, 12, 8, 1, 0.0F, false));

		lid_rotate_y = new RendererModel(this);
		lid_rotate_y.setRotationPoint(17.0F, 24.0F, -4.5F);

		walls2 = new RendererModel(this);
		walls2.setRotationPoint(-17.0F, 0.0F, 4.5F);
		lid_rotate_y.addChild(walls2);

		back_wall2 = new RendererModel(this);
		back_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls2.addChild(back_wall2);
		back_wall2.cubeList.add(new ModelBox(back_wall2, 123, 80, -16.0F, -65.0F, -15.0F, 32, 64, 1, 0.0F, false));
		back_wall2.cubeList.add(new ModelBox(back_wall2, 1, 2, -15.0F, -64.0F, -7.0F, 30, 62, 1, 0.0F, false));

		sidewall_right2 = new RendererModel(this);
		sidewall_right2.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls2.addChild(sidewall_right2);
		sidewall_right2.cubeList.add(new ModelBox(sidewall_right2, 124, 78, 15.0F, -64.0F, -14.0F, 1, 62, 9, 0.0F, false));

		sidewall_left2 = new RendererModel(this);
		sidewall_left2.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls2.addChild(sidewall_left2);
		sidewall_left2.cubeList.add(new ModelBox(sidewall_left2, 128, 76, -16.0F, -64.0F, -14.0F, 1, 62, 9, 0.0F, false));

		top_wall2 = new RendererModel(this);
		top_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls2.addChild(top_wall2);
		top_wall2.cubeList.add(new ModelBox(top_wall2, 120, 117, -16.0F, -65.0F, -14.0F, 32, 1, 9, 0.0F, false));

		bottom_wall2 = new RendererModel(this);
		bottom_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls2.addChild(bottom_wall2);
		bottom_wall2.cubeList.add(new ModelBox(bottom_wall2, 152, 117, -16.0F, -2.0F, -14.0F, 32, 1, 9, 0.0F, false));

		trim2 = new RendererModel(this);
		trim2.setRotationPoint(-17.0F, 0.0F, 4.5F);
		lid_rotate_y.addChild(trim2);
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -16.5F, -62.0F, -6.5F, 2, 58, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, 14.5F, -62.0F, -6.5F, 2, 58, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -15.0F, -65.5F, -15.5F, 30, 2, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -15.0F, -2.5F, -15.5F, 30, 2, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, 14.5F, -2.5F, -12.0F, 2, 2, 4, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -16.5F, -2.5F, -12.0F, 2, 2, 4, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -16.5F, -65.5F, -12.0F, 2, 2, 4, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, 14.5F, -65.5F, -12.0F, 2, 2, 4, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, 14.5F, -64.0F, -15.5F, 2, 62, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -16.5F, -64.0F, -15.5F, 2, 62, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -13.0F, -2.5F, -6.5F, 26, 2, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -13.0F, -65.5F, -6.5F, 26, 2, 2, 0.0F, false));

		corners2 = new RendererModel(this);
		corners2.setRotationPoint(-17.0F, 0.0F, 4.5F);
		lid_rotate_y.addChild(corners2);

		knobs9 = new RendererModel(this);
		knobs9.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs9);
		knobs9.cubeList.add(new ModelBox(knobs9, 74, 71, -17.0F, -4.0F, -16.0F, 4, 4, 4, 0.0F, false));
		knobs9.cubeList.add(new ModelBox(knobs9, 74, 71, -16.0F, -3.0F, -16.5F, 2, 2, 2, 0.0F, false));
		knobs9.cubeList.add(new ModelBox(knobs9, 74, 71, -17.5F, -3.0F, -15.0F, 2, 2, 2, 0.0F, false));
		knobs9.cubeList.add(new ModelBox(knobs9, 74, 71, -16.0F, -1.5F, -15.0F, 2, 2, 2, 0.0F, false));

		knobs10 = new RendererModel(this);
		knobs10.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs10);
		knobs10.cubeList.add(new ModelBox(knobs10, 74, 71, 13.0F, -4.0F, -16.0F, 4, 4, 4, 0.0F, false));
		knobs10.cubeList.add(new ModelBox(knobs10, 74, 71, 14.0F, -3.0F, -16.5F, 2, 2, 2, 0.0F, false));
		knobs10.cubeList.add(new ModelBox(knobs10, 74, 71, 15.5F, -3.0F, -15.0F, 2, 2, 2, 0.0F, false));
		knobs10.cubeList.add(new ModelBox(knobs10, 74, 71, 14.0F, -1.5F, -15.0F, 2, 2, 2, 0.0F, false));

		knobs11 = new RendererModel(this);
		knobs11.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs11);
		knobs11.cubeList.add(new ModelBox(knobs11, 74, 71, 13.0F, -66.0F, -16.0F, 4, 4, 4, 0.0F, false));
		knobs11.cubeList.add(new ModelBox(knobs11, 74, 71, 14.0F, -65.0F, -16.5F, 2, 2, 2, 0.0F, false));
		knobs11.cubeList.add(new ModelBox(knobs11, 74, 71, 15.5F, -65.0F, -15.0F, 2, 2, 2, 0.0F, false));
		knobs11.cubeList.add(new ModelBox(knobs11, 74, 71, 14.0F, -66.5F, -15.0F, 2, 2, 2, 0.0F, false));

		knobs12 = new RendererModel(this);
		knobs12.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs12);
		knobs12.cubeList.add(new ModelBox(knobs12, 74, 71, -17.0F, -66.0F, -16.0F, 4, 4, 4, 0.0F, false));
		knobs12.cubeList.add(new ModelBox(knobs12, 74, 71, -16.0F, -65.0F, -16.5F, 2, 2, 2, 0.0F, false));
		knobs12.cubeList.add(new ModelBox(knobs12, 74, 71, -17.5F, -65.0F, -15.0F, 2, 2, 2, 0.0F, false));
		knobs12.cubeList.add(new ModelBox(knobs12, 74, 71, -16.0F, -66.5F, -15.0F, 2, 2, 2, 0.0F, false));

		knobs13 = new RendererModel(this);
		knobs13.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs13);
		knobs13.cubeList.add(new ModelBox(knobs13, 74, 71, -17.0F, -66.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs13.cubeList.add(new ModelBox(knobs13, 74, 71, -17.5F, -65.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs13.cubeList.add(new ModelBox(knobs13, 74, 71, -16.0F, -66.5F, -7.5F, 2, 2, 2, 0.0F, false));

		knobs14 = new RendererModel(this);
		knobs14.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs14);
		knobs14.cubeList.add(new ModelBox(knobs14, 74, 71, 13.0F, -66.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs14.cubeList.add(new ModelBox(knobs14, 74, 71, 15.5F, -65.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs14.cubeList.add(new ModelBox(knobs14, 74, 71, 14.0F, -66.5F, -7.5F, 2, 2, 2, 0.0F, false));

		knobs15 = new RendererModel(this);
		knobs15.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs15);
		knobs15.cubeList.add(new ModelBox(knobs15, 74, 71, 13.0F, -4.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs15.cubeList.add(new ModelBox(knobs15, 74, 71, 15.5F, -3.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs15.cubeList.add(new ModelBox(knobs15, 74, 71, 14.0F, -1.5F, -7.5F, 2, 2, 2, 0.0F, false));

		knobs16 = new RendererModel(this);
		knobs16.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs16);
		knobs16.cubeList.add(new ModelBox(knobs16, 74, 71, -17.0F, -4.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs16.cubeList.add(new ModelBox(knobs16, 74, 71, -17.5F, -3.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs16.cubeList.add(new ModelBox(knobs16, 74, 71, -16.0F, -1.5F, -7.5F, 2, 2, 2, 0.0F, false));

		mount2 = new RendererModel(this);
		mount2.setRotationPoint(-17.0F, 0.0F, 4.5F);
		lid_rotate_y.addChild(mount2);
		mount2.cubeList.add(new ModelBox(mount2, 61, 73, 16.0F, -12.0F, -9.0F, 1, 2, 4, 0.0F, false));
		mount2.cubeList.add(new ModelBox(mount2, 61, 73, 16.0F, -36.0F, -9.0F, 1, 2, 4, 0.0F, false));
		mount2.cubeList.add(new ModelBox(mount2, 61, 73, 16.0F, -56.0F, -9.0F, 1, 2, 4, 0.0F, false));

		latch2 = new RendererModel(this);
		latch2.setRotationPoint(-17.0F, 0.0F, 4.5F);
		lid_rotate_y.addChild(latch2);
		latch2.cubeList.add(new ModelBox(latch2, 51, 76, -17.0F, -36.0F, -9.0F, 1, 4, 6, 0.0F, false));

		sticker3 = new RendererModel(this);
		sticker3.setRotationPoint(-20.0F, -45.0F, -11.0F);
		setRotationAngle(sticker3, 0.0F, 0.0F, -0.6109F);
		lid_rotate_y.addChild(sticker3);
		sticker3.cubeList.add(new ModelBox(sticker3, 3, 158, -7.0F, -3.0F, 0.4F, 14, 6, 1, 0.0F, false));

		sticker4 = new RendererModel(this);
		sticker4.setRotationPoint(0.0F, 0.0F, -0.5F);
		setRotationAngle(sticker4, 0.0F, 0.0F, 0.7854F);
		sticker3.addChild(sticker4);
		sticker4.cubeList.add(new ModelBox(sticker4, 10, 154, -4.0F, -4.0F, 0.8F, 8, 8, 1, 0.0F, false));

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 24.0F, 0.0F);
		boti.cubeList.add(new ModelBox(boti, 4, 181, -15.0F, -64.0F, -0.25F, 30, 62, 2, 0.0F, false));
	}
	
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void renderBoti(float scale, boolean raw) {
		
	}

	@Override
	public void renderOverwrite(BotiContext context) {
		
	}
	
	public void render(ExteriorTile tile) {
		
		this.lid_rotate_y.rotateAngleY = (float)Math.toRadians(IDoorType.EnumDoorType.TRUNK.getRotationForState(tile.getOpen()));
		
		box.render(0.0625F);
		lid_rotate_y.render(0.0625F);
		boti.render(0.0625F);
	}

	@Override
	public void renderEntity(TardisEntity ent) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, -0.5, 0);
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.5, 0.5, 0.5);
		Minecraft.getInstance().getTextureManager().bindTexture(TrunkExteriorRenderer.TEXTURE);
		box.render(0.0625F);
		lid_rotate_y.render(0.0625F);
		boti.render(0.0625F);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}
}