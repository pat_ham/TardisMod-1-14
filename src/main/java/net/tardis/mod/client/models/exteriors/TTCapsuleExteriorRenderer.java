package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.tileentities.exteriors.TTCapsuleExteriorTile;

public class TTCapsuleExteriorRenderer extends ExteriorRenderer<TTCapsuleExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/tt_capsule.png");
	private static final TTCapsuleExteriorModel MODEL = new TTCapsuleExteriorModel();
	
	@Override
	public void renderExterior(TTCapsuleExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, -1, 0);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(tile);
		GlStateManager.popMatrix();
	}

}
