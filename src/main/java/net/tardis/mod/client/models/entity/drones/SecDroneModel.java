package net.tardis.mod.client.models.entity.drones;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.entity.SecDroidEntity;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class SecDroneModel extends EntityModel<SecDroidEntity> {
	private final RendererModel head;
	private final RendererModel offset;
	private final RendererModel forcesheild0;
	private final RendererModel forcesheild1;
	private final RendererModel forcesheild2;

	public SecDroneModel() {
		textureWidth = 64;
		textureHeight = 64;

		head = new RendererModel(this);
		head.setRotationPoint(0.0F, 0.0F, 0.0F);
		

		offset = new RendererModel(this);
		offset.setRotationPoint(0.5F, 0.0F, 0.5F);
		head.addChild(offset);
		setRotationAngle(offset, 0.0F, 0.0F, 0.7854F);
		offset.cubeList.add(new ModelBox(offset, 0, 0, -5.0F, -5.0F, -5.0F, 9, 9, 9, 0.0F, false));

		forcesheild0 = new RendererModel(this);
		forcesheild0.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(forcesheild0, 0.3491F, 1.0472F, 0.0F);
		forcesheild0.cubeList.add(new ModelBox(forcesheild0, 0, 18, -3.0F, -3.0F, -11.0F, 6, 12, 1, 0.0F, false));
		forcesheild0.cubeList.add(new ModelBox(forcesheild0, 14, 18, -2.0F, -2.0F, -10.5F, 4, 10, 1, 0.0F, false));

		forcesheild1 = new RendererModel(this);
		forcesheild1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(forcesheild1, 0.3491F, 3.1416F, 0.0F);
		forcesheild1.cubeList.add(new ModelBox(forcesheild1, 0, 18, -3.0F, -3.0F, -11.0F, 6, 12, 1, 0.0F, false));
		forcesheild1.cubeList.add(new ModelBox(forcesheild1, 14, 18, -2.0F, -2.0F, -10.5F, 4, 10, 1, 0.0F, false));

		forcesheild2 = new RendererModel(this);
		forcesheild2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(forcesheild2, 0.3491F, -1.0472F, 0.0F);
		forcesheild2.cubeList.add(new ModelBox(forcesheild2, 0, 18, -3.0F, -3.0F, -11.0F, 6, 12, 1, 0.0F, false));
		forcesheild2.cubeList.add(new ModelBox(forcesheild2, 14, 18, -2.0F, -2.0F, -10.5F, 4, 10, 1, 0.0F, false));
	}

	@Override
	public void render(SecDroidEntity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		this.setLivingAnimations(entity, f, f1, Minecraft.getInstance().getRenderPartialTicks());
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 0.85, 0);
		
		float prevRot = ((entity.ticksExisted - 1) * 5F) % 360;
		float rot = (entity.ticksExisted * 5F) % 360;
		
		float real = prevRot + (rot - prevRot) * Minecraft.getInstance().getRenderPartialTicks();
		
		this.forcesheild0.rotateAngleY = (float)Math.toRadians(real);
		this.forcesheild1.rotateAngleY = (float)Math.toRadians(real + 120);
		this.forcesheild2.rotateAngleY = (float)Math.toRadians(real + 240);
		
		head.render(f5);
		forcesheild0.render(f5);
		forcesheild1.render(f5);
		forcesheild2.render(f5);
		
		GlStateManager.popMatrix();
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}