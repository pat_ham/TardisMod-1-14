package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.misc.IDoorType;

public class InteriorModernPoliceBoxModel extends Model implements IInteriorDoorRenderer {
	private final RendererModel base;
	private final RendererModel botireal;
	private final RendererModel leftwindow;
	private final RendererModel leftdoor;
	private final RendererModel rightwindow;
	private final RendererModel rightdoor;

	public static final ResourceLocation TEXTURE =
			new ResourceLocation(Tardis.MODID, "textures/exteriors/modern_police_box.png");

	public InteriorModernPoliceBoxModel() {
		textureWidth = 512;
		textureHeight = 512;

		base = new RendererModel(this);
		base.setRotationPoint(0.0F, 27.0F, 0.0F);
		base.cubeList.add(new ModelBox(base, 48, 392, 22.0F, -108.0F, -30.0F, 8, 108, 12, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 170, 138, -22.0F, -100.0F, -28.0F, 44, 2, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 151, 413, -29.0F, -4.0F, -30.0F, 56, 4, 8, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 312, 93, -24.0F, -107.0F, -32.2F, 48, 6, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 323, 108, -24.0F, -107.0F, -19.8F, 48, 6, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 40, 408, -26.0F, -108.0F, -32.0F, 52, 8, 12, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 72, 96, 24.0F, -108.0F, -32.4F, 2, 8, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 33, 252, -24.0F, -108.0F, -32.4F, 48, 1, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 87, 49, -24.0F, -101.0F, -32.4F, 48, 1, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 7, 120, -26.0F, -108.0F, -32.4F, 2, 8, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 174, 390, -30.0F, -108.0F, -30.0F, 8, 108, 12, 0.0F, false));

		botireal = new RendererModel(this);
		botireal.setRotationPoint(0.0F, 24.0F, -10.0F);
		botireal.cubeList.add(new ModelBox(botireal, 312, 10, -24.0F, -102.0F, -23.0F, 48, 100, 4, 0.0F, false));

		leftwindow = new RendererModel(this);
		leftwindow.setRotationPoint(-23.0F, 17.0F, -24.0F);
		leftwindow.cubeList.add(new ModelBox(leftwindow, 440, 70, 3.0F, -84.0F, -1.2F, 16, 18, 0, 0.0F, false));
		leftwindow.cubeList.add(new ModelBox(leftwindow, 440, 70, 3.0F, -84.0F, 0.8F, 16, 18, 0, 0.0F, false));

		leftdoor = new RendererModel(this);
		leftdoor.setRotationPoint(-23.0F, 17.0F, -24.0F);
		leftdoor.cubeList.add(new ModelBox(leftdoor, 80, 117, 1.0F, -90.0F, -2.0F, 20, 4, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 441, 264, 3.0F, -86.0F, -2.0F, 16, 2, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 441, 264, 19.0F, -86.0F, -2.0F, 2, 22, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 441, 264, 1.0F, -86.0F, -2.0F, 2, 22, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 441, 264, 3.0F, -75.6F, -2.0F, 16, 1, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 441, 264, 7.6F, -84.0F, -2.0F, 1, 18, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 441, 264, 13.4F, -84.0F, -2.0F, 1, 18, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 80, 117, 1.0F, -64.0F, -2.0F, 20, 4, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 128, 266, 1.0F, -60.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 128, 266, 19.0F, -60.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 475, 171, 19.0F, -54.0F, -4.0F, 2, 6, 2, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 179, 79, 4.0F, -59.0F, 0.0F, 14, 18, 0, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 65, 65, 3.0F, -60.0F, 0.4F, 20, 64, 0, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 60, 451, 1.0F, -40.0F, -2.0F, 20, 4, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 128, 266, 1.0F, -38.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 128, 266, 19.0F, -38.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 60, 451, 1.0F, -18.0F, -2.0F, 20, 4, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 128, 266, 1.0F, -16.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 128, 266, 19.0F, -16.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 60, 451, 1.0F, 2.0F, -2.0F, 20, 4, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 441, 264, 3.0F, -66.0F, -2.0F, 16, 2, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 70, 170, 21.0F, -90.0F, -4.0F, 4, 96, 4, 0.0F, false));

		rightwindow = new RendererModel(this);
		rightwindow.setRotationPoint(23.0F, 17.0F, -24.0F);
		rightwindow.cubeList.add(new ModelBox(rightwindow, 440, 70, -19.0F, -86.0F, -1.2F, 16, 20, 0, 0.0F, false));
		rightwindow.cubeList.add(new ModelBox(rightwindow, 440, 70, -19.0F, -86.0F, 0.8F, 16, 20, 0, 0.0F, false));

		rightdoor = new RendererModel(this);
		rightdoor.setRotationPoint(23.0F, 17.0F, -24.0F);
		rightdoor.cubeList.add(new ModelBox(rightdoor, 80, 117, -21.0F, -90.0F, -2.0F, 20, 4, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 441, 264, -21.0F, -86.0F, -2.0F, 2, 22, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 441, 264, -19.0F, -86.0F, -2.0F, 16, 2, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 441, 264, -3.0F, -86.0F, -2.0F, 2, 22, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 441, 264, -8.6F, -84.0F, -2.0F, 1, 18, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 441, 264, -14.4F, -84.0F, -2.0F, 1, 18, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 441, 264, -19.0F, -75.6F, -2.0F, 16, 1, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 441, 264, -19.0F, -66.0F, -2.0F, 16, 2, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 80, 117, -21.0F, -64.0F, -2.0F, 20, 4, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 128, 266, -21.0F, -60.0F, -2.0F, 2, 20, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 3, 71, -23.0F, -90.0F, -2.0F, 2, 96, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 3, 71, -25.0F, -90.0F, -2.0F, 2, 96, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 475, 171, -21.0F, -56.0F, -4.0F, 2, 12, 2, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 475, 171, -21.0F, -38.0F, -4.0F, 2, 2, 2, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 469, 181, -25.0F, -40.0F, 1.0F, 8, 4, 2, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 60, 451, -21.0F, -40.0F, -2.0F, 20, 4, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 65, 65, -21.0F, -64.0F, 0.4F, 20, 68, 0, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 272, 0, -15.0F, -54.5F, 0.0F, 8, 8, 0, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 128, 266, -3.0F, -60.0F, -2.0F, 2, 20, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 128, 266, -3.0F, -38.0F, -2.0F, 2, 20, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 128, 266, -21.0F, -38.0F, -2.0F, 2, 20, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 60, 451, -21.0F, -18.0F, -2.0F, 8, 4, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 60, 451, -13.0F, -18.0F, -2.0F, 12, 4, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 128, 266, -21.0F, -14.0F, -2.0F, 2, 16, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 128, 266, -3.0F, -16.0F, -2.0F, 2, 20, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 60, 451, -21.0F, 2.0F, -2.0F, 20, 4, 4, 0.0F, false));
	}

	@Override
	public void render(DoorEntity door) {

		GlStateManager.pushMatrix();//Tell rendering engine to add a new object to the render matrix stack
		GlStateManager.enableRescaleNormal(); //Ensures model isn't rendered fullbright all the time
		GlStateManager.translated(0, 0.95, 0); // Translation must be negative as models are loaded in upside down.
		GlStateManager.scalef(0.37f, 0.36f, 0.37f); //Scales the model down by 4

		EnumDoorState state = door.getOpenState();
		switch(state) {
			case ONE:
				this.rightdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default

				this.rightwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default

				this.leftdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				this.leftwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				break;


			case BOTH:
				this.rightdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE));

				this.rightwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE));

				this.leftdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open

				this.leftwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open

				break;


			case CLOSED://close both doors
				this.rightdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
				this.leftdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				this.rightwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
				this.leftwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));


				break;
			default:
				break;
		}

		//botireal.render(0.0625F);
		base.render(0.0625F);
		leftdoor.render(0.0625F);
		rightdoor.render(0.0625F);
		rightwindow.render(0.0625F);
		leftwindow.render(0.0625F);

		ModelHelper.renderPartBrightness(1F, this.rightwindow);
		ModelHelper.renderPartBrightness(1F, this.leftwindow);

		GlStateManager.disableRescaleNormal();//Ensures lighting on model is preserved when rescaled

		GlStateManager.popMatrix(); //Finish rendering
	}


	@Override
	public ResourceLocation getTexture() {
		return TEXTURE;
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}