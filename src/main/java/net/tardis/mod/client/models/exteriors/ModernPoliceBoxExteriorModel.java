package net.tardis.mod.client.models.exteriors;// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.client.models.IExteriorModel;
import net.tardis.mod.client.renderers.exteriors.TrunkExteriorRenderer;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ModernPoliceBoxExteriorModel extends ExteriorModel implements IExteriorModel {
	private final RendererModel base;
	private final RendererModel rightdoor;
	private final RendererModel leftdoor;
	private final RendererModel botifake;
	private final RendererModel botireal;
	private final RendererModel windows;
	private final RendererModel rightwindow;
	private final RendererModel leftwindow;
	private final RendererModel lamp;

	public ModernPoliceBoxExteriorModel() {
		textureWidth = 512;
		textureHeight = 512;

		base = new RendererModel(this);
		base.setRotationPoint(0.0F, 24.0F, 0.0F);
		base.cubeList.add(new ModelBox(base, 99, 433, -20.0F, -119.0F, -20.0F, 40, 1, 40, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 99, 433, -30.0F, -110.0F, -30.0F, 60, 2, 60, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 197, 22.0F, -108.0F, -2.0F, 8, 102, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 34, 396, -2.0F, -108.0F, 22.0F, 4, 102, 8, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 92, 34, 22.0F, -108.0F, 22.0F, 8, 102, 8, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 92, 34, -30.0F, -108.0F, 22.0F, 8, 102, 8, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 67, 7, -24.0F, -100.0F, -22.0F, 0, 94, 44, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 65, 65, -22.0F, -100.0F, 20.0F, 20, 94, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 96, 66, 2.0F, -100.0F, 20.0F, 20, 94, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 129, 375, 24.0F, -100.0F, -22.0F, 0, 94, 44, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 92, 34, 22.0F, -108.0F, -30.0F, 8, 102, 8, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 176, 277, 22.0F, -114.0F, -28.0F, 6, 4, 6, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 70, -30.0F, -108.0F, -2.0F, 8, 102, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 9, 40, 22.0F, -24.0F, -26.0F, 2, 18, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 72, 458, -22.0F, -100.0F, -28.0F, 44, 2, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 318, 69, -24.0F, -107.0F, -32.2F, 48, 6, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 176, 427, -26.0F, -108.0F, -32.0F, 52, 8, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 7, 97, 24.0F, -108.0F, -32.4F, 2, 8, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 138, 413, -24.0F, -108.0F, -32.4F, 48, 1, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 138, 413, -24.0F, -101.0F, -32.4F, 48, 1, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 58, -26.0F, -108.0F, -32.4F, 2, 8, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 0, 22.0004F, -104.0F, 2.0F, 4, 8, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, 22.0004F, -28.0F, 4.0F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, 22.0004F, -50.0F, 2.0F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, 22.0F, -74.0F, -22.0F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 49, 28, 22.0004F, -24.0F, 22.0F, 4, 18, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 59, 12, 22.0004F, -8.0F, 2.0F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, 22.0F, -50.0F, -22.0F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0F, -70.0F, -4.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0F, -70.0F, 2.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0004F, -70.0F, 20.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0004F, -48.0F, 20.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0F, -48.0F, 2.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0F, -48.0F, -22.0004F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0F, -48.0F, -4.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0F, -28.0F, 2.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0F, -26.0F, -22.0004F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, 22.0F, -28.0F, -22.0F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 59, 12, 22.0F, -8.0F, -22.0F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0F, -26.0F, -4.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0004F, -26.0F, 20.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 22.0F, -70.0F, -22.0004F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 22.0F, -96.0F, -22.0004F, 4, 22, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 21.6F, -94.0F, -9.6F, 4, 18, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 72, 458, 26.0004F, -100.0F, -22.0F, 2, 2, 44, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 22.0004F, -96.0F, 4.0F, 4, 2, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 22.0F, -76.0F, -20.0004F, 4, 2, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 22.0F, -96.0F, -4.0F, 4, 22, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 22.0F, -96.0F, 2.0F, 4, 22, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 21.6F, -85.6F, -20.0F, 4, 1, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 22.0F, -96.0F, -20.0004F, 4, 2, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 318, 69, 32.2004F, -107.0F, -24.0004F, 0, 6, 48, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 22.0004F, -96.0F, 20.0F, 4, 22, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 21.6004F, -94.0F, 14.4F, 4, 18, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 22.0004F, -76.0F, 4.0F, 4, 2, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 21.6004F, -85.6F, 4.0F, 4, 1, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 21.6F, -94.0F, -15.4F, 4, 18, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 21.6F, -94.0F, 8.6F, 4, 18, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, 22.0004F, -74.0F, 2.0F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 72, 458, 28.0004F, -108.0F, -26.0F, 4, 8, 52, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 138, 413, 22.0F, -104.0F, -22.0F, 4, 8, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 286, 103, 32.4004F, -108.0F, 24.0F, 0, 8, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 72, 458, 32.4004F, -108.0F, -24.0004F, 0, 1, 48, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 72, 458, 32.4004F, -101.0F, -24.0004F, 0, 1, 48, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 286, 103, 32.4F, -108.0F, -26.0004F, 0, 8, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 45, 17, -21.9996F, -104.0F, 22.0004F, 20, 8, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -23.9996F, -28.0F, 22.0004F, 20, 4, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -21.9996F, -50.0F, 22.0004F, 20, 4, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, 2.0004F, -74.0F, 22.0F, 20, 4, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 14, 11, -23.9996F, -24.0F, 22.0004F, 2, 18, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -21.9996F, -8.0F, 22.0004F, 20, 4, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, 2.0004F, -50.0F, 22.0F, 20, 4, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 2.0004F, -70.0F, 22.0F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -3.9996F, -70.0F, 22.0F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -22.0F, -70.0F, 22.0004F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -22.0F, -48.0F, 22.0004F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -3.9996F, -48.0F, 22.0F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 20.0004F, -48.0F, 22.0F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 2.0004F, -48.0F, 22.0F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -3.9996F, -28.0F, 22.0F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 20.0004F, -26.0F, 22.0F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, 2.0004F, -28.0F, 22.0F, 20, 4, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, 2.0004F, -8.0F, 22.0F, 20, 4, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 2.0004F, -26.0F, 22.0F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -22.0F, -26.0F, 22.0004F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, 20.0004F, -70.0F, 22.0F, 2, 20, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 20.0004F, -96.0F, 22.0F, 2, 22, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 8.6004F, -94.0F, 21.6F, 1, 18, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 8, 418, -21.9996F, -100.0F, 26.0F, 44, 2, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -19.9996F, -96.0F, 22.0004F, 16, 2, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 4.0004F, -76.0F, 22.0F, 16, 2, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 2.0004F, -96.0F, 22.0F, 2, 22, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -3.9996F, -96.0F, 22.0F, 2, 22, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 4.0004F, -85.6F, 21.6F, 16, 1, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 4.0004F, -96.0F, 22.0F, 16, 2, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 318, 69, -23.9996F, -107.0F, 32.1964F, 48, 6, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -22.0F, -96.0F, 22.0004F, 2, 22, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -15.3996F, -94.0F, 21.6004F, 1, 18, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -19.9996F, -76.0F, 22.0004F, 16, 2, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -20.0F, -85.6F, 21.6004F, 16, 1, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, 14.4004F, -94.0F, 21.6F, 1, 18, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -9.6F, -94.0F, 21.6F, 1, 18, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -21.9996F, -74.0F, 22.0004F, 20, 4, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 72, 458, -25.9996F, -108.0F, 28.0004F, 52, 8, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 45, 17, 2.0004F, -104.0F, 22.0F, 20, 8, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 48, 232, -25.9996F, -108.0F, 32.0004F, 2, 8, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 162, 445, -23.9996F, -108.0F, 32.0004F, 48, 1, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 162, 445, -23.9996F, -101.0F, 32.0004F, 48, 1, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 72, 458, 24.0004F, -108.0F, 32.0F, 2, 8, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 138, 413, -26.0004F, -104.0F, -21.9996F, 4, 8, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -26.0004F, -28.0F, -23.9996F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -26.0004F, -50.0F, -21.9996F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -26.0F, -74.0F, 2.0004F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 49, 24, -26.0008F, -24.0F, -23.9996F, 4, 18, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -26.0004F, -8.0F, -21.9996F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -26.0F, -50.0F, 2.0004F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0F, -70.0F, 2.0004F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0F, -70.0F, -3.9996F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0008F, -70.0F, -22.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0008F, -48.0F, -22.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0F, -48.0F, -3.9996F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0F, -48.0F, 20.0008F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0F, -48.0F, 2.0004F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0F, -28.0F, -3.9996F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0F, -26.0F, 20.0008F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -26.0F, -28.0F, 2.0004F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -26.0F, -8.0F, 2.0004F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0F, -26.0F, 2.0004F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0008F, -26.0F, -22.0F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 101, 106, -26.0F, -70.0F, 20.0008F, 4, 20, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -26.0F, -96.0F, 20.0008F, 4, 22, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -25.6F, -94.0F, 8.6004F, 4, 18, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 8, 418, -28.0F, -100.0F, -21.9996F, 2, 2, 44, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -26.0004F, -96.0F, -19.9996F, 4, 2, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -26.0F, -76.0F, 4.0008F, 4, 2, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -26.0F, -96.0F, 2.0004F, 4, 22, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -26.0F, -96.0F, -3.9996F, 4, 22, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -25.6F, -85.6F, 4.0004F, 4, 1, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -26.0F, -96.0F, 4.0008F, 4, 2, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 318, 69, -32.1964F, -107.0F, -23.9992F, 0, 6, 48, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -26.0008F, -96.0F, -22.0F, 4, 22, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -25.6004F, -94.0F, -15.3996F, 4, 18, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -26.0004F, -76.0F, -19.9996F, 4, 2, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -25.6004F, -85.6F, -19.9996F, 4, 1, 16, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -25.6F, -94.0F, 14.4004F, 4, 18, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 460, 232, -25.6F, -94.0F, -9.5996F, 4, 18, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 3, 216, -26.0004F, -74.0F, -21.9996F, 4, 4, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 64, 30, -32.0F, -108.0F, -25.9996F, 4, 8, 52, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 0, -26.0F, -104.0F, 2.0004F, 4, 8, 20, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 48, 232, -32.0008F, -108.0F, -25.9996F, 0, 8, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 1, 106, -32.0004F, -108.0F, -23.9992F, 0, 1, 48, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 1, 106, -32.0004F, -101.0F, -23.9992F, 0, 1, 48, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 62, 245, -31.9996F, -108.0F, 24.0008F, 0, 8, 2, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 0, -32.0F, -4.0F, -32.0F, 64, 4, 64, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 448, -30.0F, -6.0F, -30.0F, 60, 2, 60, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 92, 34, -30.0F, -108.0F, -30.0F, 8, 102, 8, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 176, 277, 22.0F, -114.0F, 22.0F, 6, 4, 6, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 176, 277, -28.0F, -114.0F, -28.0F, 6, 4, 6, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 99, 433, -26.0F, -114.0F, -26.0F, 52, 4, 52, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 99, 433, -24.0F, -118.0F, -24.0F, 48, 4, 48, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 176, 277, -28.0F, -114.0F, 22.0F, 6, 4, 6, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 85, 106, -6.0F, -122.0F, -6.0F, 12, 4, 12, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 441, 440, -2.0F, -134.0F, -2.0F, 4, 2, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 441, 440, -4.0F, -124.0F, -4.0F, 8, 2, 8, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 441, 440, -4.0F, -132.0F, -4.0F, 8, 2, 8, 0.0F, false));

		rightdoor = new RendererModel(this);
		rightdoor.setRotationPoint(23.0F, 17.0F, -24.0F);
		rightdoor.cubeList.add(new ModelBox(rightdoor, 138, 413, -21.0F, -93.0F, -2.0F, 20, 4, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 460, 232, -21.0F, -89.0F, -2.0F, 2, 22, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 460, 232, -19.0F, -89.0F, -2.0F, 16, 2, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 460, 232, -3.0F, -89.0F, -2.0F, 2, 22, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 460, 232, -8.6F, -87.0F, -1.6F, 1, 18, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 460, 232, -14.4F, -87.0F, -1.6F, 1, 18, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 460, 232, -19.0F, -78.6F, -1.6F, 16, 1, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 460, 232, -19.0F, -69.0F, -2.0F, 16, 2, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 101, 106, -21.0F, -67.0F, -2.0F, 20, 4, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 101, 106, -21.0F, -63.0F, -2.0F, 2, 20, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 3, 71, -23.0F, -93.0F, -2.0F, 2, 96, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 473, 175, -21.0F, -59.0F, -4.0F, 2, 12, 2, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 473, 175, -21.0F, -41.0F, -4.0F, 2, 2, 2, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 217, 441, -21.0F, -43.0F, -2.0F, 20, 4, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 65, 65, -21.0F, -67.0F, 0.4F, 20, 68, 0, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 272, 0, -15.0F, -57.5F, 0.0F, 8, 8, 0, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 64, 158, -3.0F, -63.0F, -2.0F, 2, 20, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 64, 158, -3.0F, -41.0F, -2.0F, 2, 20, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 64, 158, -21.0F, -41.0F, -2.0F, 2, 20, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 217, 441, -21.0F, -21.0F, -2.0F, 8, 4, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 217, 441, -13.0F, -21.0F, -2.0F, 12, 4, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 217, 441, -21.0F, -17.0F, -2.0F, 2, 16, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 217, 441, -3.0F, -19.0F, -2.0F, 2, 20, 4, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 217, 441, -21.0F, -1.0F, -2.0F, 20, 4, 4, 0.0F, false));

		leftdoor = new RendererModel(this);
		leftdoor.setRotationPoint(-23.0F, 17.0F, -24.0F);
		leftdoor.cubeList.add(new ModelBox(leftdoor, 138, 413, 1.0F, -93.0F, -2.0F, 20, 4, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 460, 232, 3.0F, -89.0F, -2.0F, 16, 2, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 460, 232, 19.0F, -89.0F, -2.0F, 2, 22, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 460, 232, 1.0F, -89.0F, -2.0F, 2, 22, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 460, 232, 3.0F, -78.6F, -1.6F, 16, 1, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 460, 232, 7.6F, -87.0F, -1.6F, 1, 18, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 460, 232, 13.4F, -87.0F, -1.6F, 1, 18, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 101, 106, 1.0F, -67.0F, -2.0F, 20, 4, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 64, 158, 1.0F, -63.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 101, 106, 19.0F, -63.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 473, 175, 19.0F, -57.0F, -4.0F, 2, 6, 2, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 179, 79, 4.0F, -62.0F, -0.1F, 14, 18, 0, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 65, 65, 3.0F, -93.0F, 0.4F, 20, 94, 0, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 101, 106, 1.0F, -43.0F, -2.0F, 20, 4, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 64, 158, 1.0F, -41.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 64, 158, 19.0F, -41.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 217, 441, 1.0F, -21.0F, -2.0F, 20, 4, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 217, 441, 1.0F, -19.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 217, 441, 19.0F, -19.0F, -2.0F, 2, 20, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 217, 441, 1.0F, -1.0F, -2.0F, 20, 4, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 460, 232, 3.0F, -69.0F, -2.0F, 16, 2, 4, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 0, 197, 21.0F, -95.0F, -4.0F, 4, 96, 4, 0.0F, false));

		botifake = new RendererModel(this);
		botifake.setRotationPoint(0.0F, 24.0F, 0.0F);
		botifake.cubeList.add(new ModelBox(botifake, 82, 1, -20.0F, -102.0F, 0.0F, 44, 96, 4, 0.0F, false));
		botifake.cubeList.add(new ModelBox(botifake, 71, 11, -20.0F, -106.0F, -24.0F, 48, 4, 36, 0.0F, false));

		botireal = new RendererModel(this);
		botireal.setRotationPoint(0.0F, 24.0F, 0.0F);
		botireal.cubeList.add(new ModelBox(botireal, 316, 10, -24.0F, -102.0F, -23.0F, 44, 96, 4, 0.0F, false));

		windows = new RendererModel(this);
		windows.setRotationPoint(0.0F, 24.0F, 0.0F);
		windows.cubeList.add(new ModelBox(windows, 450, 66, 21.2F, -94.0F, -20.0F, 4, 18, 16, 0.0F, false));
		windows.cubeList.add(new ModelBox(windows, 450, 66, 21.2004F, -94.0F, 4.0F, 4, 18, 16, 0.0F, false));
		windows.cubeList.add(new ModelBox(windows, 450, 66, 4.0004F, -94.0F, 25.196F, 16, 18, 0, 0.0F, false));
		windows.cubeList.add(new ModelBox(windows, 450, 66, -20.0F, -94.0F, 25.1964F, 16, 18, 0, 0.0F, false));
		windows.cubeList.add(new ModelBox(windows, 450, 66, -25.196F, -94.0F, 4.0004F, 4, 18, 16, 0.0F, false));
		windows.cubeList.add(new ModelBox(windows, 450, 66, -25.1964F, -94.0F, -19.9996F, 4, 18, 16, 0.0F, false));

		rightwindow = new RendererModel(this);
		rightwindow.setRotationPoint(23.0F, 17.0F, -24.0F);
		rightwindow.cubeList.add(new ModelBox(rightwindow, 450, 66, -19.0F, -89.0F, -1.2F, 16, 20, 0, 0.0F, false));

		leftwindow = new RendererModel(this);
		leftwindow.setRotationPoint(-23.0F, 17.0F, -24.0F);
		leftwindow.cubeList.add(new ModelBox(leftwindow, 450, 66, 3.0F, -87.0F, -1.2F, 16, 18, 0, 0.0F, false));

		lamp = new RendererModel(this);
		lamp.setRotationPoint(0.0F, 24.0F, 0.0F);
		lamp.cubeList.add(new ModelBox(lamp, 476, 366, 2.0F, -130.0F, -2.0F, 2, 6, 4, 0.0F, false));
		lamp.cubeList.add(new ModelBox(lamp, 476, 366, -2.0F, -130.0F, -4.0F, 4, 6, 2, 0.0F, false));
		lamp.cubeList.add(new ModelBox(lamp, 476, 366, -4.0F, -130.0F, -2.0F, 2, 6, 4, 0.0F, false));
		lamp.cubeList.add(new ModelBox(lamp, 476, 366, -2.0F, -130.0F, 2.0F, 4, 6, 2, 0.0F, false));
	}

	@Override
	public void render(ExteriorTile tile) {
		EnumDoorState state = tile.getOpen();
		switch(state) {
			case ONE:
				this.rightdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default

				this.rightwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default

				this.leftdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				this.leftwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				break;


			case BOTH:
				this.rightdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE));

				this.rightwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE));

				this.leftdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open

				this.leftwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open

				break;


			case CLOSED://close both doors
				this.rightdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
				this.leftdoor.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				this.rightwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
				this.leftwindow.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));


				break;
			default:
				break;
		}
		this.botifake.render(0.0625F);
		base.render(0.0625F);
		leftdoor.render(0.0625F);
		rightdoor.render(0.0625F);
		windows.render(0.0625F);
		rightwindow.render(0.0625F);
		leftwindow.render(0.0625F);
		lamp.render(0.0625F);

		ModelHelper.renderPartBrightness(1F, this.lamp);
		ModelHelper.renderPartBrightness(1F, this.windows);
		ModelHelper.renderPartBrightness(1F, this.rightwindow);
		ModelHelper.renderPartBrightness(1F, this.leftwindow);
	}

	public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
		RendererModel.rotateAngleX = x;
		RendererModel.rotateAngleY = y;
		RendererModel.rotateAngleZ = z;
	}

	@Override
	public void renderEntity(TardisEntity ent) {
		GlStateManager.pushMatrix();

		GlStateManager.translated(0, -0.5, 0);
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.5, 0.5, 0.5);
		Minecraft.getInstance().getTextureManager().bindTexture(TrunkExteriorRenderer.TEXTURE);

		botifake.render(0.0625F);
		//botireal.render(0.0625F);
		base.render(0.0625F);
		leftdoor.render(0.0625F);
		rightdoor.render(0.0625F);
		windows.render(0.0625F);
		rightwindow.render(0.0625F);
		leftwindow.render(0.0625F);
		lamp.render(0.0625F);

		GlStateManager.disableRescaleNormal();

		GlStateManager.popMatrix();

	}
}