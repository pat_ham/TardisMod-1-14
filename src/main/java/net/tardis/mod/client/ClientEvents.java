package net.tardis.mod.client;

import static net.minecraft.client.renderer.WorldRenderer.drawShape;

import java.util.Iterator;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.client.event.InputEvent.MouseScrollEvent;
import net.minecraftforge.client.event.InputUpdateEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.renderers.boti.BotiManager;
import net.tardis.mod.client.renderers.layers.SonicLaserRenderLayer;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.RenderHelper;
import net.tardis.mod.items.DebugItem;
import net.tardis.mod.items.SquarenessGunItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BessieHornMessage;

@Mod.EventBusSubscriber(modid = Tardis.MODID, value = Dist.CLIENT)
public class ClientEvents {
	
	public static BotiManager draw;
	
    @SubscribeEvent
    public static void onBlockHighlight(DrawBlockHighlightEvent event) {
        ActiveRenderInfo information = event.getInfo();

        // Squareness gun
        ClientPlayerEntity player = Minecraft.getInstance().player;
        if (PlayerHelper.isInEitherHand(player, TItems.SQUARENESS_GUN)) {
            event.setCanceled(true);
            RayTraceResult result = PlayerHelper.getPosLookingAt(player, 5);
            if (result != null && result.getType() == RayTraceResult.Type.BLOCK) {
                BlockRayTraceResult blockRayTraceResult = (BlockRayTraceResult) result;
                BlockPos pos = blockRayTraceResult.getPos();
                ClientWorld world = Minecraft.getInstance().world;

                double correctedRenderPosX = (pos.getX() - TileEntityRendererDispatcher.staticPlayerX);
                double correctedRenderPosY = (pos.getY() - TileEntityRendererDispatcher.staticPlayerY);
                double correctedRenderPosZ = (pos.getZ() - TileEntityRendererDispatcher.staticPlayerZ);

                //Create a cube that we can just grow one in every direction
                AxisAlignedBB box = SquarenessGunItem.getSelection(pos, player.getHorizontalFacing());

                GlStateManager.pushMatrix();
                GlStateManager.translated(correctedRenderPosX, correctedRenderPosY, correctedRenderPosZ);
                for (int iteratedX = (int) box.minX; iteratedX < (int) box.maxX; iteratedX++) {
                    for (int iteratedY = (int) box.minY; iteratedY < (int) box.maxY; iteratedY++) {
                        for (int iteratedZ = (int) box.minZ; iteratedZ < (int) box.maxZ; iteratedZ++) {
                            if (!world.isAirBlock(pos.add(iteratedX, iteratedY, iteratedZ))) {
                                GlStateManager.pushMatrix();
                                GlStateManager.translatef(iteratedX, iteratedY, iteratedZ);
                                BlockState blockState = world.getBlockState(pos.add(iteratedX, iteratedY, iteratedZ));
                                VoxelShape collisionShape = blockState.getCollisionShape(world, pos);
                                if (!collisionShape.isEmpty()) {
                                    RenderHelper.renderAABB(collisionShape.getBoundingBox(), 0.2F, 0.6F, 0.8F, 0.9F);
                                }
                                GlStateManager.popMatrix();
                            }
                        }
                    }
                }
                GlStateManager.popMatrix();

                double projectedX = information.getProjectedView().x;
                double projectedY = information.getProjectedView().y;
                double projectedZ = information.getProjectedView().z;
                for (Iterator<BlockPos> iterator = BlockPos.getAllInBox(new BlockPos(box.maxX, box.maxY, box.maxZ), new BlockPos(box.minX, box.minY, box.minZ)).iterator(); iterator.hasNext(); ) {
                    BlockPos blockPos = iterator.next();
                    BlockState shape = player.world.getBlockState(blockPos);
                    drawShape(shape.getShape((ClientWorld)player.world, blockPos, ISelectionContext.forEntity(information.getRenderViewEntity())), (double) blockPos.getX() - projectedX, (double) blockPos.getY() - projectedY, (double) blockPos.getZ() - projectedZ, 0.0F, 0.0F, 1, 1F);
                }
            }

        }

    }
    

    @SubscribeEvent
    public static void onRenderWorldLast(RenderWorldLastEvent event) {
        //Sonic
        SonicLaserRenderLayer.onRenderWorldLast(event);
        
        ClientPlayerEntity player = Minecraft.getInstance().player;
        GlStateManager.pushMatrix();
        GlStateManager.depthMask(true);
        GlStateManager.enableBlend();
        GlStateManager.translated(-player.posX, -player.posY, -player.posZ);
        if(player != null && player.getHeldItemMainhand().getItem() == TItems.DEBUG)
        	RenderHelper.renderAABB(((DebugItem)TItems.DEBUG).box, 1, 0, 0, 0.5F);
        GlStateManager.disableBlend();
        GlStateManager.popMatrix();
    }

    @SubscribeEvent
    public static void input(InputUpdateEvent event) {
        if (event.getMovementInput().jump) {
            Network.sendToServer(new BessieHornMessage());
        }
    }
    
    @SubscribeEvent
    public static void handleScroll(MouseScrollEvent event) {
    	if(Minecraft.getInstance().player != null && Minecraft.getInstance().player.getHeldItemMainhand().getItem() == TItems.DEBUG) {
    		event.setCanceled(true);
    		if(Minecraft.getInstance().player.isSneaking()) {
    			((DebugItem)TItems.DEBUG).width = ((DebugItem)TItems.DEBUG).height += event.getScrollDelta() > 0 ? 0.0625 : -0.0625;
    		}
    		else ((DebugItem)TItems.DEBUG).offsetY += event.getScrollDelta() > 0 ? 0.0625 : -0.0625;
    	}
    }
    
    @SubscribeEvent
	public static void onRenderOverlayPost(RenderGameOverlayEvent.Post event)
	{
       	int scaledWidth = Minecraft.getInstance().mainWindow.getScaledWidth();
    	int scaledHeight = Minecraft.getInstance().mainWindow.getScaledHeight();
    	PlayerEntity player = Tardis.proxy.getClientPlayer();
    	if (player != null) {
			if (PlayerHelper.isInEitherHand(player, TItems.SONIC)) {
				ItemStack stack = PlayerHelper.getHeldStack(player, Hand.MAIN_HAND);
				stack.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(sonic -> {
					if (sonic.getMode() == 0) { //If in Block Interaction Mode
						RayTraceResult result = PlayerHelper.getPosLookingAt(player, 4);
						if(result instanceof BlockRayTraceResult)
						{
							BlockRayTraceResult btr = (BlockRayTraceResult)result;
							TileEntity te = player.world.getTileEntity(btr.getPos());
							if (te != null) {
								te.getCapability(CapabilityEnergy.ENERGY).ifPresent(cap -> {
									String text = "";
									int maxStorage = cap.getMaxEnergyStored();
									int storage = cap.getEnergyStored();
									if (maxStorage >= 0) {
										text = storage + "/" + maxStorage + " FE";
									}
									GlStateManager.pushMatrix();
									FontRenderer fr = Minecraft.getInstance().fontRenderer;
									fr.drawStringWithShadow(text, scaledWidth / 2 - 15, scaledHeight / 2 - 30, 0xFFFFFF);
									GlStateManager.popMatrix();
								});
							}
						}
					}
				});
			}
    	}
    	
    	if(draw != null && draw.fbo != null)
    		draw.fbo.framebufferRenderExt(Minecraft.getInstance().mainWindow.getFramebufferWidth(), Minecraft.getInstance().mainWindow.getFramebufferHeight(), false);
    	
	}
}
