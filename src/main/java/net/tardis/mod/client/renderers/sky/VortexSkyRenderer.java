package net.tardis.mod.client.renderers.sky;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.IRenderHandler;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.RenderHelper;

@OnlyIn(Dist.CLIENT)
public class VortexSkyRenderer implements IRenderHandler {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/sky/vortex.png");
	
	@Override
	public void render(int ticks, float partialTicks, ClientWorld world, Minecraft mc) {
		GlStateManager.pushMatrix();
		GlStateManager.disableFog();
		GlStateManager.disableCull();
		GlStateManager.color3f(1, 1, 1);
		GlStateManager.translated(0, -mc.player.posY, 0);
		GlStateManager.rotated((world.getGameTime() + partialTicks) % 360, 0, 1, 0);
		mc.getTextureManager().bindTexture(TEXTURE);
		for(int i = 0; i < 200; ++i) {
			RenderHelper.renderInsideBox(-10, -20 * i, -10, 10, 20 * i, 10);
		}
		GlStateManager.enableFog();
		GlStateManager.enableCull();
		GlStateManager.popMatrix();
	}
	
	public void drawQuad(BufferBuilder bb, int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {
		bb.pos(minX, minY, minZ).tex(0, 0).endVertex();
		bb.pos(maxX, minY, minZ).tex(1, 0).endVertex();
		bb.pos(maxX, maxY, minZ).tex(1, 1).endVertex();
		bb.pos(minX, maxY, minZ).tex(0, 1).endVertex();
	}

}
