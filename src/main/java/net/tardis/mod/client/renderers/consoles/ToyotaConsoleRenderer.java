package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.ToyotaConsoleModel;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class ToyotaConsoleRenderer extends TileEntityRenderer<ToyotaConsoleTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/toyota.png");
	public static final ToyotaConsoleModel MODEL = new ToyotaConsoleModel();
	
	@Override
	public void render(ToyotaConsoleTile tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0.9, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.6, 0.6, 0.6);
		if(tileEntityIn.getVariant() != null)
			Minecraft.getInstance().getTextureManager().bindTexture(tileEntityIn.getVariant().getTexture());
		else Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(tileEntityIn);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}
}
