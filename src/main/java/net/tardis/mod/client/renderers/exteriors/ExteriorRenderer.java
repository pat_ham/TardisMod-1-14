package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.item.Items;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public abstract class ExteriorRenderer<T extends ExteriorTile> extends TileEntityRenderer<T>{

	@Override
	public void render(T tile, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y - 0.5, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		if(tile.getWorld() != null && tile.getWorld().getBlockState(tile.getPos()).has(BlockStateProperties.HORIZONTAL_FACING)) {
			Direction face = tile.getWorld().getBlockState(tile.getPos()).get(BlockStateProperties.HORIZONTAL_FACING);
			GlStateManager.rotated(face.getHorizontalAngle() - 180, 0, 1, 0);
		}
		if (this.isInverted(tile)) {
			GlStateManager.rotated(180, 0, 0, 1);
			GlStateManager.translated(0, 1.75, 0);
		}
		if(tile.getMatterState() != EnumMatterState.SOLID) {
			GlStateManager.enableAlphaTest();
			GlStateManager.enableBlend();
            GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.ONE_MINUS_SRC_ALPHA);
            GlStateManager.color4f(1, 1, 1, tile.alpha);
			this.renderExterior(tile);
			GlStateManager.disableAlphaTest();
			GlStateManager.disableBlend();
		}
		else{
			this.renderExterior(tile);
			this.renderCustomName(tile);	
		}
		
		
		GlStateManager.popMatrix();
	}
	
	public abstract void renderExterior(T tile);
	
	public void renderCustomName(T tile) {
		if(tile.getCustomName().isEmpty())
			return;
		if (tile.getCustomName().contains(Items.NAME_TAG.getName().getFormattedText())) //Prevent Tardises from being named when you haven't renamed the nametag
			return;
		GlStateManager.pushMatrix();
		GlStateManager.rotated(180, 0, 0, 1);
		if (this.isInverted(tile)) {
			GlStateManager.rotated(180, 0, 0, -1);
		}
		double maxDistance = 64;
		EntityRendererManager manager = Minecraft.getInstance().getRenderManager();
		Vec3d proj = Minecraft.getInstance().getRenderManager().info.getProjectedView();
		double d0 = tile.getDistanceSq(proj.x, proj.y, proj.z);
        if (!(d0 > (maxDistance * maxDistance))) {
	         float f = manager.playerViewY;
	         float f1 = manager.playerViewX;
	         GameRenderer.drawNameplate(manager.getFontRenderer(), tile.getCustomName(), 0, this.isInverted(tile) ? 2.25F : 4, 0, 0, f, f1, false);
		 }
		 GlStateManager.popMatrix();
	}
	
	public boolean isInverted(T tile) {
		if (tile.getCustomName().contains("Dinnerbone") || tile.getCustomName().contains("Grumm") || tile.getCustomName().contains("boti")) {
			return true;
		}
		return false;
	}

}
