package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.NemoConsoleModel;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

public class NemoConsoleRenderer extends TileEntityRenderer<NemoConsoleTile> {

	private static final NemoConsoleModel MODEL = new NemoConsoleModel();
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/nemo.png");
	
	@Override
	public void render(NemoConsoleTile console, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0.35, z + 0.5);
		if(console.getVariant() != null) {
			this.bindTexture(console.getVariant().getTexture());
		}
		else this.bindTexture(TEXTURE);
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.25, 0.25, 0.25);
		GlStateManager.rotatef(180, 0, 0, 1);
		MODEL.render(console);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}

}
