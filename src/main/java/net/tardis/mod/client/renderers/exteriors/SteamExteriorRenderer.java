package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.SteamExteriorModel;
import net.tardis.mod.tileentities.exteriors.SteampunkExteriorTile;

public class SteamExteriorRenderer extends ExteriorRenderer<SteampunkExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/steampunk.png");
	private SteamExteriorModel model = new SteamExteriorModel();

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}

	@Override
	public void renderExterior(SteampunkExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 0.15625, 0);
		GlStateManager.scalef(0.25f, 0.25f, 0.25f);
		if(tile.getVariant() != null)
			this.bindTexture(tile.getVariant().getTexture());
		else this.bindTexture(TEXTURE);
		
		this.model.render(tile);
		GlStateManager.popMatrix();
	}

}
