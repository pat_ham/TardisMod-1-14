package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.ClockExteriorModel;
import net.tardis.mod.client.renderers.boti.BotiManager;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ClockExteriorRenderer extends TileEntityRenderer<ExteriorTile> {

	public static ClockExteriorModel model = new ClockExteriorModel();
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/clock.png");
	
	public boolean hasWritten = false;
	
	BotiManager boti = new BotiManager();
	
	@Override
	public void render(ExteriorTile console, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();

		GlStateManager.translated(x + 0.5, y - 0.625, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.scaled(0.25, 0.25, 0.25);
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		model.render(console);
		
		if(console.getBotiWorld() == null) {
			GlStateManager.popMatrix();
			return;
		}
		
		boti.renderPortal(() -> {
			GlStateManager.pushMatrix();
			BlockPos offset = console.getBotiWorld().getOffset();
			
			GlStateManager.translated(x - offset.getX(), y - offset.getY(), z - offset.getZ());
			GlStateManager.scaled(4, 4, 4);
			boti.renderWorld(console.getBotiWorld());
			GlStateManager.popMatrix();
		},
				
		() -> {
			GlStateManager.pushMatrix();
			GlStateManager.translated(0, -4, 0);
			GlStateManager.rotated(180, 0, 0, 1);
			GlStateManager.scaled(4, 4, 4);
			model.renderBoti(0.0625F);
			GlStateManager.popMatrix();
		});
		GlStateManager.popMatrix();
	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}

}
