package net.tardis.mod.client.renderers.layers;


import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.IHasArm;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.HandSide;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.TItems;


public class VortexMRenderLayer extends LayerRenderer<AbstractClientPlayerEntity,PlayerModel<AbstractClientPlayerEntity>>{
	
	private final IEntityRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> renderPlayer;
	private ItemStack stack = new ItemStack(TItems.VORTEX_MANIP);
	
	public VortexMRenderLayer(
			IEntityRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> entityRendererIn) {
		super(entityRendererIn);
		this.renderPlayer = entityRendererIn;
	}

	@OnlyIn(Dist.CLIENT)
	public static boolean hasSmallArms(AbstractClientPlayerEntity player) {
		return player.getSkinType().equals("slim");
	}

	@Override
	public boolean shouldCombineTextures() {
		return false;
	}

	@Override
	public void render(AbstractClientPlayerEntity entityIn, float limbSwing, float limbSwingAmount, float partialTicks,float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		if (entityIn.inventory.hasItemStack(stack) && !PlayerHelper.isInEitherHand(entityIn, stack.getItem())) {
			boolean flag = entityIn.getPrimaryHand() == HandSide.RIGHT;
			this.renderVMOnOppositeArm(entityIn, stack, limbSwing , limbSwingAmount, partialTicks ,flag ? TransformType.THIRD_PERSON_LEFT_HAND : TransformType.THIRD_PERSON_RIGHT_HAND, entityIn.getPrimaryHand());
		}
	}
	
	private void renderVMOnOppositeArm(AbstractClientPlayerEntity entityIn, ItemStack stack, float limbSwing, float limbSwingAmount, float partialTicks ,ItemCameraTransforms.TransformType type, HandSide side) {
		boolean slim = hasSmallArms(entityIn);
		boolean holdingItem = !entityIn.getHeldItemOffhand().isEmpty();
		GlStateManager.pushMatrix();
		GlStateManager.scaled(slim ? 0.825 : 1.1, 1, 1); //Squeeze sides to make it fit alex arm if the player's skin is alex
		
		this.translateToHand(side);
		GlStateManager.translated(side == HandSide.LEFT ? (slim ? 0 : 0.125) : (slim ? 0 : -0.125), slim ? -0.025 : 0,0);
		GlStateManager.translated(0, holdingItem ? 0.1 : 0,0);
		if (entityIn.isSneaking()) {
			GlStateManager.rotated(5, 1, 0, 0); //Rotate the model backwards
			//Subtract y value to shift into arm more
			//Subtract x and z to move the model down to the left, then back to the right to align it on the arm
			GlStateManager.translated(0, 0, 0.275); 
		}
		if (entityIn.isVisuallySwimming()) {
			GlStateManager.rotated(90, -1, 1, 0.5);
			GlStateManager.rotated(limbSwing, -1, 1, 1);
//			GlStateManager.translated(-0.2, -0.125, 0.35);
//			GlStateManager.translated(-0.2, -0.125, 0.35);
//			GlStateManager.translated(limbSwingAmount / 16, -limbSwingAmount / 16, -limbSwingAmount / 16);
//			GlStateManager.translated(-0.35, 0.5, 0.25);
		}
		
		//Render the item json model
		//TODO: Find out how to do this in 1.15+
		Minecraft.getInstance().getItemRenderer().renderItem(stack, type);
		
		GlStateManager.popMatrix();
	}
	
	/**
	 * Binds model to arm swinging
	 * @implNote Only use method BETWEEN GlStataManager push and pop methods
	 * @param side
	 */
	private void translateToHand(HandSide side) {
		HandSide oppSide = HandSide.LEFT;
		switch (side) {
		case RIGHT:
			oppSide = HandSide.LEFT;
			GlStateManager.translated(0.175, 0.45, 0); //Shift model to display on left hand if main arm is right
			GlStateManager.rotated(-90, 1, 0, 0); //Rotate the model 90 degrees so it isn't flipped
			GlStateManager.rotated(3, 0, 1, 0); //Rotate the model further to correct it being a little too straight
			break;
		case LEFT:
			oppSide = HandSide.RIGHT;
			GlStateManager.translated(-0.175, 0.5, 0); //Shift model to display on right arm if main arm is left
			GlStateManager.rotated(-90, 1, 0, 0);//Rotate the model 90 degrees so it isn't flipped
			GlStateManager.rotated(-3, 0, 1, 0); //Rotate the model further to correct it being a little too straight
			break;
		default:
			oppSide = HandSide.LEFT;
			GlStateManager.translated(-0.4, 0.5, -0.125); //Shift model to display on right arm if main arm is left
			GlStateManager.rotated(-90, 1, 0, 0);
			GlStateManager.rotated(-2.5, 0, 1, 0);
			break;
		}
	     ((IHasArm)this.getEntityModel()).postRenderArm(0.0625F, oppSide);
	   }

}
