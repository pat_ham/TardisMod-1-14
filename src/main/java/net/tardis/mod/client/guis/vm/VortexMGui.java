package net.tardis.mod.client.guis.vm;


import org.lwjgl.glfw.GLFW;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.vm.widgets.DisabledTextFieldWidget;
import net.tardis.mod.client.guis.vm.widgets.VortexMButtonCatCycleDown;
import net.tardis.mod.client.guis.vm.widgets.VortexMButtonCatCycleUp;
import net.tardis.mod.client.guis.vm.widgets.VortexMButtonClose;
import net.tardis.mod.client.guis.vm.widgets.VortexMButtonFnCycleLeft;
import net.tardis.mod.client.guis.vm.widgets.VortexMButtonFnCycleRight;
import net.tardis.mod.client.guis.vm.widgets.VortexMButtonSelect;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.misc.vm.ParentFunction;
import net.tardis.mod.misc.vm.IVortexMFunction;
import net.tardis.mod.misc.vm.VortexMFunctions;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.VMFunctionMessage;

/**
 * Created by 50ap5ud5 12/09/2019
 * Standalone Vortex Manipulator Gui
 * **/

public class VortexMGui extends Screen implements IVortexMScreen{
	
	public static TranslationTextComponent title = new TranslationTextComponent("gui.vm.base");
	
	private static final ResourceLocation BACKGROUND = new ResourceLocation(Tardis.MODID, "textures/gui/vm_ui.png");
	
	private TextFieldWidget currentFunction;
	private Button select;
	private Button fnCycleLeft;
	private Button fnCycleRight;
	private Button catCycleUp;
	private Button catCycleDown;
	private Button closeBtn;
	
	private int id = 0;
	private int subIndex = 0;
	private boolean isSubFunction;
	
	public VortexMGui() {
		this(title);
		VortexMFunctions.init();
	}
	
	public VortexMGui(ITextComponent title) {
		super(title);
	}
	
	@Override
	public void renderScreen(){
		this.renderBackground();
	}
	
	@Override
	public int getMinY() {
		return this.height / 2 + 74;
	}

	@Override
	public int getMinX() {
		return this.width / 2 - 68;
	}

	@Override
	public int getMaxX() {
		return this.getMinX() + 138;
	}

	@Override
	public int getMaxY() {
		return this.getMinY() - 146;
	}
	
	@Override
	public void renderBackground() {
		Minecraft.getInstance().getTextureManager().bindTexture(BACKGROUND);
		int texWidth = 138, texHeight = 146;
		this.blit(this.width / 2 - texWidth / 2, this.height / 2 - texHeight / 2, 0, 0, texWidth, texHeight);
        this.drawCenteredString(this.font, "Selected Function:", this.getMinX() + 70, this.getMinY() - 37, 0xFFFFFF);
	}
	
	@Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground();
        super.render(mouseX, mouseY, partialTicks);
       
    }
	
	@Override
	public void init() {
		super.init();
		final int btnSelectW = 14, btnSelectH = 14;
		final int btnFnCycleW = 4, btnFnCycleH = 12;
		
		currentFunction = new DisabledTextFieldWidget(this.font, this.getMinX() + 25, this.getMinY() - 25, 90, this.font.FONT_HEIGHT + 2, VortexMFunctions.getFunction(id).getNameKey());
		fnCycleLeft = new VortexMButtonFnCycleLeft(this.getMinX() + 22, this.getMaxY() + 63, btnFnCycleW, btnFnCycleH, "", new VortexMButtonFnCycleLeft.IPressable() {
			@Override
			public void onPress(Button button) {
				decrementFunction();
			}
		});
		fnCycleRight = new VortexMButtonFnCycleRight(this.getMinX() + 38, this.getMaxY() + 63, btnFnCycleW, btnFnCycleH, "", new VortexMButtonFnCycleLeft.IPressable() {
			@Override
			public void onPress(Button button) {
				incrementFunction();
			}
		});
		catCycleUp = new VortexMButtonCatCycleUp(this.getMinX() + 26, this.getMaxY() + 59, btnFnCycleW, btnFnCycleH, "", new VortexMButtonCatCycleUp.IPressable() {
			@Override
			public void onPress(Button button) {
				incrementSubFunction();
			}
		});
		catCycleDown = new VortexMButtonCatCycleDown(this.getMinX() + 26, this.getMaxY() + 74, btnFnCycleW, btnFnCycleH, "", new VortexMButtonCatCycleDown.IPressable() {
			@Override
			public void onPress(Button button) {
				decrementSubFunction();
			}
		});
		select = new VortexMButtonSelect(this.getMinX() + 42, this.getMaxY() + 48, btnSelectW, btnSelectH, "", new VortexMButtonSelect.IPressable() {
			@Override
			public void onPress(Button button) {
				ParentFunction function = VortexMFunctions.getFunction(id);
				if (function.stateComplete().equals(false)) {
					currentFunction.setText(new StringTextComponent("Locked: WIP").applyTextStyle(TextFormatting.RED).getFormattedText());
				}
				if (function.getSubFunctionById(subIndex) != null && function.getSubFunctionById(subIndex).stateComplete().equals(false)) {
					currentFunction.setText(new StringTextComponent("Locked: WIP").applyTextStyle(TextFormatting.RED).getFormattedText());
				}
				else {
						if (isSubFunction) {
							VortexMFunctions.getFunction(id).getSubFunctionById(subIndex).onActivated(Minecraft.getInstance().world, Minecraft.getInstance().player);
							Network.INSTANCE.sendToServer(new VMFunctionMessage(id, subIndex, true, function.getSubFunctionById(subIndex).isServerSide()));
						}
						else {
							VortexMFunctions.getFunction(id).onActivated(Minecraft.getInstance().world, Minecraft.getInstance().player);
							Network.INSTANCE.sendToServer(new VMFunctionMessage(id, subIndex, false, function.isServerSide()));
						}

				}
			}
		});
		closeBtn = new VortexMButtonClose(this.getMinX() + 79, this.getMaxY() + 48, btnSelectW, btnSelectH, "", new VortexMButtonClose.IPressable() {
			@Override
			public void onPress(Button button) {
				onClose();
			}
		}); //TODO: Fix black rendering

		
		this.buttons.clear();
		this.addButton(select);
		this.addButton(fnCycleLeft);
		this.addButton(fnCycleRight);
		this.addButton(catCycleUp);
		this.addButton(catCycleDown);
		this.addButton(currentFunction);
		this.addButton(closeBtn);
		currentFunction.setEnabled(false);
		currentFunction.setText(new StringTextComponent(VortexMFunctions.getFunction(id).getNameKey()).applyTextStyle(TextFormatting.BLUE).getFormattedText()); //Set default function name on init
		currentFunction.setCursorPositionZero();
	}
		
	@Override
   public void onClose() {
        this.minecraft.displayGuiScreen(null);
        PlayerHelper.closeVMModel(this.minecraft.player); //set item model to close
   }
	
	
	@Override
	public boolean isPauseScreen() {
	   return false;
	}
		
	@Override
	public boolean keyPressed(int keyCode, int scanCode, int bitmaskModifier) {
		super.keyPressed(keyCode, scanCode, bitmaskModifier);
		final int rightKey = GLFW.GLFW_KEY_RIGHT;
		final int leftKey = GLFW.GLFW_KEY_LEFT;
		final int upKey = GLFW.GLFW_KEY_UP;
		final int downKey = GLFW.GLFW_KEY_DOWN;
		final int escKey = GLFW.GLFW_KEY_ESCAPE;
		final int enterKey = GLFW.GLFW_KEY_ENTER;
		
		switch(keyCode) {
		case rightKey: //Right arrow key
			incrementFunction();
			return true;
		case leftKey: //Left arrow key
			decrementFunction();
			return true;
		case upKey: //Up arrow key
			incrementSubFunction();
			return true;
		case downKey: //Down arrow key
			decrementSubFunction();
			return true;
		case escKey: //Close Button, for some reason this overrides ShouldCloseOnEsc()
			onClose();
			return true;
		case enterKey:
			select.onPress();
			return true;
		default:
			return false;
		}
	}
	
	public void decrementFunction() {
		--id;
		if (id < 0) {
            id  = VortexMFunctions.getFunctionList().size() - 1;	//Safety check, gets last map key value
        }
		if(VortexMFunctions.getFunctionList().containsKey(id)) {
			currentFunction.setText(new StringTextComponent(VortexMFunctions.getFunction(id).getNameKey()).applyTextStyle(TextFormatting.BLUE).getFormattedText());
			currentFunction.setCursorPositionZero();
			this.subIndex = 0;
			this.isSubFunction = false;
		}
	}
	
	public void incrementFunction() {
		if (id + 1 > VortexMFunctions.getFunctionList().size() - 1) {
            id = 0;
        } else {
            ++id;
        }
		if(VortexMFunctions.getFunctionList().containsKey(id)) {
          currentFunction.setText(new StringTextComponent(VortexMFunctions.getFunction(id).getNameKey()).applyTextStyle(TextFormatting.BLUE).getFormattedText());
          currentFunction.setCursorPositionZero();
          this.subIndex = 0;
  		  this.isSubFunction = false;
		}
		
	}
	
	public void incrementSubFunction() {
		ParentFunction function = VortexMFunctions.getFunction(id);
		if (subIndex + 1 > function.getSubFunctionList().size() - 1) {
			subIndex = 0;
		}
		else {
            ++subIndex;
        }
		if (function.getSubFunctionList().containsKey(subIndex)) {
			IVortexMFunction subFunction = VortexMFunctions.getFunction(id).getSubFunctionById(subIndex);
			currentFunction.setText(new StringTextComponent(subFunction.getNameKey()).applyTextStyle(TextFormatting.BLUE).getFormattedText());
	        currentFunction.setCursorPositionZero();
	        this.isSubFunction = true;
		}
	}
	
	public void decrementSubFunction() {
		ParentFunction function = VortexMFunctions.getFunction(id);
		--subIndex;
		if (subIndex < 0) {
			subIndex  = function.getSubFunctionList().size() - 1;	//Safety check, gets last map key value
			if (function.getSubFunctionList().size() == 0) {
				subIndex = 0;
			}
		}
		if(function.getSubFunctionList().containsKey(subIndex)) {
			IVortexMFunction subFunction = VortexMFunctions.getFunction(id).getSubFunctionById(subIndex);
			currentFunction.setText(new StringTextComponent(subFunction.getNameKey()).applyTextStyle(TextFormatting.BLUE).getFormattedText());
			currentFunction.setCursorPositionZero();
			this.isSubFunction = true;
		}
		
	}
}
