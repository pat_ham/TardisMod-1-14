package net.tardis.mod.client.guis.monitors;

import java.util.Map.Entry;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.Button.IPressable;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ProtocolMessage;
import net.tardis.mod.protocols.Protocol;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.tileentities.ConsoleTile;
/**
 * Basic template for making new Monitor GUIS
 * @implSpec Extend this to make new Monitor Sub guis
 * @implNote If you have a protocol that uses a Monitor gui, override public String getSubmenu() {} and return a name for the menu gui
 **/
public class MonitorScreen extends Screen{

	public static StringTextComponent TITLE = new StringTextComponent("Monitor");
	protected IMonitorGui parent;
	protected String menu = "main";
	private int id = 0;

    public static TranslationTextComponent backTranslation = new TranslationTextComponent("gui.tardis.previous");
    public static TranslationTextComponent selectTranslation = new TranslationTextComponent("gui.tardis.select");
    public static TranslationTextComponent nextTranslation = new TranslationTextComponent("gui.tardis.next");

	public MonitorScreen(ITextComponent titleIn) {
		super(titleIn);
	}
	
	public MonitorScreen() {
		this(TITLE);
	}
	
	public MonitorScreen(IMonitorGui monitor, String type) {
		this();
		this.parent = monitor;
		this.menu = type;
	}

	@Override
	protected void init() {
		super.init();



		id = 0;
		this.buttons.clear();
		
		for(Entry<ResourceLocation, Protocol> entry : TardisRegistries.PROTOCOL_REGISTRY.getRegistry().entrySet()) {
			if(entry.getValue().getSubmenu().equals(menu))
				this.addButton(this.addButton(this.parent.getMinX(), this.parent.getMinY(), entry.getKey()));
		}
		
		if(this.parent instanceof Screen)
			((Screen)this.parent).init(this.minecraft, this.width, this.height);
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		this.renderBackground();
		this.parent.renderMonitor();
		
		for(Widget b : this.buttons) {
			b.render(p_render_1_, p_render_2_, p_render_3_);
		}
	}
	
	public void addSubmenu(TextComponent com, String menu, int x, int y) {
		
		IPressable press = (button) -> {
			Minecraft.getInstance().displayGuiScreen(new MonitorScreen(parent, menu));
		};
		
		TextButton button = new TextButton(x, y - (id * (int)(Minecraft.getInstance().fontRenderer.FONT_HEIGHT * 1.25)),
				"> " + com.getFormattedText(), press);
		
		this.addButton(button);
		++id;
	}
	
	public void addSubmenu(TextComponent com, IPressable press) {
	
		TextButton button = new TextButton(this.parent.getMinX(), this.parent.getMinY() - (id * (int)(Minecraft.getInstance().fontRenderer.FONT_HEIGHT * 1.25)),
				"> " + com.getFormattedText(), press);
		
		this.addButton(button);
		++id;
	}
	
	public Button addButton(int x, int y, ResourceLocation key) {
		Protocol prot = TardisRegistries.PROTOCOL_REGISTRY.getValue(key);
		IPressable press = (button) -> {
			TileEntity te = Minecraft.getInstance().world.getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				Network.sendToServer(new ProtocolMessage(prot.getRegistryName()));
				prot.call(Minecraft.getInstance().world, (ConsoleTile)te);
			}
		};
		TextButton button = new TextButton(x, y - this.getUsedHeight() - (id * (int)(Minecraft.getInstance().fontRenderer.FONT_HEIGHT * 1.25)), "> " + (prot != null ? prot.getDisplayName() : "ERROR"), press);
		++id;
		return button;
	}
	
	public Button addButton(int x, int y, TextComponent trans, IPressable pres) {
		TextButton button = new TextButton(x, y - (id * (int)(Minecraft.getInstance().fontRenderer.FONT_HEIGHT * 1.25)), trans.getFormattedText(), pres);
		++id;
		return button;
	}
	
	//Override this if you need the protocols to be moved up from parent.getMinY()
	public int getUsedHeight() {
		return 0;
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}


}
