package net.tardis.mod.client.guis.vm;

import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.VMTeleportMessage;

/**
* @implSpec 
* Extend this class
* @implNote
* Override getMinY, getMinX, getMaxX, getMaxY
* Override renderScreen, then set your own texture dimensions and blit it
**/
public class VortexMTeleportGui extends VortexMFunctionScreen{
	
	public TextFieldWidget xCoord;
	public TextFieldWidget yCoord;
	public TextFieldWidget zCoord;
	private Button teleport;
	private Button back;
	private Button teleportSetting;
	private boolean preciseTeleport = false;
	private int settingId = 0;

	
	public VortexMTeleportGui(ITextComponent title) {
		super(title);
	}
	
	public VortexMTeleportGui() {
	}
	
	
	@Override
	public void init() {
		super.init();
		
		String teleportTypeTop = new TranslationTextComponent("setting.vm.teleport.top").getFormattedText();
		String teleportTypePrecise = new TranslationTextComponent("setting.vm.teleport.precise").getFormattedText();
		String teleportButton = new TranslationTextComponent("button.vm.teleport").getFormattedText();
		String backButton = new TranslationTextComponent("button.vm.back").getFormattedText();
		final int btnH = 20;
		
		xCoord = new TextFieldWidget(this.font, super.getMinX() + 45, super.getMaxY() + 75, 50, this.font.FONT_HEIGHT + 2, "");
		yCoord = new TextFieldWidget(this.font, super.getMinX() + 45, super.getMaxY() + 90, 50, this.font.FONT_HEIGHT + 2, "");
		zCoord = new TextFieldWidget(this.font, super.getMinX() + 45, super.getMaxY() + 105, 50, this.font.FONT_HEIGHT + 2, "");
		teleport = new Button(super.getMinX() + 44,super.getMaxY() + 121, this.font.getStringWidth(teleportButton) + 10,btnH, teleportButton, new Button.IPressable() {
			
			@Override
			public void onPress(Button button) {
					checkPosInput();
				}
		});
		back = new Button(super.getMinX(),super.getMaxY() + 35, this.font.getStringWidth(backButton) + 8, this.font.FONT_HEIGHT + 11, backButton, new Button.IPressable() {
			
			@Override
			public void onPress(Button button) {
				Tardis.proxy.openGUI(Constants.Gui.VORTEX_MAIN, null);
			}
		});
		teleportSetting = new Button(super.getMinX() + 170, super.getMaxY() + 50, 55, this.font.FONT_HEIGHT + 11, teleportTypeTop, new Button.IPressable() {
			
			@Override
			public void onPress(Button button) {
				incrementId();
				switch(settingId) {
				case 0:
					teleportSetting.setMessage(teleportTypeTop);
					setTeleportType(!preciseTeleport);
					break;
				case 1:
					teleportSetting.setMessage(teleportTypePrecise);
					setTeleportType(!preciseTeleport);
					break;
				}
					
			}
		});
		
		this.buttons.clear();
		this.addButton(teleport);
		this.addButton(xCoord); //addButton also adds other widget types, mapping name can be misleading
		this.addButton(yCoord);
		this.addButton(zCoord);
		this.addButton(back);
		this.addButton(teleportSetting);
		xCoord.setFocused2(true);
	}
	
	@Override
	public void renderBackground(int background) {
		super.renderBackground();
	}
	
	@Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        super.render(mouseX, mouseY, partialTicks);
        drawCenteredString(this.font, "Teleport Player", super.getMinX() + 73, super.getMaxY() + 40, 0xFFFFFF);
		drawCenteredString(this.font, "X:", super.getMinX() + 35, super.getMaxY() + 76, 0xFFFFFF);
		drawCenteredString(this.font, "Y:", super.getMinX() + 35, super.getMaxY() + 91, 0xFFFFFF);
		drawCenteredString(this.font, "Z:", super.getMinX() + 35, super.getMaxY() + 106, 0xFFFFFF);
		drawCenteredString(this.font, "Teleport Type:", super.getMinX() + 198, super.getMaxY() + 35, 0xFFFFFF);
		if (this.preciseTeleport) {
			drawCenteredString(this.font,"Warning!", super.getMinX() + 198, super.getMaxY() + 75, 0xffcc00);
			drawCenteredString(this.font,"This teleport", super.getMinX() + 198, super.getMaxY() + 85, 0xFFFFFF);
			drawCenteredString(this.font,"setting can be", super.getMinX() + 196, super.getMaxY() + 95, 0xFFFFFF);
			drawCenteredString(this.font,"dangerous!", super.getMinX() + 198, super.getMaxY() + 105, 0xFFFFFF);
		}
		displayTextBoxCoords(this.xCoord,COORD_TYPE.X);
		displayTextBoxCoords(this.yCoord,COORD_TYPE.Y);
		displayTextBoxCoords(this.zCoord,COORD_TYPE.Z);
    }
	
	@Override
	public boolean shouldCloseOnEsc() {
		return true;
	}
		
	@Override
	public void onClose() {
        super.onClose();
	}
	
	@Override
	public boolean isPauseScreen() {
	   return true;
	}
	
	public enum COORD_TYPE {
			X, Y, Z
	}
	
	private void checkPosInput() {
		boolean isValid = isStringNumeric(xCoord.getText(),COORD_TYPE.X) &&  isStringNumeric(yCoord.getText(),COORD_TYPE.Y) && isStringNumeric(zCoord.getText(),COORD_TYPE.Z);
		boolean withinRange = isWithinConfigRange(xCoord.getText(),COORD_TYPE.X) &&  isWithinConfigRange(yCoord.getText(),COORD_TYPE.Y) && isWithinConfigRange(zCoord.getText(),COORD_TYPE.Z);
		if (isValid && withinRange) {
			BlockPos tpPos = new BlockPos(getInt(xCoord.getText(),COORD_TYPE.X), getInt(yCoord.getText(), COORD_TYPE.Y), getInt(zCoord.getText(), COORD_TYPE.Z));
			Network.INSTANCE.sendToServer(new VMTeleportMessage(tpPos, this.minecraft.player.getEntityId(), preciseTeleport));
		}
	}
	
	private boolean isStringNumeric(String input, COORD_TYPE type) {
		if (input != null && !input.isEmpty()) {
			try {
				Integer.parseInt(input);
				return true;
			}
			catch(NumberFormatException nfe){
				this.minecraft.player.sendStatusMessage(new TranslationTextComponent("message.vm.invalidInput",type.toString()), false);
				return false;
			}
		}
		else if (input.isEmpty()) {
			return true;
		}
		return false;
	}
	
	private int getInt(String num, COORD_TYPE type) {
		if (isStringNumeric(num, type)) {
			if (num.isEmpty()) { //Make it so that if a textbox is empty, it will use player current coords
				return getDefaultCoordInt(type);
			}
			else {
				return Integer.parseInt(num);
			}
		}
		return getDefaultCoordInt(type);
	}
	
	private void displayTextBoxCoords(TextFieldWidget widget,COORD_TYPE type) {
		if (widget.isFocused()) {
			widget.setSuggestion(""); 
		}
		else if (widget.getText().isEmpty()){
			widget.setSuggestion(Integer.toString(this.getDefaultCoordInt(type)));
		}
	}

	private int getDefaultCoordInt(COORD_TYPE type) {
		switch (type) { //Teleports user to current position if they try to parse in non-integers
		case X:
			return (int) this.minecraft.player.posX;
		case Y:
			return (int) this.minecraft.player.posY;
		case Z:
			return (int) this.minecraft.player.posZ;
		default:
			return 0;
		}
	}
	
	public boolean getTeleportType() {
		return preciseTeleport;
	}
	
	public void setTeleportType(boolean type) {
		this.preciseTeleport = type;
	}
	
	public boolean isWithinConfigRange(String num, COORD_TYPE type) {
		if ((getInt(num, type) - getDefaultCoordInt(type)) <= TConfig.CONFIG.vmTeleportRange.get()) {
			return true;
		}
		else {
			this.minecraft.player.sendStatusMessage(new TranslationTextComponent("message.vm.illegalPos",TConfig.CONFIG.vmTeleportRange.get(),type.toString()), false);
			return false;
		}
	}
	
	public void incrementId() {
		if (settingId + 1 > 1) {
			settingId = 0;
        } else {
            ++settingId;
        }
	}
}
