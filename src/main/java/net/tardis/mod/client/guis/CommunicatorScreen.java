package net.tardis.mod.client.guis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.CommunicatorMessage;

public class CommunicatorScreen extends Screen{

	private static StringTextComponent TITLE = new StringTextComponent("");
	
	private SpaceTimeCoord pos = SpaceTimeCoord.UNIVERAL_CENTER;
	
	public CommunicatorScreen() {
		super(TITLE);
	}

	@Override
	protected void init() {
		super.init();
		
		this.addButton(new TextButton(width / 2 - 120, height / 2 + 90, "> Load into Nav-Com", but -> {
			Network.sendToServer(new CommunicatorMessage(true));
			Minecraft.getInstance().displayGuiScreen(null);
		}));
		this.addButton(new TextButton(width / 2 + 60, height / 2 + 90, "> Ignore", but -> {
			Network.sendToServer(new CommunicatorMessage(false));
			Minecraft.getInstance().displayGuiScreen(null);
		}));
		
		TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(tile -> {
			if(!tile.getDistressSignals().isEmpty())
				pos = tile.getDistressSignals().get(0);
		});
	}
	
	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		this.renderBackground();
		this.drawCenteredString(this.font, "Distress signal detected!", width / 2, height / 2 - 90, 0xFFFFFF);
		
		this.drawCenteredString(this.font, "Located at " + Helper.formatBlockPos(pos.getPos()) + " in " + Helper.formatDimName(DimensionType.byName(pos.getDimType())), width / 2, height / 2, 0xFFFFFF);
		
		super.render(p_render_1_, p_render_2_, p_render_3_);
	}

}
