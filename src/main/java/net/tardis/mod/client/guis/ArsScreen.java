package net.tardis.mod.client.guis;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.ChangePageButton;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.ItemButton;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ARSSpawnMessage;
import net.tardis.mod.tags.TardisBlockTags;

public class ArsScreen extends Screen {

	public static final StringTextComponent TITLE = new StringTextComponent("ARS");
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/ars.png");
	
	public static int WIDTH = 256, HEIGHT = 256, SLOTS = 51;
	public int page = 0;
	
	public ArsScreen() {
		super(TITLE);
	}

	@Override
	protected void init() {
		super.init();
		
		this.reloadButtons();
	}
	
	public void reloadButtons() {
		
		//Shouldn't exist, but you can only get items from the front page other wise
		for(Widget w : this.buttons) {
			w.active = false;
		}
		buttons.clear();
		
		int centerX = width / 2 - 8, centerY = height / 2 - 8;
		
		List<Item> items = new ArrayList<Item>();
		for(Block block : TardisBlockTags.ARS.getAllElements()) {
			items.add(block.asItem());
		}
		
		int maxPages = (int)Math.ceil(items.size() / SLOTS);
		if(items.size() > page * SLOTS) {
			items = items.subList(page * SLOTS, items.size());
		}
		
		List<Item> leftOver = this.addButtonToCircle(items, centerX, centerY, 90, 25);
		leftOver = this.addButtonToCircle(leftOver, centerX, centerY, 70, 16);
		leftOver = this.addButtonToCircle(leftOver, centerX, centerY, 50, 10);
		
		this.addButton(new ChangePageButton(width / 2, height / 2, true, button -> {
			page = MathHelper.clamp(page + 1, 0, maxPages);
			this.reloadButtons();
		}, false));
		
		this.addButton(new ChangePageButton(width / 2 - 20, height / 2, false, button -> {
			page = MathHelper.clamp(page - 1, 0, maxPages);
			this.reloadButtons();
		}, false));
		
	}
	
	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		this.renderBackground();
		this.minecraft.textureManager.bindTexture(TEXTURE);
		this.blit(width / 2 - WIDTH / 2, height / 2 - HEIGHT / 2, 0, 0, WIDTH, HEIGHT);

		RenderHelper.enableGUIStandardItemLighting();
		Iterator<Widget> buttons = this.buttons.iterator();
		while(buttons.hasNext()) {
			buttons.next().render(mouseX, mouseY, p_render_3_);
		}
		RenderHelper.enableStandardItemLighting();
		
		Iterator<Widget> tooltip = this.buttons.iterator();
		while(tooltip.hasNext()) {
			Widget butt = tooltip.next();
			if(butt instanceof ItemButton && Helper.isInBounds(mouseX, mouseY, butt.x, butt.y, butt.x + butt.getWidth(), butt.y + butt.getHeight()))
				this.renderTooltip(((ItemButton)butt).getItemStack(), mouseX, mouseY);
		}
		
	}
	
	public List<Item> addButtonToCircle(List<Item> list, int startX, int startY, int radius, int max) {
		int index = 0;
		
		if(max == 0)
			return list;
		
		if(max > list.size())
			max = list.size();
		
		Iterator<Item> it = list.iterator();
		while(it.hasNext()) {
			double angle = Math.toRadians((360.0 / max) * index);
			int x = (int)(Math.sin(angle) * radius), y = (int)(Math.cos(angle) * radius);
			Item item = it.next();
			this.addButton(new ItemButton(startX + x, startY + y, new ItemStack(item), button -> {
				Network.sendToServer(new ARSSpawnMessage(item));
			}));
			it.remove();
			
			++index;
			if(index >= max)
				return list;
		}
		return list;
	}

}
