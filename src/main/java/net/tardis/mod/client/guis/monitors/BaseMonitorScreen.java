package net.tardis.mod.client.guis.monitors;

import java.util.Map.Entry;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.Button.IPressable;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ProtocolMessage;
import net.tardis.mod.protocols.Protocol;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.tileentities.ConsoleTile;
/**
 * The template for new Monitor Main Menus with their own background textures
 * E.g. Steampunk Monitor GUI 
 * E.g.2 Wall Eye Monitor GUI
 * @implSpec Extend this class when you want to make a Main Monitor Menu with your own texture
 * @implNote If you want to make a sub menu, extend MonitorScreen
 **/
public abstract class BaseMonitorScreen extends Screen implements IMonitorGui{

    public static TranslationTextComponent title = new TranslationTextComponent("gui.tardis.monitor." + Tardis.MODID + ".steam");
    private int id;

	public BaseMonitorScreen(ITextComponent titleIn) {
		super(titleIn);
	}

	public BaseMonitorScreen() {
		super(title);
	}

	@Override
	protected void init() {
		super.init();

		id = 0;
		this.buttons.clear();

        for(Entry<ResourceLocation, Protocol> entry : TardisRegistries.PROTOCOL_REGISTRY.getRegistry().entrySet()) {
			if(entry.getValue().getSubmenu().equals("main"))
				this.addButton(this.addButton(this.getMinX(), this.getMinY(), entry.getKey()));
		}
        this.addSubmenu(new TranslationTextComponent("gui.tardis.exterior_properties"), but -> Minecraft.getInstance().displayGuiScreen(new ExteriorPropMonitorScreen(this)));
        this.addSubmenu(new TranslationTextComponent("gui.tardis.interior_properties"),
				(button) -> Minecraft.getInstance().displayGuiScreen(new InteriorEditScreen(this, "interior")));
        this.addSubmenu(new TranslationTextComponent("gui.tardis.security"), "security");
        this.addSubmenu(new TranslationTextComponent("gui.tardis.waypoints"), but -> Minecraft.getInstance().displayGuiScreen(new WaypointMonitorScreen(this, "waypoints")));
        this.addSubmenu(new TranslationTextComponent("gui.tardis.exterior"), but -> Minecraft.getInstance().displayGuiScreen(new ExteriorScreen(this, "exterior")));
        this.addSubmenu(new TranslationTextComponent("gui.tardis.console"),
				(button) -> Minecraft.getInstance().displayGuiScreen(new ConsoleSelectionScreen(this, "console")));
        this.addSubmenu(new TranslationTextComponent("gui.tardis.interior"),
				(button) -> Minecraft.getInstance().displayGuiScreen(new InteriorChangeScreen(this, "change_interior")));
        this.addSubmenu(new TranslationTextComponent("gui.tardis.info"),
				(button) -> Minecraft.getInstance().displayGuiScreen(new InfoDisplayScreen(this, "info_display")));
        
	}

	public void addSubmenu(TextComponent com, IPressable press) {
		
		TextButton button = new TextButton(this.getMinX(), this.getMinY() - (id * (int)(Minecraft.getInstance().fontRenderer.FONT_HEIGHT * 1.25)),
				"> " + com.getFormattedText(), press);
		
		this.addButton(button);
		++id;
	}
	
	public void addSubmenu(TextComponent com, String menu) {
		IPressable press = (button) -> {
			Minecraft.getInstance().displayGuiScreen(new MonitorScreen(this, menu));
		};
		this.addSubmenu(com, press);
	}
	
	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		
		this.renderMonitor();
		
		super.render(mouseX, mouseY, p_render_3_);
	}
	
	@Override
	public boolean shouldCloseOnEsc() {
		return true;
	}
		
	@Override
	public void onClose() {
		this.minecraft.displayGuiScreen(null);
	}
	
	public Button addButton(int x, int y, ResourceLocation key) {
		Protocol prot = TardisRegistries.PROTOCOL_REGISTRY.getValue(key);
		IPressable press = (button) -> {
			TileEntity te = Minecraft.getInstance().world.getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				Network.sendToServer(new ProtocolMessage(prot.getRegistryName()));
				prot.call(Minecraft.getInstance().world, (ConsoleTile)te);
			}
		};
		TextButton button = new TextButton(x, y - (id * (int)(Minecraft.getInstance().fontRenderer.FONT_HEIGHT * 1.25)), "> " + (prot != null ? prot.getDisplayName() : "ERROR"), press);
		++id;
		return button;
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}
	
	

}
