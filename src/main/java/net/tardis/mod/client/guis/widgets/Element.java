package net.tardis.mod.client.guis.widgets;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.client.Minecraft;

public class Element {
		
		public String type;
		public Map<String, String> mod = new HashMap<>();
		
		public static Element read(String line) {
			try {
				Element ele = new Element();
				String parsed = line.substring(line.indexOf('<') + 1, line.indexOf('>'));
				String[] div = parsed.split(" ");
				ele.type = div[0];
				for (String string : div) {
					if (string.contains("=")) {
						String[] split = string.split("=");
						ele.mod.put(split[0].replaceAll("\"", "").trim(), split[1].replaceAll("\"", ""));
					}
				}
				String rest = line.replaceAll(line.substring(line.indexOf("<"), line.indexOf(">")), "").replaceAll("<", "").replaceAll(">", "").trim();
				ele.mod.put("text", rest);
				return ele;
			} catch (Exception e) {
				return null;
			}
		}
		
		public int getHeight() {
			try {
				return mod.containsKey("height") ? Integer.parseInt(mod.get("height")) : Minecraft.getInstance().fontRenderer.FONT_HEIGHT;
			} catch (Exception e) {
			}
			return 0;
		}
}
