package net.tardis.mod.client.guis.monitors;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.IntSliderWidget;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleVariantMessage;
import net.tardis.mod.tileentities.ConsoleTile;

public class InteriorEditScreen extends MonitorScreen {
	
	public IntSliderWidget lightSlider;
	private TextButton texVarButton;
	private ConsoleTile tile;
	private int texIndex = 0;
	private TexVariant selected;
	
	public InteriorEditScreen(IMonitorGui gui, String submenu) {
		super(gui, submenu);
		
		tile = TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).orElse(null);
		if(tile != null) {
			int index = 0;
			for(TexVariant v : tile.getVariants()) {
				if(v == tile.getVariant())
					break;
				++index;
			}
			this.texIndex = index;
		}
	}

	@Override
	public void init(Minecraft p_init_1_, int p_init_2_, int p_init_3_) {
		super.init(p_init_1_, p_init_2_, p_init_3_);
		if(parent instanceof Screen) {
			((Screen)parent).init(p_init_1_, p_init_2_, p_init_3_);
		}
	}

	@Override
	protected void init() {
		super.init();
		//this.buttons.clear();
		double sliderVal = 0.1;
		TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
		if(te instanceof ConsoleTile) {
			sliderVal = ((ConsoleTile)te).getInteriorManager().getLight() / 15.0;
		}
		this.addButton(lightSlider = new IntSliderWidget(this.parent.getMinX(), this.parent.getMinY() - 10, 100, 20, sliderVal));
		
		this.addButton(new TextButton(this.parent.getMinX(), this.parent.getMinY() - 30, "> " + new TranslationTextComponent("gui." + Tardis.MODID + ".interior.hum")
				.getFormattedText(), but -> Minecraft.getInstance().displayGuiScreen(new InteriorHumsScreen(this.parent, "interior_hum"))));
		
		if(this.tile.getVariants().length > 0) {
			
			this.modTexture(0);
			
			
			this.addButton(texVarButton = new TextButton(this.parent.getMinX(), this.parent.getMinY() - 40,
					"> Console Variant: " + this.selected.getTranlation().getFormattedText(),
					but -> modTexture(1)));
		}
		
	}

    private TranslationTextComponent lightLevelText = new TranslationTextComponent("gui.tardis.interior_light_level");

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		super.render(p_render_1_, p_render_2_, p_render_3_);
		for(Widget w : this.buttons)
			w.renderButton(p_render_1_, p_render_2_, p_render_3_);

        this.drawString(this.minecraft.fontRenderer, lightLevelText.getUnformattedComponentText(),
				this.parent.getMinX(), this.parent.getMinY() - 20, 0xFFFFFF);
	}

	@Override
	public int getUsedHeight() {
		return 60;
	}
	
	public void modTexture(int i) {
		if(this.texIndex + i >= this.tile.getVariants().length)
			this.texIndex = 0;
		else if(this.texIndex + i < 0)
			this.texIndex = tile.getVariants().length - 1;
		else this.texIndex += i;
		
		this.selected = tile.getVariants()[this.texIndex];
		if(this.texVarButton != null)
			this.texVarButton.setMessage("> Console Variant: " + this.selected.getTranlation().getFormattedText());
		
		if(1 != 0)
			Network.sendToServer(new ConsoleVariantMessage(this.texIndex));
	}

}
