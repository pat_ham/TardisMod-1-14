package net.tardis.mod.client.guis.monitors;

import net.minecraft.client.Minecraft;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleChangeMessage;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.registries.consoles.Console;

public class ConsoleSelectionScreen extends MonitorScreen{
	
	private Console console;
	private int index = 0;
	
	public ConsoleSelectionScreen(IMonitorGui gui, String name) {
		super(gui, name);
	}
	
	@Override
	protected void init() {
		super.init();
		
		this.addSubmenu(new StringTextComponent("Previous"), (button) -> modIndex(-1));
		this.addSubmenu(new StringTextComponent("Select"), (button) -> {
			Network.sendToServer(new ConsoleChangeMessage(console.getRegistryName()));
			this.minecraft.displayGuiScreen(null);
		});
		this.addSubmenu(new StringTextComponent("Next"), (button) -> modIndex(1));
		
		TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(tile -> {
			int temp = 0;
			for(Console console : TardisRegistries.CONSOLE_REGISTRY.getRegistry().values()) {
				if(console.getState().getBlock() == Minecraft.getInstance().world.getBlockState(tile.getPos()).getBlock()) {
					index = temp;
					this.console = console;
//					System.out.println("console " + console);
					break;
				}
				++temp;
			}
		});
		
		this.modIndex(0);
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		super.render(p_render_1_, p_render_2_, p_render_3_);
		
		if(console != null) {
			Minecraft.getInstance().getTextureManager().bindTexture(console.getTexture());
			blit(
					((this.parent.getMaxX() - this.parent.getMinX()) / 2) + this.parent.getMinX() - 50,
					this.parent.getMaxY(), 0, 0, 100, 100, 100, 100);
		}
	}
	
	public void modIndex(int mod) {
		if(index + mod >= TardisRegistries.CONSOLE_REGISTRY.getRegistry().size())
			index = 0;
		else if(index + mod < 0)
			index = TardisRegistries.CONSOLE_REGISTRY.getRegistry().size() - 1;
		else index += mod;
		
		int temp = 0;
		for(Console console : TardisRegistries.CONSOLE_REGISTRY.getRegistry().values()) {
			if(temp == index) {
				this.console = console;
				return;
			}
			++temp;
		}
	}

}
