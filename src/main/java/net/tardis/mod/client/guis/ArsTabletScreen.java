package net.tardis.mod.client.guis;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateARSTablet;
import net.tardis.mod.registries.TardisRegistries;

public class ArsTabletScreen extends Screen {
	
	private static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/ars_tablet.png");
	private static final int MAX_PER_PAGE = 6;
	
	private int index = 0;
	
	public ArsTabletScreen() {
		super(new StringTextComponent(""));
	}
	
	public ArsTabletScreen(GuiContext cont) {
		this();
	}

	@Override
	protected void init() {
		super.init();
		
		for(Widget w : this.buttons) {
			w.active = false;
		}
		this.buttons.clear();
		
		int x = width / 2 - 100, y = height / 2 + 50;
		
		this.addButtons(x, y - 20);
		
		int size = TardisRegistries.ARS_PIECES.getRegistry().values().size() / MAX_PER_PAGE;
		
		this.addButton(new TextButton(x, height / 2 + 45, "> Next", but -> modIndex(1, size)));
		this.addButton(new TextButton(x, height / 2 + 55, "> Prev", but -> modIndex(-1, size)));
	}
	
	public void addButtons(int x, int y) {
		
		ArrayList<ARSPiece> all = Lists.newArrayList(TardisRegistries.ARS_PIECES.getRegistry().values());
		all.sort((one, two) -> {
			return one.getTranslation().getUnformattedComponentText()
					.compareToIgnoreCase(two.getTranslation().getUnformattedComponentText());
		});
		
		int startIndex = index * MAX_PER_PAGE;
		
		if(startIndex < all.size()) {
			
			int size = startIndex + MAX_PER_PAGE;
			size = size >= all.size() ? all.size() : size;
			
			List<ARSPiece> page = all.subList(startIndex, size);
			
			int id = 0;
			
			for(ARSPiece p : page) {
				this.addButton(new TextButton(x, y - (id * this.font.FONT_HEIGHT) + 5, p.getTranslation().getFormattedText(), but -> { 
					Network.INSTANCE.sendToServer(new UpdateARSTablet(p.getRegistryName()));
					Minecraft.getInstance().displayGuiScreen(null);
				}));
				++id;
			}
		}
		else index = 0;
		
	}
	
	public void modIndex(int mod, int size) {
		if(this.index + mod > size)
			this.index = 0;
		else if(this.index + mod < 0)
			this.index = size;
		else this.index += mod;
		
		this.init();
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		int width = 256, height = 173;
		this.blit(this.width / 2 - width / 2, this.height / 2 - height / 2, 0, 0, width, height);
		
		super.render(p_render_1_, p_render_2_, p_render_3_);
		this.drawCenteredString(this.font, new TranslationTextComponent("ars.message.gui_title").getFormattedText(), this.width / 2, this.height / 2 - (height / 2 - 30), 0xFFFFFF);
		this.drawCenteredString(this.font, new TranslationTextComponent("ars.message.gui_info").getFormattedText(), this.width / 2, this.height / 2 - (height / 2 - 50), 0xFFFFFF);
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}
	
	
	
	
}
