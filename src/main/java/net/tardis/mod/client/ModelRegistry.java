package net.tardis.mod.client;

import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.containers.AlembicScreen;
import net.tardis.mod.client.guis.containers.QuantiscopeSonicScreen;
import net.tardis.mod.client.guis.containers.QuantiscopeWeldScreen;
import net.tardis.mod.client.guis.containers.ReclamationScreen;
import net.tardis.mod.client.guis.containers.ShipComputerScreen;
import net.tardis.mod.client.guis.containers.VMContainerScreen;
import net.tardis.mod.client.models.entity.dalek.Dalek2005Model;
import net.tardis.mod.client.models.exteriors.TTCapsuleExteriorRenderer;
import net.tardis.mod.client.models.interiordoors.InteriorFortuneModel;
import net.tardis.mod.client.models.interiordoors.InteriorModernPoliceBoxModel;
import net.tardis.mod.client.models.interiordoors.InteriorPoliceBoxModel;
import net.tardis.mod.client.models.interiordoors.InteriorRedModel;
import net.tardis.mod.client.models.interiordoors.InteriorSteamModel;
import net.tardis.mod.client.models.interiordoors.InteriorTrunkModel;
import net.tardis.mod.client.models.interiordoors.SafeInteriorModel;
import net.tardis.mod.client.models.interiordoors.TTCapsuleInteriorModel;
import net.tardis.mod.client.renderers.DoorRenderer;
import net.tardis.mod.client.renderers.InvisEntityRenderer;
import net.tardis.mod.client.renderers.consoles.ArtDecoConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.CoralConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.GalvanicConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.HartnelConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.NemoConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.SteamConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.ToyotaConsoleRenderer;
import net.tardis.mod.client.renderers.entity.HoloPilotEntityRenderer;
import net.tardis.mod.client.renderers.entity.SecDroidRenderer;
import net.tardis.mod.client.renderers.entity.dalek.DalekRenderer;
import net.tardis.mod.client.renderers.entity.transport.RenderBessie;
import net.tardis.mod.client.renderers.entity.transport.TardisEntityRenderer;
import net.tardis.mod.client.renderers.exteriors.BrokenExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.ClockExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.FortuneExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.ModernPoliceBoxExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.PoliceBoxExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.RedExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.SafeExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.SteamExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.TrunkExteriorRenderer;
import net.tardis.mod.client.renderers.layers.VortexMRenderLayer;
import net.tardis.mod.client.renderers.monitor.MonitorRenderer;
import net.tardis.mod.client.renderers.projectiles.LaserRayRenderer;
import net.tardis.mod.client.renderers.sky.EmptyRenderer;
import net.tardis.mod.client.renderers.sky.VortexSkyRenderer;
import net.tardis.mod.client.renderers.tiles.AntiGravTileRenderer;
import net.tardis.mod.client.renderers.tiles.TardisEngineRenderer;
import net.tardis.mod.client.renderers.tiles.ToyotaSpinnyTileRenderer;
import net.tardis.mod.containers.TContainers;
import net.tardis.mod.entity.BessieEntity;
import net.tardis.mod.entity.ChairEntity;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.entity.DalekEntity;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.entity.HoloPilotEntity;
import net.tardis.mod.entity.SecDroidEntity;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.entity.projectiles.LaserEntity;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.AntiGravityTile;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.tileentities.TardisEngineTile;
import net.tardis.mod.tileentities.ToyotaSpinnyTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.SteamConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.exteriors.ClockExteriorTile;
import net.tardis.mod.tileentities.exteriors.FortuneExteriorTile;
import net.tardis.mod.tileentities.exteriors.ModernPoliceBoxExteriorTile;
import net.tardis.mod.tileentities.exteriors.PoliceBoxExteriorTile;
import net.tardis.mod.tileentities.exteriors.RedExteriorTile;
import net.tardis.mod.tileentities.exteriors.SafeExteriorTile;
import net.tardis.mod.tileentities.exteriors.SteampunkExteriorTile;
import net.tardis.mod.tileentities.exteriors.TTCapsuleExteriorTile;
import net.tardis.mod.tileentities.exteriors.TrunkExteriorTile;
import net.tardis.mod.tileentities.monitors.MonitorTile;

@Mod.EventBusSubscriber(value = Dist.CLIENT, modid = Tardis.MODID, bus = Bus.MOD)
public class ModelRegistry {
	
	public static EmptyRenderer EMPTY_RENDER = new EmptyRenderer();
	public static VortexSkyRenderer VORTEX_RENDER = new VortexSkyRenderer();
	
	@SubscribeEvent
	public static void register(FMLClientSetupEvent event) {

		//Consoles
		ClientRegistry.bindTileEntitySpecialRenderer(SteamConsoleTile.class, new SteamConsoleRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(NemoConsoleTile.class, new NemoConsoleRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(GalvanicConsoleTile.class, new GalvanicConsoleRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(CoralConsoleTile.class, new CoralConsoleRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(HartnelConsoleTile.class, new HartnelConsoleRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(ArtDecoConsoleTile.class, new ArtDecoConsoleRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(ToyotaConsoleTile.class, new ToyotaConsoleRenderer());
        
		//Exteriors
		ClientRegistry.bindTileEntitySpecialRenderer(ClockExteriorTile.class, new ClockExteriorRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(SteampunkExteriorTile.class, new SteamExteriorRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(TrunkExteriorTile.class, new TrunkExteriorRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(RedExteriorTile.class, new RedExteriorRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(PoliceBoxExteriorTile.class, new PoliceBoxExteriorRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(FortuneExteriorTile.class, new FortuneExteriorRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(ModernPoliceBoxExteriorTile.class, new ModernPoliceBoxExteriorRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(SafeExteriorTile.class, new SafeExteriorRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(TTCapsuleExteriorTile.class, new TTCapsuleExteriorRenderer());
		
		ClientRegistry.bindTileEntitySpecialRenderer(BrokenExteriorTile.class, new BrokenExteriorRenderer());
		
		//Monitors
		ClientRegistry.bindTileEntitySpecialRenderer(MonitorTile.class, new MonitorRenderer());
		
		//Other Tiles
		ClientRegistry.bindTileEntitySpecialRenderer(TardisEngineTile.class, new TardisEngineRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(AntiGravityTile.class, new AntiGravTileRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(ToyotaSpinnyTile.class, new ToyotaSpinnyTileRenderer());
       
		//Entities
		RenderingRegistry.registerEntityRenderingHandler(ControlEntity.class, InvisEntityRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(DoorEntity.class, DoorRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(ChairEntity.class, InvisEntityRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(TardisEntity.class, TardisEntityRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(HoloPilotEntity.class, HoloPilotEntityRenderer::new);
        
        //Mobs
		RenderingRegistry.registerEntityRenderingHandler(DalekEntity.class, (EntityRendererManager model) -> new DalekRenderer(model, new Dalek2005Model()));
        RenderingRegistry.registerEntityRenderingHandler(LaserEntity.class, LaserRayRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(SecDroidEntity.class, SecDroidRenderer::new);

        RenderingRegistry.registerEntityRenderingHandler(BessieEntity.class, RenderBessie::new);

		DalekRenderer.registerAllModels();
		
		Map<String, PlayerRenderer> skinMap = Minecraft.getInstance().getRenderManager().getSkinMap();
		for (PlayerRenderer renderPlayer : skinMap.values()) {
			//renderPlayer.addLayer(new SonicLaserRenderLayer(renderPlayer));
			renderPlayer.addLayer(new VortexMRenderLayer(renderPlayer));
		}
		
		ScreenManager.registerFactory(TContainers.QUANTISCOPE, QuantiscopeSonicScreen::new);
		ScreenManager.registerFactory(TContainers.QUANTISCOPE_WELD, QuantiscopeWeldScreen::new);
		ScreenManager.registerFactory(TContainers.ALEMBIC, AlembicScreen::new);
		ScreenManager.registerFactory(TContainers.RECLAMATION_UNIT, ReclamationScreen::new);
		ScreenManager.registerFactory(TContainers.VORTEX_M_BATTERY, VMContainerScreen::new);
		ScreenManager.registerFactory(TContainers.SHIP_COMPUTER, ShipComputerScreen::new);
		
		EnumDoorType.STEAM.setInteriorDoorModel(new InteriorSteamModel());
		EnumDoorType.TRUNK.setInteriorDoorModel(new InteriorTrunkModel());
		EnumDoorType.RED.setInteriorDoorModel(new InteriorRedModel());
		EnumDoorType.POLICE_BOX.setInteriorDoorModel(new InteriorPoliceBoxModel());
		EnumDoorType.FORTUNE.setInteriorDoorModel(new InteriorFortuneModel());
		EnumDoorType.MODERN_POLICE_BOX.setInteriorDoorModel(new InteriorModernPoliceBoxModel());
		EnumDoorType.SAFE.setInteriorDoorModel(new SafeInteriorModel());
		EnumDoorType.TT_CAPSULE.setInteriorDoorModel(new TTCapsuleInteriorModel());
		
		if(!Minecraft.getInstance().getFramebuffer().isStencilEnabled())
			DeferredWorkQueue.runLater(() -> Minecraft.getInstance().getFramebuffer().enableStencil());
        
	}
}
