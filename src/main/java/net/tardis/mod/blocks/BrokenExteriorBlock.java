package net.tardis.mod.blocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.LeadItem;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.experimental.advancement.TTriggers;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.misc.INeedItem;
import net.tardis.mod.misc.TardisLikes;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BrokenTardisSpawn;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class BrokenExteriorBlock extends TileBlock implements INeedItem, IDontBreak {
	
	private static final TranslationTextComponent HAVE_TARDIS = new TranslationTextComponent("status.exterior.broken.have_tardis");
	private static final TranslationTextComponent OTHERS_TARDIS = new TranslationTextComponent("status.exterior.broken.other_tardis");
	public BlockItem ITEM = new BlockItemBrokenExterior(this);
	
	public static List<TardisLikes> LIKES = new ArrayList<TardisLikes>();
	
	static {
		LIKES.add(new TardisLikes(Items.COMPASS, 5));
		LIKES.add(new TardisLikes(Items.FILLED_MAP, 10));
		LIKES.add(new TardisLikes(Items.ENDER_PEARL, 15));
		LIKES.add(new TardisLikes(Items.CLOCK, 20));
		LIKES.add(new TardisLikes(Items.ENDER_EYE, 20));
		LIKES.add(new TardisLikes(Items.BELL, 25));
	}
	
	public BrokenExteriorBlock() {
		super(Properties.create(Material.BARRIER).hardnessAndResistance(9999F));
	}
	
	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {

		//worldIn.playSound(null, pos, TSounds.CANT_START, SoundCategory.BLOCKS, 1.0F, 0.9F);

		if(worldIn.isRemote)
			return true;
		
		ItemStack stack = player.getHeldItem(handIn);
		TileEntity te = worldIn.getTileEntity(pos);
		
		boolean flag = false;

		if(te instanceof BrokenExteriorTile) {
			BrokenExteriorTile ext = (BrokenExteriorTile)te;
			DimensionType interior = null;
			Direction dir = state.get(BlockStateProperties.HORIZONTAL_FACING);
				
			if(!TardisHelper.hasTARDIS(worldIn.getServer(), player.getUniqueID()) && ext.getConsoleDimension() == null) {
				interior = TardisHelper.setupPlayersTARDIS((ServerPlayerEntity)player);
				ext.setConsoleDimension(interior);
				return true;
			}
			else {
				interior = ext.getConsoleDimension();
				if(interior == null)
					player.sendStatusMessage(HAVE_TARDIS, true);
			}
			if(interior == null)
				return false;
			
			if(!DimensionType.getKey(ext.getConsoleDimension()).getPath().contentEquals(player.getUniqueID().toString())) {
				player.sendStatusMessage(OTHERS_TARDIS, true);
				return false;
			}
			
			for(TardisLikes like : LIKES) {
				if(like.getItem() == stack.getItem()) {
                    if (!player.isCreative()) {
                        player.getHeldItem(handIn).shrink(1);
                    }
					ext.increaseLoyalty(like.getLoyaltyMod());
					flag = true;
					Network.sendTo(new BrokenTardisSpawn(pos), (ServerPlayerEntity)player);
					break;
				}
			}
			
			if(stack.getItem() == Items.LEAD) {
				if(LeadItem.attachToFence(player, worldIn, pos)) {
					ext.increaseLoyalty(-100);
					this.replace(worldIn, pos, interior, dir);
				}
				flag = true;
                TTriggers.OBTAINED.trigger((ServerPlayerEntity) player);
			}
			
			if(ext.getConsoleDimension() != null && ext.getLoyalty() > 0) {
				this.replace(worldIn, pos, interior, dir);
                TTriggers.OBTAINED.trigger((ServerPlayerEntity) player);
				flag = true;
			}
			return flag;
		}
		
		return state.onBlockActivated(worldIn, player, handIn, hit);
	}
	
	public void replace(World world, BlockPos pos, DimensionType interior, Direction dir) {
		world.setBlockState(pos, TBlocks.exterior_steampunk.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, dir));
		TileEntity door = world.getTileEntity(pos);
		if(door instanceof ExteriorTile) {
			((ExteriorTile)door).setInterior(interior);
			
			try {
				ConsoleTile console = ((ConsoleTile)world.getServer().getWorld(interior).getTileEntity(TardisHelper.TARDIS_POS));
				console.setLocation(world.getDimension().getType(), pos.down());
				console.setDestination(world.getDimension().getType(), pos.down());
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	@OnlyIn(Dist.CLIENT)
	public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
		if(rand.nextDouble() < 0.25) {
			for(int i = 0; i < 18; ++ i) {
				double angle = Math.toRadians(i * 20);
				double x = Math.sin(angle);
				double z = Math.cos(angle);


				worldIn.playSound(pos.getX(), pos.getY(), pos.getZ(), TSounds.ELECTRIC_SPARK, SoundCategory.BLOCKS, 0.5F, 1F, false);
				worldIn.addParticle(ParticleTypes.LAVA, pos.getX() + 0.5 + x, pos.getY() + rand.nextDouble(), pos.getZ() + 0.5 + z, 0, 0, 0);
			}
		}

	}


	@OnlyIn(Dist.CLIENT)
    public static void spawnHappyPart(ClientWorld world, BlockPos pos, Random rand) {
		for(int i = 0; i < 18; ++ i) {
			double angle = Math.toRadians(i * 20);
			double x = Math.sin(angle);
			double z = Math.cos(angle);
			
			world.addParticle(ParticleTypes.HEART, pos.getX() + 0.5 + x, pos.getY() + rand.nextDouble(), pos.getZ() + 0.5 + z, 0, 0, 0);
		}
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(BlockStateProperties.HORIZONTAL_FACING);
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		if(context.getPlayer() != null)
			return this.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
		return this.getDefaultState();
	}

	@Override
	public Item getItem() {
		return ITEM;
	}


    //This have been added to "increase" immersion, by removing running particles and breaking particles etc
    @Override
    public boolean addLandingEffects(BlockState state1, ServerWorld worldserver, BlockPos pos, BlockState state2, LivingEntity entity, int numberOfParticles) {
        return false;
    }

    @Override
    public boolean addRunningEffects(BlockState state, World world, BlockPos pos, Entity entity) {
        return false;
    }

    @Override
    public boolean addHitEffects(BlockState state, World worldObj, RayTraceResult target, ParticleManager manager) {
        return false;
    }

    @Override
    public boolean addDestroyEffects(BlockState state, World world, BlockPos pos, ParticleManager manager) {
        return false;
    }


	public static class BlockItemBrokenExterior extends BlockItem{

		public BlockItemBrokenExterior(Block blockIn) {
			super(blockIn, new Item.Properties().group(TItemGroups.MAIN));
		}

		@Override
		protected boolean canPlace(BlockItemUseContext cont, BlockState state) {
			return cont.getFace() == Direction.UP && cont.getWorld().getBlockState(cont.getPos().up()).isReplaceable(cont);
		}

		@Override
		protected boolean placeBlock(BlockItemUseContext context, BlockState state) {
			return context.getWorld().setBlockState(context.getPos().up(), state);
		}
		
	}


}
