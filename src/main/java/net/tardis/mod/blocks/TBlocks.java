package net.tardis.mod.blocks;

import static net.minecraft.block.Blocks.STONE;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.ObjectHolder;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.datagen.DataGen;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.TItemProperties;
import net.tardis.mod.misc.INeedItem;
import net.tardis.mod.properties.Prop;

@ObjectHolder(Tardis.MODID)
@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TBlocks {

	public static List<Item> ITEMS = new ArrayList<Item>();
	public static List<Block> BLOCKS = new ArrayList<Block>();
	
	//Consoles
	public static ConsoleBlock console_steam = register(new ConsoleBlock(), "console_steam", null, false);
	public static ConsoleBlock console_nemo = register(new ConsoleBlock(), "console_nemo", null, false);
	public static ConsoleBlock console_galvanic = register(new ConsoleBlock(), "console_galvanic", null, false);
    public static ConsoleBlock console_coral = register(new ConsoleBlock(), "console_coral", null, false);
    public static ConsoleBlock console_hartnel = register(new ConsoleBlock(), "console_hartnel", null, false);
    public static ConsoleBlock console_art_deco = register(new ConsoleBlock(), "console_art_deco", null, false);
    public static ConsoleBlock console_toyota = register(new ConsoleBlock(), "console_toyota", null, false);

	public static ExteriorBlock exterior_clock = register(new ExteriorBlock(), "exterior_clock", null, false);
	public static ExteriorBlock exterior_steampunk = register(new ExteriorBlock(), "exterior_steampunk", null, false);
	public static ExteriorBlock exterior_trunk = register(new ExteriorBlock(), "exterior_trunk", null, false);
	public static ExteriorBlock exterior_red = register(new ExteriorBlock(), "exterior_red", null, false);
	public static ExteriorBlock exterior_police_box = register(new ExteriorBlock(), "exterior_police_box", null, false);
	public static ExteriorBlock exterior_fortune = register(new ExteriorBlock(), "exterior_fortune", null, false);
	public static ExteriorBlock exterior_modern_police_box = register(new ExteriorBlock(), "exterior_modern_police_box", null, false);
	public static ExteriorBlock exterior_safe = register(new ExteriorBlock(), "exterior_safe", null, false);
	public static ExteriorBlock exterior_tt_capsule = register(new ExteriorBlock(), "exterior_tt_capsule", null, false);


	// Generic Interior Blocks
	public static RoundelBlock roundel_alabaster  = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_alabaster", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_alabaster  = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_alabaster", TItemGroups.ROUNDELS );
	public static ModWallBlock alabaster_wall = register(new ModWallBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1F, 2F), "alabaster_wall",TItemGroups.FUTURE);
	public static CubeBlock alabaster = register(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1F, 2F), "alabaster",TItemGroups.FUTURE);
	public static ModStairsBlock alabaster_stairs = register(new ModStairsBlock(Block.Properties.create(Material.ROCK), () -> STONE.getDefaultState(), SoundType.STONE, 1.25F, 4.2F), "alabaster_stairs");
	public static ModSlabBlock alabaster_slab = register(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "alabaster_slab");
	
	public static RoundelBlock roundel_wood_oak = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_wood_oak", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_offset_wood_oak = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_offset_wood_oak", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_wood_spruce = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_wood_spruce", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_offset_wood_spruce = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_offset_wood_spruce", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_wood_birch = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_wood_birch", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_offset_wood_birch = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_offset_wood_birch", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_wood_jungle = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_wood_jungle", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_offset_wood_jungle = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_offset_wood_jungle", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_wood_acacia = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_wood_acacia", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_offset_wood_acacia = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_offset_wood_acacia", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_wood_dark_oak = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_wood_dark_oak", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_offset_wood_dark_oak = register(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F), "roundel_offset_wood_dark_oak", TItemGroups.ROUNDELS);


	public static RoundelBlock roundel_concrete_white = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_white", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_white = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_white", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_orange = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_orange", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_orange = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_orange", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_magenta = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_magenta", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_magenta = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_magenta", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_light_blue = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_light_blue", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_light_blue = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_light_blue", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_yellow = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_yellow", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_yellow = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_yellow", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_lime = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_lime", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_lime = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_lime", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_pink = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_pink", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_pink = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_pink", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_gray = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_gray", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_gray = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_gray", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_light_gray = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_light_gray", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_light_gray = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_light_gray", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_cyan = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_cyan", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_cyan = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_cyan", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_purple = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_purple", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_purple = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_purple", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_blue = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_blue", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_blue = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_blue", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_brown = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_brown", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_brown = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_brown", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_green = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_green", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_green = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_green", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_red = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_red", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_red = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_red", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_concrete_black = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_concrete_black", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_concrete_black = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_concrete_black", TItemGroups.ROUNDELS );


	public static RoundelBlock roundel_terracotta_white = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_white", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_white = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_white", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_orange = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_orange", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_orange = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_orange", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_magenta = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_magenta", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_magenta = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_magenta", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_light_blue = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_light_blue", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_light_blue = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_light_blue", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_yellow = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_yellow", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_yellow = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_yellow", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_lime = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_lime", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_lime = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_lime", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_pink = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_pink", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_pink = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_pink", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_gray = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_gray", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_gray = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_gray", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_light_gray = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_light_gray", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_light_gray = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_light_gray", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_cyan = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_cyan", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_cyan = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_cyan", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_purple = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_purple", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_purple = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_purple", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_blue = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_blue", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_blue = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_blue", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_brown = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_brown", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_brown = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_brown", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_green = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_green", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_green = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_green", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_red = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_red", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_red  = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_red", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_terracotta_black = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_terracotta_black", TItemGroups.ROUNDELS );
	public static RoundelBlock roundel_offset_terracotta_black  = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_offset_terracotta_black", TItemGroups.ROUNDELS );
	
	public static RoundelBlock roundel_quartz = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_quartz", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_quartz_pillar = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_quartz_pillar", TItemGroups.ROUNDELS);
	public static RoundelBlock roundel_chiselled_quartz = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_chiselled_quartz", TItemGroups.ROUNDELS);
    public static RoundelBlock roundel_coralised = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_coralised", TItemGroups.ROUNDELS);
    public static RoundelBlock roundel_divider = register(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "roundel_divider", TItemGroups.ROUNDELS);

	public static TechStrutBlock tech_strut = register(new TechStrutBlock(), "tech_strut", TItemGroups.FUTURE);
	
	// Tech - Stairs, Slabs.
    public static ModSlabBlock tech_floor_plate_slab = register(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "tech_floor_plate_slab");
    public static ModSlabBlock tech_floor_dark_pipes_slab = register(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "tech_floor_dark_pipes_slab");
    public static ModSlabBlock tech_floor_dark_pattern_slab = register(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "tech_floor_dark_pattern_slab");
    public static ModSlabBlock tech_wall_dark_blank_slab = register(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "tech_wall_dark_blank_slab");

    public static ModStairsBlock tech_floor_plate_stairs = register(new ModStairsBlock(Block.Properties.create(Material.ROCK), () -> STONE.getDefaultState(), SoundType.STONE, 1.25F, 4.2F), "tech_floor_plate_stairs");
    public static ModStairsBlock tech_floor_dark_pipes_stairs = register(new ModStairsBlock(Block.Properties.create(Material.ROCK), () -> STONE.getDefaultState(), SoundType.STONE, 1.25F, 4.2F), "tech_floor_dark_pipes_stairs");
    public static ModStairsBlock tech_floor_dark_pattern_stairs = register(new ModStairsBlock(Block.Properties.create(Material.ROCK), () -> STONE.getDefaultState(), SoundType.STONE, 1.25F, 4.2F), "tech_floor_dark_pattern_stairs");
    public static ModStairsBlock tech_wall_dark_blank_stairs = register(new ModStairsBlock(Block.Properties.create(Material.ROCK), () -> STONE.getDefaultState(), SoundType.STONE, 1.25F, 4.2F), "tech_wall_dark_blank_stairs");

	//Grates - Blocks, Slabs, Stairs
	public static GrateBlock metal_grate = register(new GrateBlock(Block.Properties.create(Material.IRON), SoundType.LANTERN, 1.25F, 4.2F), "metal_grate");
	public static ModSlabBlock metal_grate_slab = register(new ModSlabBlock(Block.Properties.create(Material.IRON), SoundType.LANTERN, 1.25F, 4.2F), "metal_grate_slab");
	public static ModStairsClearBlock metal_grate_stairs = register(new ModStairsClearBlock(Block.Properties.create(Material.IRON), () -> Blocks.IRON_BLOCK.getDefaultState(), SoundType.LANTERN, 1.25F, 4.2F), "metal_grate_stairs");
	public static ModTrapdoorBlock metal_grate_trapdoor = register(new ModTrapdoorBlock(Block.Properties.create(Material.IRON), SoundType.LANTERN, 1.25F, 4.2F), "metal_grate_trapdoor", TItemGroups.FUTURE);
	
	// Whitaker Interior
	public static GrateBlock steel_grate_solid = register(new GrateBlock(Block.Properties.create(Material.IRON), SoundType.LANTERN, 1.25F, 4.2F), "steel_grate_solid");
	public static GrateBlock steel_grate_solid_offset = register(new GrateBlock(Block.Properties.create(Material.IRON), SoundType.LANTERN, 1.25F, 4.2F), "steel_grate_solid_offset");
	public static TransparentGrateBlock steel_grate_frame = register(new TransparentGrateBlock(Block.Properties.create(Material.IRON), SoundType.LANTERN, 1.25F, 4.2F), "steel_grate_frame");
	public static TransparentGrateBlock steel_grate_frame_offset = register(new TransparentGrateBlock(Block.Properties.create(Material.IRON), SoundType.LANTERN, 1.25F, 4.2F), "steel_grate_frame_offset");
	
	public static ModSlabBlock blue_concrete_slab = register(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "blue_concrete_slab");
	public static LightSlabBlock gallifreyan_yellow_slab = register(new LightSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "gallifreyan_yellow_slab");

	public static TranslucentBlock yellow_crystal = register(new TranslucentBlock(Block.Properties.create(Material.GLASS), SoundType.GLASS, 1.25F, 4.2F), "yellow_crystal");
	public static TranslucentSlabBlock yellow_crystal_slab = register(new TranslucentSlabBlock(Block.Properties.create(Material.GLASS), SoundType.GLASS, 1.25F, 4.2F), "yellow_crystal_slab");
	public static TranslucentStairsBlock yellow_crystal_stairs = register(new TranslucentStairsBlock(Block.Properties.create(Material.GLASS),() -> Blocks.PRISMARINE.getDefaultState(), SoundType.GLASS, 1.25F, 4.2F), "yellow_crystal_stairs");
	
	// Toyota Interior

	public static CubeBlock blue_pillar = register(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "blue_pillar");
	public static CubeBlock foam_pipes = register(new CubeBlock(Block.Properties.create(Material.WOOL), SoundType.CLOTH, 1F, 2F), "foam_pipes");
	public static LightBlock three_point_lamp = register(new LightBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F, 10), "three_point_lamp");
	
	//Machines
	public static AlembicBlock alembic = register(new AlembicBlock(),"alembic",TItemGroups.MAINTENANCE);
	public static TardisEngineBlock engine = register(new TardisEngineBlock(), "tardis_engine", TItemGroups.MAINTENANCE);
	public static ArsEggBlock ars_egg = register(new ArsEggBlock(), "ars_egg", TItemGroups.MAINTENANCE);
	public static QuantiscopeBlock quantiscope_brass = register(new QuantiscopeBlock(), "quantiscope_brass", TItemGroups.MAINTENANCE);
	public static QuantiscopeBlock quantiscope_iron = register(new QuantiscopeBlock(), "quantiscope_iron", TItemGroups.MAINTENANCE);
	public static AtriumBlock atrium_block = register(new AtriumBlock(), "atrium_block", TItemGroups.MAINTENANCE);
	public static AntiGravBlock anti_grav = register(new AntiGravBlock(), "anti_grav", TItemGroups.MAINTENANCE);
	public static ShipComputerBlock ship_computer = register(new ShipComputerBlock(), "ship_computer", TItemGroups.FUTURE);
	public static ReclamationBlock reclamation_unit = register(new ReclamationBlock(), "reclamation_unit");
	public static ArtronTapBlock artron_tap = register(new ArtronTapBlock(), "artron_tap", TItemGroups.MAINTENANCE);
	
	public static ArsStructureBlock corridor_spawn = register(new ArsStructureBlock(), "corridor_spawn", TItemGroups.FUTURE);
	public static CubeBlock corridor_kill = register(new CubeBlock(Prop.Blocks.BASIC_TECH.get()), "corridor_kill");
	
	public static BeaconBlock psychic_cube = register(new BeaconBlock(), "psychic_cube", TItemGroups.FUTURE);
	
	public static ArtronConverterBlock artron_converter = register(new ArtronConverterBlock(), "artron_converter_block", TItemGroups.MAINTENANCE);
	
	//Monitors
	public static MonitorSteampunkBlock steampunk_monitor = register(new MonitorSteampunkBlock(Prop.Blocks.BASIC_TECH.get(), Constants.Gui.MONITOR_MAIN_STEAM, -6 / 16.0, 3 / 16.0, -5.1 / 16.0, 0.77, 0.95), "steampunk_monitor", TItemGroups.MAIN);
	public static MonitorEyeBlock eye_monitor = register(new MonitorEyeBlock(Prop.Blocks.BASIC_TECH.get(), Constants.Gui.MONITOR_MAIN_EYE, -0.45, 0.25, 7 / 16.0, 0.9, 1.05), "eye_monitor", TItemGroups.MAIN);
	public static MonitorRCABlock rca_monitor = register(new MonitorRCABlock(), "rca_monitor", TItemGroups.FUTURE);
	
	//Resources
	public static CubeBlock cinnabar_ore = register(new CubeBlock(Block.Properties.create(Material.ROCK),SoundType.STONE, 1.25F, 4.2F), "cinnabar_ore");
	public static CrystalBlock xion_crystal = register(new CrystalBlock(Prop.Blocks.BASIC_CRYSTAL.get().lightValue(10)), "xion_crystal", TItemGroups.FUTURE);

	// Misc
	public static CubeBlock tech_floor_dark_pattern = register(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "tech_floor_dark_pattern");
	public static CubeBlock tech_floor_dark_pipes = register(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "tech_floor_dark_pipes");
	public static CubeBlock tech_floor_dark_plate = register(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "tech_floor_dark_plate");
	public static CubeBlock tech_floor_plate = register(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "tech_floor_plate");
	public static RoundelBlock tech_wall_dark_runner_lights = register(new RoundelBlock(Prop.Blocks.BASIC_TECH.get(), SoundType.STONE, 1.25F, 4.2F), "tech_wall_dark_runner_lights");
	public static CubeBlock tech_wall_dark_blank = register(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F), "tech_wall_dark_blank");
	public static Block invis = register(new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(0.5F, 4.2F)) {
				@Override
				public BlockRenderLayer getRenderLayer() {
					return BlockRenderLayer.TRANSLUCENT;
				}
			}, "invis", null);
	
	//Decoration
	public static BookStackBlock clutter_books = register(new BookStackBlock(Block.Properties.create(Material.WOOL), SoundType.CLOTH, 1F, 2F), "clutter_books");
	public static FloorClutterBlock clutter_paper = register(new FloorClutterBlock(Block.Properties.create(Material.WOOL), SoundType.CLOTH, 1F, 2F), "clutter_paper");
	public static HangingWireBlock wire_hang = register(new HangingWireBlock(Prop.Blocks.BASIC_TECH.get()), "wire_hang", TItemGroups.FUTURE);
	public static ChairBlock comfy_chair = register(new ChairBlock(0.25F), "comfy_chair", TItemGroups.FUTURE);
	public static ChairBlock comfy_chair_green = register(new ChairBlock(0.25F), "comfy_chair_green", TItemGroups.FUTURE);
	public static CrashChairBlock crash_chair = register(new CrashChairBlock(0.3F), "crash_chair", TItemGroups.FUTURE);
	public static CrashChairBlock crash_chair_wood = register(new CrashChairBlock(0.3F), "crash_chair_wood", TItemGroups.FUTURE);
	public static CrashChairBlock crash_chair_red = register(new CrashChairBlock(0.3F), "crash_chair_red", TItemGroups.FUTURE);
	public static NonCubeBlock telescope = register(new NonCubeBlock(Prop.Blocks.BASIC_TECH.get()), "telescope", TItemGroups.MAIN);
	public static CubeBlock blinking_lights = register(new CubeBlock(Prop.Blocks.BASIC_TECH.get().lightValue(15)), "blinking_lights", TItemGroups.FUTURE);
	public static ToyotaSpinnyBlock spinny_block = register(new ToyotaSpinnyBlock(), "spinny_thing", TItemGroups.MAIN);
	
	public static BrokenExteriorBlock broken_exterior = register(new BrokenExteriorBlock(), "broken_exterior");
    public static Block squareness = register(new SquarenessBlock(Block.Properties.create(Material.AIR).doesNotBlockMovement()), "squareness_block", null, false);
    
    //Debug use only
    public static Block structure_block = register(new SuperStructureBlock(), "structure_block", null, true);

    @SubscribeEvent
	public static void register(RegistryEvent.Register<Block> event) {
		for(Block block : BLOCKS) {
			event.getRegistry().registerAll(block);
		}
	}
	
	public static <T extends Block> T register(T block, String name, ItemGroup group, boolean hasItem) {
		ResourceLocation loc = new ResourceLocation(Tardis.MODID, name);
		block.setRegistryName(loc);

		if(hasItem) {
			if(block instanceof INeedItem) {
				ITEMS.add(((INeedItem)block).getItem().setRegistryName(loc));
			}
			else ITEMS.add(new BlockItem(block, TItemProperties.BLOCK.group(group)).setRegistryName(loc));
		}

		BLOCKS.add(block);
		
		if(block instanceof IARS)
			DataGen.ARS_DATAGEN.addToTag(block.getRegistryName());
		
		return block;
	}
	
	public static <T extends Block> T register(T block, String name, ItemGroup group){
		return register(block, name, group, true);
	}
	
	public static <T extends Block> T register(T block, String name) {
		return register(block, name, null);
	}
	
	
}
