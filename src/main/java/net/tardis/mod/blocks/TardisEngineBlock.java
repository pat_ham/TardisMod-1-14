package net.tardis.mod.blocks;


import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ChestContainer;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.TardisEngineTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class TardisEngineBlock extends TileBlock {

	public TardisEngineBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		TileEntity te = worldIn.getTileEntity(pos);
		if(te instanceof TardisEngineTile) {
			worldIn.getCapability(Capabilities.TARDIS_DATA).ifPresent((cap) -> {
				PanelInventory inv = cap.getEngineInventoryForSide(hit.getFace());
				player.openContainer(new INamedContainerProvider() {

					@Override
					public Container createMenu(int id, PlayerInventory playerInv, PlayerEntity p_createMenu_3_) {
						return new ChestContainer(ContainerType.GENERIC_9X1, id, playerInv, inv, 1);
					}

					@Override
					public ITextComponent getDisplayName() {
						return inv.getName();
					}});
			});
			 if (!Helper.isDimensionBlocked(worldIn.getDimension().getType()) && !worldIn.isRemote()) {
				player.sendStatusMessage(new StringTextComponent("You can only use the Tardis Engine in the Tardis!"), true);
			}
		}
		return true;
	}

}
