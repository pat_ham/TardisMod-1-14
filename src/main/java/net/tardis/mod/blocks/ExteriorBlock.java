package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.fluid.IFluidState;
import net.minecraft.item.Items;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ExteriorBlock extends TileBlock implements IDontBreak{
	
	public static TranslationTextComponent LOCKED = new TranslationTextComponent("message.tardis.door.locked");
	public static TranslationTextComponent UNLOCKED = new TranslationTextComponent("message.tardis.door.unlocked");
	public int rand;
	
	public ExteriorBlock() {
		super(Properties.create(Material.BARRIER).sound(SoundType.WOOD).hardnessAndResistance(99999F));
		this.setDefaultState(this.getDefaultState()
				.with(BlockStateProperties.HORIZONTAL_FACING, Direction.NORTH)
				.with(BlockStateProperties.WATERLOGGED, false));
	}
	@Override
    public void onBlockAdded(BlockState state, World world, BlockPos pos, BlockState oldState, boolean isMoving) {
		if (world.isRemote)
        	return;
        world.getCapability(Capabilities.CHUNK_LOADER, null).ifPresent(cap -> cap.add(pos));
    }
	
	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote && handIn == player.getActiveHand()) {
			TileEntity te = worldIn.getTileEntity(pos);
			if(te instanceof ExteriorTile) {
				ExteriorTile door = (ExteriorTile)te;
				if(door.getMatterState() == EnumMatterState.SOLID) {
					
					//Exterior naming
					if(player.getHeldItem(handIn).getItem() == Items.NAME_TAG) {
						door.setCustomName(player.getHeldItem(handIn).getDisplayName().getFormattedText());
						if(!player.isCreative())
							player.getHeldItem(handIn).shrink(1);
						return true;
					}
					
					if(door.isKeyValid(player.getHeldItem(handIn))) {
						door.toggleLocked();
						worldIn.playSound(null, pos, door.getLocked() ? TSounds.DOOR_LOCK : TSounds.DOOR_UNLOCK, SoundCategory.BLOCKS, 1F, 1F);
						player.sendStatusMessage(door.getLocked() ? LOCKED : UNLOCKED, true);
					}
					else door.open(player);
				}	
			}
			return true;
		}
		return true; //Return true so players don't teleport their held items into the interior
	}
	
	@Override
	public void onProjectileCollision(World worldIn, BlockState state, BlockRayTraceResult hit, Entity projectile) {
//		System.out.println("Was poked with an arrow");
		super.onProjectileCollision(worldIn, state, hit, projectile);
	}

	@Override
	public IFluidState getFluidState(BlockState state) {
		return state.get(BlockStateProperties.WATERLOGGED) ? Fluids.WATER.getDefaultState() : Fluids.EMPTY.getDefaultState();
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(BlockStateProperties.WATERLOGGED);
		builder.add(BlockStateProperties.HORIZONTAL_FACING);
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.TRANSLUCENT;
	}

	@Override
	public BlockRenderType getRenderType(BlockState state) {
		return BlockRenderType.ENTITYBLOCK_ANIMATED;
	}
	
	@Override
	public void onBlockClicked(BlockState state, World worldIn, BlockPos pos, PlayerEntity player) {
		if (!worldIn.isRemote) { 
			ExteriorTile exterior = (ExteriorTile) worldIn.getTileEntity(pos);
			if (exterior != null && exterior.getLocked()) {
				TardisHelper.getConsole(worldIn.getServer(), exterior.getInterior())
					.ifPresent(console -> console.getWorld().playSound(null, console.getPos(), TSounds.DOOR_KNOCK, SoundCategory.BLOCKS, 1F, 1F));
				worldIn.playSound(null, pos, TSounds.DOOR_KNOCK, SoundCategory.BLOCKS, 1F, 1F);
			}
		}
	}
	@Override
	public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
		if (state.hasTileEntity() && (state.getBlock() != newState.getBlock() || !newState.hasTileEntity())) {
	         worldIn.removeTileEntity(pos);
		}
		worldIn.getCapability(Capabilities.CHUNK_LOADER).ifPresent(cap -> cap.remove(pos));
	}

}
