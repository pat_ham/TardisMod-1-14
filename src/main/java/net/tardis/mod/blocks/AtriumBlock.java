package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.tardis.mod.properties.Prop;

public class AtriumBlock extends Block {

	public AtriumBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
	}

}
