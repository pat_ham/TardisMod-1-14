package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunk;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ILightCap;
import net.tardis.mod.misc.TardisBlockProperties;

public class RoundelBlock extends Block{
	
    public RoundelBlock(Block.Properties prop, SoundType sound, float hardness, float resistance) {
        super(prop.sound(sound).hardnessAndResistance(hardness, resistance));
        this.setDefaultState(this.getDefaultState().with(TardisBlockProperties.LIGHT, 0));
    }

    @Override
    public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return 0;
    }

    @Override
    public boolean isNormalCube(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return false;
    }

    @Override
    public boolean causesSuffocation(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return false;
    }

	@Override
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT;
    }
    
    @Override
    public int getLightValue(BlockState state) {
    	return state.get(TardisBlockProperties.LIGHT);
    }

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(TardisBlockProperties.LIGHT);
	}
	
	@Override
	public void onBlockAdded(BlockState state, World worldIn, BlockPos pos, BlockState oldState, boolean isMoving) {
		if(!worldIn.isRemote) {
			worldIn.getServer().enqueue(new TickDelayedTask(1, () -> {
				IChunk chunk = worldIn.getChunk(pos);
				if(chunk instanceof Chunk) {
					ILightCap cap = ((Chunk)chunk).getCapability(Capabilities.LIGHT).orElse(null);
					if(cap != null) {
						cap.addLightPos(pos);
					}
				}
			}));
		}
	}

    public boolean hasComparatorInputOverride(BlockState state) {
        return true;
    }

    public int getComparatorInputOverride(BlockState blockState, World worldIn, BlockPos pos) {
        return blockState.getLightValue();
    }
}
