package net.tardis.mod.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.properties.Prop;

/**
 * A normal cube block but allows for tooltips to be added
 */
public class ArsStructureBlock extends Block{
	
	public ArsStructureBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
	}

	@Override
	public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip,
			ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		if (Screen.hasShiftDown() || Screen.hasControlDown()) {
			tooltip.add(new TranslationTextComponent("tooltip.ars_structure.placement_offset",2));
		}
		
	}
	
	

}
