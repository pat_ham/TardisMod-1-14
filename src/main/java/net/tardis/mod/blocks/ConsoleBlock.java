package net.tardis.mod.blocks;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootContext.Builder;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.renderers.items.ConsoleItemRenderer;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.misc.INeedItem;
import net.tardis.mod.tileentities.ConsoleTile;

public class ConsoleBlock extends TileBlock implements INeedItem, IDontBreak{

	public BlockItem item = new BlockItem(this, new Item.Properties()
			.setTEISR(() -> {
				return ConsoleItemRenderer::new;
			}));


	public ConsoleBlock() {
		super(Properties.create(Material.BARRIER).variableOpacity().hardnessAndResistance(99999F, 99999F));
	}
	
	@Override
    public void onBlockAdded(BlockState state, World world, BlockPos pos, BlockState oldState, boolean isMoving) {
		
		//Make consoles go away if not in tardis dim
		if(world.getDimension().getType().getModType() != TDimensions.TARDIS) {
			world.setBlockState(pos, Blocks.AIR.getDefaultState(), 2);
			return;
		}
				
		
		if (world.isRemote) return;
		world.getCapability(Capabilities.CHUNK_LOADER, null).ifPresent(cap -> cap.add(pos));
    }

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) {
		return type.create();
	}

	@Override
	public boolean hasTileEntity(BlockState state) {
		return true;
	}

	@Override
	public List<ItemStack> getDrops(BlockState state, Builder builder) {
		return new ArrayList<ItemStack>();
	}

	@Override
	public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
		return 0;
	}

	@Override
	public boolean isNormalCube(BlockState state, IBlockReader worldIn, BlockPos pos) {
		return false;
	}
	
	@Override
	public boolean causesSuffocation(BlockState state, IBlockReader worldIn, BlockPos pos) {
	      return false;      
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.TRANSLUCENT;
	}
	
	@Override
	public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
		if (state.hasTileEntity() && (state.getBlock() != newState.getBlock() || !newState.hasTileEntity())) {
	         worldIn.removeTileEntity(pos);
	    }
		
		//Make consoles go away if not in tardis dim
		if(worldIn.getDimension().getType().getModType() != TDimensions.TARDIS) {
			worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 2);
			return;
		}
		
		if(!worldIn.isRemote) {
			if(newState.getBlock() != state.getBlock()) {
				TileEntity te = worldIn.getTileEntity(pos);
				if(te instanceof ConsoleTile)
					((ConsoleTile)te).removeControls();
			}
		}
		if(newState.getBlock() instanceof ConsoleBlock) {
			TileEntity te = worldIn.getTileEntity(pos);
			if(te instanceof ConsoleTile)
				((ConsoleTile)te).getOrCreateControls();
		}
	}
	
	@Override
	public Item getItem() {
		return item;
	}
	
}
