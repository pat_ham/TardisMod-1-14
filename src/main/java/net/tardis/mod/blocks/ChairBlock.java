package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Hand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.entity.ChairEntity;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.helper.VoxelShapeUtils;
import net.tardis.mod.properties.Prop;

public class ChairBlock extends Block implements IARS{
	
	public static final VoxelShape NORTH = createVoxelShape();
	public static final VoxelShape EAST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_90);
	public static final VoxelShape SOUTH = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_180);
	public static final VoxelShape WEST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.COUNTERCLOCKWISE_90);
	
	private float yOffset = 0.25F;
	
	public ChairBlock(float yOffset) {
		super(Prop.Blocks.BASIC_WOOD.get());
		this.yOffset = yOffset;
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.getDefaultState()
				.with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(BlockStateProperties.HORIZONTAL_FACING);
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.CUTOUT_MIPPED;
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote) {
			if(worldIn.getEntitiesWithinAABB(ChairEntity.class, new AxisAlignedBB(0, 0, 0, 1, 1, 1).offset(pos)).isEmpty()) {
				ChairEntity chair = TEntities.CHAIR.create(worldIn);
				chair.setPosition(pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5);
				chair.setYOffset(this.yOffset);
				player.startRiding(chair);
				worldIn.addEntity(chair);

			}
		}
		return true;
	}
	
	public static VoxelShape createVoxelShape() {
		VoxelShape shape = VoxelShapes.create(0.0703125, 0.5, 0.875, 0.9375, 1.5, 1.0);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.8703125, 0.59375, 0.0625, 1.0015625, 0.71875, 0.9375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.0046875, 0.59375, 0.0625, 0.1265625, 0.71875, 0.9375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.8703125, 0.375, 0.0625, 0.9859375, 0.59375, 0.9375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0109375, 0.375, 0.0625, 0.1265625, 0.59375, 0.9375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.8703125, 0.71875, 0.71875, 1.0015625, 1.296875, 0.9375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.0046875, 0.71875, 0.71875, 0.1265625, 1.296875, 0.9375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0703125, 0.375, 0.0, 0.9375, 0.5, 1.0), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0625, 0.3125, 0.0625, 0.9375, 0.40625, 0.9375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.015625, 0.015625, 0.015625, 0.109375, 0.109375, 0.109375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.015625, 0.28125, 0.015625, 0.109375, 0.375, 0.109375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.0, 0.03125, 0.09375, 0.625, 0.09375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.015625, 0.609375, 0.015625, 0.109375, 0.703125, 0.0625), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.890625, 0.015625, 0.015625, 0.984375, 0.109375, 0.109375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.890625, 0.28125, 0.015625, 0.984375, 0.375, 0.109375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.890625, 0.609375, 0.015625, 0.984375, 0.703125, 0.0625), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.90625, 0.0, 0.03125, 0.96875, 0.625, 0.09375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.890625, 0.609375, 0.9375, 0.984375, 0.703125, 0.984375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.890625, 0.015625, 0.890625, 0.984375, 0.109375, 0.984375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.890625, 0.28125, 0.890625, 0.984375, 0.375, 0.984375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.90625, 0.0, 0.90625, 0.96875, 0.625, 0.96875), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.015625, 0.609375, 0.9375, 0.109375, 0.703125, 0.984375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.015625, 0.015625, 0.890625, 0.109375, 0.109375, 0.984375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.015625, 0.28125, 0.890625, 0.109375, 0.375, 0.984375), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.0, 0.90625, 0.09375, 0.625, 0.96875), IBooleanFunction.OR);
		return shape;
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		switch(state.get(BlockStateProperties.HORIZONTAL_FACING)) {
		case EAST: return EAST;
		case SOUTH: return SOUTH;
		case WEST: return WEST;
		default: return NORTH;
		}
	}
	
	@Override
	public BlockState mirror(BlockState state, Mirror mirrorIn) {
		return state.rotate(mirrorIn.toRotation(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot) {
		return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}

}
