package net.tardis.mod.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.properties.Prop;

public class ArtronConverterBlock extends TileBlock {

	public static BooleanProperty EMIT = BooleanProperty.create("emit");
	
	public ArtronConverterBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote) {
			if (handIn == player.getActiveHand()) {
				worldIn.setBlockState(pos, state.with(EMIT, !state.get(EMIT)));
				PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.artron_convertor.mode", state.get(EMIT) ? "Push" : "Pull"), true);
			}
		}
		return true;
	}
	
	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(EMIT);
	}

	@Override
	public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip,
			ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		tooltip.add(Constants.Translations.TOOLTIP_HOLD_SHIFT);
		if (Screen.hasShiftDown()) {
			tooltip.add(new TranslationTextComponent("tooltip.artron_converter.purpose"));
			tooltip.add(new TranslationTextComponent("tooltip.artron_converter.use"));
		}
		
	}
	
	
}
