package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.fluid.IFluidState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.FilledMapItem;
import net.minecraft.item.Items;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraft.world.storage.MapData;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.monitors.MonitorTile;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;

public class MonitorBlock extends TileBlock implements IWaterLoggable{
	
	private double textX;
	private double textY;
	private double textZ;
	private double width, height;
	private int guiID = Constants.Gui.MONITOR_MAIN_STEAM;
	
	public MonitorBlock(Block.Properties prop, int guiID, double x, double y, double z, double width, double height) {
		super(prop);
		this.setDefaultState(this.getDefaultState().with(BlockStateProperties.WATERLOGGED, false).with(BlockStateProperties.HANGING, false));
		this.textX = x;
		this.textY = y;
		this.textZ = z;
		this.width = width;
		this.height = height;
		this.guiID = guiID;
	}

	@SuppressWarnings("deprecation")
	@Override
	public IFluidState getFluidState(BlockState state) {
		return state.get(BlockStateProperties.WATERLOGGED) ? Fluids.WATER.getStillFluidState(false) : super.getFluidState(state);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) {
		TileEntity te = super.createTileEntity(state, world);
		if(te instanceof MonitorTile) {
			((MonitorTile)te).setGuiXYZ(this.textX, this.textY, this.textZ, this.width, this.height);
		}
		return te;
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(BlockStateProperties.WATERLOGGED);
		builder.add(BlockStateProperties.HORIZONTAL_FACING);
		builder.add(BlockStateProperties.HANGING);
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		IFluidState fluid = context.getWorld().getFluidState(context.getPos());
		return super.getStateForPlacement(context)
				.with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite())
				.with(BlockStateProperties.WATERLOGGED, fluid.getFluidState().isTagged(FluidTags.WATER))
				.with(BlockStateProperties.HANGING, context.getFace() == Direction.DOWN);
	}
	
	@SuppressWarnings("deprecation")
	public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
		BlockState state = super.updatePostPlacement(stateIn, facing, facingState, worldIn, currentPos, facingPos);
		if (state.get(BlockStateProperties.WATERLOGGED)) {
			worldIn.getPendingFluidTicks().scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickRate(worldIn));
		}
		return state;
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		
		if(player.isSneaking()) {
			if(!worldIn.isRemote && handIn == player.getActiveHand()) {
				MonitorTile monitor = (MonitorTile)worldIn.getTileEntity(pos);
				monitor.setMode(monitor.getMode() == MonitorMode.INFO ? MonitorMode.SCANNER : MonitorMode.INFO);
				monitor.updateBoti();
			}
			return true;
		}
		
		if(!worldIn.isRemote && player.getHeldItem(handIn).getItem() == Items.FILLED_MAP) {
			TardisHelper.getConsoleInWorld(worldIn).ifPresent(tile -> {
				MapData data = FilledMapItem.getMapData(player.getHeldItem(handIn), worldIn);
				tile.setDestination(data.dimension, new BlockPos(data.xCenter, tile.getDestination().getY(), data.zCenter));
				worldIn.playSound(null, pos, TSounds.EYE_MONITOR_INTERACT, SoundCategory.BLOCKS, 1F, 1F);
			});
			return true;
		}

		
		if (Helper.isDimensionBlocked(worldIn.getDimension().getType())) {
			if(worldIn.isRemote) {
				Tardis.proxy.openGUI(this.guiID, new GuiContext());

			}

		}
		else if (!worldIn.isRemote()) {
			player.sendStatusMessage(Constants.Translations.NO_USE_OUTSIDE_TARDIS, true);
		}
		
		return true;
	}

	@Override
	public BlockState rotate(BlockState state, IWorld world, BlockPos pos, Rotation direction) {
		return state.with(BlockStateProperties.HORIZONTAL_FACING, direction.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}
	
	@Override
	public BlockState mirror(BlockState state, Mirror mirrorIn) {
		return state.rotate(mirrorIn.toRotation(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot) {
		return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}


}
