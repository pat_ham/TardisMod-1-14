package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler;

public class MoodChangeItem extends Item{

	private int moodChange = 0;

	public MoodChangeItem(Properties properties, int moodChange) {
		super(properties);
		this.moodChange = moodChange;
	}
	
	@Override
	public ActionResultType onItemUse(ItemUseContext context) {
		TileEntity te = context.getWorld().getTileEntity(context.getPos());
		if(te instanceof ConsoleTile) {
			EmotionHandler emote = ((ConsoleTile)te).getEmotionHandler();
			emote.setMood(emote.getMood() + moodChange);
			if(!context.getPlayer().isCreative())
				context.getItem().shrink(1);
			return ActionResultType.SUCCESS;
		}
		return super.onItemUse(context);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		tooltip.add(new TranslationTextComponent("tooltip." + Tardis.MODID + ".mood_change"));
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}

}
