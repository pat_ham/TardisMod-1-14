package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.UseAction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.projectiles.LaserEntity;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.misc.TDamage;
import net.tardis.mod.sounds.TSounds;

public class LaserGunItem extends BaseItem{
		
		public static final String AMMO_KEY = "ammo";
		private int cooldownTicks, damage;
		
		public LaserGunItem(int cooldownTicks, int damage) {
			super(new Properties().maxStackSize(1).group(TItemGroups.MAIN));
			this.cooldownTicks = cooldownTicks;
	        this.damage = damage;
		}
		
		public static void setAmmo(ItemStack stack, int ammo) {
			if (!stack.hasTag()) stack.setTag(new CompoundNBT());
			stack.getTag().putInt(AMMO_KEY, ammo);
		}
		
		public static int getAmmo(ItemStack stack) {
			if (stack.hasTag())
				return stack.getTag().getInt(AMMO_KEY);
			return 0;
		}
		
		public static ItemStack getAmmoInInventory(NonNullList<ItemStack> stacks, Item item) {
			for (ItemStack stack : stacks) {
				if (stack.getItem() == item) return stack;
			}
			return ItemStack.EMPTY;
		}
		

		
		@Override
		public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
			ItemStack gun = playerIn.getHeldItem(handIn);
			if (playerIn.isSneaking()) {
				ItemStack ammo = getAmmoInInventory(playerIn.inventory.mainInventory, Items.ARROW); //Temporary
				if (!ammo.isEmpty()) {
					setAmmo(gun, getAmmo(gun) + (ammo.getCount() * 5));
					ammo.shrink(ammo.getCount());
				}
				return new ActionResult<>(ActionResultType.SUCCESS, gun);
			} 
			else {
				playerIn.setActiveHand(handIn);
				return new ActionResult<>(ActionResultType.SUCCESS, gun);
			}
		}
		
		@Override
		public int getUseDuration(ItemStack stack) {
		      return 72000;
		}
		
		@Override
		public UseAction getUseAction(ItemStack stack) {
		      return UseAction.BOW;
		}

		@Override
		public void onPlayerStoppedUsing(ItemStack stack, World worldIn, LivingEntity entityLiving, int timeLeft) {
			if (entityLiving instanceof PlayerEntity) {
				PlayerEntity playerIn = (PlayerEntity) entityLiving;
				if (getAmmo(stack) > 0 || playerIn.abilities.isCreativeMode) {
					if (!worldIn.isRemote) {
						LaserEntity laser = new LaserEntity(TEntities.LASER,playerIn.posX, playerIn.posY + 1.25F, playerIn.posZ,worldIn, playerIn, this.damage, TDamage.LASER, new Vec3d(0,1,1));
						laser.shoot(playerIn, playerIn.rotationPitch, playerIn.rotationYaw, 0.0F, 2.5F, 0F);
						worldIn.addEntity(laser);
						if (!playerIn.abilities.isCreativeMode) {
							playerIn.getCooldownTracker().setCooldown(this, this.cooldownTicks);
							LaserGunItem.setAmmo(stack, LaserGunItem.getAmmo(stack) - 1);
						}
					}
					//Temp sound - feel free to find a cybergun sound for cybergun
					worldIn.playSound(null, playerIn.getPosition(), TSounds.DALEK_FIRE, SoundCategory.HOSTILE, 1F, 1F);
				}
			}
		}
		
		@Override
		public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
			return false;
		}
		
		@OnlyIn(Dist.CLIENT)
		@Override
		public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
			ITextComponent AMMO = new TranslationTextComponent("tooltip.gun.ammo", getAmmo(stack));
			ITextComponent AMMO_NONE = new TranslationTextComponent("tooltip.gun.ammo.none");
			if (getAmmo(stack) > 0) {
				tooltip.add(AMMO);
			}
			else {
				tooltip.add(AMMO_NONE);
			}
            if (Screen.hasShiftDown()) {
				tooltip.add(new TranslationTextComponent("tooltip.gun.howto", Minecraft.getInstance().gameSettings.keyBindSneak.getLocalizedName(), Items.ARROW.getName().getUnformattedComponentText()));
			} else {
                tooltip.add(new TranslationTextComponent("item.info.shift"));
			}
			super.addInformation(stack, worldIn, tooltip, flagIn);
		}
	
}
