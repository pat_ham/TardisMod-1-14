package net.tardis.mod.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceContext.BlockMode;
import net.minecraft.util.math.RayTraceContext.FluidMode;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.monitors.MonitorTile;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;

public class MonitorRemoteItem extends Item {

	public MonitorRemoteItem() {
		super(Prop.Items.ONE.get().group(TItemGroups.MAIN));
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		Vec3d start = playerIn.getPositionVec().add(0, playerIn.getEyeHeight(), 0);
		BlockRayTraceResult result = worldIn.rayTraceBlocks(new RayTraceContext(start, start.add(playerIn.getLookVec().scale(24)), BlockMode.COLLIDER, FluidMode.NONE, playerIn));
		if(result != null) {
			TileEntity te = worldIn.getTileEntity(result.getPos());
			if(te instanceof MonitorTile) {
				MonitorTile mon = (MonitorTile)te;
				mon.setMode(mon.getMode() == MonitorMode.INFO ? MonitorMode.SCANNER : MonitorMode.INFO);
				if(mon.getMode() == MonitorMode.SCANNER)
					mon.updateBoti();
			}
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

}
