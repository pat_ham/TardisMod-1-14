package net.tardis.mod.items;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.Helper;

public class StatRemoteItem extends Item{
	
	public StatRemoteItem(Properties properties) {
		super(properties);
	}
	
	@Override
    public ActionResultType onItemUse(ItemUseContext context) {
		if (!context.getWorld().isRemote && !Helper.isDimensionBlocked(context.getWorld().getDimension().getType())) {
			context.getItem().getCapability(Capabilities.REMOTE_CAP).ifPresent(cap -> {
				cap.onClick(context.getWorld(), context.getPlayer(), context.getPos());
			});
			return ActionResultType.SUCCESS;
		}
		else if (Helper.isDimensionBlocked(context.getWorld().getDimension().getType())){
			context.getPlayer().sendStatusMessage(Constants.Translations.CANT_USE_IN_TARDIS, true);
			return ActionResultType.FAIL;
		}
		return ActionResultType.SUCCESS;
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		stack.getCapability(Capabilities.REMOTE_CAP).ifPresent(cap -> {
			String flightStatus = cap.isInFlight() ? "Destination" : "Current";
			tooltip.add(new TranslationTextComponent("tooltip.stat_remote.tardis_owner", cap.getOwner() != null ? worldIn.getPlayerByUuid(cap.getOwner()).getName().getFormattedText() : "Null"));
			tooltip.add(new TranslationTextComponent("tooltip.stat_remote.exterior_dim", flightStatus ,Helper.formatDimName(cap.getExteriorDim())));
			tooltip.add(new TranslationTextComponent("tooltip.stat_remote.exterior_pos", flightStatus, Helper.formatBlockPos(cap.getExteriorPos())));
			tooltip.add(new TranslationTextComponent("tooltip.stat_remote.in_flight", cap.isInFlight()));
			tooltip.add(new TranslationTextComponent("tooltip.stat_remote.journey", cap.getJourney()));
			tooltip.add(new TranslationTextComponent("tooltip.stat_remote.fuel", cap.getFuel()));
		});
		tooltip.add(new TranslationTextComponent("item.info.shift"));
		if (Screen.hasShiftDown()) {
			tooltip.clear();
			tooltip.add(0, this.getDisplayName(stack));
			tooltip.add(new TranslationTextComponent("tooltip.stat_remote.use"));
		}
	}
	
	@Override
	public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
		return false;
	}

	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		if(!worldIn.isRemote) {
			if (entityIn instanceof PlayerEntity) {
				stack.getCapability(Capabilities.REMOTE_CAP).ifPresent(cap -> {
					if (cap.getOwner() == null) {
						cap.setOwner(entityIn.getUniqueID());
					}
					cap.tick(worldIn, entityIn);
				});
			}
			if (worldIn.getGameTime() % 20 == 0) {
				syncCapability(stack); //sync capabilities each second instead of every tick
			}
		}
		super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
	}
	
	@Nullable
    @Override
    public CompoundNBT getShareTag(ItemStack stack) { //sync capability to client
        CompoundNBT tag = stack.getOrCreateTag();
        stack.getCapability(Capabilities.REMOTE_CAP).ifPresent(cap -> 
        	{
        		tag.put("cap_sync", cap.serializeNBT());
        	});
        return tag;
    }

    @Override
    public void readShareTag(ItemStack stack, @Nullable CompoundNBT nbt) {
        super.readShareTag(stack, nbt);
        if (nbt != null) {
            if (nbt.contains("cap_sync")) {
                stack.getCapability(Capabilities.REMOTE_CAP).ifPresent(handler -> handler.deserializeNBT(nbt.getCompound("cap_sync")));
            }
        }
    }
    
	public static void syncCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.getOrCreateTag().merge(stack.getShareTag());
        }
    }

    public static void readCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.readShareTag(stack.getOrCreateTag());
        }
    }
}
