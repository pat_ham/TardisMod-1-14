package net.tardis.mod.items;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.artron.IArtronBattery;
import net.tardis.mod.client.renderers.SonicRenderer;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.sonic.ISonicPart;
import net.tardis.mod.sonic.SonicManager;
import net.tardis.mod.sonic.capability.ISonic;
import net.tardis.mod.sonic.capability.SonicCapability;
import net.tardis.mod.sounds.TSounds;

/**
 * Created by Swirtzly
 * on 22/08/2019 @ 20:15
 */
public class SonicItem extends Item implements IArtronBattery {

    private static final Integer MAX_CHARGE = 100;

    public SonicItem() {
        super(new Item.Properties().maxStackSize(1).group(TItemGroups.MAIN).setTEISR(() -> SonicRenderer::new));
    }

    public static void syncCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.getOrCreateTag().merge(stack.getShareTag());
        }
    }

    public static void readCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.readShareTag(stack.getOrCreateTag());
        }
    }

    @Override
    public boolean onEntitySwing(ItemStack stack, LivingEntity entity) {
        if (entity.world.isRemote) {
            return super.onEntitySwing(stack, entity);
        }
        if (entity instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) entity;
            if (!player.isSneaking()) return false;
            if (player.getCooldownTracker().hasCooldown(this)) {
                return false;
            }
            ISonic data = SonicCapability.getForStack(stack).orElse(null);
            data.setMode(data.getMode() + 1);
            PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent(getCurrentMode(stack).getLangKey()), true);
            syncCapability(stack);
        }
        return super.onEntitySwing(stack, entity);
    }
    

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
    	ItemStack itemstack = playerIn.getHeldItem(handIn);
       if(handIn == playerIn.getActiveHand() && !worldIn.isRemote) {
           if (!playerIn.getCooldownTracker().hasCooldown(itemstack.getItem())) {
               playerIn.setActiveHand(handIn);
           }
       }
        return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
    }

    @Override
    public void onPlayerStoppedUsing(ItemStack stack, World world, LivingEntity livingEntity, int timeLeft) {
        if (world.isRemote) return;
        SonicManager.ISonicMode mode = getCurrentMode(stack);
        if (livingEntity instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) livingEntity;

            RayTraceResult raytraceresult = PlayerHelper.getPosLookingAt(player, getCurrentMode(stack).getReachDistance());
            
            if(raytraceresult != null) {
                if (raytraceresult.getType() == RayTraceResult.Type.BLOCK) {
                    BlockRayTraceResult blockraytraceresult = (BlockRayTraceResult) raytraceresult;
                    BlockPos pos = blockraytraceresult.getPos();
                    boolean flag = mode.processBlock(player, world.getBlockState(pos), player.getActiveItemStack(), pos);
                    runPassFail(player, world, pos, flag);
                    SonicCapability.getForStack(stack).ifPresent(cap -> cap.sync(player, Hand.MAIN_HAND));
                } else if (raytraceresult.getType() == RayTraceResult.Type.ENTITY) {
                    EntityRayTraceResult entityRayTraceResult = (EntityRayTraceResult) raytraceresult;
                    boolean worked = mode.processEntity(player, entityRayTraceResult.getEntity(), stack);
                    runPassFail(player, world, entityRayTraceResult.getEntity().getPosition(), worked);
                    SonicCapability.getForStack(stack).ifPresent(cap -> cap.sync(player, Hand.MAIN_HAND));
                }
                
            }
        }
    }

    public void runPassFail(PlayerEntity playerEntity, World world, BlockPos pos, boolean worked) {
        world.playSound(null, pos.getX(), pos.getY(), pos.getZ(), worked ? TSounds.SONIC_GENERIC : TSounds.SONIC_FAIL, SoundCategory.PLAYERS, 0.25F, 1.0F);
    }

    @Override
    public void onUsingTick(ItemStack stack, LivingEntity player, int count) {
        getCurrentMode(stack).updateHeld((PlayerEntity) player, stack);
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        syncCapability(stack);
        super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
    }


    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
        if (worldIn == null) return;
        SonicCapability.getForStack(stack).ifPresent((data) -> {
            SonicManager.ISonicMode mode = getCurrentMode(stack);
            tooltip.add(new TranslationTextComponent("sonic.mode.type", new TranslationTextComponent(mode.getLangKey())));
            tooltip.add(new TranslationTextComponent("sonic.mode.type.desc", new TranslationTextComponent(mode.getDescriptionLangKey())));
            tooltip.add(new TranslationTextComponent("sonic.mode.charge", data.getCharge()));
            if (Screen.hasShiftDown() && !Screen.hasControlDown()) {
                if (mode.hasAdditionalInfo()) {
                    tooltip.addAll(mode.getAdditionalInfo());
                }
            } else {
                if (mode.hasAdditionalInfo()) {
                    tooltip.add((new TranslationTextComponent("item.info.shift")));
                }
            }

            if (Screen.hasShiftDown() && Screen.hasControlDown()) {
                for (ISonicPart.SonicPart value : ISonicPart.SonicPart.values()) {
                    SonicBasePart.SonicComponentTypes part = SonicCapability.getForStack(stack).orElse(null).getSonicPart(value);
                    tooltip.add(part.getName());
                }
            }
        });
    }

    @Override
    public void onCreated(ItemStack stack, World worldIn, PlayerEntity playerIn) {
        super.onCreated(stack, worldIn, playerIn);
        SonicCapability.getForStack(stack).ifPresent(ISonic::randomiseParts);
    }

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {

    	if(this.getGroup() != group && group != ItemGroup.SEARCH) {
    		return;
    	}
        for (SonicBasePart.SonicComponentTypes componentTypes : SonicBasePart.SonicComponentTypes.values()) {
            ItemStack stack = new ItemStack(TItems.SONIC);
            for (ISonicPart.SonicPart value : ISonicPart.SonicPart.values()) {
                SonicCapability.getForStack(stack).ifPresent((data) -> data.setSonicPart(componentTypes, value));
            }
            if (!items.contains(stack)) {
                items.add(stack);
            }
        }
    }


    @Override
    public boolean itemInteractionForEntity(ItemStack sonic, PlayerEntity player, LivingEntity target, Hand hand) {
        SonicManager.ISonicMode mode = getCurrentMode(sonic);
        if (player.getCooldownTracker().hasCooldown(sonic.getItem())) return false;

        boolean flag = mode.processEntity(player, target, sonic);
        runPassFail(player, player.world, target.getPosition(), flag);
        return flag;
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return Integer.MAX_VALUE;
    }

    public static SonicManager.ISonicMode getCurrentMode(ItemStack stack) {
        ISonic data = SonicCapability.getForStack(stack).orElseGet(null);
        return SonicManager.SONIC_TYPES_ARRAY[data.getMode()].getSonicType();
    }

    @Override
    public float charge(ItemStack sonic, float amount) {
        ISonic data = SonicCapability.getForStack(sonic).orElse(null);
        if (data.getCharge() >= MAX_CHARGE) {
            data.setCharge(MAX_CHARGE);
            syncCapability(sonic);
            return data.getCharge();
        }
        data.setCharge(data.getCharge() + amount);
        syncCapability(sonic);
        return data.getCharge();
    }

    @Override
    public float discharge(ItemStack sonic, float amount) {
        ISonic data = SonicCapability.getForStack(sonic).orElse(null);
        if (data.getCharge() <= 0) {
            data.setCharge(0);
            return data.getCharge();
        }
        data.setCharge(data.getCharge() - amount);
        return data.getCharge();
    }

    @Override
    public boolean canPlayerBreakBlockWhileHolding(BlockState state, World worldIn, BlockPos pos, PlayerEntity player) {
        return false;
    }


    @Override
    public boolean shouldSyncTag() {
        return true;
    }

    @Nullable
    @Override
    public CompoundNBT getShareTag(ItemStack stack) {
        CompoundNBT tag = stack.getOrCreateTag();
        SonicCapability.getForStack(stack).ifPresent(handler -> tag.put("cap_sync", handler.serializeNBT()));
        return tag;
    }

    @Override
    public void readShareTag(ItemStack stack, @Nullable CompoundNBT nbt) {
        super.readShareTag(stack, nbt);
        if (nbt != null) {
            if (nbt.contains("cap_sync")) {
                SonicCapability.getForStack(stack).ifPresent(handler -> handler.deserializeNBT(nbt.getCompound("cap_sync")));
            }
        }
    }

	@Override
	public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
		return oldStack.getItem() != newStack.getItem();
	}

	@Override
	public float getMaxCharge(ItemStack stack) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getCharge(ItemStack stack) {
		// TODO Auto-generated method stub
		return 0;
	}
}
