package net.tardis.mod.items;

import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.sonic.ISonicPart;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TItems {

    /**
	 * Object holders are trash
	 */
	public static Item CIRCUITS = null;
	public static Item KEY = null;
	public static Item INT_DOOR = null;
	public static Item KEY_01 = null;
	public static Item KEY_PIRATE = null;

	public static Item BLANK_UPGRADE = null;
	public static Item ATRIUM_UPGRADE = null;
	public static Item ELECTRO_CONVERT_UPGRADE = null;
	public static Item TELE_STRUCTURE_UPGRADE = null;

	public static Item VORTEX_MANIP = null;
	public static Item VM_MODULE = null;
	public static Item VM_STRAP = null;
	
	//Machine items
	public static Item STATTENHEIM_REMOTE = null;
	public static Item MONITOR_REMOTE = null;
	public static Item POCKET_WATCH = null;
	public static Item SONIC = null;
    public static Item SQUARENESS_GUN = null;
	public static Item EARTHSHOCK_GUN = null;
	public static Item ARS_TABLET = null;
	
	//Component parts
	public static Item DEMAT_CIRCUIT = null;
	public static Item THERMOCOUPLING = null;
	public static Item FLUID_LINK = null;
	public static Item CHAMELEON_CIRCUIT = null;
	public static Item INTERSTITAL_ANTENNA = null;
	public static Item TEMPORAL_GRACE = null;
	
	public static Item ARTRON_CAPACITOR;
	public static Item LEAKY_ARTRON_CAPACITOR;
	
	public static Item MERCURY_BOTTLE = null;
	public static Item CINNABAR = null;
	
	
	public static DalekSpawnItem SPAWN_EGG_DALEK_TIMEWAR = null;
	public static DalekSpawnItem SPAWN_EGG_DALEK_SEC = null;
	public static DalekSpawnItem SPAWN_EGG_DALEK_CLASSIC = null;
	public static DalekSpawnItem SPAWN_EGG_DALEK_SW = null;
	public static DalekSpawnItem SPAWN_EGG_DALEK_SUPREME = null;
	public static DalekSpawnItem SPAWN_EGG_DALEK_IMPERIAL = null;
	
	public static Item ANTIDEPRESSANT = null;
	public static Item DEPRESSANT = null;

	public static Item SONIC_EMITTER = null;
	public static Item SONIC_HANDLE = null;
	public static Item SONIC_ACTIVATOR = null;
	public static Item SONIC_END = null;
	
	public static Item ARTRON_BATTERY = null;
	public static Item ARTRON_BATTERY_MED = null;
	public static Item ARTRON_BATTERY_HIGH = null;
	public static Item ARTRON_BATTERY_CREATIVE = null;
	
	public static Item MANUAL = null;
	
	public static Item DEBUG = null;
	public static Item TAPE_MESURE = null;

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		event.getRegistry().registerAll(
				KEY = createItem(new KeyItem(), "tardis_key"),
				INT_DOOR = createItem(new SpawnerItem(TEntities.DOOR), "int_door"),
				KEY_01 = createItem(new KeyItem(),"key_01"),
				KEY_PIRATE = createItem(new KeyItem(), "key_pirate"),
				VORTEX_MANIP = createItem(new VortexManipItem(),"vm"),
				VM_MODULE = createItem(new BaseItem(),"vm_module"),
				VM_STRAP = createItem(new BaseItem(),"vm_strap"),
				POCKET_WATCH = createItem(new PocketWatchItem(), "pocket_watch"),
				STATTENHEIM_REMOTE = createItem(new StatRemoteItem(Prop.Items.ONE.get().group(TItemGroups.MAIN)),"stattenheim_remote"),
				//MONITOR_REMOTE = createItem(new MonitorRemoteItem(), "monitor_remote"),
				
				ATRIUM_UPGRADE = createItem(new BaseItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE)), "atrium_upgrade"),
				ELECTRO_CONVERT_UPGRADE = createItem(new BaseItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE)), "electro_convert_upgrade"),
				BLANK_UPGRADE = createItem(new BaseItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE)), "blank_upgrade"),
				TELE_STRUCTURE_UPGRADE = createItem(new BaseItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE)), "tele_structure_upgrade"),
				
				CIRCUITS = createItem(new BaseItem(Prop.Items.SIXTY_FOUR.get().group(TItemGroups.MAINTENANCE)), "circuits"),
				
				DEMAT_CIRCUIT = createItem(new TardisPartItem(Prop.Items.ONE.get().maxDamage(1000).group(TItemGroups.MAINTENANCE)), "dematerialisation_circuit"),
				THERMOCOUPLING = createItem(new TardisPartItem(), "thermocoupling"),
				FLUID_LINK = createItem(new TardisPartItem(), "fluid_link"),
				CHAMELEON_CIRCUIT = createItem(new TardisPartItem(), "chameleon_circuit"),
				INTERSTITAL_ANTENNA = createItem(new TardisPartItem(), "interstital_antenna"),
				TEMPORAL_GRACE = createItem(new TardisPartItem(), "temporal_grace"),
				
				ARTRON_CAPACITOR = createItem(new ArtronCapacitorItem(128, 1), "artron_capacitor"),
				LEAKY_ARTRON_CAPACITOR = createItem(new ArtronCapacitorItem(32, 0.2F), "leaky_capacitor"),
				
				MERCURY_BOTTLE = createItem(new BaseItem(Prop.Items.SIXTY_FOUR.get().group(TItemGroups.MAINTENANCE)), "mercury_bottle"),
				CINNABAR = createItem(new BaseItem(Prop.Items.SIXTY_FOUR.get().group(TItemGroups.MAINTENANCE)),"cinnabar"),
				ARTRON_BATTERY = createItem(new ArtronBatteryItem(0.5F, 0.75F, 250F, false),"artron_battery"),
				ARTRON_BATTERY_MED = createItem(new ArtronBatteryItem(1.25F, 0.5F, 750F, false),"artron_battery_medium"),
				ARTRON_BATTERY_HIGH = createItem(new ArtronBatteryItem(2F, 0.25F, 1500F, false),"artron_battery_high"),
				ARTRON_BATTERY_CREATIVE = createItem(new ArtronBatteryItem(Float.MAX_VALUE, 0F, Float.MAX_VALUE, true),"artron_battery_creative"),

				SPAWN_EGG_DALEK_TIMEWAR = createItem(new DalekSpawnItem(), "dalek_timewar"),
				SPAWN_EGG_DALEK_SEC = createItem(new DalekSpawnItem(), "dalek_sec"),
				SPAWN_EGG_DALEK_SW = createItem(new DalekSpawnItem(), "dalek_special_weapons"),
				SPAWN_EGG_DALEK_CLASSIC = createItem(new DalekSpawnItem(), "dalek_classic"),
				SPAWN_EGG_DALEK_SUPREME = createItem(new DalekSpawnItem(), "dalek_supreme"),
				SPAWN_EGG_DALEK_IMPERIAL = createItem(new DalekSpawnItem(), "dalek_imperial"),
				
				EARTHSHOCK_GUN = createItem(new LaserGunItem(10,10), "earthshock_gun"),
				ARS_TABLET = createItem(new ARSTabletItem(), "ars_tablet"),
				
				ANTIDEPRESSANT = createItem(new MoodChangeItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE), 100), "antidepressant"),
				DEPRESSANT = createItem(new MoodChangeItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE), -100), "depressant"),
				SONIC_EMITTER = createItem(new SonicBasePart(ISonicPart.SonicPart.EMITTER), "sonic_emitter"),
				SONIC_ACTIVATOR = createItem(new SonicBasePart(ISonicPart.SonicPart.ACTIVATOR), "sonic_activator"),
				SONIC_HANDLE = createItem(new SonicBasePart(ISonicPart.SonicPart.HANDLE), "sonic_handle"),
				SONIC_END = createItem(new SonicBasePart(ISonicPart.SonicPart.END), "sonic_end"),
				SONIC = createItem(new SonicItem(), "sonic"),

                MANUAL = createItem(new ManualItem(), "manual"),
                SQUARENESS_GUN = createItem(new SquarenessGunItem(), "squareness_gun"),
                
                //Debug tools
                DEBUG = createItem(new DebugItem(), "debug"),
                TAPE_MESURE = createItem(new TapeMeasureItem(), "tape_measure")
		);
		
		for(Item item : TBlocks.ITEMS) {
			event.getRegistry().register(item);
		}

		//DalekDispenseBehaviour.init();
	}

	public static <T extends Item> T createItem(T item, String name){
		item.setRegistryName(Tardis.MODID, name);
		return item;
	}
}
