package net.tardis.mod.items;

import java.util.List;
import java.util.UUID;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.itemgroups.TItemGroups;

public class KeyItem extends BaseItem{
	
	public KeyItem() {
		super(new Properties().maxStackSize(1).group(TItemGroups.MAIN));
	}
	
	public static void setOwner(ItemStack stack, UUID id) {
		if(!stack.hasTag())
			stack.setTag(new CompoundNBT());
		stack.getTag().putString("tardis_key", id.toString());
	}
	
	public static UUID getOwner(ItemStack stack) {
		if(stack.hasTag() && stack.getTag().contains("tardis_key")) {
			return UUID.fromString(stack.getTag().getString("tardis_key"));
		}
		return null;
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		if(getOwner(stack) != null)
			tooltip.add(new TranslationTextComponent("tooltip.key.owner", getOwner(stack) != null ? worldIn.getPlayerByUuid(getOwner(stack)).getName().getFormattedText() : ""));
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}

	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entity, int itemSlot, boolean isSelected) {
		if(entity instanceof PlayerEntity && getOwner(stack) == null) {
			setOwner(stack, ((PlayerEntity)entity).getUniqueID());
		}
		super.inventoryTick(stack, worldIn, entity, itemSlot, isSelected);
	}
	
	
}
