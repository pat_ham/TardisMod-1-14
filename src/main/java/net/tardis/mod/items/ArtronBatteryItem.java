package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.artron.IArtronBattery;

/**
 * Created by 50ap5ud5
 * on 24 Mar 2020 @ 9:29:34 am
 */

//Used for providing charge to items
//Also used to power machines
public class ArtronBatteryItem extends TardisPartItem implements IArtronBattery{
	
	private static final String CHARGE = "artron";
	private final float chargeRateMultiplier;
	private final float dischargeRateMultiplier;
	private final float maxChargeCapacity;
	private final boolean isCreative;
	
	public ArtronBatteryItem(float chargeRateMultiplier, float dischargeRateMultiplier, float maxChargeCapacity, boolean isCreative) {
		this.chargeRateMultiplier = chargeRateMultiplier;
		this.dischargeRateMultiplier = dischargeRateMultiplier;
		this.maxChargeCapacity = maxChargeCapacity;
		this.isCreative = isCreative;
	}
	
	//Returns how much was charged
	//Also writes the changes in data to nbt
	@Override
	public float charge(ItemStack stack, float amount) {
		amount = this.getChargeRate(amount);
		float charge = stack.getOrCreateTag().getFloat(CHARGE);
		float maxCharge = this.getMaxCharge(stack);
		//If adding more will go over the max charge, return however much is needed to reach max.
		if (charge + amount >= maxCharge) {
			float chargeToAdd = maxCharge - charge;
			this.writeCharge(stack, charge + chargeToAdd); //We need to write to nbt everytime we want to save data
			return chargeToAdd;
		}
		else {
			this.writeCharge(stack, charge + amount);
			return amount;
		}
	}
	

	@Override
	public float discharge(ItemStack stack, float amount) {
		float current = stack.getOrCreateTag().getFloat(CHARGE);
		float dischargeRate = this.getDischargeRate(amount);
		float chargeToTake = current - dischargeRate;
		if(amount <= current && chargeToTake > 0) {
			writeCharge(stack, chargeToTake);
			return amount;
		}
		writeCharge(stack, 0);
		return current;
	}
	

	@Override
	public float getMaxCharge(ItemStack stack) {
		return this.maxChargeCapacity;
	}

	@Override
	public float getCharge(ItemStack stack) {
		return readCharge(stack);
	}
	
	public void writeCharge(ItemStack stack, float charge) {
		stack.getOrCreateTag().putFloat(CHARGE, charge);
	}
	
	public float readCharge(ItemStack stack) {
		return stack.getOrCreateTag().getFloat(CHARGE);
	}
	
	public float getDischargeRate(float amount) {
		return amount * this.dischargeRateMultiplier;
	}
	
	public float getChargeRate(float amount) {
		return amount * this.chargeRateMultiplier;
	}
	
	

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		if (this.isCreative) {
			ItemStack stack = playerIn.getHeldItem(handIn);
			if (this.getCharge(stack) == 0) {
				this.charge(stack, Float.MAX_VALUE);
				return new ActionResult<ItemStack>(ActionResultType.SUCCESS, stack);
			}
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		if (this.isCreative && this.getCharge(stack) == 0)
			tooltip.add(new TranslationTextComponent("tooltip.artron_battery.creative_setup"));
		tooltip.add(new TranslationTextComponent("tooltip.artron_battery.charge", this.getCharge(stack)));
		tooltip.add(new TranslationTextComponent("tooltip.artron_battery.max_charge", this.getMaxCharge(stack)));
		tooltip.add(new TranslationTextComponent("item.info.shift"));
		if (Screen.hasShiftDown()) {
			tooltip.clear();
			tooltip.add(0, this.getDisplayName(stack));
			tooltip.add(new TranslationTextComponent("tooltip.artron_battery.howto"));
			tooltip.add(new TranslationTextComponent("item.info.shift_control"));
			if (Screen.hasControlDown()) {
				tooltip.clear();
				tooltip.add(0, this.getDisplayName(stack));
				tooltip.add(new TranslationTextComponent("tooltip.artron_battery.discharge_multiplier", this.dischargeRateMultiplier));
				tooltip.add(new TranslationTextComponent("tooltip.artron_battery.charge_multiplier", this.chargeRateMultiplier));
			}
		}
	}
	
	
	
}
