package net.tardis.mod.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.energy.TardisEnergy;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public interface ITardisWorldData extends INBTSerializable<CompoundNBT>{
	
	PanelInventory getEngineInventoryForSide(Direction dir);
	TardisEnergy getEnergy();
	void tick();
	
	public static class TardisWorldProvider implements ICapabilitySerializable<CompoundNBT>{

		private ITardisWorldData data;
		
		public TardisWorldProvider(World world) {
			this.data = new TardisWorldCapability(world);
		}
		
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == Capabilities.TARDIS_DATA ? LazyOptional.of(() -> (T)data) : LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return data.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			data.deserializeNBT(nbt);	
		}
		
	}
	
	public static class TardisWorldStorage implements IStorage<ITardisWorldData>{

		@Override
		public INBT writeNBT(Capability<ITardisWorldData> capability, ITardisWorldData instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<ITardisWorldData> capability, ITardisWorldData instance, Direction side, INBT nbt) {
			instance.deserializeNBT((CompoundNBT)nbt);
		}
		
	}

}
