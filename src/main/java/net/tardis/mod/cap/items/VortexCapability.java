package net.tardis.mod.cap.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.ModDimension;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.artron.IArtronBattery;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.sounds.TSounds;

public class VortexCapability implements IVortexCap {
	
	private ItemStack stack;
	private ItemStackHandler handler;
	private int index = 0;
	private float dischargeAmount;
	private boolean hasTeleported = false;
	private boolean isOpen = false;
	private boolean used = false;
	
	//Client
	private float totalCharge = 0;
	
	public VortexCapability() {}
	
	public VortexCapability(ItemStack stack) {
		this.stack = stack;
		this.handler = new ItemStackHandler();
	}
	
	public VortexCapability(ItemStack stack, int size) {
		this.stack = stack;
		this.handler = new ItemStackHandler(size);
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.put("battery_inv", handler.serializeNBT());
        tag.putBoolean("has_teleported", this.hasTeleported);
        tag.putFloat("total_charge", this.getTotalCurrentCharge());
        tag.putFloat("discharge_amount", dischargeAmount);
        tag.putBoolean("used", this.used);
		return tag;
		
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.handler.deserializeNBT(nbt.getCompound("battery_inv"));
		this.hasTeleported = nbt.getBoolean("has_teleported");
		this.totalCharge = nbt.getFloat("total_charge");
		this.dischargeAmount = nbt.getFloat("discharge_amount");
		this.used = nbt.getBoolean("used");
	}

	@Override
	public float getShieldHealth() {
		return 0;
	}

	@Override
	public void setShieldHealth(float health) {
		
	}
	
	//TODO: Hook into Shield value
	//TODO: Damage the player from vortex lightning if they run out of charge whilst in the Vortex
	@Override
	public void tick(PlayerEntity player) {
		 if (player instanceof ServerPlayerEntity) {
			 ServerPlayerEntity serverPlayer = (ServerPlayerEntity)player;
			 ServerWorld world = serverPlayer.getServerWorld();
			 ModDimension dim = world.getDimension().getType().getModType();
			 if (used) {
				 if (dim == TDimensions.VORTEX) {
					 int numSeconds = TConfig.CONFIG.vmFuelUsageTime.get(); //Artron discharge every x amount of seconds defined by config
					 ItemStack slotStack = handler.getStackInSlot(this.index); //cache this value
					 if(slotStack.getItem() instanceof IArtronBattery) { //If this is an Artron battery
						 float stackCharge = ((IArtronBattery)slotStack.getItem()).getCharge(slotStack);
						 if (stackCharge == 0){
							 safetyCheck();
							 slotStack = handler.getStackInSlot(this.index);
						 }
						 else {
							 if(world.getGameTime() % (20 * numSeconds) == 0) {
								 if (stackCharge >= this.dischargeAmount) { //If we have enough charge, use it up
									 ((IArtronBattery)slotStack.getItem())
									 .discharge(slotStack, this.dischargeAmount);
								 }
								 if (stackCharge < this.dischargeAmount && stackCharge > 0) { //If there's not enough charge, use up remaining charge
									 ((IArtronBattery)slotStack.getItem())
									 .discharge(slotStack, this.dischargeAmount);
								 }
								 if (stackCharge == 0){ //Move to the next slot if we deplete this battery
									 safetyCheck();
									 slotStack = handler.getStackInSlot(this.index);
								 }
							 }
					 	}
					 }
					 else if (!(slotStack.getItem() instanceof IArtronBattery)){ //If it's not artron battery (should be empty), check next slot for battery
						 safetyCheck();
						 slotStack = handler.getStackInSlot(this.index);
					 }
					 if (player.posY <= 10) {
						 this.hasTeleported = true;
						 this.serializeNBT(); //Ensures the value is saved, so the sound and cooldown etc. can always be played
					 } 
				 } 
				 //Apply cooldown, potion effects and sounds
				 if (dim != TDimensions.VORTEX && this.hasTeleported) {
					 world.playSound(null, player.getPosition(), TSounds.VM_TELEPORT_DEST, SoundCategory.BLOCKS, 0.25F, 1F);
					 serverPlayer.getCooldownTracker().setCooldown(TItems.VORTEX_MANIP, TConfig.CONFIG.vmCooldownTime.get() * 20);
					 serverPlayer.getCapability(Capabilities.PLAYER_DATA).ifPresent(playerCap -> { //Apply potion effects if we've traveller over half our teleport range limit
	             		if (playerCap.getDisplacement() > TConfig.CONFIG.vmTeleportRange.get() / 2) {
//	             			world.rand.nex
	             			 TConfig.CONFIG.vmSideEffects.get().forEach(effect  -> {
	 				        	serverPlayer.addPotionEffect(new EffectInstance(Helper.getEffectFromRL(new ResourceLocation(effect)), TConfig.CONFIG.vmSideEffectTime.get().get(TConfig.CONFIG.vmSideEffects.get().indexOf(effect)) * 20,1, true, true));
	 				     });
	             		}
	             	});
					this.hasTeleported = false; //Set flag to prevent stepping into this again.
					this.used = false; //At the end of the journey, ensures that  we won't affect the VM's fuel etc. if you enter the vortex via other means
					this.serializeNBT();
				 }
			 }
			
		 }
		 
	}
	
	public int safetyCheck() {
		return this.index = this.index < this.handler.getSlots() - 1 ? this.index+ 1 : 0;
	}

	
	/**
	 * 
	 * @param discharge
	 * @return The amount that couldn't be discharged
	 */
	public float dischargeEnergy(float discharge) {
		float amt = discharge;
		for(int i = 0; i < this.handler.getSlots(); ++i) {
			ItemStack stack = this.handler.getStackInSlot(i);
			if(stack.getItem() instanceof IArtronBattery) {
				IArtronBattery batt = (IArtronBattery)stack.getItem();
				amt = amt - batt.discharge(stack, amt);
			}
		}
		
		return amt;
	}
	
	@Override
	public int getBatteries() {
		int battCount = 0;
		for(int i = 0; i < this.handler.getSlots(); ++i) {
			ItemStack stack = this.handler.getStackInSlot(i);
			if(stack.getItem() instanceof IArtronBattery)
				++battCount;
		}
		return battCount;
	}
	
	
	@Override
	public float getDischargeAmount() {
		return this.dischargeAmount;
	}
	
	
	@Override
	public void setDischargeAmount(float amount) {
		this.dischargeAmount = amount;
	}
	
	/**
	 * Server side calculation of the total charge in the VM
	 */
	@Override
	public float getTotalCurrentCharge() {
		float charge = 0F;
		for(int i = 0; i < this.handler.getSlots(); ++i) {
			ItemStack stack = this.handler.getStackInSlot(i);
			if(stack.getItem() instanceof IArtronBattery)
				charge += ((IArtronBattery)stack.getItem()).getCharge(stack);
		}
		return charge;
	}
	
	/**
	 * Get total charge for client only tooltips
	 */
	@Override
	public float getClientTotalCurrentCharge() {
		return this.totalCharge;
	}

	/**
	 * Get the container's item handler
	 */
	@Override
	public ItemStackHandler getItemHandler() {
		return this.handler;
	}
	
	/**
	 * Set the item model predicate
	 */
	@Override
	public void setOpen(boolean open) {
		this.isOpen = open;
	}
	
	/**
	 * Used to get the item model predicate value
	 */
	@Override
	public boolean getOpen() {
		return this.isOpen;
	}
	
	/**
	 * Sets a flag to indicate if the player has successfully teleported
	 * @implNote Used to prevent spamming of sounds, aftereffects.
	 */
	@Override
	public void setTeleported(boolean teleport) {
		this.hasTeleported = teleport;
	}
	
	/**
	 * 
	 * Tells us if the VM was used to do something and that the result was not caused by something else
	 * @implNote Prevents VM from losing fuel etc. if something else teleports players into the Vortex Dim
	 */
	@Override
	public boolean wasVMUsed() {
		return this.used;
	}
	
	/**
	 * Sets the VM usage
	 */
	@Override
	public void setVmUsed(boolean type) {
		this.used = type;
	}

	
}
