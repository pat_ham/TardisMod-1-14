package net.tardis.mod.cap.items;

import java.util.UUID;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.StabilizerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;

public class RemoteCapability implements IRemote{
	
	private ConsoleTile tile;
	private ItemStack remote;
	
	private UUID owner = null;

	//Client varaibles
	private float timeLeft = 0;
	private SpaceTimeCoord location = SpaceTimeCoord.UNIVERAL_CENTER;
	private boolean isInFlight;
	private float fuel = 0;
	
	public RemoteCapability(ItemStack stack) {
		this.remote = stack;
	}
	
	@Override
	public SpaceTimeCoord getExteriorLocation() {
		return this.location;
	}

	@Override
	public float getJourney() {
		return this.timeLeft;
	}

	@Override
	public void setJourneyTime(float time) {
		this.timeLeft = time;
	}

	@Override
	public UUID getOwner() {
		return this.owner;
	}
	
	@Override
	public void setOwner(UUID id) {
		this.owner = id;	
	}

	@Override
	public void onClick(World world, PlayerEntity player, BlockPos pos) {
		if(!world.isRemote && owner != null) {
			this.findTardis(world);
			if(tile != null && !tile.isRemoved()) {
				tile.setDestination(player.dimension, pos.up());
				StabilizerControl stabilizer = tile.getControl(StabilizerControl.class);
				ThrottleControl throttle = tile.getControl(ThrottleControl.class);
				HandbrakeControl handbrake = tile.getControl(HandbrakeControl.class);
				if(throttle != null)
					throttle.setAmount(1.0F);
				if (stabilizer != null)
					stabilizer.setStabilized(true);
				if (handbrake != null)
					handbrake.setFree(true);
				tile.takeoff();
				player.getEntityWorld().playSound(null, player.getPosition(), TSounds.REMOTE_ACCEPT, SoundCategory.BLOCKS, 1F, 1F);
			}
			
		}
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		if(owner != null) {
			tag.putString("owner", this.owner.toString());
		}
		tag.putFloat("time_left", this.timeLeft);
		tag.put("loc", this.location.serialize());
		tag.putBoolean("is_flying", this.isInFlight);
		tag.putFloat("fuel", this.fuel);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		if(nbt.contains("owner")) {
			this.owner = UUID.fromString(nbt.getString("owner"));
		}
		if(nbt.contains("loc")) {
			this.location = SpaceTimeCoord.deserialize(nbt.getCompound("loc"));
		}
		this.timeLeft = nbt.getFloat("time_left");
		this.isInFlight = nbt.getBoolean("is_flying");
		this.fuel = nbt.getFloat("fuel");
	}
	
	@Override
	public void setExteriorLocation(SpaceTimeCoord coord) {
		this.location = coord;
		
	}


	@Override
	public void tick(World world, Entity ent) {
		if(!world.isRemote) {
			if (world.getGameTime() % 20 == 0) {
				this.findTardis(world);
				if (this.tile != null && owner !=null) {
					this.setExteriorLocation(new SpaceTimeCoord(tile.getDestinationDimension(),tile.getDestination()));
					this.setJourneyTime(tile.getPercentageJourney());
					this.isInFlight = tile.isInFlight();
					this.fuel = tile.getArtron();
					this.serializeNBT();
				}
			}
			if (tile != null && owner!= null) {
				if (ent.getEntityWorld().getDimension().getType().getModType() != TDimensions.TARDIS) { //Play an arrived sound if we aren't in the Tardis
					if (tile.flightTicks == 10) {
						ent.getEntityWorld().playSound(null, ent.getPosition(),TSounds.REACHED_DESTINATION, SoundCategory.BLOCKS, 1F, 1F); //Play a sound to entity if tardis has landed
					}
				}
			}
		}
		
	}

	@Override
	public BlockPos getExteriorPos() {
		return this.location.getPos();
	}

	@Override
	public DimensionType getExteriorDim() {
		return DimensionType.byName(this.location.getDimType());
	}
	
	@Override
	public void findTardis(World world) {
		if(tile == null || tile.isRemoved()) {
			DimensionType type = DimensionType.byName(new ResourceLocation(Tardis.MODID, owner.toString()));
			if(type != null) {
				ServerWorld inter = world.getServer().getWorld(type);
				if(inter != null) {
					TileEntity te = inter.getTileEntity(TardisHelper.TARDIS_POS);
					if(te instanceof ConsoleTile)
						this.tile = (ConsoleTile)te;
				}
			}
			else {
				System.err.println(Constants.Message.NO_TARDIS);
			}
		}
	}

	@Override
	public boolean isInFlight() {
		return this.isInFlight;
	}

	@Override
	public float getFuel() {
		return this.fuel;
	}
	
	

}
